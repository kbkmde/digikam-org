---
date: "2017-03-30T15:05:23-06:00"
title: "Meta - Design"
author: "Pat David"
description: "Meta Design page about www.digikam.org"
---

These are design notes created during the migration and re-design of the website (2017-03).

![/img/mockup-front-B.png](/img/mockup-front-B.png)

This mockup source svg file: [mockup-digikam.svg](/img/mockup-digikam.svg)

### Typography

* [Fira Sans][]
* [Oxygen][]
* [Libre Baskerville][]

[Fira Sans]: https://fonts.google.com/specimen/Fira+Sans
[Oxygen]: https://fonts.google.com/specimen/Oxygen
[Libre Baskerville]: https://fonts.google.com/specimen/Libre+Baskerville

### Colors

Modified [solarized][] (dark) colorscheme (shifted to true grays)

Use | name  | hex
----|-------|-----
Background | base 03 | #262626
Background (highlights)| base 02 | #303030
Comments/Secondary content | base 01 | #6B6B6B
Body/Default/Primary Text | base 0 | #919191
Optional Emphasized Content | base 1 | #9E9E9E

<div style='width: 100%; background-color: #262626; color: #919191; padding:1rem; border: solid 1px gray;'>
Background: #262626
<div style='background-color: #303030; color: #919191;'>
Background (highglights): #303030</div>
<div style='color: #6b6b6b;'>Secondary content: #6b6b6b</div>
<div style='color: #919191;'>Primary content: #919191</div>
<div style='color: #9E9E9E;'>Emphasized content: #9E9E9E</div>
<div style='color: #af8700;'>Yellow: #af8700</div>
<div style='color: #d75f00;'>Orange: #d75f00</div>
<div style='color: #d70000;'>Red:  #d70000</div>
<div style='color: #af005f;'>Magenta:  #af005f</div>
<div style='color: #5f5faf;'>Violet:  #5f5faf</div>
<div style='color: #0087ff;'>Blue:  #0087ff</div>
<div style='color: #00afaf;'>Cyan:  #00afaf</div>
<div style='color: #5f8700;'>Green:  #5f8700</div>
<div style='color: #0068C6;'>KDE Blue:  #0068C6</div>

</div>

[solarized]: http://ethanschoonover.com/solarized
