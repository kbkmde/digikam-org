---
date: "2011-02-23T09:49:00Z"
title: "digiKam Tricks 2.0 Released"
author: "Dmitri Popov"
description: "The digiKam Tricks book version 2.0 is now available. Here is what's new in this release: New book cover Convert Photos to Black and White"
category: "news"
aliases: "/node/578"

---

<p>The <a rel="nofollow" href="http://www.digikam.org/drupal/node/543">digiKam Tricks</a> book version 2.0 is now available. Here is what's new in this release:</p>
<ul>
<li>New book cover</li>
<li>Convert Photos to Black and White</li>
<li>Geotag Photos with Open GPS Tracker and digiKam</li>
<li>Instant Vintage Photo Effects with the FIL Script for GIMP</li>
<li>Work with Photo Metadata in digiKam</li>
<li>New figures added</li>
<li>Numerous tweaks and fixes</li>
<li>Touch up Photos with GIMP Scripts removed</li>
<li>Geotag Photos with Geotag removed</li>
</ul>
<p>Readers who already purchased the book will receive the new version free of charge. If you haven't received your copy, please send me your order confirmation as proof of purchase to&nbsp;<a href="mailto:dmpop@linux.com">dmpop@linux.com</a> and I'll email you the latest version of the book. Happy reading!</p>

<div class="legacy-comments">

</div>