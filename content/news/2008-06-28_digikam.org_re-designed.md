---
date: "2008-06-28T12:09:00Z"
title: "digikam.org re-designed..."
author: "digiKam"
description: "Since few weeks ago, the whole web site have been re-designed. The following task have been done: Gerhard Kulzer has migrated site CMS from Drupal"
category: "news"
aliases: "/node/352"

---

<p>Since few weeks ago, the whole web site have been re-designed. The following task have been done:</p>

<ul>
   <li><b>Gerhard Kulzer</b> has migrated site CMS from <a href="http://drupal.org">Drupal</a> 4.x to 6.x.</li>
   <li>All logo, page layouts, and graphisms have been made by <b>Risto Saukonpää</b>.</li>
   <li>Gerhard has installed <a href="http://en.wikipedia.org/wiki/Captcha">Captcha</a> module to be able to post comments without to use account.</li>
   <li>All screenshots have been updated with <a href="http://en.wikipedia.org/wiki/KDE4">KDE4</a> release (incoming digiKam 0.10.0).</li>
   <li>New Tour Movies section have been added with ogf and flash video.</li>
   <li>All screenshots are now hosted to <a href="http://www.flickr.com/groups/digikam-labs">Flickr photo-set</a>.</li>
   <li>All videos are now hosted to <a href="http://www.youtube.com/group/digikam">YouTube channel</a>.</li>
</ul>

<p>Still to do more video demo and screeshots of new KDE4 version of digiKam in action. A new page to describe the new features of digiKam 0.10.0 must be done too.</p>

<p>I would to thanks all contributors who have help to re-design digikam.org. I hope than new web site will be fine for all users.</p>

<a href="http://www.flickr.com/photos/digikam/2603909990/" title="0.10.0-albumgui05 by digiKam team, on Flickr"><img src="http://farm4.static.flickr.com/3286/2603909990_43bedb2e39.jpg" width="500" height="382" alt="0.10.0-albumgui05"></a>
<div class="legacy-comments">

  <a id="comment-17594"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/352#comment-17594" class="active">Previous design was better</a></h3>    <div class="submitted">Submitted by Miha (not verified) on Sat, 2008-06-28 12:44.</div>
    <div class="content">
     <p>I have just to say that previous blueish design was better. I hate this orange links on black background - my eyes (and possibly eyes of others) are suffering... :) </p>
<p>Ok I really don't want to be so critic - refreshed content is very good and Digikam rules :). </p>
<p>Next time you have to (!!!) ask Nuno (http://pinheiro-kde.blogspot.com/) for design:)</p>
<p>Grettings; Miha</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-17596"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/352#comment-17596" class="active">I don't agree that the old</a></h3>    <div class="submitted">Submitted by gissi (not verified) on Sat, 2008-06-28 13:38.</div>
    <div class="content">
     <p>I don't agree that the old design was better. IMO the new website is a big leap forward and looks really great now.</p>
<p>However, I have some suggestions:</p>
<p>1. Are you sure you need need the right sidebar? The "poll" and "recent blog posts" widgets seem to be quite unnecessary to me and the buttons could be simply added to the left sidebar.</p>
<p>2. It's hard to get a quick overview over the left sidebar, as a) there are too much links at all and b) the top level links are not highlighted. For instance, you could simply merge all the "Kipi Plugins" subpages to one page.</p>
<p>3. The feed is not showing up in the url lineedit, although one exists. This should be easy to fix.</p>
<p>Thanks for your great work. Digikam makes organizing photos really a lot easier for me!</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-17599"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/352#comment-17599" class="active">Jep, it would be realy better</a></h3>    <div class="submitted">Submitted by Miha (not verified) on Sat, 2008-06-28 18:34.</div>
    <div class="content">
     <p>Jep, it would be realy better if site has only one sidebar and expanding menu (show/hide) for instance for Kipi Plugins.</p>
         </div>
    <div class="links">» </div>
  </div>
</div></div><a id="comment-17595"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/352#comment-17595" class="active">The new homepage is very</a></h3>    <div class="submitted">Submitted by Anonymous (not verified) on Sat, 2008-06-28 13:09.</div>
    <div class="content">
     <p>The new homepage is very nice, as well as digikam.<br>
So many open source projects have really boring and primitive homepages, which is a shame.<br>
But this one is one of the few exceptions, the other projects should take it as an example.<br>
Good job.<br>
Keep improving.</p>
         </div>
    <div class="links">» </div>
  </div>
<a id="comment-17597"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/352#comment-17597" class="active">Rename to Digikam?</a></h3>    <div class="submitted">Submitted by Anonymous (not verified) on Sat, 2008-06-28 18:08.</div>
    <div class="content">
     <p>When will digiKam be renamed to Digikam? :)</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-17598"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/352#comment-17598" class="active">There is no reason to rename</a></h3>    <div class="submitted">Submitted by digiKam on Sat, 2008-06-28 18:27.</div>
    <div class="content">
     <p>There is no reason to rename "digiKam" to "Digikam"...</p>
<p>digiKam</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-17602"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/352#comment-17602" class="active">Re: There is no reason to rename</a></h3>    <div class="submitted">Submitted by Anonymous (not verified) on Sat, 2008-06-28 23:22.</div>
    <div class="content">
     <p>Well, here are some reasons:</p>
<ul>
<li>Names start with a capital letter (in English and most (all?) other languages which have capital letters)</li>
<li>digiKam looks like LOLspeak and thus gives a kind of immature impression</li>
<li>Digikam is easier to type, because normally, there's no capital letter within a word</li>
<li>It will get misspelled less often (<a href="http://codeincarnate.com/entry/kyle/2008/06/28/digikamorg-redesigned-using-drupal">latest example</a>, where it is spelled Digikam)</li>
<li>How should it be spelled when it is at the start of a sentence? digiKam or DigiKam? The first goes against English orthography and the second against the "digiKam with a lowercase <em>d</em>" name.</li>
<li>Amarok did it too (formerly amaroK)</li>
</ul>
         </div>
    <div class="links">» </div>
  </div>
</div></div><a id="comment-17600"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/352#comment-17600" class="active">Hello
You really should turn</a></h3>    <div class="submitted">Submitted by Xavier (not verified) on Sat, 2008-06-28 21:44.</div>
    <div class="content">
     <p>Hello</p>
<p>You really should turn on full RSS articles.</p>
         </div>
    <div class="links">» </div>
  </div>
<a id="comment-17604"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/352#comment-17604" class="active">A good look but...</a></h3>    <div class="submitted">Submitted by <a href="http://www.lindaetmarguerite.net" rel="nofollow">Floréal</a> (not verified) on Sun, 2008-06-29 04:47.</div>
    <div class="content">
     <p>Hello,</p>
<p>I like the new design a lot ! But I search why screenshots were now hosted by flickr ?</p>
<p>Sorry for my bad english, but I am french !</p>
<p>Translation :</p>
<p>J'adore ce nouveau design ! Mais je me demande pourquoi les screenshots sont désormais hébergés sur flickr ? Question accès, c'est moins joli je pense !</p>
<p>Good look !</p>
<p>Floréal.</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-17605"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/352#comment-17605" class="active">&gt; I like the new design a lot</a></h3>    <div class="submitted">Submitted by digiKam on Sun, 2008-06-29 07:20.</div>
    <div class="content">
     <p>&gt; I like the new design a lot ! But I search why screenshots were now hosted by flickr ?</p>
<p>Because, during Drupal 6.x migration, all old screenshots have been lost. Like we would to update all using KDE4 version, i have used Flickr to host all screen capture. It's sound like more robust and better to use.</p>
<p>digiKam</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-17622"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/352#comment-17622" class="active">Direct link to image</a></h3>    <div class="submitted">Submitted by Matthias (not verified) on Fri, 2008-07-04 11:49.</div>
    <div class="content">
     <p>Is it possible to link the screenshot directly to the image in stead of the flickr site?<br>
For the above image it would be: <a href="http://farm4.static.flickr.com/3286/2603909990_aa9a79a1f5_o_d.png">http://farm4.static.flickr.com/3286/2603909990_aa9a79a1f5_o_d.png</a><br>
I would like this because I use right click - open with - kuickshow to view screenshots. It's also easier when you have more screenshots to download them all.</p>
         </div>
    <div class="links">» </div>
  </div>
</div></div><a id="comment-17608"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/352#comment-17608" class="active">Non-Free Video Formats (aka YouTube Sucks)</a></h3>    <div class="submitted">Submitted by <a href="http://www.tigen.org/kevin.kofler/" rel="nofollow">Kevin Kofler</a> (not verified) on Mon, 2008-06-30 07:29.</div>
    <div class="content">
     <p>Yet another Free Software project only making their screencasts available through YouTube which only supports patent-encumbered formats. Plus, you can't even download the videos without hacks, they're only presented for live viewing through the proprietary Flash technology. What's wrong with offering Ogg Theora downloads?</p>
<p>See also <a href="http://www.j5live.com/2007/12/11/open-formats-are-the-only-way-to-guarantee-an-open-web/">http://www.j5live.com/2007/12/11/open-formats-are-the-only-way-to-guarantee-an-open-web/</a>.</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-17609"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/352#comment-17609" class="active">It's false, OGG video files</a></h3>    <div class="submitted">Submitted by digiKam on Mon, 2008-06-30 07:52.</div>
    <div class="content">
     <p>It's false,</p>
<p>OGG video files are available to download too at the same time than Youtube stuff, and in full screen size format ! Look on <a href="http://www.digikam.org/drupal/tour">Tour Movies</a> page for details...</p>
<p>digiKam</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-17621"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/352#comment-17621" class="active">Great!</a></h3>    <div class="submitted">Submitted by <a href="http://www.tigen.org/kevin.kofler/" rel="nofollow">Kevin Kofler</a> (not verified) on Thu, 2008-07-03 01:53.</div>
    <div class="content">
     <p>Oh, OK, sorry for the pointless whine then. ;-)</p>
         </div>
    <div class="links">» </div>
  </div>
</div></div><a id="comment-17620"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/352#comment-17620" class="active">Wish List for digiKam and KPhotoAlbum</a></h3>    <div class="submitted">Submitted by <a href="http://alpha1.jalbum.net/Photo_Organization_Reviews/slides/S01.html" rel="nofollow">Alpha1</a> (not verified) on Tue, 2008-07-01 12:45.</div>
    <div class="content">
     <p>I have been trying several different photo organization tools over the past few months. While Picasa2 from Google is definitely the fastest and easiest for simply importing, simple image correction/manipulation and uploading to the Google site, it really doesn't do the job of organization of a huge photo collection. In my opinion, the two best systems are digiKam and KPhotoAlbum. What I would REALLY like to see is a merge of these two, having the best features of each. The Homepage listed is the first slide in a presentation I recently made to the Victoria LINUX Users Group (VLUG). If you are interested, simply go to that page and advance through the slides.</p>
<p>digiKam:<br>
The things I really like about digiKam 0.9.2-final (under KDE 3.5.8) in Kubuntu are the following:<br>
It is very stable<br>
It handles all my RAW photos<br>
The image correction/manipulation is very nice and easy to use.<br>
It has the ability to provide the kind of tagging and filtering that should make it possible find the photos I want in the future, once the huge job of annotating everything is complete.<br>
The things that are not so good:<br>
Image correction is quite slow (compared to picasa2, or even GIMP)<br>
It does not permit off-line images to have their information stored in the database. I think someone said this would be available in 10.x but I cannot see if this is the case from the documentation I see on the web site.<br>
While it is POSSIBLE to tag images images in a manner to form a nice hierarchical structure, it is a LOT of work!</p>
<p>KPhotoAlbum 3.0.2 ((under KDE 3.5.8) in Kubuntu<br>
The good:<br>
The ability to have the thumbnails and metadata for images NOT on the computer is terrific!  For example, if I have hundreds of images on a CD, I load them onto my hard drive, annotate them all (including where they reside) then delete them again. Now, in my Album, I have all the thumbnails for those images (with top right corner cut off so I know they are not on my hard drive) plus all the annotation, EXIF data, etc.<br>
Annotating (i.e. tagging) images and creating a hierarchical structure is very easy (once you get used to it).  It is much easier to create the data base structure and to do the annotation than it is with digiKam.<br>
The bad:<br>
It is not terribly stable.<br>
It does NOT recognize my RAW photos (Pentax .pef)<br>
There is no inherent image correction<br>
In my version, invoking an external program (such as GIMP) to do the correction causes KPhotoAlbum to crash.</p>
<p>---------------<br>
I am looking forward to upgrading my Kubuntu to 8.04 with KDE 4 in the near future, and to trying digiKam 10.x to see what improvements have been made, and also to try the newer version of KPhotoAlbum (3.1.1 and the one planned for KDE 4).  </p>
<p>I would like to adopt one of these two for all my photo organization, but am reluctant to invest the horrendous amount of time needed to categorize all my photos until either one of these overcomes its difficulties, or there is a new package combining the best features of each of these.</p>
<p>Thanks to all the people who continue to work to improve both packages.</p>
         </div>
    <div class="links">» </div>
  </div>

</div>
