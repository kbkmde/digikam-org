---
date: "2007-12-30T12:26:00Z"
title: "digiKam 0.9.4 splashscreens : call to photographers..."
author: "digiKam"
description: "Like digiKam 0.9.3 have been released for Christmas time, it's time to start next 0.9.4 release plan... and of course the time has come to"
category: "news"
aliases: "/node/281"

---

Like digiKam 0.9.3 have been released for Christmas time, it's time to start next 0.9.4 release plan... and of course the time has come to find the ideal splash-screens to go with it. Now is your chance to join the ranks of the precious few who have had their artwork associated with a release of digiKam! 

<br><br>

SplashScreen is a simple way for users to contribute to digiKam project. The pictures must be correctly exposed and composed, and the subject must be choosen using a real photographer inspiration. 

<br><br>

The next stable release 0.9.4 of digiKam is planed around april 2008. This version includes a new great feature to show counts of items in album folder view (like KMail folder view for example). Code is already in svn to test. Look this <a href="http://bugs.kde.org/show_bug.cgi?id=96388">KDE bugzilla entry</a> for details...

<br><br>

For this release, we need 2 new splash-screens dedicaced to digiKam and Showfoto startup. You can send me <a href="http://www.digikam.org/?q=contact">by e-mail</a> your Color or Black and White pictures, taken horizontally. We need only photos, no need to put few marks about program name and version. We have a template to do it later. 

<br><br>

See the official 0.9.3 and 0.10.0 (KDE4) <a href="http://www.digikam.org/?q=node/254">splash-screen contest results</a> as exemple.
<div class="legacy-comments">

</div>