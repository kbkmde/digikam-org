---
date: "2008-11-03T17:27:00Z"
title: "Kipi-plugins 0.2.0-beta3 for KDE4 released"
author: "digiKam"
description: "The 3rd beta release of digiKam plugins box is out. With this new release, Kipi-plugins are now compilable under Windows using MinGW and Microsoft Visual"
category: "news"
aliases: "/node/382"

---

<p>The 3rd beta release of digiKam plugins box is out.</p>

<p>With this new release, Kipi-plugins are now compilable under Windows using MinGW and Microsoft Visual C++. Precompiled packages are available with KDE-Windows installer. See <a href="http://windows.kde.org">KDE-Windows project</a> for details.</p>

<a href="http://www.flickr.com/photos/digikam/3008002686/" title="flickrexportwin32_02 by digiKam team, on Flickr"><img src="http://farm4.static.flickr.com/3020/3008002686_39825a367e.jpg" width="500" height="375" alt="flickrexportwin32_02"></a>

<p>See below the list of new features and bugs-fix coming with this release:</p>

<b>General</b>        : Port to CMake/KDE4/QT4.<br>
<b>General</b>        : all plugins can be compiled natively under Microsoft Windows.<br>
<b>JPEGLossLess</b>   : XMP metadata support.<br>
<b>RAWConverter</b>   : XMP metadata support.<br>
<b>TimeAdjust</b>     : XMP metadata support.<br>
<b>MetadataEdit</b>   : XMP metadata support with EXIF, IPTC, and Comments editor to sync XMP tags.<br>
<b>MetadataEdit</b>   : IPTC Editor dialog is more compliant with IPTC.org recommendations.<br>
<b>MetadataEdit</b>   : IPTC Editor add the capability to set multiple values for a tag.<br>
<b>SendImages</b>     : Complete re-write of emailing tool.<br>
<b>GPSSync</b>        : New tool to edit GPS track list using googlemaps.<br>
<b>RAWConverter</b>   : Raw files are decoded in 16 bits color depth using same auto-gamma and auto-white  methods provided by dcraw with 8 bits color depth RAW image decoding.<br>
<b>SlideShow</b>      : Now filenames, captions and progress indicators can have transparent backgrounds with OpenGL SlideShow as well.<br>
<b>SlideShow</b>      : Added thumbnails into image list.<br>
<b>SlideShow</b>      : Now you can play your favourite music during slideshow.<br>
<b>DNGConverter</b>   : New plugin to convert RAW camera image to Digital NeGative (DNG).<br><br>

001 ==&gt; 135451 : GPSSync            : Improve the gui of the gpssync plugin.<br>
002 ==&gt; 135386 : GPSSync            : Show track on the google map.<br>
003 ==&gt; 149497 : GPSSync            : Geolocalization kipi plugin does not work with non-jpeg file types.<br>
004 ==&gt; 145746 : GPSSync            : Do not recreate thumbs when geolocalizing images.<br>
005 ==&gt; 158792 : GPSsync            : Plugin do not working with Canon RAW CR2.<br>
006 ==&gt; 165078 : GPSSync            : GPS correlator does not show thumbnails.<br>
007 ==&gt; 165278 : GPSSync            : Geotagging dialog doesn't display all thumbnails.<br>
008 ==&gt; 134299 : SimpleViewerExport : Read orientation of image from EXIF.<br>
009 ==&gt; 150076 : SlideShow          : Playing music during a slideshow.<br>
010 ==&gt; 172283 : SlideShow          : SlideShow crashes host application.<br>
011 ==&gt; 172337 : SlideShow          : Slideshow enhancements by ken-burns effects.<br>
012 ==&gt; 173276 : SlideShow          : digikam crashs after closing.<br>

<div class="legacy-comments">

  <a id="comment-17936"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/382#comment-17936" class="active">It is still a pity the Ubuntu</a></h3>    <div class="submitted">Submitted by <a href="http://juriansluiman.nl" rel="nofollow">Mithras86</a> (not verified) on Mon, 2008-11-03 23:12.</div>
    <div class="content">
     <p>It is still a pity the Ubuntu developers don't deliver the kipi-plugins package for KDE4. You're doing really great work by improving Digikam and Kipi-plugins. I hope I can test the new gpssync thing (for me one of the most important parts) really soon!</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-17937"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/382#comment-17937" class="active">Same!</a></h3>    <div class="submitted">Submitted by François (not verified) on Tue, 2008-11-04 11:45.</div>
    <div class="content">
     <p>I was about to write the same thing! It's really a pain to test Digikam 0.10 on Ubuntu, and it's the most popular Linux distro. It's really a shame. I would love to help debugging, but it's really too complicated for me to compile both Digikam and the Kipi plugins... So I'm just waiting, it's frustrating.</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-17938"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/382#comment-17938" class="active">me too... compiling digikam</a></h3>    <div class="submitted">Submitted by da_seeb (not verified) on Wed, 2008-11-05 11:30.</div>
    <div class="content">
     <p>me too... compiling digikam is a big task because you need some libraries from kde 4.2.</p>
<p>No possibility that digikam developers create a package?</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-17939"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/382#comment-17939" class="active">I'm forth in the thread</a></h3>    <div class="submitted">Submitted by xeros (not verified) on Wed, 2008-11-05 17:45.</div>
    <div class="content">
     <p>I'm forth in the thread asking for deb packages for Kubuntu 8.10.</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-17943"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/382#comment-17943" class="active">fifth</a></h3>    <div class="submitted">Submitted by masc (not verified) on Sat, 2008-11-08 20:52.</div>
    <div class="content">
     <p>took me days to get digikam 0.10 compiled under Hardy. Just upgraded to Intrepid. Nothing works. Please, could someone provide packages.</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-17944"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/382#comment-17944" class="active">Wiki for installing digikam 0.10 in Kubuntu 8.10</a></h3>    <div class="submitted">Submitted by Gandalf (not verified) on Sun, 2008-11-09 19:45.</div>
    <div class="content">
     <p>You don't need packages, rather grab the really recent code and give it a try. If you are on a fresh install of Kubuntu 8.10, <a href="http://wiki.kde.org/tiki-index.php?page=Digikam+Compilation+on+Kubuntu+Intrepid">this new wiki page</a> will give you a step-by-step instruction how to get the latest digikam running. All you need to know is how to type commands into the command line. Beware - this manual won't work for earlier versions of kubuntu!</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-18092"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/382#comment-18092" class="active">Thanks</a></h3>    <div class="submitted">Submitted by AJ (not verified) on Mon, 2008-12-29 14:50.</div>
    <div class="content">
     <p>Followed the instructions on the wiki, works like treat<br>
Cheers</p>
         </div>
    <div class="links">» </div>
  </div>
</div></div></div><a id="comment-18000"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/382#comment-18000" class="active">Not so big, just do:   emerge</a></h3>    <div class="submitted">Submitted by Anonymous (not verified) on Wed, 2008-11-26 17:18.</div>
    <div class="content">
     <p>Not so big, just do:   emerge ....  and wait</p>
         </div>
    <div class="links">» </div>
  </div>
</div></div></div>
</div>
