---
date: "2011-06-29T17:18:00Z"
title: "digiKam Software Collection 2.0.0 Release Candidate is out..."
author: "digiKam"
description: "Dear all digiKam fans and users! digiKam team is proud to announce the digiKam Software Collection 2.0.0 release candidate! With this release, digiKam include a"
category: "news"
aliases: "/node/610"

---

<p>Dear all digiKam fans and users!</p>

<p>digiKam team is proud to announce the digiKam Software Collection 2.0.0 release candidate!</p>

<p>With this release, digiKam include a lots of bugs fixes to progress in stability for future production use. This is the last stage before final release planed at end of July 2011</p>

<a href="http://www.flickr.com/photos/digikam/5873878380/" title="facedetection-win32 by digiKam team, on Flickr"><img src="http://farm6.static.flickr.com/5155/5873878380_9fca2f1101_z.jpg" width="640" height="360" alt="facedetection-win32"></a>

<p>digiKam include since 2.0.0-beta5 a new tool to export on RajCe web service.</p>

<a href="http://www.flickr.com/photos/digikam/5702563277/" title="rajcetool by digiKam team, on Flickr"><img src="http://farm3.static.flickr.com/2678/5702563277_bfbcaaf5cf_z.jpg" width="640" height="256" alt="rajcetool"></a>

<p>digiKam include since 2.0.0-beta4 <b>Group of items feature</b> and <b>MediaWiki export tool</b>.</p>

<a href="http://www.flickr.com/photos/digikam/5564467832/" title="wikimediatool-windows by digiKam team, on Flickr"><img src="http://farm6.static.flickr.com/5062/5564467832_98ec55be99_z.jpg" width="640" height="211" alt="wikimediatool-windows"></a>

<a href="http://www.flickr.com/photos/digikam/5488098135/" title="digiKam-groupitems by digiKam team, on Flickr"><img src="http://farm6.static.flickr.com/5179/5488098135_17e6307452_z.jpg" width="640" height="360" alt="digiKam-groupitems"></a>

<p>digiKam include since 2.0.0-beta3 <b>Color Labels</b> and <b>Pick Labels</b> feature to simplify image cataloging operations in your photograph workflow.</p>

<a href="http://www.flickr.com/photos/digikam/5453984332/" title="digikampicklabel2 by digiKam team, on Flickr"><img src="http://farm6.static.flickr.com/5013/5453984332_bba7f267da_z.jpg" width="640" height="256" alt="digikampicklabel2"></a>

<p>Also, digiKam include since 2.0.0-beta2 the <b>Tags Keyboard Shorcuts</b> feature to simplify tagging operations in your photograph workflow.</p>

<a href="http://www.flickr.com/photos/digikam/5389657319/" title="tagshortcuts2 by digiKam team, on Flickr"><img src="http://farm6.static.flickr.com/5218/5389657319_edb75c8ccb_z.jpg" width="640" height="256" alt="tagshortcuts2"></a>

<p>digiKam software collection 2.0.0 include all Google Summer of Code 2010 projects, as <b>XMP sidecar support</b>, <b>Face Recognition</b>, <b>Image Versioning</b>, and <b>Reverse Geocoding</b>. All this works have been processed during <a href="http://techbase.kde.org/Projects/Digikam/CodingSprint2010">Coding Sprint 2010</a>. You can find a resume of this event <a href="http://www.digikam.org/drupal/node/538">at this page</a>.</p>

<a href="http://www.flickr.com/photos/digikam/5323487542/" title="digikam2.0.0-19 by digiKam team, on Flickr"><img src="http://farm6.static.flickr.com/5008/5323487542_fdea5ef240.jpg" width="500" height="400" alt="digikam2.0.0-19"></a>

<p>This beta release is not yet stable. Do not use yet in production. Please report all bugs to KDE bugzilla following indications <a href="http://www.digikam.org/drupal/support">from this page</a>. The release plan can be seen <a href="http://www.digikam.org/drupal/about/releaseplan">at this url</a>.</p>

<a href="http://www.flickr.com/photos/digikam/5319746295/" title="digikam2.0.0-05 by digiKam team, on Flickr"><img src="http://farm6.static.flickr.com/5130/5319746295_df7bba3672_z.jpg" width="640" height="256" alt="digikam2.0.0-05"></a>

<p>See <a href="https://projects.kde.org/projects/extragear/graphics/digikam/repository/revisions/4fd402c2ef8dc5e28d8d357a7a79fc4f252dabd8/raw/NEWS">the list of digiKam file closed</a> with this release into KDE bugzilla.</p>

<p>See <a href="https://projects.kde.org/projects/extragear/graphics/kipi-plugins/repository/revisions/70479df2cbe3e6d72028f2c1ca427f94955af3de/raw/NEWS">the list of Kipi-plugins file closed</a> with this release into KDE bugzilla.</p>

<p>digiKam software collection tarball can be downloaded from <a href="http://sourceforge.net/projects/digikam/files">SourceForge Repository</a></p>

<a href="http://www.flickr.com/photos/digikam/5319746291/" title="digikam2.0.0-04 by digiKam team, on Flickr"><img src="http://farm6.static.flickr.com/5249/5319746291_60bcdc0b28_z.jpg" width="640" height="256" alt="digikam2.0.0-04"></a>

<p>Happy digiKam testing...</p>
<div class="legacy-comments">

  <a id="comment-19924"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/610#comment-19924" class="active">:-)</a></h3>    <div class="submitted">Submitted by <a href="http://nienhueser.de/blog" rel="nofollow">Dennis Nienhüser</a> (not verified) on Wed, 2011-06-29 21:45.</div>
    <div class="content">
     <p>Thanks, I like digikam a lot and this release seems to bring some extra cool new features. Keep on the great work!</p>
         </div>
    <div class="links">» </div>
  </div>
<a id="comment-19925"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/610#comment-19925" class="active">Thanks!</a></h3>    <div class="submitted">Submitted by <a href="http://about.me/johannrenner" rel="nofollow">Johann Renner</a> (not verified) on Thu, 2011-06-30 04:46.</div>
    <div class="content">
     <p>Thank you guys for this awesome software. Every release I love it a little more :)</p>
         </div>
    <div class="links">» </div>
  </div>
<a id="comment-19926"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/610#comment-19926" class="active">digikam.exe</a></h3>    <div class="submitted">Submitted by <a href="http://www.croucrou.com" rel="nofollow">croucrou</a> (not verified) on Thu, 2011-06-30 20:47.</div>
    <div class="content">
     <p>Windows version coooooll</p>
         </div>
    <div class="links">» </div>
  </div>
<a id="comment-19927"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/610#comment-19927" class="active">Thanks (especially for the Windows version)!!!</a></h3>    <div class="submitted">Submitted by Martin (not verified) on Fri, 2011-07-01 20:37.</div>
    <div class="content">
     <p>Finally I can ditch Kubuntu which I had installed solely for DigiKam usage. Thanks again for this great software.</p>
         </div>
    <div class="links">» </div>
  </div>
<a id="comment-19928"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/610#comment-19928" class="active">Facebook export - functionality diminished</a></h3>    <div class="submitted">Submitted by Stephen S (not verified) on Mon, 2011-07-04 18:59.</div>
    <div class="content">
     <p>Perhaps it is a new security thing of Facebook itself, but I don't see why I have to copy and paste the URL from facebook into the facebook export utility in order for it to work. Please change it back to how it was in 2.0.0 beta 6 and previous. I'm not sure how to contact the makers of the kipi plugin itself, which is why I'm mentioning it here.</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-19930"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/610#comment-19930" class="active">Facebook export with KIPI 2.0.0 RC</a></h3>    <div class="submitted">Submitted by <a href="http://miriup.de" rel="nofollow">Dirk Tilger</a> (not verified) on Tue, 2011-07-05 03:36.</div>
    <div class="content">
     <p>The API used by KIPI 2.0.0 beta6 and before is a deprecated legacy API that is on Facebooks' roadmap for removal. Myself and others additionally have had problems accessing some features on Facebook when using the old authentication scheme.</p>
<p>FB as a web application has its API mainly attuned for web applications. For this reason the alternative to copy&amp;pasting the link I have come up with so far is to embed a browser, which is a substantial task.</p>
<p>However, the validity of the session can be extended by requesting additionally the right for offline use, so that you have to copy&amp;paste that link only once in a while. I've been contemplating about this and I think your post has just convinced me to do so.</p>
         </div>
    <div class="links">» </div>
  </div>
</div><a id="comment-19929"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/610#comment-19929" class="active">On windows install, XP SP3 OS</a></h3>    <div class="submitted">Submitted by edkiefer (not verified) on Mon, 2011-07-04 20:46.</div>
    <div class="content">
     <p>On windows install, XP SP3 OS digikam or showphoto don't even start . I get error "the application failed to initialize properly (0xc0150002), click ok to terminate the application ".<br>
This happens on both and all start menu shortcuts are wrong but that is easy fix .<br>
not sure if it needs more modules installed as I see KDE-window installer but seems its for old digikam</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-19933"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/610#comment-19933" class="active">from event view I was getting</a></h3>    <div class="submitted">Submitted by edkiefer (not verified) on Tue, 2011-07-05 20:50.</div>
    <div class="content">
     <p>from event view I was getting these trying to start digikam .<br>
1) dependent assembly Microsoft VC90 OpenMP could not be found and last error<br>
was the referenced assembly is not installed on your system.</p>
<p>2)Resolved partial assembly failed for Microsoft VC90 OpenMP. Reference error<br>
message: the referenced assembly is not installed on your system.</p>
<p>3)Generate activation context failed for C:\program<br>
files\digikam\KDE4-MSVC\bin\kdcraw.dll . Reference error message: the operation<br>
completed successfully</p>
<p>After installing windows Visual C++ 2008 Runtime x86 (vcredist_x86.exe) and now digikam<br>
runs fine, so anyone getting same problem check event viewer .</p>
         </div>
    <div class="links">» </div>
  </div>
</div><a id="comment-19931"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/610#comment-19931" class="active">Like a professional</a></h3>    <div class="submitted">Submitted by Jay (not verified) on Tue, 2011-07-05 12:46.</div>
    <div class="content">
     <p>Dear Gilles, dear $digikam,</p>
<p>what do you think about changing the slogan</p>
<p>"manage your photographs like a professional"</p>
<p>to something like</p>
<p>"manage your photographs professinally"</p>
<p>?</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-19932"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/610#comment-19932" class="active">because it gives the</a></h3>    <div class="submitted">Submitted by Jay (not verified) on Tue, 2011-07-05 12:53.</div>
    <div class="content">
     <p>because it gives the impression digikam is "like professional tools", while on the other hand, digikam _is_ a professional tool, I think the slogan makes digikam smaller than it really is.</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-19935"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/610#comment-19935" class="active">I think you’re right :)</a></h3>    <div class="submitted">Submitted by Anonymous (not verified) on Sat, 2011-07-09 12:45.</div>
    <div class="content">
     <p>I think you’re right :)</p>
         </div>
    <div class="links">» </div>
  </div>
</div></div><a id="comment-19934"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/610#comment-19934" class="active">I need some explanations</a></h3>    <div class="submitted">Submitted by Csaba Mészáros (not verified) on Wed, 2011-07-06 21:02.</div>
    <div class="content">
     <p>It's all about face recognition:<br>
1. Am I right that the intention with face recognition is to scan a catalog of pictures to collect faces, AND after that, using one identified face to detect all the pictures containing the same/similar face?<br>
If yes, how to achieve this result, since I can only tag faces one-by-one?<br>
If I am wrong, please explain the concept of face recognition.</p>
<p>2. It's nearly impossible to use already added tags over the image canvas. The dropdown box scrolls automatically to the "Unknown" tag, and its size is much too small. The automatically added "People" master-tag is rather bothering, than useful.</p>
<p>Thanks for your answers! It's a great application.</p>
         </div>
    <div class="links">» </div>
  </div>

</div>
