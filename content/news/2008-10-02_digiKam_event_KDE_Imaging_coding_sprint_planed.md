---
date: "2008-10-02T16:49:00Z"
title: "digiKam event: KDE Imaging coding sprint planed..."
author: "digiKam"
description: "A coding sprint is planed to Genoa - Italy by me and Angelo Naselli. The goal of this event is to improve KDE4 digikam and"
category: "news"
aliases: "/node/374"

---

<p>A coding sprint is planed to <b>Genoa - Italy</b> by me and <b>Angelo Naselli</b>. The goal of this event is to improve KDE4 digikam and kipi-plugins code.</p>

<p>The event dates are set from 31 october to 2 november. We will hosted by <a href="http://www.alid.it">ALID Association</a> which will provide a room with internet connection. The event will be sponsored by <a href="http://ev.kde.org">KDE-ev</a>.</p>

<p>Peoples who will contribute to this events are:</p>
<ul>
<li><a href="http://www.digikam.org/drupal/blog/8">Marcel Wiesweg</a>    (digiKam)</li>
<li><a href="http://www.kipi-plugins.org">Angelo Naselli</a>    (kipi-plugins)</li>
<li><a href="http://www.kolumbus.fi/kare.sars">Kåre Särs</a>         (kipi-plugins)</li>
<li><a href="http://agateau.wordpress.com">Aurélien Gâteau</a>   (kipi-plugins)</li>
<li><a href="http://vardhman.blogspot.com">Vardhman Jain</a>     (kipi-plugins)</li>
<li><a href="http://colin.guthr.ie">Colin Guthrie</a>     (kipi-plugins)</li>
<li><a href="http://adjamblog.wordpress.com">Andrea Diamantini</a> (kipi-plugins)</li>
<li><a href="http://www.valeriofuoglio.it">Valerio Fuoglio</a>   (kipi-plugins)</li>
</ul>

<p>
I you want to meet us during this event, you are welcome. ALID Association is located at <b>Genoa, Via Giovanni Torti, 35</b>. See the map given below:
</p>

<p>
<iframe width="425" height="350" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="http://maps.google.it/maps?f=q&amp;hl=en&amp;geocode=&amp;time=&amp;date=&amp;ttype=&amp;q=Via+Giovanni+Torti,+35,+16143+Genova+Genova,+Liguria,+Italia+(Zenzero)&amp;sll=44.406945,8.964426&amp;sspn=0.010884,0.020792&amp;ie=UTF8&amp;om=1&amp;ll=44.414287,8.96862&amp;spn=0.010593,0.019312&amp;z=14&amp;iwloc=addr&amp;output=embed&amp;s=AARTsJpTdNHpgFu7HRhXr47FLIrD155MZw"></iframe><br><small><a href="http://maps.google.it/maps?f=q&amp;hl=en&amp;geocode=&amp;time=&amp;date=&amp;ttype=&amp;q=Via+Giovanni+Torti,+35,+16143+Genova+Genova,+Liguria,+Italia+(Zenzero)&amp;sll=44.406945,8.964426&amp;sspn=0.010884,0.020792&amp;ie=UTF8&amp;om=1&amp;ll=44.414287,8.96862&amp;spn=0.010593,0.019312&amp;z=14&amp;iwloc=addr&amp;source=embed" style="color:#0000FF;text-align:left">View Larger Map</a></small>
</p>

<p>
We want to thanks in advance <b>KDE-ev</b> and <b>ALID Association</b> to make this event possible. If you want to help this Italian association, all donations are welcome. Just contact <a href="mailto: anaselli at linux dot it">Angelo Nasseli</a> for details.
</p>

<div class="legacy-comments">

  <a id="comment-17841"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/374#comment-17841" class="active">can't be much closer to a</a></h3>    <div class="submitted">Submitted by mxttie (not verified) on Fri, 2008-10-03 15:21.</div>
    <div class="content">
     <p>can't be much closer to a railway station I guess :) quite handy for the travellers :) have fun!</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-17856"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/374#comment-17856" class="active">Well you can't see that well</a></h3>    <div class="submitted">Submitted by Angelo Naselli (not verified) on Wed, 2008-10-08 16:19.</div>
    <div class="content">
     <p>Well you can't see that well from google map, but rail station is really near...<br>
not its entrance thoguh :p</p>
         </div>
    <div class="links">» </div>
  </div>
</div><a id="comment-17842"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/374#comment-17842" class="active">digiKam</a></h3>    <div class="submitted">Submitted by Stefan Waidheim (not verified) on Sun, 2008-10-05 20:09.</div>
    <div class="content">
     <p>I have something for the coders to sink their teeth into: It took me almost a week but i fiddled around and made a patch that enables digiKam to at least see all my photos. The thumbnails are fine, but it won't show any of them fullsize. I only get "An error is occured with media player..." The actual image viewing code is way above my head so this is all I have. The patch is made against current svn revision 868218. I hope someone can squash this bug for good.</p>
<p>--- graphics.trunk/digikam/libs/database/collectionscanner.h    2008-10-05 20:51:59.000000000 +0200<br>
+++ graphics.patch/digikam/libs/database/collectionscanner.h    2008-10-05 20:20:39.000000000 +0200<br>
@@ -237,7 +237,6 @@<br>
     void incrementDeleteRemovedCompleteScanCount();<br>
     void resetDeleteRemovedSettings();<br>
     bool checkDeleteRemoved();<br>
-    void loadNameFilters();<br>
     int countItemsInFolder(const QString&amp; directory);<br>
     DatabaseItem::Category category(const QFileInfo &amp;info);</p>
<p>--- graphics.trunk/digikam/libs/database/collectionscanner.cpp  2008-10-05 20:51:59.000000000 +0200<br>
+++ graphics.patch/digikam/libs/database/collectionscanner.cpp  2008-10-05 20:20:19.000000000 +0200<br>
@@ -39,6 +39,7 @@<br>
 // KDE includes.</p>
<p> #include<br>
+#include </p>
<p> // LibKDcraw includes.</p>
<p>@@ -88,10 +89,6 @@<br>
         wantSignals = false;<br>
     }</p>
<p>-    QStringList       nameFilters;<br>
-    QSet     imageFilterSet;<br>
-    QSet     videoFilterSet;<br>
-    QSet     audioFilterSet;<br>
     QList        scannedAlbums;<br>
     bool              wantSignals;</p>
<p>@@ -140,25 +137,6 @@<br>
     }<br>
 }</p>
<p>-void CollectionScanner::loadNameFilters()<br>
-{<br>
-    QStringList imageFilter, audioFilter, videoFilter;<br>
-    DatabaseAccess().db()-&gt;getFilterSettings(&amp;imageFilter, &amp;videoFilter, &amp;audioFilter);<br>
-<br>
-    // one list for filtering when listing a dir, with wildcard globbing<br>
-    foreach (const QString &amp;suffix, imageFilter)<br>
-        d-&gt;nameFilters &lt;&lt; "*." + suffix;<br>
-    foreach (const QString &amp;suffix, audioFilter)<br>
-        d-&gt;nameFilters &lt;&lt; "*." + suffix;<br>
-    foreach (const QString &amp;suffix, videoFilter)<br>
-        d-&gt;nameFilters &lt;&lt; "*." + suffix;<br>
-<br>
-    // three sets to find category of a file<br>
-    d-&gt;imageFilterSet = imageFilter.toSet();<br>
-    d-&gt;audioFilterSet = audioFilter.toSet();<br>
-    d-&gt;videoFilterSet = videoFilter.toSet();<br>
-}<br>
-<br>
 void CollectionScanner::completeScan()<br>
 {<br>
     emit startCompleteScan();<br>
@@ -166,7 +144,6 @@<br>
     // lock database<br>
     DatabaseTransaction transaction;</p>
<p>-    loadNameFilters();<br>
     d-&gt;resetRemovedItemsTime();</p>
<p>     //TODO: Implement a mechanism to watch for album root changes while we keep this list<br>
@@ -241,7 +218,6 @@<br>
         return;<br>
     }</p>
<p>-    loadNameFilters();<br>
     d-&gt;resetRemovedItemsTime();</p>
<p>     CollectionLocation location = CollectionManager::instance()-&gt;locationForAlbumRootPath(albumRoot);<br>
@@ -287,7 +263,6 @@<br>
     int albumId = checkAlbum(location, album);<br>
     qlonglong imageId = DatabaseAccess().db()-&gt;getImageId(albumId, fileName);</p>
<p>-    loadNameFilters();<br>
     if (imageId == -1)<br>
     {<br>
         scanNewFile(info, albumId);<br>
@@ -460,7 +435,7 @@<br>
         itemIdSet &lt;&lt; scanInfos[i].id;<br>
     }</p>
<p>-    const QFileInfoList list = dir.entryInfoList(d-&gt;nameFilters, QDir::AllDirs | QDir::Files  | QDir::NoDotAndDotDot /*not CaseSensitive*/);<br>
+    const QFileInfoList list = dir.entryInfoList(QStringList() &lt;&lt; "*.*" &lt;&lt; "*", QDir::AllDirs | QDir::Files  | QDir::NoDotAndDotDot /*not CaseSensitive*/);<br>
     QFileInfoList::const_iterator fi;</p>
<p>     for (fi = list.constBegin(); fi != list.constEnd(); ++fi)<br>
@@ -592,12 +567,12 @@</p>
<p> DatabaseItem::Category CollectionScanner::category(const QFileInfo &amp;info)<br>
 {<br>
-    QString suffix = info.suffix().toLower();<br>
-    if (d-&gt;imageFilterSet.contains(suffix))<br>
+    KMimeType::Ptr type = KMimeType::findByFileContent( info.filePath() );<br>
+    if ( (*type).is("image") )<br>
         return DatabaseItem::Image;<br>
-    else if (d-&gt;audioFilterSet.contains(suffix))<br>
+    if ( (*type).is("audio") )<br>
         return DatabaseItem::Audio;<br>
-    else if (d-&gt;videoFilterSet.contains(suffix))<br>
+    if ( (*type).is("video") )<br>
         return DatabaseItem::Video;<br>
     else<br>
         return DatabaseItem::Other;</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-17844"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/374#comment-17844" class="active">Re: digikam</a></h3>    <div class="submitted">Submitted by Marcel Wiesweg (not verified) on Tue, 2008-10-07 09:40.</div>
    <div class="content">
     <p>Stefan, please open a bug report on bugs.kde.org and describe your problem.<br>
Judging from your patch, you disable the name filtering, so obviously something is wrong with that on your setup. Please tell us which image formats trigger the problem.</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-17847"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/374#comment-17847" class="active">I cannot use the bugtracker</a></h3>    <div class="submitted">Submitted by Stefan Waidheim (not verified) on Tue, 2008-10-07 16:05.</div>
    <div class="content">
     <p>I cannot use the bugtracker atm, as I'm behind a transparent proxy which makes it unuseable. This will improve in the nest weeks, i hope.<br>
Yes, the patch removes the filename-based type filtering and uses the mimetype-based one from kdelibs. This has puzzled me for a long time, as every other program like Dolphin, Krusader, Gwenview or the Konqueror gives no trouble. I'm not sure the patch is correct (it most probably is not) but at least it works and all my photos are now found.<br>
There is no problem with any specific image format. The bug apperas with all of them, mostly jpeg or png, but I have a some tiff and an increasing number of raw files too. The reason is in the filename-based detection which won't work if there is no filename extension or an unknown one, like just a numeric suffix.</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-17848"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/374#comment-17848" class="active">I doesn't work with raw files.</a></h3>    <div class="submitted">Submitted by digiKam on Tue, 2008-10-07 17:30.</div>
    <div class="content">
     <p>This is a wrong way. With all raw files format, using KDELibs mime type wrapper, it will never work. Why ? Because Camera makers has used the tiff format to contruct private container.</p>

<p>It's the hell. If you use KDELibs to handle RAW files, a lots will be open as tiff files using libtiff library which will generate a failure of course.</p>

<p>And like you can see <a href="http://websvn.kde.org/trunk/KDE/kdegraphics/libs/libkdcraw/libkdcraw/rawfiles.h?view=markup">at this place</a>, The list of RAW file formats is huge...</p>

<p>Alternative is to parse file content to detect if it's really a RAW file. It's a big puzzle. Perhaps with LibRaw will work, but i'm sure that it will be a slower solution against to check file extension as well.</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-17852"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/374#comment-17852" class="active">The slowliness is very true,</a></h3>    <div class="submitted">Submitted by Stefan Waidheim (not verified) on Tue, 2008-10-07 21:07.</div>
    <div class="content">
     <p>The slowliness is very true, DigiKam takes a couple of minutes to scan through even a few thousand photos. And I know my patch is only a crude workaround. The optimum would be somewhere in the middle, first guessing on filename extensions and if it fails on actual file content. DigiKam should already do this if you put "*" in the config dialog, but it does not. That's the bug I tried to fix.</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-17854"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/374#comment-17854" class="active">Startup is another problem...</a></h3>    <div class="submitted">Submitted by digiKam on Wed, 2008-10-08 05:14.</div>
    <div class="content">
     Slow startup is another problem. There is already a <a href="http://bugs.kde.org/show_bug.cgi?id=172269
">file in bugzilla</a> about...         </div>
    <div class="links">» </div>
  </div>
</div></div></div><a id="comment-17889"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/374#comment-17889" class="active">DigiKam is a new discovery for me!</a></h3>    <div class="submitted">Submitted by <a href="http://www.amot.wordpress.com" rel="nofollow">flacvest</a> (not verified) on Fri, 2008-10-24 17:02.</div>
    <div class="content">
     <p>Hi, I just wanted to say thanks for the following:</p>
<p>Using libdcraw and improving upon it, as it's the only thing out there that supports the Foveon X3 sensor in the Sigma SD-14 Digital SLR, a really cool camera I've rented a few times I think I might purchase it...</p>
<p>Being 16-bit...</p>
<p>Being quick enough...</p>
<p>Having this little jewel for me to discover: Inpainting to remove artefacts!!! SWEET!!!<br>
Junk Eradication!</p>
<p>Being even better though different than GIMP and Krita. I say better because the Inpainting feature is a better dustbusting feature than the other two Open Source competitors and puts everything else I've played with out there to shame. Kudos!</p>
<p>Using LittleCMS...</p>
<p>Too bad you're not an Xfce application, but at least I don't have to pull in all of KOffice to use digiKam... I LIKE modularity!</p>
<p>And, last but not least, thanks for making a REAL Photo application guys and gals!</p>
         </div>
    <div class="links">» </div>
  </div>
</div></div>
</div>