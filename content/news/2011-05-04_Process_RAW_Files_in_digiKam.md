---
date: "2011-05-04T09:11:00Z"
title: "Process RAW Files in digiKam"
author: "Dmitri Popov"
description: "digiKam usually does a decent job of decoding RAW files using the default settings. But if you prefer to have complete control of how the"
category: "news"
aliases: "/node/596"

---

<p>digiKam usually does a decent job of decoding RAW files using the default settings. But if you prefer to have complete control of how the application processes RAW files, choose Settings » Configure digiKam, switch to the RAW Decoding section, and enable the Always open the Raw Import Tool to customize settings option. Next time you open a RAW file for editing, digiKam drops you into the RAW Import interface where you can tweak the RAW import and post-processing settings. <a href="http://scribblesandsnaps.wordpress.com/2011/05/04/process-raw-files-in-digikam/">Continue to read</a></p>

<div class="legacy-comments">

</div>