---
date: "2011-03-09T09:31:00Z"
title: "Install the Latest Beta of digiKam on Ubuntu 10.10"
author: "Dmitri Popov"
description: "If you are running Ubuntu 10.10 or its derivatives and you are itching to try the latest version of digiKam, you don’t have to go"
category: "news"
aliases: "/node/584"

---

<p>If you are running Ubuntu 10.10 or its derivatives and you are itching to try the latest version of digiKam, you don’t have to go through the rigmarole of compiling the application yourself. Philip Johnsson did the hard work for you and released the neatly packaged version of digiKam on his PPA, so you can easily install the latest beta version of the next major release of digiKam with a minimum of fuss. <a href="http://scribblesandsnaps.wordpress.com/2011/03/09/install-the-latest-beta-of-digikam-on-ubuntu-10-10/">Continue to read</a></p>

<div class="legacy-comments">

</div>