---
date: "2013-08-07T13:11:00Z"
title: "Apply EXIF Metadata to Multiple Photos in digiKam"
author: "Dmitri Popov"
description: "If you happen to use digiKam for managing photos scanned from negatives, you'll appreciate the application's capabilities to add and edit EXIF metadata. Using digiKam's"
category: "news"
aliases: "/node/702"

---

<p>If you happen to use digiKam for managing photos scanned from negatives, you'll appreciate the application's capabilities to add and edit EXIF metadata. Using digiKam's dedicated interface for managing metadata, you can add key EXIF values, such as maker, device model, aperture, shutter speed, ISO, focal length, etc., to the scanned photos (provided you have these data handy).</p>
<p><img class="size-medium wp-image-3810" alt="Editing EXIF in digiKam" src="http://scribblesandsnaps.files.wordpress.com/2013/07/digikam-exif.png?w=500" width="500" height="445"></p>
<p>However, digiKam doesn't allow you to apply the same EXIF data to multiple photos in a single operation.</p>
<p><a href="http://scribblesandsnaps.com/2013/08/07/apply-exif-metadata-to-multiple-photos-in-digikam/">Continue to read</a></p>

<div class="legacy-comments">

</div>