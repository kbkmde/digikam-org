---
date: "2011-07-11T08:22:00Z"
title: "Process Photos with Batch Queue Manager and a Bash Script"
author: "Dmitri Popov"
description: "One of digiKam’s lesser known features is the ability to link scripts to notifications. At first sight, this may seem like a rather obscure functionality,"
category: "news"
aliases: "/node/613"

---

<p>One of digiKam’s lesser known features is the ability to link scripts to notifications. At first sight, this may seem like a rather obscure functionality, but it can be put to some clever uses. Say, you want&nbsp;to keep a portfolio of&nbsp;selected photos on a mobile device. Resizing multiple photos to a specified size to make it easier to view them on the mobile device&nbsp;and transferring the processed photos from digiKam to the mobile device manually is not very practical. And this is where the ability to trigger scripts via&nbsp;notifications&nbsp;can come in handy. You can attach a simple Bash script to the&nbsp;Batch queue completed&nbsp;notification, so it’s triggered automatically&nbsp;when the&nbsp;Batch Queue Manager tool is done processing photos.</p>
<p><a href="http://scribblesandsnaps.wordpress.com/2011/07/11/process-photos-with-digikams-batch-queue-manager-and-a-bash-script/">Continue to read</a></p>

<div class="legacy-comments">

</div>