---
date: "2012-12-11T11:45:00Z"
title: "digiKam Recipes 3.9.51 Released"
author: "Dmitri Popov"
description: "A new version of the digiKam Recipes ebook is available for your reading pleasure. Besides a few minor tweaks, the new version of the digiKam"
category: "news"
aliases: "/node/676"

---

<p>A new version of the digiKam Recipes ebook is available for your reading pleasure. Besides a few minor tweaks, the new version of the <a href="http://dmpop.homelinux.com/digikamrecipes/">digiKam Recipes</a> ebook includes the following new material:</p>
<ul>
<li>Use Photoshop Curve Presets with digiKam</li>
<li>Import Photos from a Remote Server into digiKam</li>
</ul>
<p><img class="size-medium wp-image-2676" title="digikamrecipes-3935-fbreader" alt="" src="https://scribblesandsnaps.files.wordpress.com/2012/06/digikamrecipes-3935-fbreader.png?w=281" width="281" height="500"></p>
<p><a href="http://scribblesandsnaps.com/2012/12/11/digikam-recipes-3-9-51-released/">Continue to read</a></p>

<div class="legacy-comments">

</div>