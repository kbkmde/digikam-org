---
date: "2013-05-08T07:42:00Z"
title: "Sharpening in digiKam"
author: "Dmitri Popov"
description: "The Sharpen tool in digiKam offers three sharpening methods: simple sharp, unsharp mask, and refocus. Each method has its advantages and drawbacks. The simple sharp"
category: "news"
aliases: "/node/693"

---

<p>The Sharpen tool in digiKam offers three sharpening methods: simple sharp, unsharp mask, and refocus. Each method has its advantages and drawbacks. The simple sharp technique uses a standard convolution matrix algorithm to improve image details. If you are curious about the nitty-gritty of the convolution matrix, the&nbsp;<a href="http://docs.gimp.org/en/plug-in-convmatrix.html">GIMP documentation</a> provides a brief description of the algorithm.</p>
<p><img class="size-medium wp-image-3586" alt="Using the Refocus sharpening method" src="http://scribblesandsnaps.files.wordpress.com/2013/04/digikam-refocussharpen.png?w=500" width="500" height="391"></p>
<p><a href="http://scribblesandsnaps.com/2013/05/08/sharpening-in-digikam/">Continue to read</a></p>

<div class="legacy-comments">

</div>