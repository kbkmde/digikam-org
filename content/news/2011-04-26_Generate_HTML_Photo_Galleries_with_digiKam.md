---
date: "2011-04-26T07:11:00Z"
title: "Generate HTML Photo Galleries with digiKam"
author: "Dmitri Popov"
description: "There are so many clever ways to share photos on the Web that the idea of creating a static HTML photo gallery may sound positively"
category: "news"
aliases: "/node/594"

---

<p>There are so many clever ways to share photos on the Web that the idea of creating a static HTML photo gallery may sound positively obsolete. But in certain situations, the ability to turn a bunch of photos into an HTML gallery can come in rather handy. For starters, serving a bunch of static HTML pages is less complicated than setting up a dedicated photo sharing solution, which can be useful if you want to host a photo gallery on your own server. An HTML gallery usually requires less resources, too, so you can host it on modest hardware. Also, you can store an HTML gallery on a USB stick and use it as your photo portfolio you can display even when offline. <a href="http://scribblesandsnaps.wordpress.com/2011/04/26/generate-html-photo-galleries-with-digikam/">Continue to read</a></p>

<div class="legacy-comments">

</div>