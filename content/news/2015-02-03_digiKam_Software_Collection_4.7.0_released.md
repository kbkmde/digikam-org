---
date: "2015-02-03T06:07:00Z"
title: "digiKam Software Collection 4.7.0 released..."
author: "digiKam"
description: "Dear digiKam fans and users, A new year, a new release... The digiKam Team is proud to announce the release of digiKam Software Collection 4.7.0."
category: "news"
aliases: "/node/731"

---

<a href="https://www.flickr.com/photos/digikam/16431156881"><img src="https://farm8.staticflickr.com/7327/16431156881_4c840eb2bf_c.jpg" width="800" height="225"></a>

<p>Dear digiKam fans and users,</p>

<p>
A new year, a new release... The digiKam Team is proud to announce the release of digiKam Software Collection 4.7.0. This release includes many bugs fixes from <a href="https://plus.google.com/u/0/107171232114475191915/about">Maik Qualmann</a> who propose patches to maintain KDE4 version while <a href="https://techbase.kde.org/Projects/Digikam/CodingSprint2014#KDE_Framework_Port">KF5 port is under progress</a>.
</p>

<p>
See the new list of the <a href="https://bugs.kde.org/buglist.cgi?f1=cf_versionfixedin&amp;o1=equals&amp;query_format=advanced&amp;bug_status=RESOLVED&amp;bug_status=NEEDSINFO&amp;bug_status=VERIFIED&amp;bug_status=CLOSED&amp;v1=4.7.0&amp;product=digikam&amp;product=digikamimageplugins&amp;product=kipiplugins&amp;product=showfoto">issues closed</a> in digiKam 4.7.0 available through the KDE Bugs-tracking System.
</p>

<p>The digiKam software collection tarball can be downloaded from the <a href="http://download.kde.org/stable/digikam/digikam-4.7.0-1.tar.bz2.mirrorlist">KDE repository</a>.
</p>

<p>Have an happy new year, playing with your photos using this new 2015 release.</p>

<p>digiKam Team...</p>
<div class="legacy-comments">

  <a id="comment-20975"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/731#comment-20975" class="active">refreshing images</a></h3>    <div class="submitted">Submitted by dave (not verified) on Tue, 2015-02-03 15:12.</div>
    <div class="content">
     <p>Hi!<br>
I have a question.<br>
How come I can scroll up and down using Picasa and the images does not have to be refreshed or loaded again, but in digiKam<br>
images appears as white rectangles until they are loaded again? This happens mainly with folders with many photos.<br>
What kind of magic does Picasa use that noone else can replicate?</p>
<p>David</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-21045"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/731#comment-21045" class="active">I de flesta fall kommer du</a></h3>    <div class="submitted">Submitted by <a href="http://www.sverigeapoteketonline.net/viagra.html" rel="nofollow">Viagra</a> (not verified) on Mon, 2015-06-08 05:56.</div>
    <div class="content">
     <p>I de flesta fall kommer du att rekommenderas att börja med en 50 mg piller tas före samlag - inte mer än en gång per dag. Om det fungerar bra, kan du få råd att hålla sig med <a href="http://www.apotekgothenburg.com/viagra.html">Viagra Göteborg</a> 50 mg tabletter eller prova 25 mg sådana. Om den inte gör det, kommer du också få prova 100 mg sådana, som för närvarande är det starkaste alternativet för Viagra.</p>
         </div>
    <div class="links">» </div>
  </div>
</div><a id="comment-20976"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/731#comment-20976" class="active">At the risk of sounding like</a></h3>    <div class="submitted">Submitted by cc (not verified) on Fri, 2015-02-06 23:54.</div>
    <div class="content">
     <p>At the risk of sounding like a broken record: I would like to request an updated build for windows. I would also take build instructions (as requested <a href="https://bugs.kde.org/show_bug.cgi?id=342481">on the bugtracker</a>), but right now i can't see how i could build this myself. The bugtracker item describes the problem in more detail. Maybe someone here knows how to do it.</p>
         </div>
    <div class="links">» </div>
  </div>
<a id="comment-20977"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/731#comment-20977" class="active">Real-time sliders?</a></h3>    <div class="submitted">Submitted by Anonymous (not verified) on Mon, 2015-02-09 13:40.</div>
    <div class="content">
     <p>Hi,<br>
All photo management software have moved to a realtime slider-based interface where the preview is synchronised with the changing values. Can digikam do that too?</p>
<p>Cheers.</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-20978"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/731#comment-20978" class="active">what is that ?</a></h3>    <div class="submitted">Submitted by digiKam on Mon, 2015-02-09 13:58.</div>
    <div class="content">
     <p>Please give link, screenshot, info...</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-20987"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/731#comment-20987" class="active">He may be talking about</a></h3>    <div class="submitted">Submitted by Piter Dias (not verified) on Mon, 2015-03-09 02:27.</div>
    <div class="content">
     <p>He may be talking about softwares like darktable and Lightroom, where the changes are applied to the image view after you move a sliding bar. The changes are not saved to the original files, but to the sidecar ones.</p>
         </div>
    <div class="links">» </div>
  </div>
</div></div><a id="comment-20981"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/731#comment-20981" class="active">Inpainting still does not work properly</a></h3>    <div class="submitted">Submitted by Holger Wagemann (not verified) on Tue, 2015-02-24 13:50.</div>
    <div class="content">
     <p>Hello,</p>
<p>I've tested new digikam 4.7.0 a little bit, nice software, but inpainting does not work.</p>
<p>The bug is reported:</p>
<p>https://bugs.kde.org/show_bug.cgi?id=232926</p>
<p>Please fix it :-)</p>
<p>Kind regards,<br>
  Holger</p>
         </div>
    <div class="links">» </div>
  </div>

</div>