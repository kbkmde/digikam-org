---
date: "2000-01-01T00:00:00Z"
title: "Altitude information available in kipi Geolocator plugin (3D info)"
author: "Nobody"
description: "As of yesterday, and from svn checkout only, when locating images with the kipi-plugin Geolocator, the altitude information is provided in addition to the lon,"
category: "news"
aliases: "/node/299"

---

<p>As of yesterday, and from svn checkout only, when locating images with the kipi-plugin Geolocator, the <strong>altitude information is provided</strong> in addition to the lon, lat coordinated.<br>
In the KDE4 0.10 version this works also in the tracklist mode where you can show and geolocate several images at the same time.</p>

<div class="legacy-comments">

</div>