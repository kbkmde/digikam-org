---
date: "2012-11-12T11:54:00Z"
title: "digiKam Software Collection 3.0.0-beta3 is out.."
author: "digiKam"
description: "Dear all digiKam fans and users! After one summer of active development with Google Summer of Code 2012 students, and one two beta releases, digiKam"
category: "news"
aliases: "/node/674"

---

<a href="http://www.flickr.com/photos/digikam/8154264538/" title="splash-digikam by digiKam team, on Flickr"><img src="http://farm8.staticflickr.com/7271/8154264538_131ed768c1.jpg" width="500" height="307" alt="splash-digikam"></a>

<p>Dear all digiKam fans and users!</p>

<p>After one summer of active development with Google Summer of Code 2012 students, and one two beta releases, digiKam team is proud to announce the third beta of digiKam Software Collection 3.0.0.
This version is currently under development, following GoSC 2012 projects <a href="http://community.kde.org/Digikam/GSoC2012">listed here</a>.</p>

<p>See <a href="https://bugs.kde.org/buglist.cgi?f1=cf_versionfixedin&amp;o1=equals&amp;query_format=advanced&amp;bug_status=RESOLVED&amp;bug_status=NEEDSINFO&amp;bug_status=VERIFIED&amp;bug_status=CLOSED&amp;v1=3.0.0&amp;product=digikam&amp;product=digikamimageplugins&amp;product=kipiplugins&amp;product=showfoto">the list of files closed</a> with this release into KDE bugzilla.</p>

<p>digiKam software collection tarball can be downloaded from <a href="http://download.kde.org/unstable/digikam/digikam-3.0.0-beta3.tar.bz2.mirrorlist">KDE repository</a></p>

<p>This version is for testing purposes. <b>Please do not use it yet in production!</b></p>

<p>Happy digiKam testing...</p>
<div class="legacy-comments">

  <a id="comment-20425"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/674#comment-20425" class="active">Hi.
Will it be later possible</a></h3>    <div class="submitted">Submitted by Happy Digikam User (not verified) on Mon, 2012-11-12 12:40.</div>
    <div class="content">
     <p>Hi.<br>
Will it be later possible to uploade the pictures to google+ via the kipi plugins?</p>
<p>Thank you for this great Application. I really enjoy using it. :)</p>
<p>Kind Regards</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-20430"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/674#comment-20430" class="active">AFAIK google plus do not have</a></h3>    <div class="submitted">Submitted by AC (not verified) on Tue, 2012-11-13 12:50.</div>
    <div class="content">
     <p>AFAIK google plus do not have write API yet.</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-20463"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/674#comment-20463" class="active">Hi,
isn't G+ and Picasa quite</a></h3>    <div class="submitted">Submitted by <a href="http://www.mynethome.de" rel="nofollow">Markus</a> (not verified) on Mon, 2012-11-26 18:03.</div>
    <div class="content">
     <p>Hi,<br>
isn't G+ and Picasa quite the same? Google ismerging more and more, so maybe you can just upload to picasa and publish the photos on G+ ?<br>
cheers<br>
 Markus</p>
         </div>
    <div class="links">» </div>
  </div>
</div></div><a id="comment-20426"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/674#comment-20426" class="active">today i go to digikam.org</a></h3>    <div class="submitted">Submitted by Anonymous (not verified) on Mon, 2012-11-12 14:09.</div>
    <div class="content">
     <p>today i go to digikam.org with firefox<br>
then<br>
i arrive to a page saying they must check i am not a robot !</p>
<p>i had to fill a captcha according to reach digikam.org</p>
<p>what is this ?</p>
<p>i have also this sort sort of pb with "kde dot" site</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-20427"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/674#comment-20427" class="active">http://www.omat.nl/2012/07/30</a></h3>    <div class="submitted">Submitted by Anonymous (not verified) on Mon, 2012-11-12 14:25.</div>
    <div class="content">
     <p>http://www.omat.nl/2012/07/30/improved-security-and-cdn-network-for-our-sites/ incapsula is being used</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-20428"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/674#comment-20428" class="active">yes it improves security but</a></h3>    <div class="submitted">Submitted by Anonymous (not verified) on Mon, 2012-11-12 20:12.</div>
    <div class="content">
     <p>yes it improves security but it does not work</p>
<p>it does not remember i am not a robot in the contrary of what they say</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-20460"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/674#comment-20460" class="active">annoyance</a></h3>    <div class="submitted">Submitted by Anonymous (not verified) on Tue, 2012-11-20 18:34.</div>
    <div class="content">
     <p>because it's trying to track you, and if you have privacy and security extensions in FF, it can't do that.  this is how you're coerced into sacrificing your privacy</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-20461"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/674#comment-20461" class="active">Can second</a></h3>    <div class="submitted">Submitted by Hans (not verified) on Sat, 2012-11-24 01:31.</div>
    <div class="content">
     <p>I agree. This is extremely annoying. I thought the website was hacked or had technical problems. Requiring JavaScript and cookies makes no sense for a website like digikam.org. It worked perfectly before and, judging by the millions of websites that don't annoy the user in this way, it's not a must for security. Please return to respecting the visitors' privacy and, by the way, security.</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-20464"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/674#comment-20464" class="active">Same Problem</a></h3>    <div class="submitted">Submitted by <a href="http://www.booktup.com" rel="nofollow">Booktup</a> (not verified) on Thu, 2012-12-06 00:11.</div>
    <div class="content">
     <p>I agree it irritated me quite a bit I am not a robot dang it...</p>
         </div>
    <div class="links">» </div>
  </div>
</div></div></div></div></div><a id="comment-20429"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/674#comment-20429" class="active">hi!
whats the status</a></h3>    <div class="submitted">Submitted by justme (not verified) on Mon, 2012-11-12 20:50.</div>
    <div class="content">
     <p>hi!<br>
whats the status regarding face recognition?<br>
https://bugs.kde.org/show_bug.cgi?id=271679</p>
<p>as it was part of gsoc2012: will it be working in the 3.0.0 release?</p>
<p>br<br>
justme</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-20459"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/674#comment-20459" class="active">Same question here</a></h3>    <div class="submitted">Submitted by urcindalo (not verified) on Sun, 2012-11-18 08:30.</div>
    <div class="content">
     <p>Nobody? I'm also eagerly awaiting this capability. In fact, it is the only one Google SoC 2012 new feature I'm sorely missing.<br>
What's the difference between an "under progess" and a "not completed" status?<br>
Will this feature finally make it into Digikam 3.0 when officially released? If not, will it make it in the near future?<br>
Please, answer our questions :)</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-20465"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/674#comment-20465" class="active">Idem</a></h3>    <div class="submitted">Submitted by Nicolas (not verified) on Fri, 2012-12-07 09:34.</div>
    <div class="content">
     <p>this is THE feature I'm also waiting for.<br>
Could you please give us the status ?<br>
Thanks for this great software (would be still greater with face recognition ;-) )</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-20466"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/674#comment-20466" class="active">not completed...</a></h3>    <div class="submitted">Submitted by digiKam on Fri, 2012-12-07 10:20.</div>
    <div class="content">
     <p>Student working on this project as started to work too late and do not have provided a suitable solution... I think we will need to report this project in next GoSC 2013...</p>
         </div>
    <div class="links">» </div>
  </div>
<a id="comment-20468"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/674#comment-20468" class="active">The GSoC student was working</a></h3>    <div class="submitted">Submitted by Marcel Wiesweg (not verified) on Sun, 2012-12-09 13:54.</div>
    <div class="content">
     <p>The GSoC student was working one of those who do just what it needed to get passed and disappeared with the last day of the project. Sometimes you can't know that in advance, his proposal was excellent.<br>
At the moment, I dont have time to finish the work and/or complement it with the FisherFaces code from OpenCV. This is planned though.</p>
         </div>
    <div class="links">» </div>
  </div>
</div></div></div><a id="comment-20467"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/674#comment-20467" class="active">picasa album import</a></h3>    <div class="submitted">Submitted by Anonymous (not verified) on Sun, 2012-12-09 11:43.</div>
    <div class="content">
     <p>Hi guys,</p>
<p>I regularly come back here to check if there is any progress on importing picasa albums. I found this bug/feature request from 2009, but there seems to be no progress whatsoever:<br>
https://bugs.kde.org/show_bug.cgi?id=217991</p>
<p>For me, not having picasa metadata import functionality in digikam (keep my local albums!) is the only reason for keeping around the outdated 3.0 version of picasa, after google has officially dropped linux support.</p>
<p>Thanks,<br>
Chris</p>
         </div>
    <div class="links">» </div>
  </div>

</div>
