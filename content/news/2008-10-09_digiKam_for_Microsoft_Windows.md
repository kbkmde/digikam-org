---
date: "2008-10-09T13:16:00Z"
title: "digiKam for Microsoft Windows !"
author: "digiKam"
description: "\"Et voilà...\" This is an important stage for digiKam project. A native port under Microsoft Windows... Since digiKam code has been ported to Qt4/KDE4, multi-platform"
category: "news"
aliases: "/node/378"

---

<p><b>"Et voilà..."</b></p>

<p>This is an important stage for digiKam project. A native port under Microsoft Windows...</p>

<a href="http://www.flickr.com/photos/digikam/2926449590/" title="digikam-firstrun-01 by digiKam team, on Flickr"><img src="http://farm4.static.flickr.com/3017/2926449590_f44d7f9363.jpg" width="500" height="375" alt="digikam-firstrun-01"></a>

<p>Since digiKam code has been ported to Qt4/KDE4, multi-platform support become a reality. We have already a MACOS-X port done by <a href="http://gustavoboiko.org">Gustavo Boiko</a> from Mandriva team... Now, Windows port have been completed by <a href="http://saroengels.blogspot.com">Patrick Spendrin</a> from <a href="http://windows.kde.org">KDE-Windows team</a></p><p>

</p><p>To be able to compile digiKam under Windows, GPhoto2 support is become optional, because libgphoto2 is not yet ported to this operating system. Perhaps in the future, this will change, but for the moment, it's not the case... Of course under Linux and MACOS-X, GPhoto2 support still here.</p>

<a href="http://www.flickr.com/photos/digikam/2926449582/" title="digikam-camera-configuration by digiKam team, on Flickr"><img src="http://farm4.static.flickr.com/3155/2926449582_5944062da2.jpg" width="500" height="375" alt="digikam-camera-configuration"></a>

<p>But nothing is clean yet. digiKam compiles and runs, but more advanced tests need to be done to validate this port. Code will be published with the next 0.10.0-beta5 release, so you must stay tuned in this room...</p>

<div class="legacy-comments">

  <a id="comment-17860"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/378#comment-17860" class="active">Bravo</a></h3>    <div class="submitted">Submitted by <a href="http://benemie.fr/" rel="nofollow">Trancept</a> (not verified) on Thu, 2008-10-09 14:57.</div>
    <div class="content">
     <p>So Digikam works now on Linux, MacOS and Windows ?<br>
Just one word : bravo !</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-17861"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/378#comment-17861" class="active">felicitation!! </a></h3>    <div class="submitted">Submitted by <a href="http://ktank.free.fr" rel="nofollow">Anonymous</a> (not verified) on Thu, 2008-10-09 17:51.</div>
    <div class="content">
     <p>So that's a good news... Now, the fights between Picassa and digikam can be start!</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-17863"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/378#comment-17863" class="active">I used digikam 1 year on</a></h3>    <div class="submitted">Submitted by Anonymous (not verified) on Thu, 2008-10-09 20:21.</div>
    <div class="content">
     <p>I used digikam 1 year on linux, now picasa for 1 month on windows. I obviously prefer digikam, but picasa has also some nice feature worth copying :)<br>
P.S. double captcha (preview+save) is a bit overkill</p>
         </div>
    <div class="links">» </div>
  </div>
</div><a id="comment-17868"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/378#comment-17868" class="active">Excellent!  I've been waiting</a></h3>    <div class="submitted">Submitted by Anonymous (not verified) on Sat, 2008-10-11 02:28.</div>
    <div class="content">
     <p>Excellent!  I've been waiting for a Windows port!!!</p>
         </div>
    <div class="links">» </div>
  </div>
<a id="comment-17892"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/378#comment-17892" class="active">but where can i find the</a></h3>    <div class="submitted">Submitted by judas (not verified) on Mon, 2008-10-27 19:49.</div>
    <div class="content">
     <p>but where can i find the windows version to download</p>
         </div>
    <div class="links">» </div>
  </div>
</div><a id="comment-17862"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/378#comment-17862" class="active">One Word</a></h3>    <div class="submitted">Submitted by Maxime Delome (not verified) on Thu, 2008-10-09 19:51.</div>
    <div class="content">
     <p>Congratulation</p>
         </div>
    <div class="links">» </div>
  </div>
<a id="comment-17864"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/378#comment-17864" class="active">It's "Mac OS X", not</a></h3>    <div class="submitted">Submitted by Markus (not verified) on Fri, 2008-10-10 00:54.</div>
    <div class="content">
     <p>It's "Mac OS X", not "MACOS-X".</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-17976"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/378#comment-17976" class="active">it just works</a></h3>    <div class="submitted">Submitted by Anonymous (not verified) on Sat, 2008-11-15 21:20.</div>
    <div class="content">
     <p>it just works...</p>
         </div>
    <div class="links">» </div>
  </div>
</div><a id="comment-17865"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/378#comment-17865" class="active">Three Words...</a></h3>    <div class="submitted">Submitted by <a href="http://kubuntulover.blogspot.com" rel="nofollow">Bugsbane</a> (not verified) on Fri, 2008-10-10 01:35.</div>
    <div class="content">
     <p>Embrace, Extend... Extinguish!</p>
<p>Heh, heh, heh... ;D</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-17866"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/378#comment-17866" class="active">Cool!</a></h3>    <div class="submitted">Submitted by <a href="http://dandascalescu.com" rel="nofollow">Dan Dascalescu</a> (not verified) on Fri, 2008-10-10 06:41.</div>
    <div class="content">
     <p>YAY! Kick Picasa's ass!</p>
         </div>
    <div class="links">» </div>
  </div>
</div><a id="comment-17867"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/378#comment-17867" class="active">Congratulation</a></h3>    <div class="submitted">Submitted by jacques (not verified) on Fri, 2008-10-10 18:57.</div>
    <div class="content">
     <p>Nice success.</p>
         </div>
    <div class="links">» </div>
  </div>
<a id="comment-17871"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/378#comment-17871" class="active">I did it</a></h3>    <div class="submitted">Submitted by Highelf (not verified) on Tue, 2008-10-14 07:19.</div>
    <div class="content">
     <p>Thanks to Patrick I was also able to run digikam on windows. So there are at least 2 running version of digikam on windows :)<br>
Highelf</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-17873"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/378#comment-17873" class="active">Could you please tell me how</a></h3>    <div class="submitted">Submitted by Anonymous (not verified) on Wed, 2008-10-15 04:06.</div>
    <div class="content">
     <p>Could you please tell me how to compile it in windows. I am not able to find the correct link to the application and it's dependencies.</p>
<p>Thanks,</p>
         </div>
    <div class="links">» </div>
  </div>
<a id="comment-17874"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/378#comment-17874" class="active">Any chance of...</a></h3>    <div class="submitted">Submitted by Anonymous (not verified) on Wed, 2008-10-15 21:43.</div>
    <div class="content">
     <p>...a binary Windows release???</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-17876"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/378#comment-17876" class="active">Yes Please a Binary release</a></h3>    <div class="submitted">Submitted by Anonymous (not verified) on Thu, 2008-10-16 13:30.</div>
    <div class="content">
     <p>Yes Please a Binary release for windows...</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-17877"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/378#comment-17877" class="active">Compiling/installing digikam on Windows</a></h3>    <div class="submitted">Submitted by <a href="http://windows.kde.org" rel="nofollow">SaroEngels</a> (not verified) on Thu, 2008-10-16 18:36.</div>
    <div class="content">
     <p>There will be a binary release as soon as I could fix the most notable bugs in it (it is currently not really useable on windows). Then I will upload the package for all compilers to the KDE mirrors, so that digikam can be installed as a normal KDE on Windows package ( see http://techbase.kde.org/Projects/KDE_on_Windows/Installation how to install KDE on Windows).<br>
To build digikam from source we normally use some scripts called emerge http://techbase.kde.org/Getting_Started/Build/KDE4/Windows/emerge . If you follow the instructions there, you should be able to build digikam, nevertheless you might hit some problems underway(+it needs a lot of space and time), I can't guarantee that it works.</p>
<p>regards,</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-17879"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/378#comment-17879" class="active">Thanks for the update. If not</a></h3>    <div class="submitted">Submitted by Anonymous (not verified) on Fri, 2008-10-17 02:53.</div>
    <div class="content">
     <p>Thanks for the update. If not much, if we can convert RAW to DNG on windows that would be helpful.</p>
         </div>
    <div class="links">» </div>
  </div>
<a id="comment-17891"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/378#comment-17891" class="active">Any timeline on this, Pl</a></h3>    <div class="submitted">Submitted by Anonymous (not verified) on Mon, 2008-10-27 02:35.</div>
    <div class="content">
     <p>Do you have any date,when the windows version will be ready? Also, will it support LX3 raw(rw2) files?</p>
<p>Thanks,</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-17893"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/378#comment-17893" class="active">Digikam binaries</a></h3>    <div class="submitted">Submitted by <a href="http://windows.kde.org" rel="nofollow">SaroEngels</a> (not verified) on Tue, 2008-10-28 09:34.</div>
    <div class="content">
     <p>Ok, as of yesterday, the first digikam binary packages are available in the installer, I guess most features will not work correctly though so please file bugs under bugs.kde.org so we can find those bugs soon.</p>

<p>Look how to install KDE binary packages <a href="http://techbase.kde.org/Projects/KDE_on_Windows/Installation">at this url</a></p>         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-17894"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/378#comment-17894" class="active">MarbleWidget.dll</a></h3>    <div class="submitted">Submitted by Anonymous (not verified) on Tue, 2008-10-28 17:46.</div>
    <div class="content">
     <p>I installed it on WindowsXP. Couples of things<br>
- It starts looking for all the files in C:\ and checks to see if they are images.<br>
- Then it failed in MarbleWidget.dll, Could not locate. I tried copying one from someplace else but I guess the version did not match and it did not go any further - just crashed.</p>
<p>Any idea how to get it.</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-17895"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/378#comment-17895" class="active">Convert to DNG</a></h3>    <div class="submitted">Submitted by Anonymous (not verified) on Tue, 2008-10-28 20:26.</div>
    <div class="content">
     <p>Hi,<br>
Do you know how can I convert the RAW files in to DNG. I am not able to find the options for that.</p>
<p>Do I need to install any other package.</p>
<p>BTW, it runs fine, I have not seen any crashes yet after the widget issue got fixed.<br>
Thanks,</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-17896"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/378#comment-17896" class="active">I have just set a rule to</a></h3>    <div class="submitted">Submitted by digiKam on Tue, 2008-10-28 21:02.</div>
    <div class="content">
     <p>I have just set a rule to make a stand alone version of DNG converter plugin. It can be run outside digiKam as well. Program is called "dngconverter.exe" and installed into your KDE4 binary directory (as digiKam).</p>
<p>But you need to wait next beta release to see this program in your computer. My changes have be done just after kipi-plugins 0.2.0-beta3 release used in KDE4 for windows.</p>
<p>In all case, RAW to DNG converter tool is available in Batch menu from digiKam main interface.</p>
<p>digiKam</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-17898"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/378#comment-17898" class="active">panasonic lx3 raw files not working</a></h3>    <div class="submitted">Submitted by Anonymous (not verified) on Wed, 2008-10-29 14:17.</div>
    <div class="content">
     <p>Latest digikam is not able to view the lx3 rw2 files. It is able to get the thumbnail but the in the image editor it shows purple lines only. Some sample files are :<br>
http://www.photographyblog.com/reviews_panasonic_lumix_dmc_lx3_3.php </p>
<p>On the issue of DNGconverter, thanks for the update. Looking forward to it, it will be unique in itself.</p>
<p>Any idea if it will support lx 3?</p>
<p>Thanks,</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-17899"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/378#comment-17899" class="active">RAW format are decoded using</a></h3>    <div class="submitted">Submitted by digiKam on Wed, 2008-10-29 14:24.</div>
    <div class="content">
     <p>RAW format are decoded using <a href="http://www.libraw.org">LibRaw library</a>. LX3 is already supported currently...</p>

<p>LibRaw is used in 0.9.5 implementation (for KDE3, not yet released, beta1 planed soon) and 0.10.0-beta5 (KDE4) released today.</p>


         </div>
    <div class="links">» </div>
  </div>
<a id="comment-17987"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/378#comment-17987" class="active">I tried it too, and i have</a></h3>    <div class="submitted">Submitted by Anonymous (not verified) on Tue, 2008-11-18 12:44.</div>
    <div class="content">
     <p>I tried it too, and i have the same problem.<br>
I will be glad if you can give support for <a href="http://pxgp.net/panasonic">Panasonic</a> :)</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-18018"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/378#comment-18018" class="active">LibRaw needs to be updated</a></h3>    <div class="submitted">Submitted by Anonymous (not verified) on Mon, 2008-12-01 21:15.</div>
    <div class="content">
     <p>libraw mentions panasonic LX3 in its list of supported cameras but is not compiled with the latest dcraw.c-&gt;libraw_common.cpp converter.<br>
A compare of identify function in libraw and dcraw shows libraw is missing some 6 new cameras.</p>
<p>I can do it, if you know which conversion program do they use to generate the base file.<br>
Hopefully it will be updated soon.</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-18022"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/378#comment-18022" class="active">LibRaw update...</a></h3>    <div class="submitted">Submitted by digiKam on Wed, 2008-12-03 14:29.</div>
    <div class="content">
     <p>I have contacted Alex Tutubalin from LibRaw. He will update library soon...</p>
<p>Gilles</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-18029"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/378#comment-18029" class="active">LibRaw 0.6.2 is out</a></h3>    <div class="submitted">Submitted by digiKam on Fri, 2008-12-05 07:59.</div>
    <div class="content">
     <p>LibRaw 0.6.2 is out and i have backported code to libkdcraw for KDE3 and KDE4.</p>
<p>digiKam</p>
         </div>
    <div class="links">» </div>
  </div>
</div></div></div></div></div><a id="comment-17940"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/378#comment-17940" class="active">Stand alone DNG Converter tool under Windows...</a></h3>    <div class="submitted">Submitted by digiKam on Fri, 2008-11-07 13:38.</div>
    <div class="content">
     <p>See below a fresh screenshoot of a stand alone version of DNG Converter tool running under Windows.</p>

<a href="http://www.flickr.com/photos/digikam/3009595519/" title="dngconverter_standalone_win32 by digiKam team, on Flickr"><img src="http://farm4.static.flickr.com/3026/3009595519_9b745e1c05.jpg" width="500" height="315" alt="dngconverter_standalone_win32"></a>

<p>digiKam</p>         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-18017"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/378#comment-18017" class="active">Hi Gilles,
I am trying to</a></h3>    <div class="submitted">Submitted by Anonymous (not verified) on Mon, 2008-12-01 21:11.</div>
    <div class="content">
     <p>Hi Gilles,<br>
I am trying to write a simple stand alone converter in windows and I am trying to reuse your code. </p>
<p>I am having some problems. Could you please see if you can help me out.<br>
In DCRawInfoContainer you have cameraXYZ array of float[4][3]. This does not seems to be getting filled by LibKDCRaw. Do you know where does it get filled and how to map it to cam_xyz in adobe dng sdk.</p>
<p>Thanks</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-18030"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/378#comment-18030" class="active">stand alone DNG converter already exist...</a></h3>    <div class="submitted">Submitted by digiKam on Fri, 2008-12-05 08:03.</div>
    <div class="content">
     <p>Stand alone DNG converter already exist for Linux and Windows: see screenshot just behind this post. It's a version of kipi plugins compiled without a kipi host. You can use it as well...</p>
<p>cam_xyz matrix is filed by libraw accordingly with make and model Exif informations of original RAW image.</p>
<p>digiKam</p>
         </div>
    <div class="links">» </div>
  </div>
</div></div></div></div><a id="comment-17929"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/378#comment-17929" class="active">Was really psyched to finally</a></h3>    <div class="submitted">Submitted by GenoSV (not verified) on Sun, 2008-11-02 15:51.</div>
    <div class="content">
     <p>Was really psyched to finally be able to use digicam on Windows so installed it through the KDE for Windows Installer, although it fails to start reporting some error in libkdcraw.dll . My Windows is in swedish, but a translation would be something like: "Cannot find the procedure startaddress _ZN11([etcetc lots of random letters]) in the DLL-file libkdcraw".</p>
<p>Any fix for this?<br>
(running Windows Vista)</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-17941"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/378#comment-17941" class="active">I have the same problem</a></h3>    <div class="submitted">Submitted by loupy87 (not verified) on Fri, 2008-11-07 21:03.</div>
    <div class="content">
     <p>I have the same problem ...<br>
Windows XP</p>
         </div>
    <div class="links">» </div>
  </div>
</div><a id="comment-18011"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/378#comment-18011" class="active">I use digikam on Linux</a></h3>    <div class="submitted">Submitted by Anonymous (not verified) on Sun, 2008-11-30 14:35.</div>
    <div class="content">
     <p>I use digikam on Linux</p>
         </div>
    <div class="links">» </div>
  </div>
</div></div></div></div></div></div><a id="comment-17971"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/378#comment-17971" class="active">programming jobs</a></h3>    <div class="submitted">Submitted by Anonymous (not verified) on Sat, 2008-11-15 10:23.</div>
    <div class="content">
     <p>Do you realize that you are actually making programming jobs obsolete? I work on a similar product and I'm hoping to be able to make a living out of it, and there are many others like me. You are also empowering software monopolies such as Adobe, because it becomes impossible for smaller companies to stay in business with such competition. digiKam is really good, and it's free - how are we supposed to compete with that?<br>
Please keep digiKam Linux only!</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-17974"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/378#comment-17974" class="active">if you can't beat them, join them</a></h3>    <div class="submitted">Submitted by <a href="http://plouj.com/" rel="nofollow">Plouj</a> (not verified) on Sat, 2008-11-15 19:39.</div>
    <div class="content">
     <p>I think it would be a win-win situation if you or your company actually sponsors digikam by allotting developer time to this project and making a business out of providing support (bug fixing, adding requested features) and stable builds for the digikam software. It is my understanding that this is basically how a lot of "open source" companies (redhat, qt software, mysql, zimbra, etc) successfully operate today.</p>
         </div>
    <div class="links">» </div>
  </div>
<a id="comment-17975"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/378#comment-17975" class="active">Join digiKam. I am totally</a></h3>    <div class="submitted">Submitted by mikmach (not verified) on Sat, 2008-11-15 21:12.</div>
    <div class="content">
     <p>Join digiKam. I am totally serious. Due to pluggable architecture of KDE and digiKam itself it is possible to work on main program together and write some commercial/closed source plug-ins.</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-17981"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/378#comment-17981" class="active">Depends. According to</a></h3>    <div class="submitted">Submitted by Anonymous (not verified) on Sun, 2008-11-16 00:50.</div>
    <div class="content">
     <p>Depends. According to freshmeat, DigiKam is GPL, so you can't distribute any dynamically-linked plugins unless these are GPL-compatibel. This means everyone is allowed to redistribute your code at any price including zero.</p>
<p>Makes no sense to start a business on such a foundation.</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-17982"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/378#comment-17982" class="active">No, it's wrong...</a></h3>    <div class="submitted">Submitted by digiKam on Sun, 2008-11-16 11:31.</div>
    <div class="content">
     <p>a plugins is not linked with digiKam, but with libkipi shared library which is the plugins interface used by host program as digiKam, Gwenview, and KphotoAlbum.</p>
<p>libkipi is currently in GPL, but this can be changed easily as LGPL. it's not a problem...</p>
<p>digiKam</p>
         </div>
    <div class="links">» </div>
  </div>
<a id="comment-17983"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/378#comment-17983" class="active">You would have to link only</a></h3>    <div class="submitted">Submitted by mikmach (not verified) on Sun, 2008-11-16 13:38.</div>
    <div class="content">
     <p>You would have to link only to KIPI plugins. Yes, they are now GPL but it should not be great problem to relicense them. It is tedious task but if someone would be really interested...</p>
<p>Also there are some other areas where it is possible to extend digiKam by closed source thingies.</p>
         </div>
    <div class="links">» </div>
  </div>
</div></div><a id="comment-17977"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/378#comment-17977" class="active">Which compagny ? which photo management program under Windows ?</a></h3>    <div class="submitted">Submitted by digiKam on Sat, 2008-11-15 21:30.</div>
    <div class="content">
     <p>Personally, i'm curious to know which photo-management program for Windows you develop and sell?</p>
<p>Also, can you imagine that after many years of work on digiKam/kipi-plugins projects, and especially a lots of time to port code under Windows and MAC OS-X, i will put all in a trash ? I'm not stupid... this is not serious...</p>
<p>digiKam</p>
         </div>
    <div class="links">» </div>
  </div>
<a id="comment-17978"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/378#comment-17978" class="active">"I work on a similar product</a></h3>    <div class="submitted">Submitted by gwenhwyfaer (not verified) on Sat, 2008-11-15 23:02.</div>
    <div class="content">
     <p>"<em>I work on a similar product and I'm hoping to be able to make a living out of it, and there are many others like me</em>"</p>
<p>In the words of Steve Jobs, "...yeah. Don't do that."</p>
         </div>
    <div class="links">» </div>
  </div>
</div><a id="comment-17993"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/378#comment-17993" class="active">when?</a></h3>    <div class="submitted">Submitted by <a href="http://rob71.wordpress.com" rel="nofollow">rob moretti</a> (not verified) on Fri, 2008-11-21 21:37.</div>
    <div class="content">
     <p>Hi!When the definetivy realise for windows? i'm waiting... i want digikam!!!</p>
         </div>
    <div class="links">» </div>
  </div>

</div>
