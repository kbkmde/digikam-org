---
date: "2008-06-28T21:11:00Z"
title: "Preparing first 0.10.0-beta1 for KDE4..."
author: "digiKam"
description: "There is one year, Marcel and me we have started to port digiKam code to Qt4/KDE4. Yes, already one year of work...Tempus fugit... Helped by"
category: "news"
aliases: "/node/353"

---

<p>There is one year, Marcel and me we have started to port digiKam code to Qt4/KDE4. Yes, already one year of work...<b>Tempus fugit</b>...</p>

<a href="http://www.flickr.com/photos/digikam/2603074859/" title="0.10.0-albumgui04 by digiKam team, on Flickr"><img src="http://farm4.static.flickr.com/3250/2603074859_c42ab758e9.jpg" width="500" height="382" alt="0.10.0-albumgui04"></a>

<p>Helped by Laurent Montel, code have been ported progresivly. This have take a while because digiKam code is huge and API changes are enormous. This is a complex task to do it : porting to new api, make regression tests, fix new bugs, an validate new code... Also, when bugs come from KDE4 or Qt4 shared libs, the puzzle become infernal to solve. Laurent, as KDE core developper, has reported all problems discovered with digiKam port. He have make an important role during this time</p>

<p>digiKam code is not alone to be ported. We have played with all shared librairies code used by digiKam (libkipi, libkexiv2, and libkdcraw). Unforget too, kipi-plugins which are not yet fully ported.</p>

<a href="http://www.flickr.com/photos/digikam/2608034196/" title="0.10.0-kipi-plugins08 by digiKam team, on Flickr"><img src="http://farm4.static.flickr.com/3259/2608034196_e46a7183a9.jpg" width="500" height="382" alt="0.10.0-kipi-plugins08"></a>

<p>This is why, the KDE4 port is long and not simple. And i can said that i'm not yet fully satisfied by this job. Not all regression tests have be done, and it still know bugs to fix before to plan a final release.</p>

<p>Also, a lots of new features have been implemented with KDE4 port: XMP support, new Database schema/interface, a lots of new search tools (fuzzy, duplicates, geolocation, metadata, etc...), sidebar integration everywhere, multiple roots albums, network transparency, etc... New features are nice, but introduce new bugs of course.</p>

<p>But, we cannot delay to infine. With Marcel, we have decided to prepare the first beta1 release. All main new features have been implemented. it will be time to toogle in a beta cycle to polish and finalize code everywhere... before the end of this year. It will be a long stage, but it's necessary. I won't to publish a buggy "stable" release...</p>

<p>This is why i recommend to all people who will test this new release, to run digiKam outside a production environnement. Use a dedicated repository of photos to not take a risk to lost something.
To resume, <b>do not use yet digiKam for KDE4 in production</b>, until the final release.</p>

<p>digiKam 0.10.0-beta1 is planed for next week end. It will be an exiting moment for me, and for all digiKam users i hope...</p>

<a href="http://www.flickr.com/photos/digikam/2607928751/" title="0.10.0-imageeditor06 by digiKam team, on Flickr"><img src="http://farm4.static.flickr.com/3005/2607928751_c5a85dd112.jpg" width="500" height="400" alt="0.10.0-imageeditor06"></a>

<div class="legacy-comments">

  <a id="comment-17601"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/353#comment-17601" class="active">Digikam 0.10.0 from svn</a></h3>    <div class="submitted">Submitted by Francesco Battaglini (not verified) on Sat, 2008-06-28 22:30.</div>
    <div class="content">
     <p>Digikam 0.10.0 from svn doesn't want to compile on my opensuse 11.0 since April. I've met all the dipendencies and I don't understand what's wrong. Make simply stop without errors at a certain point. I really love digikam and I'd like to help finding bugs but I can't get it working.</p>
         </div>
    <div class="links">» </div>
  </div>
<a id="comment-17603"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/353#comment-17603" class="active">It will be worth the wait.</a></h3>    <div class="submitted">Submitted by Louis (not verified) on Sun, 2008-06-29 02:31.</div>
    <div class="content">
     <p>The current version works great within KDE4, so I haven't really missed it.  I do look forward to the new version though, and will probably test it.  Digikam is an awesome program, and it just keeps getting better.  Thank you guys for all your hard work.</p>
         </div>
    <div class="links">» </div>
  </div>
<a id="comment-17606"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/353#comment-17606" class="active">Congratulation in advance for</a></h3>    <div class="submitted">Submitted by <a href="http://ifiction.free.fr/" rel="nofollow">Farvardin</a> (not verified) on Sun, 2008-06-29 19:11.</div>
    <div class="content">
     <p>Congratulation in advance for this amazing work!<br>
Digikam is one of my favorite and most useful app. and I'm looking toward the kde4 integration, and the windows version as well so I could start recommend it for windows users...</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-17607"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/353#comment-17607" class="active">The windows port is not yet</a></h3>    <div class="submitted">Submitted by digiKam on Sun, 2008-06-29 21:08.</div>
    <div class="content">
     <p>The windows port is not yet validate. Code is certainly ready to compile under Windows, but we haven't yet tested. Hacking Linux version is the priority.</p>
<p>The code will need certainly to be polished under Windows. Also, few rules need to be done with CMake to disable some parts which cannot be used under Windows, especially GPhoto2 interface. We have take a care to use standard C++ header everywhere when it's possible for non Qt4/KDE4 code. </p>
<p>All the rest must compile as well: libkipi, libkexiv2, and libkdcraw are already ported to windows (code is pure Qt4) and maintened and tested by KDE core developpers who works on the whole KDE4 Windows port.</p>
<p>Others depencies must be fine : LensFun, Exiv2, libtiff, libpng, libJpeg, littleCMS, etc... are fully portable.</p>
<p>digiKam</p>
         </div>
    <div class="links">» </div>
  </div>
</div><a id="comment-17610"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/353#comment-17610" class="active">Geotagging on RAW? </a></h3>    <div class="submitted">Submitted by <a href="http://www.cinemasie.com" rel="nofollow">François Tissandier</a> (not verified) on Mon, 2008-06-30 08:40.</div>
    <div class="content">
     <p>Please please please tell me that it's possible to geotag RAW files :) This would really be a great improvement for Digikam. Currently I'm not using Digikam because of this and the lack of XMP support. So it's quite frustrating, because the functionalities are really impressive! The implementation of geolocalization is really nice, I hope I can use it in KDE4. </p>
<p>And keep up the good work!</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-17611"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/353#comment-17611" class="active">Hello François,
I have a</a></h3>    <div class="submitted">Submitted by Anonymous (not verified) on Mon, 2008-06-30 10:56.</div>
    <div class="content">
     <p>Hello François,</p>
<p>I have a similar issue, so I create XMP files for each of my raw and use a tool called geotag (http://geotag.sourceforge.net). Set the preferences in it to always write to XMP files. Hopefully in 10.0 it will keep the XMP files with the raw when copying/moving etc.</p>
<p>-- Stephen.</p>
         </div>
    <div class="links">» </div>
  </div>
<a id="comment-17612"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/353#comment-17612" class="active">Yes RAW can be geotagged,</a></h3>    <div class="submitted">Submitted by digiKam on Mon, 2008-06-30 14:06.</div>
    <div class="content">
     <p>Yes RAW can be geotagged, since the new database schema host all GPS info. Of course, RAW file metadata still untouched: RAW files still read only. This is why geolocation are stored in database (to be able to perform search over a map).</p>
<p>digiKam</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-17613"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/353#comment-17613" class="active">Thanks for the details</a></h3>    <div class="submitted">Submitted by <a href="http://www.cinemasie.com" rel="nofollow">François Tissandier</a> (not verified) on Mon, 2008-06-30 14:39.</div>
    <div class="content">
     <p>Thanks Gilles</p>
<p>Are there any particular reasons why it could not be written to RAW files? I know most of them are proprietary formats, but there are tools to write the geotags to most RAWs, and it seems to be working fine. It's just too heavy for me (command line tools), especially as I locate the photo from a map, my camera doesn't record its position.</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-17614"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/353#comment-17614" class="active">digiKam use Exiv2 shared</a></h3>    <div class="submitted">Submitted by digiKam on Mon, 2008-06-30 14:45.</div>
    <div class="content">
     <p>digiKam use Exiv2 shared library to play with metadata. currently only JPEG file metadata can be written on the fly.</p>
<p>In current Exiv2 implementation, TIFF/EP writting mode is under finalization. All main RAW file formats as NEF for example are based on TIFF/EP. We need time to check if nothing is broken with RAW format using TIFF/EP writting mode before to use it.</p>
<p>digiKam</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-17615"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/353#comment-17615" class="active">Thanks!</a></h3>    <div class="submitted">Submitted by <a href="http://www.cinemasie.com" rel="nofollow">François Tissandier</a> (not verified) on Mon, 2008-06-30 15:06.</div>
    <div class="content">
     <p>Ok, I hope it will be validated soon :) Thanks for your answer.</p>
         </div>
    <div class="links">» </div>
  </div>
</div></div></div></div><a id="comment-17616"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/353#comment-17616" class="active">Hi,
Thank you for the update!</a></h3>    <div class="submitted">Submitted by Erik (not verified) on Mon, 2008-06-30 19:57.</div>
    <div class="content">
     <p>Hi,<br>
Thank you for the update! I managed to build digikam svn, but I had to copy a few .pc files for kipi, kdcraw and exiv2 from /usr/kde/svn/lib/pkgconfig/ to /usr/lib/pkgconfig/ to make cmake detect those libraries that come with kdegraphics these days. Digikam for KDE4 looks very nice and already works good I have to say! Good work!</p>
<p>Unfortunately I have not been able to build kipi-plugins from svn. The error I get is:</p>
<p>CMake Error: Error in cmake code at<br>
/root/graphics/kipi-plugins/CMakeLists.txt:27:<br>
INCLUDE Could not find include file: FindPackageHandleStandardArgs<br>
Current CMake stack:<br>
[1]     /root/graphics/kipi-plugins/CMakeLists.txt<br>
CMake Error: Kexiv2_DIR is not set.  It must be set to the directory containing Kexiv2Config.cmake in order to use Kexiv2.</p>
<p>I am far from a cmake guru, so could you give me a hint on how to get it to work?</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-17617"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/353#comment-17617" class="active">FindPackageHandleStandardArgs</a></h3>    <div class="submitted">Submitted by digiKam on Mon, 2008-06-30 21:20.</div>
    <div class="content">
     <p>FindPackageHandleStandardArgs is a CMake macro defined in KDELibs collections. Check your KDE4 developement packages. Here it compile fine using default Mandriva 2008.1 KDE 4.0.3 RPMs</p>
<p>digiKam</p>
         </div>
    <div class="links">» </div>
  </div>
</div>
</div>
