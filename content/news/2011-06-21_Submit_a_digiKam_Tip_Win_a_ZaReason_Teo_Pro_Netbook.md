---
date: "2011-06-21T09:54:00Z"
title: "Submit a digiKam Tip, Win a ZaReason Teo Pro Netbook"
author: "Dmitri Popov"
description: "WorldLabel has kindly agreed to sponsor a competition for the best digiKam tip, where the winner will bag a cool Ubuntu-based Teo Pro netbook from"
category: "news"
aliases: "/node/608"

---

<p><img src="http://zareason.com/shop/images/P/silver_left%20300x300.png" alt="Teo Pro netbook" width="150" height="150"></p>

<a href="http://www.worldlabel.com/">WorldLabel</a> has kindly agreed to sponsor a competition for the best digiKam tip, where the winner will bag a cool <a href="http://zareason.com/shop/Teo-Pro-Netbook.html">Ubuntu-based Teo Pro netbook</a> from ZaReason.

<a href="http://scribblesandsnaps.wordpress.com/2011/06/21/submit-a-digikam-tip-win-a-zareason-teo-pro-netbook/">Continue to read</a>
<div class="legacy-comments">

</div>