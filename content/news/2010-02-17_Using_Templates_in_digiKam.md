---
date: "2010-02-17T00:20:00Z"
title: "Using Templates in digiKam"
author: "Dmitri Popov"
description: "You will be forgiven for thinking that copyrighting photos belongs to the realm of the professional photographer. Even if you consider yourself an amateur, providing"
category: "news"
aliases: "/node/502"

---

<a href="http://www.flickr.com/photos/digikam/4367034307/" title="template by digiKam team, on Flickr"><img src="http://farm5.static.flickr.com/4033/4367034307_64ed1227dd.jpg" width="500" height="400" alt="template"></a>

<p>You will be forgiven for thinking that copyrighting photos belongs to the realm of the professional photographer. Even if you consider yourself an amateur, providing copyright and contact information for your photos is a prudent thing to do. Embedding copyright info into each photo may sound like a daunting proposition, but digiKam provides a nifty template feature which lets you create copyright templates and apply them to multiple photos in one go.</p>

<a href="http://scribblesandsnaps.wordpress.com/2010/02/17/using-templates-in-digikam/">Continue to read...</a>
<div class="legacy-comments">

  <a id="comment-19072"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/502#comment-19072" class="active">Great article, I have added</a></h3>    <div class="submitted">Submitted by <a href="http://australiacheapflights.net" rel="nofollow">flights to australia</a> (not verified) on Sun, 2010-03-14 09:56.</div>
    <div class="content">
     <p>Great article, I have added your blog to my favourites I really like it!</p>
<p>Regards,<br>
Andy</p>
         </div>
    <div class="links">» </div>
  </div>

</div>
