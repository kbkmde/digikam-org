---
date: "2005-08-19T20:53:00Z"
title: "digiKam podcast"
author: "site-admin"
description: "ThePodcastNetwork has made a great podcast, which is a review / tutorial of digiKam. It is an one hour audio file which guides you through"
category: "news"
aliases: "/node/34"

---

<p>ThePodcastNetwork has made a great <a href="http://www.thepodcastnetwork.com/linuxuser/2005/08/15/the-gnulinux-user-show-11/">podcast</a>, which is a review / tutorial of digiKam. It is an one hour audio file which guides you through the basics of digiKam and lets you hear how a new user experiences digiKam for the first time.</p>

<div class="legacy-comments">

</div>