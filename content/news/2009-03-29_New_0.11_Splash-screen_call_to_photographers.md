---
date: "2009-03-29T12:37:00Z"
title: "New 0.11 Splash-screen: call to photographers !"
author: "digiKam"
description: "Yesterday, Marcel has moved digiKam 0.11 source code from a development branch to trunk in KDE subversion repository. 0.11 include now the famous Batch Queue"
category: "news"
aliases: "/node/437"

---

<a href="http://websvn.kde.org/*checkout*/trunk/extragear/graphics/digikam/data/pics/splash-digikam.png?revision=945625" title="digiKam 0.10.0 splash"><img src="http://websvn.kde.org/*checkout*/trunk/extragear/graphics/digikam/data/pics/splash-digikam.png?revision=945625" width="500" height="307" alt="digiKam 0.10.0 splash"></a>

<p>Yesterday, Marcel has moved digiKam 0.11 source code from a development branch to trunk in KDE subversion repository. 0.11 include now the famous <a href="http://www.digikam.org/node/427">Batch Queue Manager</a>, and it will be ready to test soon in production...</p>

<p>So, it's time to start next 0.11 release plan... and of course the time has come to find the ideal splash-screens to go with it. Now is your chance to join the ranks of the precious few who have had their artwork associated with a release of digiKam!</p> 

<p>Splash-screen is a simple way for users to contribute to digiKam project. The pictures must be correctly exposed and composed, and the subject must be chosen using a real photographer inspiration.</p> 

<a href="http://websvn.kde.org/*checkout*/trunk/extragear/graphics/digikam/data/pics/splash-showfoto.png?revision=945625" title="Showfoto 0.10.0 splash"><img src="http://websvn.kde.org/*checkout*/trunk/extragear/graphics/digikam/data/pics/splash-showfoto.png?revision=945625" width="500" height="307" alt="Showfoto 0.10.0 splash"></a>

<p>For this release, we need 2 new splash-screens dedicated to digiKam and Showfoto startup. You can send me <a href="http://www.digikam.org/?q=contact">by e-mail</a> your Color or Black and White pictures, taken horizontally. We need only photos, no need to put few marks about program name and version. We have a template to do it later. I will review the best items with the team later...</p> 

<p>As reference, see behind the official digiKam and Showfoto 0.10 splash-screens.</p>

<p>Have fun to contribute to digiKam project... and thanks in advance...</p>
<div class="legacy-comments">

  <a id="comment-18409"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/437#comment-18409" class="active">Hi, i know this isn't a</a></h3>    <div class="submitted">Submitted by mofux (not verified) on Sun, 2009-03-29 16:38.</div>
    <div class="content">
     <p>Hi, i know this isn't a photograph, but probably interesting as well...</p>

<p>A vector graphic i've done some time ago, probably you like it:</p>

<a href="http://www.pic-upload.de/view-1767823/digikam3.png.html" title="digiKam vector graphic splash"><img src="
http://www.pic-upload.de/29.03.09/brtaw.png" width="410" height="325" alt="digiKam vector graphic splash"></a>

         </div>
    <div class="links">» </div>
  </div>
<a id="comment-18410"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/437#comment-18410" class="active">Update: list of current candidates sent by contributors</a></h3>    <div class="submitted">Submitted by digiKam on Sun, 2009-03-29 20:08.</div>
    <div class="content">
     <a href="http://www.flickr.com/photos/digikam/3394839655/" title="IMG_2603_DxO_raw by digiKam team, on Flickr"><img src="http://farm4.static.flickr.com/3555/3394839655_b5e1107b57_t.jpg" width="100" height="67" alt="IMG_2603_DxO_raw"></a>

<a href="http://www.flickr.com/photos/digikam/3395891082/" title="CIMG0406 by digiKam team, on Flickr"><img src="http://farm4.static.flickr.com/3573/3395891082_7419b2836b_t.jpg" width="100" height="67" alt="CIMG0406"></a>

<a href="http://www.flickr.com/photos/digikam/3395216755/" title="hkong by digiKam team, on Flickr"><img src="http://farm4.static.flickr.com/3454/3395216755_29d83e9d31_t.jpg" width="100" height="67" alt="hkong"></a>

<a href="http://www.flickr.com/photos/digikam/3397424103/" title="lagon by digiKam team, on Flickr"><img src="http://farm4.static.flickr.com/3618/3397424103_d41c6df2c0_t.jpg" width="100" height="67" alt="lagon"></a>

<a href="http://www.flickr.com/photos/digikam/3397653703/" title="IMG_3972 by digiKam team, on Flickr"><img src="http://farm4.static.flickr.com/3441/3397653703_09edba68ce_t.jpg" width="100" height="67" alt="IMG_3972"></a>

<a href="http://www.flickr.com/photos/digikam/3398463534/" title="IMG_5716 by digiKam team, on Flickr"><img src="http://farm4.static.flickr.com/3598/3398463534_15192d38db_t.jpg" width="100" height="67" alt="IMG_5716"></a>

<a href="http://www.flickr.com/photos/digikam/3397653905/" title="IMG_5718 by digiKam team, on Flickr"><img src="http://farm4.static.flickr.com/3649/3397653905_d1bc44f819_t.jpg" width="100" height="67" alt="IMG_5718"></a>

<a href="http://www.flickr.com/photos/digikam/3398463766/" title="IMG_5750 by digiKam team, on Flickr"><img src="http://farm4.static.flickr.com/3556/3398463766_c526431089_t.jpg" width="100" height="67" alt="IMG_5750"></a>

<a href="http://www.flickr.com/photos/digikam/3399200841/" title="rudebekia by digiKam team, on Flickr"><img src="http://farm4.static.flickr.com/3626/3399200841_f509efdcd2_t.jpg" width="100" height="67" alt="rudebekia"></a>

<a href="http://www.flickr.com/photos/digikam/3399199587/" title="chevaux by digiKam team, on Flickr"><img src="http://farm4.static.flickr.com/3658/3399199587_5625673fd0_t.jpg" width="100" height="67" alt="chevaux"></a>

<a href="http://www.flickr.com/photos/digikam/3400037584/" title="WR_20081102_E5_b02_2093_COPY by digiKam team, on Flickr"><img src="http://farm4.static.flickr.com/3613/3400037584_90ac7e0d24_t.jpg" width="100" height="67" alt="WR_20081102_E5_b02_2093_COPY"></a>

<a href="http://www.flickr.com/photos/digikam/3400037784/" title="WR_20071104_LX_103_0875_COPY by digiKam team, on Flickr"><img src="http://farm4.static.flickr.com/3634/3400037784_c0ff77bb5e_t.jpg" width="100" height="67" alt="WR_20071104_LX_103_0875_COPY"></a>

<a href="http://www.flickr.com/photos/digikam/3399229519/" title="WR_20061007_LX_101_0017_COPY by digiKam team, on Flickr"><img src="http://farm4.static.flickr.com/3579/3399229519_f075356cd0_t.jpg" width="100" height="67" alt="WR_20061007_LX_101_0017_COPY"></a>

<a href="http://www.flickr.com/photos/digikam/3400920189/" title="digkam_splash_kanwar_plaha_1024 by digiKam team, on Flickr"><img src="http://farm4.static.flickr.com/3444/3400920189_221c1443dc_t.jpg" width="100" height="67" alt="digkam_splash_kanwar_plaha_1024"></a>

<a href="http://www.flickr.com/photos/digikam/3400949999/" title="showfoto_splash_Kanwar_Plaha by digiKam team, on Flickr"><img src="http://farm4.static.flickr.com/3637/3400949999_f4242d6f98_t.jpg" width="100" height="67" alt="showfoto_splash_Kanwar_Plaha"></a>

<a href="http://www.flickr.com/photos/digikam/3401775414/" title="img_2598 by digiKam team, on Flickr"><img src="http://farm4.static.flickr.com/3538/3401775414_56ded51fdb_t.jpg" width="100" height="67" alt="img_2598"></a>

<a href="http://www.flickr.com/photos/digikam/3403716042/" title="goeland by digiKam team, on Flickr"><img src="http://farm4.static.flickr.com/3541/3403716042_5157c46789_t.jpg" width="100" height="67" alt="goeland"></a>

<a href="http://www.flickr.com/photos/digikam/3404065573/" title="3236407312_7830cc8ba8_b by digiKam team, on Flickr"><img src="http://farm4.static.flickr.com/3587/3404065573_7f47c0183c_t.jpg" width="100" height="67" alt="3236407312_7830cc8ba8_b"></a>

<a href="http://www.flickr.com/photos/digikam/3404876834/" title="2425312074_344738359f_b by digiKam team, on Flickr"><img src="http://farm4.static.flickr.com/3602/3404876834_e052d62bb6_t.jpg" width="100" height="67" alt="2425312074_344738359f_b"></a>

<a href="http://www.flickr.com/photos/digikam/3406364858/" title="pc119328 by digiKam team, on Flickr"><img src="http://farm4.static.flickr.com/3614/3406364858_83636a9572_t.jpg" width="100" height="67" alt="pc119328"></a>

<a href="http://www.flickr.com/photos/digikam/3406364500/" title="pb017588 by digiKam team, on Flickr"><img src="http://farm4.static.flickr.com/3456/3406364500_eebbab04a8_t.jpg" width="100" height="67" alt="pb017588"></a>

<a href="http://www.flickr.com/photos/digikam/3406364120/" title="p8204737 by digiKam team, on Flickr"><img src="http://farm4.static.flickr.com/3554/3406364120_ed38b74428_t.jpg" width="100" height="67" alt="p8204737"></a>

<a href="http://www.flickr.com/photos/digikam/3405552009/" title="p8104501 by digiKam team, on Flickr"><img src="http://farm4.static.flickr.com/3472/3405552009_7c4effc0bc_t.jpg" width="100" height="67" alt="p8104501"></a>

<a href="http://www.flickr.com/photos/digikam/3405551557/" title="p2230528 by digiKam team, on Flickr"><img src="http://farm4.static.flickr.com/3645/3405551557_9c05c20536_t.jpg" width="100" height="67" alt="p2230528"></a>

<a href="http://www.flickr.com/photos/digikam/3405551129/" title="IMG_1151 by digiKam team, on Flickr"><img src="http://farm4.static.flickr.com/3557/3405551129_254191685a_t.jpg" width="100" height="67" alt="IMG_1151"></a>

<a href="http://www.flickr.com/photos/digikam/3406990975/" title="The_Alps by digiKam team, on Flickr"><img src="http://farm4.static.flickr.com/3357/3406990975_aa048ef357_t.jpg" width="100" height="67" alt="The_Alps"></a>

<a href="http://www.flickr.com/photos/digikam/3406990825/" title="Lake_EveningSun by digiKam team, on Flickr"><img src="http://farm4.static.flickr.com/3537/3406990825_1da1ce4e91_t.jpg" width="100" height="67" alt="Lake_EveningSun"></a>

<a href="http://www.flickr.com/photos/digikam/3410753589/" title="_MG_4585 by digiKam team, on Flickr"><img src="http://farm4.static.flickr.com/3372/3410753589_f2d0962f1e_t.jpg" width="100" height="67" alt="_MG_4585"></a>

<a href="http://www.flickr.com/photos/digikam/3411573374/" title="_MG_6745.CR2 by digiKam team, on Flickr"><img src="http://farm4.static.flickr.com/3655/3411573374_e17a3742d6_t.jpg" width="100" height="67" alt="_MG_6745.CR2"></a>

<a href="http://www.flickr.com/photos/digikam/3410834507/" title="_MG_7787 by digiKam team, on Flickr"><img src="http://farm4.static.flickr.com/3578/3410834507_9fc13c7596_t.jpg" width="100" height="67" alt="_MG_7787"></a>

<a href="http://www.flickr.com/photos/digikam/3413865979/" title="digikam_Sebastian by digiKam team, on Flickr"><img src="http://farm4.static.flickr.com/3160/3413865979_3bf2d0617d_t.jpg" width="100" height="67" alt="digikam_Sebastian"></a>

<a href="http://www.flickr.com/photos/digikam/3413882411/" title="Schottland_Skye_Tour_West__Neist_Point_2008 by digiKam team, on Flickr"><img src="http://farm4.static.flickr.com/3580/3413882411_23bc8d8413_t.jpg" width="100" height="67" alt="Schottland_Skye_Tour_West__Neist_Point_2008"></a>

<a href="http://www.flickr.com/photos/digikam/3415003617/" title="IMG_1273_coloured_1 by digiKam team, on Flickr"><img src="http://farm4.static.flickr.com/3343/3415003617_bca44b4d8f_t.jpg" width="100" height="67" alt="IMG_1273_coloured_1"></a>

<a href="http://www.flickr.com/photos/digikam/3415809870/" title="IMG_1282_partly_coloured_1 by digiKam team, on Flickr"><img src="http://farm4.static.flickr.com/3359/3415809870_3ba5d9f2d0_t.jpg" width="100" height="67" alt="IMG_1282_partly_coloured_1"></a>

<a href="http://www.flickr.com/photos/digikam/3417916600/" title="dscf2955 by digiKam team, on Flickr"><img src="http://farm4.static.flickr.com/3550/3417916600_2dce422cfb_t.jpg" width="100" height="67" alt="dscf2955"></a>

<a href="http://www.flickr.com/photos/digikam/3417938744/" title="dscf2255 by digiKam team, on Flickr"><img src="http://farm4.static.flickr.com/3349/3417938744_3c1f3d99c4_t.jpg" width="100" height="67" alt="dscf2255"></a>

<a href="http://www.flickr.com/photos/digikam/3420537867/" title="p8120257 by digiKam team, on Flickr"><img src="http://farm4.static.flickr.com/3646/3420537867_ba09472462_t.jpg" width="100" height="67" alt="p8120257"></a>

<a href="http://www.flickr.com/photos/digikam/3420537505/" title="p8050181 by digiKam team, on Flickr"><img src="http://farm4.static.flickr.com/3544/3420537505_7597e4b094_t.jpg" width="100" height="75" alt="p8050181"></a>

<a href="http://www.flickr.com/photos/digikam/3422247898/" title="IMG_4998 by digiKam team, on Flickr"><img src="http://farm4.static.flickr.com/3657/3422247898_2bf4e7dd4e_t.jpg" width="100" height="67" alt="IMG_4998"></a>

<a href="http://www.flickr.com/photos/digikam/3423148451/" title="2909908887_c58e0738e1_o by digiKam team, on Flickr"><img src="http://farm4.static.flickr.com/3660/3423148451_8071f03a2d_t.jpg" width="100" height="67" alt="2909908887_c58e0738e1_o"></a>

<a href="http://www.flickr.com/photos/digikam/3423959084/" title="2985655159_350316c6a5_b by digiKam team, on Flickr"><img src="http://farm4.static.flickr.com/3311/3423959084_838a47d7e6_t.jpg" width="100" height="67" alt="2985655159_350316c6a5_b"></a>

<a href="http://www.flickr.com/photos/digikam/3423992996/" title="r0013787_landscape by digiKam team, on Flickr"><img src="http://farm4.static.flickr.com/3109/3423992996_160a424f23_t.jpg" width="100" height="67" alt="r0013787_landscape"></a>

<a href="http://www.flickr.com/photos/digikam/3425029806/" title="rla_7516b by digiKam team, on Flickr"><img src="http://farm4.static.flickr.com/3633/3425029806_64c2ca4c62_t.jpg" width="100" height="67" alt="rla_7516b"></a>

<a href="http://www.flickr.com/photos/digikam/3426182930/" title="002 by digiKam team, on Flickr"><img src="http://farm4.static.flickr.com/3645/3426182930_a8622d4c8c_t.jpg" width="100" height="67" alt="002"></a>

<a href="http://www.flickr.com/photos/digikam/3425374475/" title="001 by digiKam team, on Flickr"><img src="http://farm4.static.flickr.com/3543/3425374475_6983ef0c03_t.jpg" width="100" height="67" alt="001"></a>

<a href="http://www.flickr.com/photos/digikam/3426183082/" title="003 by digiKam team, on Flickr"><img src="http://farm4.static.flickr.com/3577/3426183082_d978c51713_t.jpg" width="100" height="67" alt="003"></a>

<a href="http://www.flickr.com/photos/digikam/3425374627/" title="004 by digiKam team, on Flickr"><img src="http://farm4.static.flickr.com/3411/3425374627_bab4b6572f_t.jpg" width="100" height="67" alt="004"></a>

<a href="http://www.flickr.com/photos/digikam/3425670241/" title="IMGP5820 by digiKam team, on Flickr"><img src="http://farm4.static.flickr.com/3650/3425670241_64dd5ecfda_t.jpg" width="100" height="67" alt="IMGP5820"></a>

<a href="http://www.flickr.com/photos/digikam/3427654426/" title="img_0604-11 by digiKam team, on Flickr"><img src="http://farm4.static.flickr.com/3401/3427654426_fbdeca5a31_t.jpg" width="100" height="67" alt="img_0604-11"></a>

<a href="http://www.flickr.com/photos/digikam/3426845561/" title="img_0603-1 by digiKam team, on Flickr"><img src="http://farm4.static.flickr.com/3590/3426845561_27483f702b_t.jpg" width="100" height="67" alt="img_0603-1"></a>

         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-18411"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/437#comment-18411" class="active">Can't view the pictures</a></h3>    <div class="submitted">Submitted by Jonas (not verified) on Mon, 2009-03-30 15:34.</div>
    <div class="content">
     <p>I would like to see the candidats in larger version, but to to this I've to login. But I do not want to create Yahoo account for this. Why don't you store the pictures (not only this candidat pictures) on the digikam server and display them for example with lightbox. Drupal has the power to do this ;)</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-18412"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/437#comment-18412" class="active">For (c) reasons...</a></h3>    <div class="submitted">Submitted by digiKam on Mon, 2009-03-30 15:46.</div>
    <div class="content">
     <p>For the moment, Flickr group to review splashs is private and only viewable by digiKam team...</p>
<p>I don't want to publish all pictures over the world. There are some rights to preserve here...</p>
<p>digiKam</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-18413"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/437#comment-18413" class="active">Gilles,
Why not create a</a></h3>    <div class="submitted">Submitted by Julien Narboux (not verified) on Tue, 2009-03-31 08:25.</div>
    <div class="content">
     <p>Gilles,</p>
<p>Why not create a Flickr group for splashscreen propositions ? people who submit pictures for Digikam splashscreen should put Creative Common licence compatible with Digikam Licence ? and everybody could "vote" ?</p>
<p>Julien</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-18414"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/437#comment-18414" class="active">ah, good idea...</a></h3>    <div class="submitted">Submitted by digiKam on Tue, 2009-03-31 08:53.</div>
    <div class="content">
     <p>But unforget that not all peoples use Flickr... So i need to manage it myself. I will look if i can switch current Flickr group from private to public to take a care on right of content. </p>
<p>Gilles</p>
         </div>
    <div class="links">» </div>
  </div>
</div></div></div><a id="comment-18440"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/437#comment-18440" class="active">Splashscreen : remove the slogan !</a></h3>    <div class="submitted">Submitted by Fred (not verified) on Thu, 2009-04-16 21:12.</div>
    <div class="content">
     <p>Frankly as long as "like a professional" (meaning "since you're using Digikam you're obviously not one) disappears, you can pick whatever picture you like.</p>
<p>I've been working in, let's say, the "image business" for *years* (about 15 or 18 all in all), mostly film (as in moving images, be it on chemical or tape support), in advertising and television (the latter being Canal+, I'm sure it will ring a bell with Gilles, and yes I'm French too) before dropping that domain completely for mostly full time IT which was what actually paid the bills anyway (having the power cut every other month was getting tiresome anyway). </p>
<p>Nowadays I'm self employed in IT (switching people to Linux) and working towards a semi professional status in photography. I know I have the "eye" to make an image (or I certainly wouldn't have landed a job at C+), and I have the technical side covered but I'm definitely not an artist (I've met way too many artists to want to be one, sorry).</p>
<p>Anyway I've been running Linux since late 93 or early 94. I ran Unix before that. I've managed mainframes, minis, all kinds of crap. And I used take pictures. On rolls of plastic with noxious chemicals even.<br>
So I don't want to see my image management program telling me I'm managing my images "like the professionals" when I've had miy stuff on TV, when I've donated naturalist images to a museum. And when I've been kicking Mac user's ass with Linux as far back as 97 (you mean you can use FTP, a web browser *and* an editor at the same time ?).<br>
So please, please, remove that crappy slogan.</p>
<p>Suggestions :<br>
"Professional open source photography management"<br>
"High end open source image management"<br>
You get the idea.</p>
<p>Or nothing even.</p>
<p>But not "it could be something good but it's crappy because it's open source and we suck and we run Linux but when we get out of our mom's basement we'll earn a living and but a Mac or Windows". I don't want that stuck in my face each time I start digiKam which is getting much better with each version (and it was really quite good to start with, there really aren't that many things to fix with 0.10, it's a very good release). </p>
<p>But the slogan...<br>
just<br>
has<br>
to<br>
go</p>
         </div>
    <div class="links">» </div>
  </div>
</div><a id="comment-18415"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/437#comment-18415" class="active">Version 1.0?</a></h3>    <div class="submitted">Submitted by Sebastian (not verified) on Tue, 2009-03-31 09:01.</div>
    <div class="content">
     <p>Is there a plan to release a version 1.0 some when?<br>
I mean this application is of such a high quality, usability and stability it deserves to have at least a 1 in front of the dot. :-)</p>
<p>Gilles, I will send you some picture proposals as well. Will be a pleasure, being able to contribute in the development in that way.</p>
<p>Best regards,<br>
Sebastian</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-18416"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/437#comment-18416" class="active">ok... i will study this point...</a></h3>    <div class="submitted">Submitted by digiKam on Tue, 2009-03-31 09:42.</div>
    <div class="content">
     <p>I will talk about 1.0 release version id with other team members.</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-18421"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/437#comment-18421" class="active">I think it is a good idea.</a></h3>    <div class="submitted">Submitted by Julien (not verified) on Tue, 2009-03-31 17:45.</div>
    <div class="content">
     <p>I think it is a good idea. Maybe 1.0 = 0.11 + non destructive editing ?</p>
<p>Julien</p>
         </div>
    <div class="links">» </div>
  </div>
</div></div><a id="comment-18428"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/437#comment-18428" class="active">"Manage your photographs like</a></h3>    <div class="submitted">Submitted by BigWhale (not verified) on Sat, 2009-04-11 19:22.</div>
    <div class="content">
     <p>"Manage your photographs like a professional with the power of open source."</p>
<p>This should be changed. At least the 'like a professional' part should be left out. It says 'our garage software will almost work'. Who are those 'professionals' that we want to be like? :)</p>
<p>This is somehow self-degrading.</p>
         </div>
    <div class="links">» </div>
  </div>

</div>
