---
date: "2010-04-30T22:45:00Z"
title: "GSoC: Non-destructive image editing for digiKam"
author: "Martin Klapetek"
description: "Hello to all the great KDE people and especially digiKam fans! My full name is Martin Klapetek, but everyone calls me Marty and I can"
category: "news"
aliases: "/node/513"

---

<p>Hello to all the great KDE people and especially digiKam fans! </p>
<p>My full name is Martin Klapetek, but everyone calls me Marty and I can be found on most services like IM or IRC by the name mck182. I'm 22 and I'm from Czech republic and ever since I started with Linux, I immediately become a huge fan of KDE (3.5 back then). I'm a terrible musician (playing guitar, bass guitar, sometimes even piano [you don't want to be there]), promising art painter and hopefully-soon-to-be graduated college student with bachelor's degree in Computer Science.</p>
<p>I've been accepted to this year's Google Summer of Code with project to enable non-destructive image editing and image versioning for digiKam.</p>
<p>I'm very keen on taking photos and I've been using Google's Picasa for organizing the quantum of pictures I've taken. But the Linux version was more and more unusable lately for me, mainly because problems with multilibs (I'm running 64bit system and Picasa needs 32bit Qt libs and I'm using non-official-distro Qt packages, so there were some conflicts) and also because I hate it's wine-ish look &amp; feel (&amp; speed) and non-integrationess. So I've decided to use something more KDE fashioned and the sight was set on digiKam.</p>
<p>I like Picasa's possibility to edit your images in any way and then anytime you come back and feel like it's not good enough, you can simply reset the image to its original state and start over. This is the feature I'd like to bring to digiKam during this summer. This will go along with feature for having multiple versions with different modifications of one image and easily switching between them.</p>
<p>The non-destructive editing will work fully automatized. Now when you do some changes eg. use some filters or adjust levels, you need to save your image first and if you just save, you lose the original. With the non-destructive editing, it will automatically create new version from the original and it will store all the changes there, all without bothering user about saving the image and without actually touching the original. Every time you switch to the original and start new modifications, it will create another version alongside with the first one. Each version will also be manually forkable to another new version. </p>
<p>That's in short my project for GSoC 2010. I'm very excited to finally do some real work for KDE and to contribute to the world of open source. I'm looking forward to work with the already-shown-to-be-awesome people from digiKam team and especially with Marcel Wiesweg, my mentor :)</p>
<p>I wish happy coding and lots of fun to all the accepted students. Let's make KDE proud of us :)</p>
<p>Marty</p>
<p>PS: More detailed posts will come when the real work will start ;)</p>

<div class="legacy-comments">

  <a id="comment-19157"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/513#comment-19157" class="active">Congratulations!</a></h3>    <div class="submitted">Submitted by <a href="http://krita.org" rel="nofollow">Kubuntiac</a> (not verified) on Fri, 2010-04-30 23:21.</div>
    <div class="content">
     <p>I've actually avoided using a lot of the features before, not knowing if they were destructive or not (eg image rotate) and whether they required recompression if using a lossy format. I'm thrilled to hear your application got accepted, and look forward eagerly to seeing how you go. Congratulations on getting your proposal accepted. Have fun. Code hard. Release early and often!  :)</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-19161"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/513#comment-19161" class="active">Thanks!</a></h3>    <div class="submitted">Submitted by Martin Klapetek on Sat, 2010-05-01 18:40.</div>
    <div class="content">
     <p>Thanks!</p>
<p>The development will take place in KDE's SVN, so if you're skilled enough to build on your own, you can have the changes running almost instantly ;) Although SVN mostly equals to "unstable" (simply bleeding edge), so be advised :)</p>
         </div>
    <div class="links">» </div>
  </div>
</div><a id="comment-19158"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/513#comment-19158" class="active">Congrats</a></h3>    <div class="submitted">Submitted by <a href="http://bmaron.net" rel="nofollow">eMerzh</a> (not verified) on Fri, 2010-04-30 23:31.</div>
    <div class="content">
     <p>Congrats for your nomination...<br>
Please make digikam a better place to hold and rework photos ;)</p>
<p>I think the hard part of the job will be to find a nice UI to group photos that are edits (crop, colors, ...) from another one...</p>
<p>I also wish there were a way to handle raw and theres "developpements" or photo that look the same  (like 10 shots of the same mountain from the same point of view)...</p>
<p>Good luck with your work<br>
;)</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-19162"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/513#comment-19162" class="active">Thanks!</a></h3>    <div class="submitted">Submitted by Martin Klapetek on Sat, 2010-05-01 18:42.</div>
    <div class="content">
     <p>Thank you!</p>
<p>There is also some plans with versioning development from RAW images ;) But the UI changes are still a subject of discussion. But I'll try to post some mock-ups as soon as we figure something out, so stay tuned ;)</p>
         </div>
    <div class="links">» </div>
  </div>
</div><a id="comment-19159"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/513#comment-19159" class="active">Image versioning would be a</a></h3>    <div class="submitted">Submitted by Jochen S. (not verified) on Sat, 2010-05-01 09:49.</div>
    <div class="content">
     <p>Image versioning would be a really cool feature for digikam. All the best and happy coding!<br>
Greetz,<br>
Jochen</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-19163"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/513#comment-19163" class="active">Hopefully I'll manage to</a></h3>    <div class="submitted">Submitted by Martin Klapetek on Sat, 2010-05-01 18:43.</div>
    <div class="content">
     <p>Hopefully I'll manage to finish it in a perfect usable state, that's my goal :)</p>
         </div>
    <div class="links">» </div>
  </div>
</div><a id="comment-19160"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/513#comment-19160" class="active">Great news!</a></h3>    <div class="submitted">Submitted by Normand C. (not verified) on Sat, 2010-05-01 18:32.</div>
    <div class="content">
     <p>Hey, congrats Marty!!!</p>
<p>I've been a digiKam user for a couple of months now, even though I'm on GNOME, since it's so much superior to F-Spot in features. But I couldn't understand how digiKam could still miss a feature that F-Spot has had for years!!! You can't know how happy your project makes me!!! :-)</p>
<p>What would be great is for the different versions and the original to be "stacked" together, and only have one of them shown in the thumbnails view, with some menu to choose which one is shown. You may want to take a look at F-Spot to understand what I'm talking about.</p>
<p>Anyway, good luck with your project!</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-19164"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/513#comment-19164" class="active">Thanks!</a></h3>    <div class="submitted">Submitted by Martin Klapetek on Sat, 2010-05-01 18:49.</div>
    <div class="content">
     <p>Yeah, I've checked F-Spot to see the feature in work before this application and the idea is something like that, have only one image shown among thumbs, but when shown, show also the stack of all versions. But I need to discuss this a little bit more with the mentor, especially the UI changes. I'd like to bring KDE Usability team into this. </p>
<p>Thanks, comments like these are awesome motivation! :)</p>
         </div>
    <div class="links">» </div>
  </div>
</div><a id="comment-19165"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/513#comment-19165" class="active">Thanks Marty,
that's what I'm</a></h3>    <div class="submitted">Submitted by linubug (not verified) on Sat, 2010-05-01 19:50.</div>
    <div class="content">
     <p>Thanks Marty,</p>
<p>that's what I'm waiting for since I got in contact to linux and digikam two years ago. For me, lack of versioning is the most important item why I'm still using Photoshop Elements on Windows (other items are real albums and easily moving the files to another folder but still keeping them in the same place of the browser).</p>
<p>If you're reaching your goal, I finally could get rid of windows and dualboot. </p>
<p>All the best!</p>
         </div>
    <div class="links">» </div>
  </div>
<a id="comment-19166"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/513#comment-19166" class="active">great news</a></h3>    <div class="submitted">Submitted by Anonymous (not verified) on Sun, 2010-05-02 11:25.</div>
    <div class="content">
     <p>I'm using digikam since about 2 years for organizing all my pictures and i really like the handling of IPTC tags, Exif's, categories and all the other stuff in the sidebar which makes it easy to organize also a huge amount of photos.<br>
But i didn't like the picture editor showfoto. I also realy would love a feature which makes multiple version editing possible and i really would like to have it integrated in the UI so thumbnails can be grouped.<br>
Another point i would like to mention is the workflow in showfoto. In my oppinion a UI like rawtherapee has, is much more useable for improving photos becouse you can easily switch between main editing tools like exposure correction, color, sharpening/denoising and cropping/rotaton, which are allways shown in the sidebar. Maybe the UI of rawtherapee 3.0 could be inspiring for you to create beside a really powerful image organizing tool a great image editing tool.</p>
<p>good luck</p>
<p>Martin</p>
<p>(I'm sorry for my bad english)</p>
         </div>
    <div class="links">» </div>
  </div>
<a id="comment-19168"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/513#comment-19168" class="active">Congratulations</a></h3>    <div class="submitted">Submitted by JulienN (not verified) on Mon, 2010-05-03 09:11.</div>
    <div class="content">
     <p>Congratulations !</p>
<p>This is the main missing feature of DigiKam, I am looking forward for your implementation !</p>
<p>Julien</p>
         </div>
    <div class="links">» </div>
  </div>
<a id="comment-19169"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/513#comment-19169" class="active">Good idea, but...</a></h3>    <div class="submitted">Submitted by damian (not verified) on Mon, 2010-05-03 23:55.</div>
    <div class="content">
     <p>Hi, I think non-destructive image editing is really good most times but sometimes I just want to rotate an image and write over the old one, will the gui allow this?<br>
Anyway thanks for your work :)</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-19173"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/513#comment-19173" class="active">It's already available</a></h3>    <div class="submitted">Submitted by Normand C. (not verified) on Wed, 2010-05-05 08:23.</div>
    <div class="content">
     <p>Damian,</p>
<p>What you're asking can already be done in DigiKam 1.0's thumbnails view. Hover with the cursor over a thumbnail, and on the top right corner, two arrows appear. You click on either one and bingo! your image is rotated to the left or to the right. It's pretty fast!</p>
         </div>
    <div class="links">» </div>
  </div>
</div><a id="comment-19170"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/513#comment-19170" class="active">Hi
congrats to being accepted</a></h3>    <div class="submitted">Submitted by Daniel (not verified) on Tue, 2010-05-04 07:57.</div>
    <div class="content">
     <p>Hi</p>
<p>congrats to being accepted to GSOC!</p>
<p>As for the new feature, it would be great if one could turn it on and off easily. The reason is that not all users of DigiKam might need or want it: I, for example, mostly rotate and do some quick standard improvements to my pictures, but I rarely do major changes/ experiments. I don't want the program to save a version of every change, I feel it would just clutter up my hard drive. In fact, that is one of the reasons I moved away from Picasa!! I prefer saving the changes myself when necessary (and give the file a meaningful name on the way) than have the program do it for me all the time. I could imagine that there are many users of this type: so experienced and independent that they do save files when necessary, but not professional enough to need many different versions and nondestructive editing.</p>
<p>I also agree with previous posts that a good UI is crucial.</p>
<p>Have fun coding, and please do the best you can!</p>
<p>Thanks for all your efforts!</p>
<p>Daniel</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-19171"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/513#comment-19171" class="active">not necessary to worry</a></h3>    <div class="submitted">Submitted by qpal (not verified) on Tue, 2010-05-04 11:18.</div>
    <div class="content">
     <p>Hi Daniel.</p>
<p>It is not necessary to worry about effect of non-destructing editing to space on your hard drive. </p>
<p>The huge advantage of non-destructing editing is that you do not have to make any backup copy (in case you really need original picture) of your files when you are editing them.</p>
<p>I have modified picture using Picasa to show you what information is added.<br>
This is example of full record you may find on .picasa.ini file on folder where modified picture is stored:<br>
[IMG_2063_4_5_tonemapped.jpg]<br>
backuphash=1133<br>
filters=retouch=1;finetune2=1,0.111111,0.103860,0.044912,00000000,0.000000;bw=1;grain2=1;glow2=1,0.650000,3.000000;</p>
<p>There has been no changes on photo (it is idea of non-destructing editing), I suppose only thumbnail (preview picture) in album has been changed in order to display thumbnails as fast as possible. No copies of photo has been made.</p>
<p>Please correct me if I am wrong.</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-19172"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/513#comment-19172" class="active">Thanks for your detailed</a></h3>    <div class="submitted">Submitted by Daniel (not verified) on Wed, 2010-05-05 08:12.</div>
    <div class="content">
     <p>Thanks for your detailed reply!</p>
<p>It's been a while since I last used Picasa, so I don't know how it works today (it used to store several copies of the same photo, and that sucked). What you write sounds like a good solution, but I still have a question: if the .ini file in the folder is lost, then all changes to all pictures are lost? I mean, these files are often hidden, and one might accidentally delete it; or if I copy the photos onto another medium without copying the [picasa].ini, then the new medium only has the original photos without modifications? How does it work if I send the photo by email?</p>
<p>Thanks for your answer - I hope I am helping in pointing out what worries/troubles medium-experienced DigiKam users might have.</p>
<p>Best</p>
<p>Daniel</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-19174"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/513#comment-19174" class="active">Yes, all changes are lost,</a></h3>    <div class="submitted">Submitted by Martin Klapetek on Wed, 2010-05-05 17:45.</div>
    <div class="content">
     <p>Yes, all changes are lost, except cropping and maybe rotating (not sure with this one). Cropping is done by making a backup copy of the original. The .ini file is just one per folder, so if you're moving whole folder, you're fine, but moving one image will lose its changes. But you lost your changes even when opening with win picasa. Especially cropping is completely misplaced. I don't know how about today, but this happened to me 6 months ago, when I though, that the format has to be the same. Appereantly, it wasn't.</p>
<p>I used to make changes to some images in Picasa and then exported the pictures I wanted to move around, upload, email etc. to another folder. Picasa handles this very nicely, it won't duplicate the pics in your library, but instead, it will put this folder under "Exported" category  and you can browse them separately in there.</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-19175"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/513#comment-19175" class="active">Thanks again for your quick</a></h3>    <div class="submitted">Submitted by Daniel (not verified) on Thu, 2010-05-06 08:45.</div>
    <div class="content">
     <p>Thanks again for your quick and detailed reply.</p>
<p>I am not sure, but I could imagine that one of the most standard use cases for digikam looks like this: (i) shoot a lot of photos at a party / on holiday; (ii) check, select, and do most important improvements to photos, all of this in a relatively efficient and quick way; and (iii) share with friends (who were on holiday or on party or just wanna see the pics), either by email, online service, or portable storage - those people usually won't use digikam. </p>
<p>In this case, I wouldn't want to have to export first so that changes are saved, it's an additional step and in the end does clutter up my hard drive. So it would be really nice if either non-destructive image editing was optional (e.g. an option in settings) or if there was a an intuitive way to do and save changes the old-fashioned way as well.</p>
<p>What do you (and others) think?</p>
<p>Best</p>
<p>Daniel</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-19206"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/513#comment-19206" class="active">why not do this in a similar</a></h3>    <div class="submitted">Submitted by Anonymous (not verified) on Tue, 2010-05-25 06:42.</div>
    <div class="content">
     <p>why not do this in a similar way to lightroom or aperture. Save the changes in a database that can be saved copied or moved. This way there is only one file to keep track of and not 380,000 as in my catalog.</p>
         </div>
    <div class="links">» </div>
  </div>
</div></div></div></div></div><a id="comment-19177"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/513#comment-19177" class="active">Congrats!</a></h3>    <div class="submitted">Submitted by <a href="http://111bits.net" rel="nofollow">111bits</a> (not verified) on Mon, 2010-05-10 23:23.</div>
    <div class="content">
     <p>Congrats, Martin! :) And thanks! :)<br>
digikam is my favourite image-software and with this dream feature (for me) it will become just marvellous!<br>
I am very happy about this project and wish you good luck and happy hacking! :)</p>
         </div>
    <div class="links">» </div>
  </div>
<a id="comment-19205"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/513#comment-19205" class="active">What about editing done with other programs?</a></h3>    <div class="submitted">Submitted by Chris K (not verified) on Sat, 2010-05-22 12:43.</div>
    <div class="content">
     <p>I am delighted to hear about this feature, which is the one thing I missed from f-spot.  I wonder will it be possible to edit a photo in another program and these changes also to be non-destructive?  For instance, I prefer Gwenview's red eye removal tool to Digikam's and often use Digikam to open an image in Gwenview and edit it there - would these changes be non-destructive?  Likewise, many users like to open images in GIMP for editing - would these changes be non-destructive?  I know f-spot allows for non-destructive editing with other programs - will this also be the case for Digikam?</p>
<p>Also, will audio and video files be able to be opened and edited in external programs and saved non-destructively?</p>
<p>Thanks again for the marvellous work you and the other developers are doing on Digikam - it is much appreciated.</p>
         </div>
    <div class="links">» </div>
  </div>

</div>