---
date: "2008-03-31T20:53:00Z"
title: "digiKam : Google summer of code - call for students"
author: "digiKam"
description: "As KDE is a mentoring organization for the Google Summer of Code 2008 (see this page for details) it would be great if we get"
category: "news"
aliases: "/node/305"

---

As KDE is a mentoring organization for the Google Summer of Code 2008 (see <a href="http://code.google.com/soc/2008">this page</a> for details) it would be great if we get some nice projects related to digiKam.<br><br>

If you are a student interested in a paid summer internship with Google, mentored by members of the digiKam team, please get in contact with us now.<br><br>

Important: Application deadline: April 7 2008<br><br>

Possible projects are:<br><br>

1/ <a href="http://bugs.kde.org/show_bug.cgi?id=149485">Advanced image resize for image editor</a><br>
==&gt; KDE3 and KDE4<br><br>

2/ <a href="http://bugs.kde.org/show_bug.cgi?id=144593">New High Dynamic Range (HDR) plugin</a><br>
==&gt; KDE3 and KDE4<br><br>

  This could involve<br>
  - contrast masking<br>
  - HDR generation and tone-mapping (but we have qtpfsgui for that ...!)<br>
  - enfuse (see the <a href="http://wiki.panotools.org/Enfuse">hugin project</a> for details)<br><br>

3/ <a href="http://bugs.kde.org/show_bug.cgi?id=146866">KROSS integration</a><br>
==&gt; KDE4 only<br><br>

  This would be very powerful as it allows to script digiKam via python, ruby etc.<br>
  (For example realized for krita).<br><br>

4/ <a href="http://bugs.kde.org/show_bug.cgi?id=146288">Face detection/recognition</a><br>
==&gt; KDE3 and KDE4<br><br>

Note that it is not possible to mentor all projects at the same time, so in case of many applicants
it might be necessary to do a selection.<br><br>

First steps:<br><br>

a) you should join the digikam-devel mailing list<br>
b) choose a project which interests you. Of course you can extend any of the projects listed above, or even propose a new one (and tell us about it on the mailing list).<br>
c) install the current version from svn<br>
d) optionally: to demonstrate your coding skills:<br>
   fix any of pending bugs by providing a patch in the bug-tracker ;-)<br>
e) Before submitting your proposal you need contact us to discuss all relevant details.<br>
f) When it comes to writing the proposal, the page <a href="http://pradeepto.livejournal.com/12565.html">How to write applications for KDE Google Summer Of Code?</a> is highly recommended!<br>
g) All necessary steps are given in the <a href="http://groups.google.com/group/google-summer-of-code-announce/web/guide-to-the-gsoc-web-app-for-student-applicants ">Guide to the Google Summer of Code Web App for Student Applicants</a><br><br>

Best,<br><br>

The digiKam Team
<div class="legacy-comments">

  <a id="comment-17914"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/305#comment-17914" class="active">Note that it is not possible</a></h3>    <div class="submitted">Submitted by red (not verified) on Sat, 2008-11-01 11:04.</div>
    <div class="content">
     <p>Note that it is not possible to mentor all projects at the same time, so in case of many applicants it might be necessary to do a selection.</p>
         </div>
    <div class="links">» </div>
  </div>

</div>