---
date: "2011-01-03T09:45:00Z"
title: "Cross-processing with digiKam"
author: "Dmitri Popov"
description: "Although digiKam can’t rival dedicated image manipulation applications like the GIMP, it does have several essential editing tools which can help you to turn plain"
category: "news"
aliases: "/node/560"

---

<p>Although digiKam can’t rival dedicated image manipulation applications like the GIMP, it does have several essential editing tools which can help you to turn plain photos into striking images. For example, using the Curves Adjustment tool, you can apply the cross-processing effect to a photo. <a href="http://scribblesandsnaps.wordpress.com/2011/01/03/cross-processing-with-digikam/">Continue to read</a></p>

<div class="legacy-comments">

  <a id="comment-19722"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/560#comment-19722" class="active">Continue to write?</a></h3>    <div class="submitted">Submitted by Will Stephenson (not verified) on Mon, 2011-01-03 11:54.</div>
    <div class="content">
     <p>Where's the rest of this post?</p>
<p>PS I'd love to see a series of recipes how to create Hipstamatic-like effects using Digikam.</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-19725"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/560#comment-19725" class="active">RE: Continue to write?</a></h3>    <div class="submitted">Submitted by <a href="http://ffejery.creativemisconfiguration.com" rel="nofollow">Jeffery MacEachern</a> (not verified) on Tue, 2011-01-04 19:33.</div>
    <div class="content">
     <p>Just click the Continue to Read button (again, if coming here via the Planet)</p>
         </div>
    <div class="links">» </div>
  </div>
</div>
</div>