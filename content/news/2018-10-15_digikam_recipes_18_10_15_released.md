---
date: "2018-10-15T00:00:00"
title: "digiKam Recipes 18.10.15 released"
author: "Dmitri Popov"
description: "A new revision of the digiKam Recipes book is available"
category: "news"
---

It's time for another [digiKam Recipes](https://gumroad.com/l/digikamrecipes/) update. The most visible change in the latest revision is the new book cover. All screenshots were also updated to reflect changes in the current version of digiKam. In addition to the visual tweaks, the latest revision features new content. [Continue reading](http://scribblesandsnaps.com/2018/10/15/digikam-recipes-18-10-15-released/)...
