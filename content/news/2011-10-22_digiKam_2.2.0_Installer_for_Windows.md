---
date: "2011-10-22T21:26:00Z"
title: "digiKam 2.2.0 Installer for Windows"
author: "Dmitri Popov"
description: "Good news for those who want to run the latest version of digiKam on Windows. The SourceForge repository now has a Windows installer of the"
category: "news"
aliases: "/node/631"

---

<p>Good news for those who want to run the latest version of digiKam on Windows. The &nbsp;<a href="http://sourceforge.net/projects/digikam/files/digikam/2.2.0/">SourceForge</a>&nbsp;repository now has a Windows installer of the latest digiKam version courtesy of Ananta Palani.</p>
<p><img class="alignnone size-medium wp-image-2077" title="digikam_wininstaller" src="http://scribblesandsnaps.files.wordpress.com/2011/10/digikam_wininstaller.png?w=500" alt="" width="500" height="277"></p>
<p><a href="http://scribblesandsnaps.wordpress.com/2011/10/22/digikam-2-2-0-installer-for-windows/">Continue to read</a></p>

<div class="legacy-comments">

</div>