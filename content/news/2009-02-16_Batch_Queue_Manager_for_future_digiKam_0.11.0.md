---
date: "2009-02-16T14:46:00Z"
title: "Batch Queue Manager for future digiKam 0.11.0..."
author: "digiKam"
description: "Since first beta releases of digiKam and kipi-plugins for KDE4, i receive a lots of mails about to know where are all Batch kipi-plugins... as"
category: "news"
aliases: "/node/427"

---

<a href="http://www.flickr.com/photos/digikam/3297273062/" title="batchqueuemanager4 by digiKam team, on Flickr"><img src="http://farm4.static.flickr.com/3313/3297273062_07b2355a10.jpg" width="500" height="313" alt="batchqueuemanager4"></a>

<p>Since first beta releases of digiKam and kipi-plugins for KDE4, i receive a lots of mails about to know where are all Batch kipi-plugins... as batch converter, batch resize, etc... In fact these tools will never ported to KDE4 ... by me...</p>

<a href="http://www.flickr.com/photos/digikam/3283890553/" title="0.9.5-batchmenu by digiKam team, on Flickr"><img src="http://farm4.static.flickr.com/3570/3283890553_0eb1b421d2.jpg" width="500" height="400" alt="0.9.5-batchmenu"></a>

<p>KDE4 Port of kipi-plugins include only 3 batch tools: <a href="http://www.digikam.org/node/344">Raw converter</a>, <a href="http://www.digikam.org/node/393">DNG converter</a>, and <a href="http://www.digikam.org/drupal/node/392">Remove Red Eyes</a>. Why not to port all others batch tools ? Because they are limited in usability: you can only process one action on images at the same time, and require a lots of interactions from user. We need something better where more than one action can be processed at the same time...</p>

<p>Well, with new <b>Batch Queue Manager</b> implemented in digiKam core, this become a reality. Like you can see with screenshots below. The first one is a configuration panel dedicated to this tool. For the moment, you can only set-up target album where all processed items will be created and behavior if target files already exist at this place.</p>

<a href="http://www.flickr.com/photos/digikam/3284643166/" title="0.11.0-digiKamSetupBatchQueueManager by digiKam team, on Flickr"><img src="http://farm4.static.flickr.com/3466/3284643166_5c79c0c404.jpg" width="500" height="400" alt="0.11.0-digiKamSetupBatchQueueManager"></a>

<p>To add items to Batch Queue Manager, you can use context menu from icon-view, Image menu from Album interface, or drag and drop. You can place items to current queue or create a new one. There is no limitation with queues and items.</p><p>

<a href="http://www.flickr.com/photos/digikam/3284655336/" title="0.11.0-digiKamBatchQueueManagerPopUpMenu by digiKam team, on Flickr"><img src="http://farm4.static.flickr.com/3376/3284655336_dc5214dde1.jpg" width="500" height="400" alt="0.11.0-digiKamBatchQueueManagerPopUpMenu"></a>

<a href="http://www.flickr.com/photos/digikam/3284576374/" title="0.11.0-digiKamBatchQueueManager by digiKam team, on Flickr"><img src="http://farm4.static.flickr.com/3589/3284576374_80c743bd67.jpg" width="500" height="400" alt="0.11.0-digiKamBatchQueueManager"></a>

</p><p>Batch Queue Manager interface is easy to understand. It's composed of:</p>

<ul>
<li>A pool of queues where are stored images to process.</li>
<li>A chained list of batch tools to process for each queue. A chained list is defined for all items hosted by a queue.</li>
<li>A view to edit batch tool settings.</li>
<li>A view to list all batch tools available.</li>
</ul>

<p>To assign tools to a queue, use drag and drop. Of course, you can change Batch tools order in list... When all is ready, just press <b>Run</b> button. Process are played in background using multi-threading and you can switch to others parts of digiKam, or take a coffee...</p>

<a href="http://www.flickr.com/photos/digikam/3283854349/" title="0.11.0-digiKamBatchQueueManagerDesc by digiKam team, on Flickr"><img src="http://farm4.static.flickr.com/3382/3283854349_bdddf37f6f.jpg" width="500" height="400" alt="0.11.0-digiKamBatchQueueManagerDesc"></a>

<p>Bach Queue Manager is not yet complete. I plan to add an option to save a chained list of <b>Customized Tools</b> to re-use it later, and another one to be able to use current Batch <b>Kipi-plugins</b>. For this last one, libkipi and plugins need to be adapted. Batch Queue Manager will be released with next digiKam 0.11.0</p>
<div class="legacy-comments">

  <a id="comment-18249"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/427#comment-18249" class="active">This is awesome. DigiKam is</a></h3>    <div class="submitted">Submitted by maninalift (not verified) on Mon, 2009-02-16 18:11.</div>
    <div class="content">
     <p>This is awesome. DigiKam is not only the best piece of photo-managing software I have seen (Apple's offerings look foolish by comparison) but one of the nicest pieces of software in KDE.</p>
<p>PS if this is for 0.11.0 how long 'till that release?</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-18250"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/427#comment-18250" class="active">0.11.0 plan...</a></h3>    <div class="submitted">Submitted by digiKam on Mon, 2009-02-16 18:32.</div>
    <div class="content">
     <p>Well, it's not yet defined, but we will try to release more faster than 0.10.0...</p>
<p>digiKam</p>
         </div>
    <div class="links">» </div>
  </div>
</div><a id="comment-18251"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/427#comment-18251" class="active">When in svn?</a></h3>    <div class="submitted">Submitted by Picasso (not verified) on Mon, 2009-02-16 21:07.</div>
    <div class="content">
     <p>Sounds great - when will the new tool be in svn?</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-18252"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/427#comment-18252" class="active">It's already in svn...</a></h3>    <div class="submitted">Submitted by digiKam on Mon, 2009-02-16 21:18.</div>
    <div class="content">
     <p>Code is already in svn to a dedicated <a href="http://websvn.kde.org/branches/extragear/graphics/digikam/0.11"> development branch</a></p>

<p>digiKam</p>


         </div>
    <div class="links">» </div>
  </div>
</div><a id="comment-18254"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/427#comment-18254" class="active">Great!</a></h3>    <div class="submitted">Submitted by mikmach (not verified) on Mon, 2009-02-16 23:10.</div>
    <div class="content">
     <p>Is it possible that "Custom Tools" will include call for external programs, not only digiKam/kipi-plugins services?</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-18255"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/427#comment-18255" class="active">Looks good.  Will there also</a></h3>    <div class="submitted">Submitted by Anonymous (not verified) on Tue, 2009-02-17 00:39.</div>
    <div class="content">
     <p>Looks good.  Will there also be a capability to resize images in the batch queue?</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-18257"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/427#comment-18257" class="active">it's will be my next batch tool to implement...</a></h3>    <div class="submitted">Submitted by digiKam on Tue, 2009-02-17 06:01.</div>
    <div class="content">
     <p>it's will be my next batch tool to implement...</p>
         </div>
    <div class="links">» </div>
  </div>
<a id="comment-18271"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/427#comment-18271" class="active">Resize Batch Tool done in svn </a></h3>    <div class="submitted">Submitted by digiKam on Fri, 2009-02-20 18:20.</div>
    <div class="content">
     <a href="http://www.flickr.com/photos/digikam/3295642416/" title="batchqueuemanager3 by digiKam team, on Flickr"><img src="http://farm4.static.flickr.com/3512/3295642416_29073facd4.jpg" width="500" height="313" alt="batchqueuemanager3"></a>         </div>
    <div class="links">» </div>
  </div>
</div><a id="comment-18256"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/427#comment-18256" class="active">yes it's planed...</a></h3>    <div class="submitted">Submitted by digiKam on Tue, 2009-02-17 06:01.</div>
    <div class="content">
     <p>yes it's planed...</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-18259"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/427#comment-18259" class="active">That will definitely be</a></h3>    <div class="submitted">Submitted by 2Ben (not verified) on Tue, 2009-02-17 09:39.</div>
    <div class="content">
     <p>That will definitely be great. The benefits of the complete refactoring induced by KDE4 in general really spreads to all its key applications.<br>
Digikam is, with Amarok, a shiny proof of this :)</p>
         </div>
    <div class="links">» </div>
  </div>
</div></div><a id="comment-18260"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/427#comment-18260" class="active">Looks great! Will this be</a></h3>    <div class="submitted">Submitted by Martin (not verified) on Tue, 2009-02-17 11:33.</div>
    <div class="content">
     <p>Looks great! Will this be available to other kipi applications?</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-18261"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/427#comment-18261" class="active">No. it's developed using</a></h3>    <div class="submitted">Submitted by digiKam on Tue, 2009-02-17 11:45.</div>
    <div class="content">
     <p>No. it's developed using digiKam core components.</p>
         </div>
    <div class="links">» </div>
  </div>
</div><a id="comment-18262"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/427#comment-18262" class="active">batch save out of album</a></h3>    <div class="submitted">Submitted by Mattia (not verified) on Tue, 2009-02-17 19:12.</div>
    <div class="content">
     <p>Why not to add a way of saving processed images out of digikam album tree? e.g. if I need to resize a batch of photo just to send them in email, I would prefer to save them in a specific position of my filesystem, rather that in my album tree...</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-18263"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/427#comment-18263" class="active">planed too...</a></h3>    <div class="submitted">Submitted by digiKam on Tue, 2009-02-17 21:23.</div>
    <div class="content">
     <p>It's planed too, but for the moment, as Batch Queue Manager core is under development, i prefer to export processed images to a separate place to not break my collection if something going wrong.</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-18268"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/427#comment-18268" class="active">So maybe writing to separate</a></h3>    <div class="submitted">Submitted by xeros (not verified) on Wed, 2009-02-18 21:07.</div>
    <div class="content">
     <p>So maybe writing to separate directory with original album tree inside would be a good choice for that - I'm waiting for something like this to rebuild my albums for burning to DVDs.<br>
And one more thing - is it able to use external programs for "Custom Tools" for video files in albums, too?<br>
I wish I could use use mencoder as batch queue program in digiKam to convert my all camera videos in album tree to DIVX/XVID for use in my DVD Player.</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-18272"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/427#comment-18272" class="active">or...</a></h3>    <div class="submitted">Submitted by digiKam on Fri, 2009-02-20 18:22.</div>
    <div class="content">
     <p>I thinking about to add a new settings view, just behind Queues List to show Queue configuration, as target directories. Each queue will have a dedicated config.</p>
<p>Using video converter from command line will be possible. i don't see any restriction here.</p>
         </div>
    <div class="links">» </div>
  </div>
</div><a id="comment-18284"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/427#comment-18284" class="active">Something like this...</a></h3>    <div class="submitted">Submitted by digiKam on Mon, 2009-02-23 14:37.</div>
    <div class="content">
     <p>Now Queue Manager setup page become obsolete. Like you can see on bottom left corner, each Queue from Queues-Pool has a dedicated settings where target folder can be selected.</p>

<a href="http://www.flickr.com/photos/digikam/3302947993/" title="batchqueuemanager5 by digiKam team, on Flickr"><img src="http://farm4.static.flickr.com/3402/3302947993_831f526a2e.jpg" width="500" height="375" alt="batchqueuemanager5"></a>         </div>
    <div class="links">» </div>
  </div>
</div></div><a id="comment-18273"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/427#comment-18273" class="active">new subdirectory?</a></h3>    <div class="submitted">Submitted by wanthalf (not verified) on Sat, 2009-02-21 03:57.</div>
    <div class="content">
     <p>Does the first picture mean that I will be able to pre-set the queue manager (through the global settings) to create automatically subdirectories for processed images in every folder I want to process and save the processed images there?</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-18274"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/427#comment-18274" class="active">...</a></h3>    <div class="submitted">Submitted by wanthalf (not verified) on Sat, 2009-02-21 04:02.</div>
    <div class="content">
     <p>...sorry, I meant the second... screenshot, of course. Or is it just a setting of one common default global folder for all processed pictures?</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-18275"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/427#comment-18275" class="active">For the moment, it's a common</a></h3>    <div class="submitted">Submitted by digiKam on Sat, 2009-02-21 08:23.</div>
    <div class="content">
     <p>For the moment, it's a common default settings for all queues to process...</p>
         </div>
    <div class="links">» </div>
  </div>
</div></div><a id="comment-18279"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/427#comment-18279" class="active">batch rename will be ported?</a></h3>    <div class="submitted">Submitted by Ricardson Williams (not verified) on Sun, 2009-02-22 15:42.</div>
    <div class="content">
     <p>batch rename will be ported? Its very useful for me.</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-18286"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/427#comment-18286" class="active">Batch rename is greatly</a></h3>    <div class="submitted">Submitted by Tim Middleton (not verified) on Mon, 2009-02-23 18:23.</div>
    <div class="content">
     <p>Batch rename is greatly missed by me also. </p>
<p>My solution for now which works for my purposes (which usually involves renaming with timestamp variation in name, and sequence number) is to use the "jhead" command line utility. Something like this (example from man page):</p>
<p>jhead -n%Y%m%d-%H%M%S-%04i *.jpg</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-18287"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/427#comment-18287" class="active">port is under progress...</a></h3>    <div class="submitted">Submitted by digiKam on Mon, 2009-02-23 18:31.</div>
    <div class="content">
     <p>As i would not port BatchProcessImages tool to KDE4, Aurélien gateau from GWenview project (which use kipi-plugins too) has decided to port code as well. But i'm not sure if it will be available with kipi-plugins 0.2.0 release.</p>
<p>For digiKam, nothing change. I continue to code Batch Queue Manager which is for me a better way for the future.</p>
<p>Also, i plan to do a rename tool with Batch Queue Manager...</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-18329"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/427#comment-18329" class="active">now, it's ported and run...</a></h3>    <div class="submitted">Submitted by digiKam on Wed, 2009-03-11 09:14.</div>
    <div class="content">
     <p>Simple Batch Rename kipi-plugin is now ported to KDE4 in svn trunk, and will be released with 0.2.0.</p>

<a href="http://www.flickr.com/photos/digikam/3346482788/" title="Batch Rename Tool by digiKam team, on Flickr"><img src="http://farm4.static.flickr.com/3409/3346482788_9e21f2f162.jpg" width="500" height="400" alt="Batch Rename Tool"></a>

<p>digiKam</p>         </div>
    <div class="links">» </div>
  </div>
</div></div><a id="comment-18288"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/427#comment-18288" class="active">File Renaming implemented...</a></h3>    <div class="submitted">Submitted by digiKam on Wed, 2009-02-25 13:03.</div>
    <div class="content">
     <p>Today, in svn, File Renaming of queue destination files has been implemented.</p>

<a href="http://www.flickr.com/photos/digikam/3309085124/" title="batchqueuemanager9 by digiKam team, on Flickr"><img src="http://farm4.static.flickr.com/3634/3309085124_9dcfb069c1.jpg" width="500" height="400" alt="batchqueuemanager9"></a>         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-18322"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/427#comment-18322" class="active">Today I installed Digikam-011</a></h3>    <div class="submitted">Submitted by Ricardson Williams (not verified) on Sun, 2009-03-08 13:59.</div>
    <div class="content">
     <p>Today I installed Digikam-011 (rev: 996632) because I want Rename option, but to rename I have to choose one of "Batch Tools Avaliable" but in this case I only want rename the files nothing more... have some way? I'm use the kipi-plugins shipped in KDE 4.2.</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-18323"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/427#comment-18323" class="active">right.</a></h3>    <div class="submitted">Submitted by digiKam on Sun, 2009-03-08 14:11.</div>
    <div class="content">
     <p>Currently, rename operations are only applied if on batch tool is applied.</p>
<p>Good new : Simple Batch Rename kipi plugin is now ported and suitable. Code is in svn trunk of course.</p>
<p>digiKam</p>
         </div>
    <div class="links">» </div>
  </div>
</div></div></div><a id="comment-18282"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/427#comment-18282" class="active">mencoder</a></h3>    <div class="submitted">Submitted by kalymnos (not verified) on Mon, 2009-02-23 05:38.</div>
    <div class="content">
     <p>if you add a module mencoder would be possible to think tou the stock motion.<br>
vet of 25 or 15 frames per second.<br>
thank you</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-18310"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/427#comment-18310" class="active">This require another batch component concept.</a></h3>    <div class="submitted">Submitted by digiKam on Thu, 2009-03-05 10:30.</div>
    <div class="content">
     <p>Your idea is to merge all images to a single container. Currently, all batch tools apply changes on whole images one by one.</p>
<p>So, we need another batch tool type to generate common container, as MPEG2, AVI, PDF, PS, ODF, Flash, ect...</p>
<p>digiKam</p>
         </div>
    <div class="links">» </div>
  </div>
</div><a id="comment-18295"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/427#comment-18295" class="active">This is far too complex for</a></h3>    <div class="submitted">Submitted by Anonymous (not verified) on Tue, 2009-03-03 00:59.</div>
    <div class="content">
     <p>This is far too complex for any non-technical user to understand. Digikam is going in the wrong direction. Tools need to get simpler and not more complex.</p>
<p>(And while we're at it, the captchas used here are too hard to decipher, too.)</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-18303"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/427#comment-18303" class="active">Have you tried it, or just</a></h3>    <div class="submitted">Submitted by Stefano (not verified) on Wed, 2009-03-04 14:06.</div>
    <div class="content">
     <p>Have you tried it, or just judging by static screenshots? I don't see it any conceptually different from any other batch queue manager.</p>
         </div>
    <div class="links">» </div>
  </div>
<a id="comment-18305"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/427#comment-18305" class="active">Why ???</a></h3>    <div class="submitted">Submitted by digiKam on Wed, 2009-03-04 14:34.</div>
    <div class="content">
     <p>Why It's too complex to use ? give me some good arguments. You must be contructive instead to be critic as well!</p>
<p>If you have better ideas than mine, make a mockup, propose gui design and concept, etc... </p>
<p>And if you can, code and provide a patch !</p>
<p>If you can do anything else, well go back to windows and buy a closed source software.</p>
<p>To resume: it's too easy to critic like a troll.</p>
<p>digiKam</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-18332"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/427#comment-18332" class="active">This was not meant to be a</a></h3>    <div class="submitted">Submitted by Anonymous (not verified) on Sat, 2009-03-14 02:55.</div>
    <div class="content">
     <p>This was not meant to be a troll critic and I certainly won't go back to Windows since I dumped that 8 years ago in favor of KDE. I was just trying to say that it is not clear what the flow is in that screen, e.g. where do I start? What is a "Base tool" and why is it different from an "Assigned tool"? Do the "tool settings" affect base or assigned tools?</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-18334"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/427#comment-18334" class="active">It's simple....</a></h3>    <div class="submitted">Submitted by digiKam on Mon, 2009-03-16 12:20.</div>
    <div class="content">
     <p>The work-flow is very simple:</p>
<p>1- place image to batch on the queue, using D&amp;D or context menu from Album icon view.<br>
2- Base tools are Batch processing tools available on digiKam.<br>
3- With a queue selected, where your image are set, just select Base tool to assign. You can use D&amp;D or double click on a tool from Base list.<br>
4- The tool are assigned on top middle area. The order is important here. You can change order using D&amp;D, context menu, or tool bar buttons.<br>
5- when you select an assigned tool, the top right area show tool settings relevant. each tool can has a customized settings.<br>
6- You can create more than one queue with different images to process ad different assigned tool list.<br>
7- on the bottom left, you can set current queue settings. This is not Tools settings. It's general settings for queue as the place where target files must be created or the behavior to use if target file already exist.<br>
8- When all is ready, press Run button, and take a coffee...</p>
<p>That all. </p>
<p>digiKam</p>
         </div>
    <div class="links">» </div>
  </div>
<a id="comment-18480"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/427#comment-18480" class="active">WoW GOLD</a></h3>    <div class="submitted">Submitted by <a href="http://www.superpowerleveling.com" rel="nofollow">wow power leveling</a> (not verified) on Thu, 2009-04-30 03:12.</div>
    <div class="content">
     <p>you can get <a href="http://www.wotlk-powerleveling.com">wow power leveling</a> and <a href="http://www.wotlkgold.net/faq.asp">wow gold</a> wow power leveling</p>
         </div>
    <div class="links">» </div>
  </div>
</div></div><a id="comment-18321"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/427#comment-18321" class="active">Well, the non-technical user</a></h3>    <div class="submitted">Submitted by Anonymous (not verified) on Sat, 2009-03-07 18:00.</div>
    <div class="content">
     <p>Well, the non-technical user is not doing anything what this tool is for. This is for professionals who need to maintain big amount of photos for different places etc. </p>
<p>But, even from that. This manager is as easy to use than others and it is very powerfull (not tested, just what now saw on the screenshots) so.</p>
         </div>
    <div class="links">» </div>
  </div>
<a id="comment-18479"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/427#comment-18479" class="active">WoW GOLD</a></h3>    <div class="submitted">Submitted by <a href="http://www.igsstar.com" rel="nofollow">wow power leveling</a> (not verified) on Thu, 2009-04-30 03:11.</div>
    <div class="content">
     <p>The World Leading <a href="http://www.wow-powerlevel.net">wow power leveling</a> and <a href="http://www.eing.com/PLindex.aspx">wow gold</a> wow power leveling</p>
         </div>
    <div class="links">» </div>
  </div>
</div><a id="comment-18302"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/427#comment-18302" class="active">Grats, Gilles, that's what</a></h3>    <div class="submitted">Submitted by Stefano (not verified) on Wed, 2009-03-04 14:04.</div>
    <div class="content">
     <p>Grats, Gilles, that's what all of us were waiting for.</p>
<p>Just a dumb question: does it always work on copies of the original files?</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-18304"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/427#comment-18304" class="active">Both...</a></h3>    <div class="submitted">Submitted by digiKam on Wed, 2009-03-04 14:30.</div>
    <div class="content">
     <p>If you set target album as original, and you don't change file formats (using convert to PNG/TIFF/JPEG for ex.) and file names (using renaming rules settings), original files will be overwritten. There is a settings to ask to user if files must be overwritten if they already exists.</p>
<p>digiKam</p>
         </div>
    <div class="links">» </div>
  </div>
</div>
</div>
