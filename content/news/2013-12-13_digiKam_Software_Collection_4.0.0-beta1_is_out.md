---
date: "2013-12-13T18:28:00Z"
title: "digiKam Software Collection 4.0.0-beta1 is out.."
author: "digiKam"
description: "Dear all digiKam fans and users! digiKam team is proud to announce the first beta release of digiKam Software Collection 4.0.0. This version currently under"
category: "news"
aliases: "/node/708"

---

<a href="http://www.flickr.com/photos/digikam/11355012336/" title="digiKam4.0.0-beta1 by digiKam team, on Flickr"><img src="http://farm6.staticflickr.com/5514/11355012336_d0c8dd6583_c.jpg" width="800" height="225" alt="digiKam4.0.0-beta1"></a>

<p>Dear all digiKam fans and users!</p>

<p>digiKam team is proud to announce the first beta release of digiKam Software Collection 4.0.0.
This version currently under development, including a new tool dedicated to organize whole tags hierarchy. This new feature is relevant of <a href="http://www.google-melange.com/gsoc/proposal/review/google/gsoc2013/slavuttici/35001">GoSC-2013 project</a> from Veaceslav Munteanu. This project include also a back of Nepomuk support in digiKam broken since a while due to important KDE API changes. Veaceslav has also implemented multiple selection and multiple drag-n-drop capabilities on Tags Manager and Tags View from sidebars, and the support for writing face rectangles in Windows Live Photo format.</p>

<p>Another feature introduced in this release is a new maintenance tool dedicated to parse image quality and auto-tags items automatically using Pick Labels. This tool is relevant to another <a href="http://www.google-melange.com/gsoc/proposal/review/google/gsoc2013/gwty/1">GoSC-2013 project</a> from Gowtham Ashok. This tool require feedback and hack to be finalized for this release.</p>

<p>Other pending GoSC-2013 projects will be included in next 4.0.0 beta release, step by step, when code will be considerate as good quality, and ready to test by end users. See the <a href="http://community.kde.org/Digikam/GSoC2013#Roadmap_and_Releases_Plan_including_all_GSoC-2013_works">release plan</a> for details.</p>

<p>As usual with a major release, we have a long list of <a href="https://bugs.kde.org/buglist.cgi?f1=cf_versionfixedin&amp;o1=equals&amp;query_format=advanced&amp;bug_status=RESOLVED&amp;bug_status=NEEDSINFO&amp;bug_status=VERIFIED&amp;bug_status=CLOSED&amp;v1=4.0.0&amp;product=digikam&amp;product=digikamimageplugins&amp;product=kipiplugins&amp;product=showfoto">files closed</a> with this release into KDE bugzilla.</p>

<p>digiKam software collection tarball can be downloaded from <a href="http://download.kde.org/unstable/digikam/digikam-4.0.0-beta1.tar.bz2.mirrorlist">KDE repository</a></p>

<p>This version is for testing purposes. <b>Please do not use it yet in production!</b></p>

<p>Happy digiKam testing...</p>
<div class="legacy-comments">

  <a id="comment-20661"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/708#comment-20661" class="active">I just wanted to thank all</a></h3>    <div class="submitted">Submitted by Anonymous (not verified) on Sat, 2013-12-14 23:28.</div>
    <div class="content">
     <p>I just wanted to thank all digikam contributors. Since i've started following KDE digikam has consistently improved. It's a great piece of quality software and i wish you all the best to keep going strong.</p>
         </div>
    <div class="links">» </div>
  </div>
<a id="comment-20662"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/708#comment-20662" class="active">Windows Beta</a></h3>    <div class="submitted">Submitted by Peter (not verified) on Thu, 2013-12-19 11:30.</div>
    <div class="content">
     <p>If anybody could provide a Windows build I'd be happy to test the beta and report issues. I have seen a number of problems with the 3.4 version but wanted to wait for 4.0 beta to see whether they are fixed.</p>
<p>The concept and possibilities of digiKam are by far the best I could find. Unfortunately, it crashes for me on Windows (face detection, local contrast, ...).</p>
<p>Thanks, Peter</p>
<p>PS: I know I could build it myself but it's been too long since I have plowed through dependencies and such.</p>
         </div>
    <div class="links">» </div>
  </div>
<a id="comment-20663"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/708#comment-20663" class="active">A bit of  aid</a></h3>    <div class="submitted">Submitted by Pedro Rodrigues (not verified) on Sun, 2013-12-29 23:30.</div>
    <div class="content">
     <p>First of all I must follow the previous commenter in thanking for the fantastic work done in digikam.</p>
<p>I also wish to request a bit of aid in preparing some bug reports.<br>
I am runing digikam 3.5.0 in debian jessie (testing) and I found two abnormal behaviors that I was not yet able to confirm if they are due to a packaging problem or an upstream bug. I need to confirm this in order to properly address and report the bugs.</p>
<p>The first one is:<br>
I tried digikam 3.5 from experimental in jessie/testing mostly to avoid the face recognition crash bug in 3.4. However I find it very unstable in the face recognition behavior, even more then 3.4. It even managed to (consistently) crash my box. Other odd behavior is that tag autocompletion in face recognition interface does not wok. It presentes tags list compatible with typed prefix, however, it is inpossible to choose from the list.<br>
I tested the distributed deb binaries and also deb binaries generated from deb source package.</p>
<p>The other is:<br>
After upgrading some libraries, I found the raw import tool presenting negative values to the shift(linear) parameter.<br>
I restored it to a zero value and the images produced are a lot overexposed. Up to that upgrade, overexposing started near values 1 and 2 of this parameter. Now this interval is shifted nearly to -1.5 -0.5.<br>
It seems to be doing the correct processing but the values are strange. </p>
<p>Do someone experienced these behaviors in other distros that not debian?<br>
It is also important to know if some of these behaviours are present in 4.0 that I ad not the oportunity to test yet.</p>
<p>Thank you in advance for any help provided</p>
<p>Pedro</p>
         </div>
    <div class="links">» </div>
  </div>
<a id="comment-20664"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/708#comment-20664" class="active">At last this beta version is</a></h3>    <div class="submitted">Submitted by <a href="http://www.taamirplus.com/" rel="nofollow">Taamir Azka</a> (not verified) on Thu, 2014-01-09 23:44.</div>
    <div class="content">
     <p>At last this beta version is released for testing. Thanks digiKam. Will be back with some review about this beta version</p>
         </div>
    <div class="links">» </div>
  </div>

</div>
