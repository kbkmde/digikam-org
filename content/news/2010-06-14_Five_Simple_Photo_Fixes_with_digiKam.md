---
date: "2010-06-14T22:28:00Z"
title: "Five Simple Photo Fixes with digiKam"
author: "Dmitri Popov"
description: "digiKam is an immensely powerful photo application, so learning all its features requires time and effort. But this capable photo management application also offers a"
category: "news"
aliases: "/node/523"

---

<p>digiKam is an immensely powerful photo application, so learning all its features requires time and effort. But this capable photo management application also offers a few easy to use features which you can use to instantly improve your shots. <a href="http://www.linux-magazine.com/Online/Blogs/Productivity-Sauce-Dmitri-s-open-source-blend-of-productive-computing/Five-Simple-Photo-Fixes-with-digiKam">Continue to read</a></p>

<div class="legacy-comments">

</div>