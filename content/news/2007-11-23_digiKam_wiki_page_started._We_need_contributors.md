---
date: "2007-11-23T21:13:00Z"
title: "digiKam wiki page started. We need contributors..."
author: "digiKam"
description: "A wiki allows users to easily create, edit and link web pages. Wikis are used for collaborative websites, power community websites, and are gaining high"
category: "news"
aliases: "/node/274"

---

<p>A wiki allows users to easily create, edit and link web pages. Wikis are used for collaborative websites, power community websites, and are gaining high interest from commercial companies to provide affordable and effective intranets. It is all about <strong>knowledge management</strong>.</p>
<p>Major open-source projects have a wiki page - now digiKam has one as too... </p>
<p>It waits for your contribution, so please add them <a href="http://userbase.kde.org/Digikam">here...</a></p>
<p>Thanks in advance for your help!</p>

<div class="legacy-comments">

</div>