---
date: "2010-05-11T13:54:00Z"
title: "GSoC: Reverse Geocoding and Improved Map Search project for digiKam"
author: "Gabriel Voicu"
description: "Hello to all digikam and KDE fans! My name is Gabriel Voicu, I am 20 years old and I come from Bucharest, Romania where I'm"
category: "news"
aliases: "/node/515"

---

<p>Hello to all digikam and KDE fans!
</p>

<p>
My name is Gabriel Voicu, I am 20 years old and I come from Bucharest, Romania where I'm studying Computer Science and Engineering at University Politehnica of Bucharest. I enjoy programming, playing basket-ball and spending time with my girlfriend.
</p>

<p>
I was very happy when I saw that I was accepted to this year Google Summer of Code with the project "Reverse Geocoding and Improved Map Search" because I consider this feature very useful when talking about images geotagging. 
</p>

<p>
Since I've been accepted, I studied the documentation, made an UI proposal and made the tabs from the Correlator2 Widget collapse and expand when an user clicks the same tab. It felt very good to see it working, because it was the first time when I put my code into a really big project. 
</p>

<p>
Now let me explain a little bit about this project. It has 4 parts, but now I will write in detail only about the first quarter. Here, I should make a Reverse Geocoding Widget who will give the user the possibility to add human-readable information such as country, city and street into his images metadata, based on already existing GPS coordinates. I will use backends from Google Maps, Open Street Map Nominatim or geonames.org to retrieve the information, then I will store it into the XMP or IPTC. With information located into the images metadata, the user will have an option to automatically tag his images by location. The Reverse Geeocoding Widget will be integrated into the already existing Correlator2 Widget developed by Michael G. Hansen (who is also my mentor) and the rest of the team. I've put here an UI proposal, so if you have any idea about an improvement, please let me know. The project has also a wiki page <a href="http://community.kde.org/Digikam/GSoC2010/ReverseGeocoding"> here. </a> 
</p>

<a href="http://www.flickr.com/photos/digikam/4598028879/" title="Reverse Geocoding Widget by digiKam team, on Flickr"><img src="http://farm2.static.flickr.com/1264/4598028879_26a74c41f0.jpg" width="477" height="500" alt="Reverse Geocoding Widget"></a>

<p>
The other three parts from this project would be: improving of the Worldmap Widget, adding "search by location" into the Advances Search Tool and adding the option to display images directly on the map instead of the icon-based virtual album.
</p>

<p>
I'm very happy to work on a KDE project and I'm looking forward to continue working with digiKam developers because they are doing a very good job and also, they are very friendly people.
</p>

<div class="legacy-comments">

  <a id="comment-19179"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/515#comment-19179" class="active">Exciting Stuff!</a></h3>    <div class="submitted">Submitted by <a href="http://krita.org" rel="nofollow">Kubuntiac</a> (not verified) on Tue, 2010-05-11 15:58.</div>
    <div class="content">
     <p>Anything that helps me keep the data about my thousands of images complete, is very welcome indeed! Congratulations on getting selected, getting started and  getting blogging! :) Looking forward to playing with this!</p>
         </div>
    <div class="links">» </div>
  </div>
<a id="comment-19180"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/515#comment-19180" class="active">Bravo frate</a></h3>    <div class="submitted">Submitted by <a href="http://www.revistapadurilor.ro" rel="nofollow">Rumanu</a> (not verified) on Tue, 2010-05-11 15:59.</div>
    <div class="content">
     <p>eu chiar folosesc digikam</p>
         </div>
    <div class="links">» </div>
  </div>
<a id="comment-19182"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/515#comment-19182" class="active">Good luck!</a></h3>    <div class="submitted">Submitted by <a href="http://www.FreezingMoon.org" rel="nofollow">Dread Knight</a> (not verified) on Wed, 2010-05-12 00:48.</div>
    <div class="content">
     <p>Mult noroc cu proiectul :-)</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-19186"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/515#comment-19186" class="active">Thank you</a></h3>    <div class="submitted">Submitted by Gabriel Voicu on Wed, 2010-05-12 18:40.</div>
    <div class="content">
     <p>Thank you for congrats, I hope that you will like and use my work.</p>
         </div>
    <div class="links">» </div>
  </div>
</div><a id="comment-19183"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/515#comment-19183" class="active">geotag is a good example to copy</a></h3>    <div class="submitted">Submitted by philippe (not verified) on Wed, 2010-05-12 12:51.</div>
    <div class="content">
     <p>geotag gui is very handy<br>
http://geotag.sourceforge.net/</p>
<p>today i prefer to use geotag (not for correlating but the other features : reverse geocoding, accurate geotag)</p>
<p>why i use geoatag because it's gui is good for mass work<br>
it's a good idea to use a table</p>
<p>please experiment geotag you'll discover a handy gui to make mass work</p>
<p>it's easy :<br>
- you can run it without installing it<br>
http://geotag.sourceforge.net/?q=node/3</p>
<p>- you can run it by installing it<br>
. to download<br>
http://sourceforge.net/projects/geotag/files/<br>
. to launch it<br>
for example<br>
/usr/bin/java -Xmx256M -jar ~/bin/geotag/geotag.jar</p>
<p>addendum :<br>
- you need exiftool (there is a mandriva packet)</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-19184"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/515#comment-19184" class="active">Geotagging</a></h3>    <div class="submitted">Submitted by Fri13 on Wed, 2010-05-12 13:19.</div>
    <div class="content">
     <p>I think best way to geotaggin happends right from the album interface like other tagging. </p>
<p>You select photos and then you drop them to wanted place on the map. The cursor changes as a cross for accurate tagging. And maybe the map panel could show the already placed positions so you can easily drop on them to have same and you can see what was the last place where you tagged photos.</p>
<p>There could be a extra dialoge for more accurate tagging, with the table. But the geotagging itself should be very drag'n'drop for those who does not have GPS location information. As far I have tested the Geotag, it does not feel to be so handy.</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-19188"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/515#comment-19188" class="active">hello
1. i speak only about</a></h3>    <div class="submitted">Submitted by philippe (not verified) on Thu, 2010-05-13 08:10.</div>
    <div class="content">
     <p>hello</p>
<p>1. i speak only about reverse geotagging<br>
- i don't speak about correlating which is good in digikam (better than geotag)<br>
- i don't speak about geotaging with a map/satellite view which is not good in digikam (geotag is better, i send a bug wish report about that<br>
https://bugs.kde.org/show_bug.cgi?id=236533</p>
<p>2. i speak about the gui of the reverse geocoding to make mass work<br>
i just say when you make mass work without using a batch a table is better, a screen wholy dedicated to the table is better than i can see on the screenshot.</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-19189"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/515#comment-19189" class="active">Okay, there was the mistake.</a></h3>    <div class="submitted">Submitted by Fri13 on Thu, 2010-05-13 22:00.</div>
    <div class="content">
     <p>Okay, there was the mistake. For me the geotagging includes the reverse geotagging (automatically searching the location name for the GPS coordinates). Yes, there is difference for having just the pin in the map showing how many photos there is. But same time I always add automatically tags for place so I can find the area from tag-tree itself if I can not remember the place on the map, but just the place name (and vice versa).</p>
<p>By my opinion, all this should happend totally automatically. That user just imports the photos when there is GPS locations in them (by camera) and they get automatically placed to map searches or generates place names in tag-tree under "Places" or what user has selected. (It has some problems like if user want specific order).<br>
Or when user just imports photos and tag them itself or import the GPS location from other device (the external GPS locator). </p>
<p>The UI should be so easy that normal user just types the place name or points the position in map and all the technical stuff is done by the digiKam itself. Even if there ain't automatically added tags to tag-tree, the photo itself would include the EXIF (etc) metadata what could be used to show photos when user just types search word "city_name".</p>
<p>That is something what we really can do very much automatically accurate. And we only end up to have automatic context tagging so user would not need to tag photos anymore anyway. Like digiKam would reconize if there is a horse or lion in the photo. Or is the photo a landscape photo or is it a documentary photo (that is impossible, because the context will change it such way that not even most people understand it).</p>
<p>I expect lots of nice things to come from this GSOC year and all those projects sounds very promising. I have spended many hours just for designing different kind UI's and functions for geotagging and I have tried always take the position of user who just has learn how to import photos to computer. In Finland there was very long time ago a acronym "ATK" what meant "Automatical Information Management". It was dropped many years ago because it did not reflect the real situation in the computer environments. No matter how much we improved the computers, we always ended to situation where there was very big need for human to be part of the information management. It was never automatic.</p>
<p>What I hope to see one day from digiKam, is that I could just plug the memorycard (reader) to computer, digiKam would automatically start importing photos, it would automatically tag the persons in the photos, check if photos were taken in important holiday (birthdays, christmas, vacation in calender etc) and tag with them. Tag the GPS information (if such exist) and add the places names. And lots of other things. So that I could just in the end just come back to take/switch the card when digiKam says it can be ejected. And then when I have time, I could take a round to rate photos and possible add locations (just by dropping them over the map if there is no GPS information) and still as much as possible the process would be automatic.</p>
<p>The UI is very important for the normal user. But first we need to get the technology really in the use. That is the hard part.</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-19199"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/515#comment-19199" class="active">I agree with you</a></h3>    <div class="submitted">Submitted by Gabriel Voicu on Sun, 2010-05-16 02:02.</div>
    <div class="content">
     <p>Hello Fri13,</p>
<p>I agree with you when talking about fully automation. Everybody wants that and I'm pretty sure that almost all this features mentioned by you will be found in digiKam in some time. I said almost because, as you said, I don't think that there will come a time where the human factor will disappear. All can we do is try to minimize his efforts, but never reach the zero limit. </p>
<p>For example, I cannot automate the control where the user selects where the resulted location tags will be integrated in the tag tree. What can I do instead is minimize the number of clicks or put a voice recognition instead and let him spell the tag name.</p>
<p>In conclusion, I will try to automate as much as I can the reverse geocoding process and I'm trying to design an interface that can be intuitive to a new user. Hope that you will like it or come with further proposals, because my purpose is to make a quality product not "just an UI".</p>
         </div>
    <div class="links">» </div>
  </div>
</div><a id="comment-19200"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/515#comment-19200" class="active">Hello philippe</a></h3>    <div class="submitted">Submitted by Gabriel Voicu on Sun, 2010-05-16 02:00.</div>
    <div class="content">
     <p>Hello philippe,</p>
<p>1. Thank you for your comment and for your ideas. Since GPSSync2(KIPI Plugin which includes all the geolocation widgets) is still in production every idea is good welcomed. </p>
<p>2. The screenshot doesn't show the full quality of the GPSSync2, because it's only a sketch made by me to show you the Reverse Geocode Widget. I will make a more suggestive one and put it here as soon as possible.<br>
The UI is split between map and photo list  because one can drag-and-drop a photo on the map and then coordinates (and location tags) will be automatically added. Personally, I think that this is a more powerfull feature than an UI covered by a big table with all the images. But this is my point of view and in the end the user decides what product will use, so if you have more ideas please tell me to help me make a better tool for you. ;)</p>
         </div>
    <div class="links">» </div>
  </div>
</div></div><a id="comment-19185"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/515#comment-19185" class="active">Good ideas</a></h3>    <div class="submitted">Submitted by Gabriel Voicu on Wed, 2010-05-12 18:35.</div>
    <div class="content">
     <p>First of all, thank you both for ideas and for explanations.</p>
<p>@philipe:<br>
I had a look at geotag, it's nice, maybe I will take some ideas from there and optimize to make something even more user-friendly inside digiKam. Also, I will implement backends from 3 services: Google Maps, Open Street Map and Geonames.org, to give the user the possibility of choosing the most accurate results. </p>
<p>@Fri13:<br>
Well, the Correlator2 Widget has incorporated inside a world map(the white rectangle from the upper-left corner inside my user-interface proposal). The user has the possibility to drag-and-drop selected photos on the map and it puts automatically GPS Coordinates inside the photos. After that, if the user wants to apply Reverse Geocoding to his photos, it should go to "Reverse Geocoding" tab inside Correlator2 Widget, click 2-3 buttons and it will have his photos tagged by location. :D </p>
<p>However, I will take in consideration your idea, because it would make the service very easy to use.</p>
         </div>
    <div class="links">» </div>
  </div>
</div><a id="comment-19187"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/515#comment-19187" class="active">Nice!</a></h3>    <div class="submitted">Submitted by eiapopeia (not verified) on Wed, 2010-05-12 22:32.</div>
    <div class="content">
     <p>I am really looking forward to the results of your work. Good Luck on your job!</p>
         </div>
    <div class="links">» </div>
  </div>

</div>
