---
date: "2009-09-19T13:16:00Z"
title: "New renaming tool for digiKam"
author: "Andi Clemens"
description: "In the past renaming images in digiKam was not very powerful. Most users (me too) renamed their images with KRename, a quite powerful batch renamer."
category: "news"
aliases: "/node/480"

---

<p><a href="http://www.flickr.com/photos/26732399@N05/3933368807/" title="advancedRenameWdget von andiclemens bei Flickr"><img src="http://farm4.static.flickr.com/3436/3933368807_b3f423f081.jpg" width="500" height="100" alt="advancedRenameWdget"></a></p>
<p>In the past renaming images in digiKam was not very powerful. Most users (me too) renamed their images with KRename, a quite powerful batch renamer. But calling KRename from the "Open With..." dialog in digiKam was not very intuitive, it just felt wrong.</p>
<p>digiKam can manage nearly every piece of information found in an image, therefore it would be nice to have all this information available for file renaming, too, since renaming is one of the basic managing tasks.<br>
Sure there is a KIPI plugin for batch renaming, but this plugin is not very powerful and has no access to the digiKam database.</p>
<p>So I started to implement a new utility for digiKam: AdvancedRename. The goal of this tool is quite simple:<br>
Allow the user to enter EVERY possible information digiKam might have about an image and let the user decide in which order these information are added to the file name.</p>
<p>The easiest way to do this is to have a line edit widget, that accepts special characters and replaces them with the parsed information for every image, in a same way KRename does it.<br>
The problem:<br>
Although very powerful, it can become quite annoying entering all those token characters by hand. A token should be added with a single click, the line edit then receives its focus back and the user can continue to type in other information.<br>
Tokens that can take parameters should have a config dialog, so that in the best case the user doesn't have to remember any parameters at all.</p>
<p>Another important goal was to be able to modify each parsed information afterwards, for example to remove unwanted strings from file or directory names. But unlike in KRename, these modifiers should be chainable and be applied to any token, not only the file name.<br>
So after fiddling around for many weeks, the first version of AdvancedRename is ready to use.</p>
<p></p><h3>Its features so far:</h3><ul><li>Add information about file, directory and camera names<br>
	</li><li>Add date time information<br>
	</li><li>Add sequence numbers<br>
	</li><li>Add metadata keys (<b>Exif</b>, <b>IPTC</b> and <b>XMP</b> are supported and can be selected with a searchable dialog)<br>
	</li><li>Modify any parse result with chainable modifiers (lowercase, uppercase, first letter of each word uppercase, trimmed, simplified, range, replace...)<br>
	</li><li>Easily add the tokens with quick access buttons, either through a buttonbox or a toolbutton menu (depending on the style settings)<br>
	</li><li>Syntax Highlighting of options in renaming string<br>
</li></ul><p></p>
<p><a href="http://www.flickr.com/photos/26732399@N05/4080328746/" title="tooltip.png von andiclemens bei Flickr"><img src="http://farm4.static.flickr.com/3092/4080328746_55c0520d5f_m.jpg" width="240" height="156" alt="tooltip.png" style="margin:10px;"></a></p>
<p>It is by far not perfect (what is perfect anyway ;-)), but most of the renaming goals should be convertible already.<br>
</p><h3 class="western">TODO:</h3><ul><li>when a token or modifier is selected, enable an edit button to change the token with a GUI<br>
	</li><li>add more parsers (image statistics, some more digiKam related information etc...)<br>
</li></ul><p></p>
<p></p><h3>Update: New UniqueModifier</h3><br>
I implemented a new modifier that should guarantee unique strings in a renaming option. It will append a suffix number if a string has already been assigned. This can be useful for example when used in combination with a [date] option. See the following example screenshot:<br>
<a href="http://www.flickr.com/photos/26732399@N05/4138381199/" title="UniqueModifier in digiKam von andiclemens bei Flickr"><img src="http://farm3.static.flickr.com/2513/4138381199_48e9e23e4f_m.jpg" width="240" height="213" alt="UniqueModifier in digiKam"></a> <p></p>
<p></p><h3>An example</h3><br>
<a href="http://www.flickr.com/photos/26732399@N05/4079568995/" title="replace.png von andiclemens bei Flickr"><img src="http://farm3.static.flickr.com/2441/4079568995_5f7780aac5_m.jpg" width="235" height="240" alt="replace.png" style="margin:10px;"></a> <a href="http://www.flickr.com/photos/26732399@N05/4079569301/" title="trimmed.png von andiclemens bei Flickr"><img src="http://farm3.static.flickr.com/2754/4079569301_e07c7fa2de_m.jpg" width="240" height="183" alt="trimmed.png" style="margin:10px;"></a> <a href="http://www.flickr.com/photos/26732399@N05/4121367571/" title="Syntax Highlighting in digiKam rename tool von andiclemens bei Flickr"><img src="http://farm3.static.flickr.com/2599/4121367571_852cd83250_m.jpg" width="222" height="240" alt="Syntax Highlighting in digiKam rename tool"></a><p></p>
<p>These screenshots show how modifiers can be chained. First we replace the word "Amazing" from the filename with "Boring" (notice the extra whitespace we added in this example). Then we apply a TrimmedModifier to remove leading, trailing and extra whitespace again. Finally an UppercaseModifier is applied.<br>
Every token can be chained with modifiers. If you use modifier strings somewhere else in the renaming string, they will be displayed as normal text.</p>

<div class="legacy-comments">

</div>