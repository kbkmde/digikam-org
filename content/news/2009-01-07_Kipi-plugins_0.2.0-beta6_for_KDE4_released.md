---
date: "2009-01-07T15:32:00Z"
title: "Kipi-plugins 0.2.0-beta6 for KDE4 released"
author: "digiKam"
description: "Happy New Year !!! The 6th KDE4 beta release of digiKam plugins box is out. With this new release, a new plugin named FbExport is"
category: "news"
aliases: "/node/419"

---

<p>Happy New Year !!!</p>
<p>The 6th KDE4 beta release of digiKam plugins box is out.</p>

<a href="http://www.flickr.com/photos/digikam/3169600865/" title="FaceBookExport-kipiplugin-KDE4 by digiKam team, on Flickr"><img src="http://farm2.static.flickr.com/1181/3169600865_dd55c4fced.jpg" width="500" height="400" alt="FaceBookExport-kipiplugin-KDE4"></a>

<p>With this new release, a new plugin named <b>FbExport</b> is born. It's dedicated to export images to <a href="http://www.facebook.com">FaceBook web service</a>.</p>

<p>Another plugin dedicated to export images to <a href="http://www.smugmug.com">SmugMug web service</a> is also available since beta5.</p>

<p>A plugin is available to batch convert RAW camera images to Adobe DNG container (see <a href="http://en.wikipedia.org/wiki/Digital_Negative_(file_format)">this Wikipedia entry</a> for details). To be able to compile this plugin, you need last libkdcraw from svn trunk (will be released with KDE 4.2). To extract libraries source code from svn, look at <a href="http://www.digikam.org/download?q=download/svn#checkout-kde4">this page</a> for details.</p>

<p>Kipi-plugins are compilable under Windows using MinGW and Microsoft Visual C++. Precompiled packages are available with KDE-Windows installer. See <a href="http://windows.kde.org">KDE-Windows project</a> for details.</p>

<p>See below the list of new features and bugs-fix coming with this release:</p>

<b>General</b>        : Port to CMake/KDE4/QT4.<br>
<b>General</b>        : all plugins can be compiled natively under Microsoft Windows.<br>
<b>JPEGLossLess</b>   : XMP metadata support.<br>
<b>RAWConverter</b>   : XMP metadata support.<br>
<b>TimeAdjust</b>     : XMP metadata support.<br>
<b>MetadataEdit</b>   : XMP metadata support with EXIF, IPTC, and Comments editor to sync XMP tags.<br>
<b>MetadataEdit</b>   : IPTC Editor dialog is more compliant with IPTC.org recommendations.<br>
<b>MetadataEdit</b>   : IPTC Editor add the capability to set multiple values for a tag.<br>
<b>SendImages</b>     : Complete re-write of emailing tool.<br>
<b>GPSSync</b>        : New tool to edit GPS track list using googlemaps.<br>
<b>RAWConverter</b>   : Raw files are decoded in 16 bits color depth using same auto-gamma and auto-white  methods provided by dcraw with 8 bits color depth RAW image decoding.<br>
<b>SlideShow</b>      : Now filenames, captions and progress indicators can have transparent backgrounds with OpenGL SlideShow as well.<br>
<b>SlideShow</b>      : Added thumbnails into image list.<br>
<b>SlideShow</b>      : Now you can play your favourite music during slideshow.<br>
<b>DNGConverter</b>   : New plugin to convert RAW camera image to Digital NeGative (DNG).<br>
<b>RemoveRedEyes</b>  : New plugin to remove red eyes in batch. Tool is based on OpenCV library.<br>
<b>Calendar</b>       : Ported to QT4/KDE4.<br>
<b>Calendar</b>       : Support RAW images.<br>
<b>AcquireImages</b>  : Under Windows, TWAIN interface is used to scan image using flat scanner.<br>
<b>SlideShow</b>      : Normal effects are back.<br>
<b>SlideShow</b>      : New effect "Cubism".<br>
<b>SlideShow</b>      : Added support for RAW images.<br>
<b>DNGConverter</b>   : plugin is now available as a stand alone application.<br>
<b>SlideShow</b>      : New effect "Mosaic".<br>
<b>PrintWizard</b>    : Ported to QT4/KDE4.<br>
<b>PrintWizard</b>    : Added a new option to skip cropping, the image is scaled automatically.<br>
<b>RemoveRedEyes</b>  : Added an option to preview the correction of the currently selected image.<br>
<b>SmugExport</b>     : New plugin to export images to a remote SmugMug web service (http://www.smugmug.com).<br>
<b>FbExport</b>       : New plugin to export images to a remote Facebook web service (http://www.facebook.com).<br>
<b>FlickrExport</b>   : Add support of Photosets.<br>
<b>SmugExport</b>     : Add support for Album Templates.<br><br>

001 ==&gt; 135451 : GPSSync            : Improve the gui of the gpssync plugin.<br>
002 ==&gt; 135386 : GPSSync            : Show track on the google map.<br>
003 ==&gt; 149497 : GPSSync            : Geolocalization kipi plugin does not work with non-jpeg file types.<br>
004 ==&gt; 145746 : GPSSync            : Do not recreate thumbs when geolocalizing images.<br>
005 ==&gt; 158792 : GPSsync            : Plugin do not working with Canon RAW CR2.<br>
006 ==&gt; 165078 : GPSSync            : GPS correlator does not show thumbnails.<br>
007 ==&gt; 165278 : GPSSync            : Geotagging dialog doesn't display all thumbnails.<br>
008 ==&gt; 134299 : SimpleViewerExport : Read orientation of image from EXIF.<br>
009 ==&gt; 150076 : SlideShow          : Playing music during a slideshow.<br>
010 ==&gt; 172283 : SlideShow          : SlideShow crashes host application.<br>
011 ==&gt; 172337 : SlideShow          : Slideshow enhancements by ken-burns effects.<br>
012 ==&gt; 173276 : SlideShow          : digiKam crashs after closing.<br>
013 ==&gt; 172910 : GalleryExport      : Gallery crash (letting crash digiKam) with valid data and url.<br>
014 ==&gt; 161855 : GalleryExport      : Remote Gallery Sync loses password.<br>
015 ==&gt; 154752 : GalleryExport      : Export to Gallery2 seems to fail, but in fact works.<br>
016 ==&gt; 157285 : SlideShow          : digiKam advanced slideshow not working with Canon RAWs.<br>
017 ==&gt; 175316 : GPSSync            : digiKam crashes when trying to enter gps data.<br>
018 ==&gt; 169637 : MetadataEdit       : EXIF Data Editing Error.<br>
019 ==&gt; 174954 : HTMLExport         : Support for remote urls.<br>
020 ==&gt; 176640 : MetadataEdit       : Wrong reference to website.<br>
021 ==&gt; 176749 : TimeAdjust         : Incorrect time/date setting with digiKam.<br>
022 ==&gt; 165370 : GPSSync            : crash when saving geocoordinates.<br>
023 ==&gt; 165486 : GPSSync            : Next and Previous photo button for editing geolocalization coordinates.<br>
024 ==&gt; 175296 : GPSSync            : Geolocalization a handy gui to manage all about geotagging.<br>
025 ==&gt; 152520 : MetadataEdit       : Unified entry of metadata.<br>
026 ==&gt; 135320 : Calendar           : Calendar year do not match locale.<br>
027 ==&gt; 116970 : Calendar           : New calendar generator plugin features.<br>
028 ==&gt; 177999 : General            : Kipi-plugins Shortcuts are not listed in the shortcuts editor from Kipi host application.<br>
029 ==&gt; 160236 : JPEGLossLess       : Image Magick convert operations return with errors.<br>
030 ==&gt; 144183 : RawConverter       : Path in RAW-converter broken with the letter "ß"<br>
031 ==&gt; 178495 : MetadataEdit       : Problem with metadata editor.<br>
032 ==&gt; 175219 : PicasaWebExport    : Album creating wrong date in picasaweb.<br>
033 ==&gt; 175222 : PicasaWebExport    : Unable to create non listed album.<br>
034 ==&gt; 166672 : FbExport           : Export to Facebook.<br>
035 ==&gt; 129623 : FlickrExport       : Option to upload to specific photoset.<br>
036 ==&gt; 161775 : FlickrExport       : Photoset support.<br>

<div class="legacy-comments">

  <a id="comment-18101"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/419#comment-18101" class="active">Gilles, you wanted to say</a></h3>    <div class="submitted">Submitted by Lure (not verified) on Wed, 2009-01-07 17:56.</div>
    <div class="content">
     <p>Gilles, you wanted to say "KIPI plugins" in first statement.<br>
And thanks for release!</p>
         </div>
    <div class="links">» </div>
  </div>
<a id="comment-18102"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/419#comment-18102" class="active">Batch resize?</a></h3>    <div class="submitted">Submitted by wanthalf (not verified) on Wed, 2009-01-07 23:39.</div>
    <div class="content">
     <p>How about the batch resize plugin? I'd like to switch to 0.10, but without this one it's pretty useless for me...</p>
<p>(And 0.9 wants old KIPI which blocks new KIPI so that I cannot use kdegraphics from KDE4.)</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-18104"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/419#comment-18104" class="active">About Batch tools for KDE4...</a></h3>    <div class="submitted">Submitted by digiKam on Thu, 2009-01-08 08:13.</div>
    <div class="content">
     <p>Look my response in this <a href="http://www.digikam.org/node/411#comment-18077">this thread</a> for details...</p>
<p>digiKam</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-18111"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/419#comment-18111" class="active">I've probably heared it</a></h3>    <div class="submitted">Submitted by wanthalf (not verified) on Thu, 2009-01-08 20:36.</div>
    <div class="content">
     <p>I've probably heared it somewhere before, thanks. So it will become a feature of digikam core, not a plugin anymore? That is of course a great idea, but a bad news it won't come so soon, then. Maybe I should start resizing with imagemagick again, inbetween, don't know what is the difference in quality (I used Lanczos, but I am not sure it is the best one). My fault I cannot help you with this, but anyway good luck with this great project!</p>
         </div>
    <div class="links">» </div>
  </div>
</div></div><a id="comment-18103"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/419#comment-18103" class="active">kipi-plugins don't show up</a></h3>    <div class="submitted">Submitted by Nate (not verified) on Thu, 2009-01-08 04:18.</div>
    <div class="content">
     <p>Hi there, </p>
<p>I was able to follow the steps here (http://www.digikam.org/download?q=download/svn#checkout-kde4) with no problems. Everything installed to /opt and it seems to all be there. However, when I start digikam in /opt/kde4/share/applications/kde4 it loads but there are no kipi-plugins available. Am I missing a setting somewhere? Thanks!</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-18107"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/419#comment-18107" class="active">the solution</a></h3>    <div class="submitted">Submitted by Nate (not verified) on Thu, 2009-01-08 18:57.</div>
    <div class="content">
     <p>It ends up the plugins I had in /opt/kde4/share/kde4/services needed to be in /usr/share/kde4/services.</p>
         </div>
    <div class="links">» </div>
  </div>
</div><a id="comment-18139"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/419#comment-18139" class="active">Super</a></h3>    <div class="submitted">Submitted by <a href="http://www.seyretvideo.net" rel="nofollow">izle</a> (not verified) on Sat, 2009-01-10 20:53.</div>
    <div class="content">
     <p>This program super ! How much this orginal? I like this program Thank you very much.</p>
         </div>
    <div class="links">» </div>
  </div>
<a id="comment-18147"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/419#comment-18147" class="active">Bug in galleryexport plugin</a></h3>    <div class="submitted">Submitted by SOULfly_B (not verified) on Wed, 2009-01-14 18:51.</div>
    <div class="content">
     <p>Just a little comment to say that https://bugs.kde.org/show_bug.cgi?id=178204 is still valid.</p>
<p>Thanks for your amazing work.</p>
         </div>
    <div class="links">» </div>
  </div>

</div>
