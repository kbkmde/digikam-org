---
date: "2010-09-06T20:44:00Z"
title: "KDE Imaging Coding Sprint 2010"
author: "Martin Klapetek"
description: "Last week, from 27th to 29th August, developers of KDE Imaging group met for their third coding sprint organized by digiKam lead developer"
category: "news"
aliases: "/node/538"

---

<p><a href="http://www.flickr.com/photos/digikam/4620818767/" title="kdecodingsprint2010 by digiKam team, on Flickr"><img src="http://farm5.static.flickr.com/4031/4620818767_5fb7450bc0_m.jpg" width="240" height="105" alt="kdecodingsprint2010"></a></p>
<p style="text-align: justify;">Last week, from 27th to 29th August, developers of KDE Imaging group met for their third coding sprint organized by digiKam lead developer, in his hometown, the beautiful southern French city Aix-en-Provence. The time was shortly after the end of Google Summer of Code, in which digiKam participated  this year with three students. It was therefore a perfect timing for us GSoC students to get to know our mentors in person and get to work with them side-by-side.</p>
<p><a href="http://www.flickr.com/photos/digikam/4937547612/" title="IMG_0001_half.JPG by digiKam team, on Flickr" style="margin: 8px; float: right;"><img src="http://farm5.static.flickr.com/4118/4937547612_53a7bf9548_m.jpg" width="240" height="180" alt="IMG_0001_half.JPG"></a></p>
<p style="text-align: justify;">On Thursday morning, I met Andreas Huggel, who flew down from Malaysia. Andreas is Exiv2 developer and is working closely with Gilles on metadata support for digiKam. We went together for lunch during which we found out that it was the first event like this for both of us. A few hours later, we met two indian guys having absolved a 24 hour flight, Aditya Bhatt and Kunal Ghosh. Aditya is a GSoC student working on face detection and recognition for digiKam and Kunal is a Summer of Kode developer integrating an ECMAScript support into digiKam.</p>
<p style="text-align: justify;">In the Thursday evening, we met Michael G. Hansen and Marcel Wiesweg at the Maison des Associations Tavans, where we attended local the Linux User Group's meeting, watching Gilles giving a talkabout digiKam. Shortly after that, Gabriel Voicu, another GSoC student working on geotagging features for digiKam, was dropped by bus near Aix-en-Provence, no idea about his whereabouts, he sent us his GPS coordinates and we looked him up using Google Maps, so Gilles could pick him up by car.</p>
<p><a href="http://www.flickr.com/photos/digikam/4941061746/" title="DSC01566.JPG by digiKam team, on Flickr" style="margin: 8px; float: left;"><img src="http://farm5.static.flickr.com/4117/4941061746_2ef1ef24d6_m.jpg" width="240" height="160" alt="DSC01566.JPG"></a></p>
<p style="text-align: justify;">On Friday morning, we all met at hotel Paul's breakfast. We could finally put real faces to all the email addresses and IRC nicks. Any social barriers were very shortly overcome and chatty discussions were started, about cultural and national differencies, about our lives, school, work and of course KDE and digiKam. A very lively discussion continued during our way through the really nice city back to the Maison des Associations Tavans, which was also our "coding place". Friday was very hot and dry in Aix. But also very fruitful- in terms of work as well as in terms of awesome fruit markets.</p>
<p style="text-align: justify;">Kunal was working with Gilles on improving the scripting support in digiKam. The main idea is to add a new script interface for digiKam, based on QtScript API. This interface will provide a simple way to manage a list of custom scripts, dedicated to add new tools in Batch Queue Manager. The digiKam API will be exported and available to customized scripts. A settings widget have been designed by Kunal to setup scripts in Batch Queue Manager. Gilles and Kunal have reviewed GUI and current implementation to patch Batch Queue Manager with a new script view, assignable to items to process. This will be available in the upconimg 2.0 version.</p>
<p><a href="http://www.flickr.com/photos/digikam/4940482623/" title="IMG_0319.JPG by digiKam team, on Flickr" style="margin: 8px; float: right;"><img src="http://farm5.static.flickr.com/4142/4940482623_daa8db5cf1_m.jpg" width="180" height="240" alt="IMG_0319.JPG"></a></p>
<p style="text-align: justify;">Aditya was improving the face detection/recognition stuff, together with Marcel and Michael a discussion about ways to store face tags in metadata was begun. Aditya then worked hard with Marcel on polishing the face detection, debugging the database code, implementing the use of using internal tags, which are not visible to users, for storing face scanning related data. We also checked how other projects work, especially Apple's iPhoto with Andreas' MacBook. Aditya then also discussed the possibility of having a unified batch task manager. "Currently, most of the batch jobs in digiKam run in the GUI thread and make the UI slow, such as the fingerprint generator, metadata read/write, etc. We should have proper multithreading for this (my batch face detector partially implements this), and have a nice unified batch task manager, as one place to control all batch jobs.", he adds. Last but not least, Aditya did a lot of bugfixing in libraries used by face detection/recognition in digiKam - libface and libkface. All the code was then stress-tested on Gilles's whole photo collection. "It works pleasantly well, but also giving us a few directions for debugging (read: crashes)" says Marcel.</p>
<p style="text-align: justify;">
In the noon lunch-break, Gilles' wife and son joined us and we all enjoyed a fine French cuisine with Gilles explaining to us what food is what. "Then came the surprise, a menu card which I couldn't read and the menu which consisted of mainly non-vegetarian food. Well my bet on the lunch order paid off and I had a pleasant meal and also identified food I should keep away from :)", says Kunal. As work was waiting for us, we didn't stay too long and soon headed back to coding.</p>
<p style="text-align: justify;">
Andreas, besides a lot of coding on Exiv2 project, also discussed with Gilles support of new types of metadata for video files. Then with Marcel he analysed an issue with the algorithm to distinguish images and also got a first-hand update of the current metadata process flow in digiKam. "It was interesting to realize how pronounciation of words becomes important when you talk to each other face-to-face as opposed to exchanging emails and how much a bit of gesturing and scribbling on the black board helps to convey a message.", says Andreas.</p>
<p><a href="http://www.flickr.com/photos/digikam/4940505875/" title="IMG_0356.JPG by digiKam team, on Flickr" style="margin: 8px; float: right;"><img src="http://farm5.static.flickr.com/4115/4940505875_cba1500536_m.jpg" width="240" height="180" alt="IMG_0356.JPG"></a></p>
<p style="text-align: justify;">Friday evening was in the light of italian cuisine with the menu in Italian (not that it would make much of a difference from French to me). This was a great evening with great amount of talking about KDE and related stuff, but also Gnome, Linux, Microsoft, Android, computers, phones...and all that what you would expect from a bunch of developers. Also Laurent Espitalier, who drove down from Isére, France, joined us that evening.</p>
<p style="text-align: justify;">
Saturday we started again at hotel Paul with breakfast, during which we again discussed everything and nothing. Me and Aditya were stoked on FotoWall's animated GUI and we discussed some animation possibilities inside digiKam. We both agreed, that digiKam could use some more "live" UI. I've already started looking into Qt Animations framework and I have a few places in digiKam in mind, which were like directly created for animations. But first things first.</p>
<p><a href="http://www.flickr.com/photos/digikam/4941162720/" title="DSC01216.JPG by digiKam team, on Flickr" style="margin: 8px; float: left;"><img src="http://farm5.static.flickr.com/4093/4941162720_b5f3be41b2_m.jpg" width="240" height="180" alt="DSC01216.JPG"></a> </p>
<p style="text-align: justify;">I continued my work on image versioning support for digiKam and managed to extend the functionality of the 'Revert' button in the image editor. Now you can revert to the last saved version, and if this is a subversion of some original image, you can then revert to the original as well. With Marcel we solved hiding/showing all image versions except the current version using internal tags, we redefined the versioning concept a little bit and solved lots of bugs too. I found out, that even though it works ok on my computer, Gilles had some rather severe issues with it on his computer, so this brought me to lookup through the whole code, do lots of cleanups and create a simpler and more flexible design.</p>
<p style="text-align: justify;">
Michael worked together with Gabriel on improving the new search UI in the map views. During discussions with Marcel, these three seemed to solve the question of an intuitive workflow for searching, selecting, filtering, zooming and panning on the map, a functionality currently exposed by 12 buttons in a row. Together they came to a UI mockup and an intended workflow where a click willl usually do what the user may expect and images do not disappear, but are greyed out if filtered or unselected. "We're pretty confident that we found a good solution, so Gabriel and I talked about implementation details and Gabriel started coding on it.", says Michael. He also added two new things into GPSSync - a split map view, which shows off the model-view capabilities and a new configuration dialog. With Gilles, Michael got into discussion about how sidecar files should be handled.</p>
<p style="text-align: justify;">
In Saturday's noon-break, we went to discover the local markets, with plenty of colorful fruit and vegetables, beautiful flowers, crispy bread and baguettes, souvenirs, and piles of other stuff too - old vinyl and cassette music, historic newspaper from 1894, bars of soaps, silver cutlery, glass, dresses, hunting knives...you name it. During our walks through the city, we all took many pictures of this really beautiful city and its offerings, which you can find on <a href="http://www.flickr.com/groups/kde-imaging-codingsprint/pool/">flickr</a>. After quick lunch, we headed back and did more coding and polishing. Luckily it was quite windy that day, so the temperature wasn't as high as friday.</p>
<p><a href="http://www.flickr.com/photos/digikam/4937553022/" title="IMG_0012_half.JPG by digiKam team, on Flickr"><img src="http://farm5.static.flickr.com/4115/4937553022_475779d496_m.jpg" width="240" height="148" alt="IMG_0012_half.JPG"></a></p>
<p style="text-align: justify;">Laurent Espitallier uses and promotes digiKam at his work with one French government agency, where they use Nikon Project software for managing photos taken by team of photographers and which are later used in documentations. With digiKam, he's working on database import from this Nikon software, but also from applications such as iPhoto, Picasa or Aperture. Laurent and Gilles also discussed the importance of XMP sidecar files support for all read-only image formats, for example RAW files. With Nikon Project to digiKam migration, the goal is to be able to patch digiKam database with all Nikon<br>
keywords tree-view and all virtual folders created (in Nikon, Tags and Virtual folders are separated concepts). Only the first feature can be easily achieved when pictures are back-ported without a need to write a specific Nikon database parser. The images are just exported from Nikon collection using right options and then imported in digiKam.</p>
<p><a href="http://www.flickr.com/photos/50387249@N04/4941979798/" title="P1030886 by zzzahu, on Flickr" style="margin: 8px; float: left;"><img src="http://farm5.static.flickr.com/4073/4941979798_064030bd68_m.jpg" width="240" height="180" alt="P1030886"></a></p>
<p style="text-align: justify;">During the day, we had a refreshment in form of the famous Czech beer, which I brought with me and for which Gilles provided proper cooling. Also I can't forget the great French wines we tried during dinners. The Saturday's dinner was really great. We settled in a small quiet and very nice restaurant and as everybody was really enjoying it, no one felt any need to go back to sleep even though the time has advanced very close to the midnight.</p>
<p style="text-align: justify;">I was leaving on Sunday, shortly before noon, so I didn't have much chance to do any coding, but the rest, except Gabriel, who already left during the night, stayed and kept working. It was hard to say goodbye to such a great team after all that awesome time we spent together. I am really glad and very thankful that we could meet each other in person, for which I'd like to use Andreas' words: "It will make future team work easier and more fun". It is most certainly true and this would not be possible if it wasn't for KDE and especially KDE e.V. The most biggest thanks to the e.V. for making this happen. <b>Thank you!</b></p>

<div class="legacy-comments">

  <a id="comment-19504"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/538#comment-19504" class="active">When collecting ideas of face</a></h3>    <div class="submitted">Submitted by peter (not verified) on Mon, 2010-09-06 22:43.</div>
    <div class="content">
     <p>When collecting ideas of face detection gui, also have a look at picasa&gt;3.5 which also runs fine on current wine.</p>
         </div>
    <div class="links">» </div>
  </div>
<a id="comment-19505"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/538#comment-19505" class="active">Good work Martin ! a very</a></h3>    <div class="submitted">Submitted by kunal (not verified) on Tue, 2010-09-07 06:01.</div>
    <div class="content">
     <p>Good work Martin ! a very good log of our coding sprint :)</p>
<p>regards,<br>
Kunal</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-19507"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/538#comment-19507" class="active">Great report and great work!</a></h3>    <div class="submitted">Submitted by Lure (not verified) on Tue, 2010-09-07 09:01.</div>
    <div class="content">
     <p>Thank you very much for detail report and all the work you are doing. It is great to see many new names in Digikam/KIPI development team!</p>
         </div>
    <div class="links">» </div>
  </div>
</div><a id="comment-19508"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/538#comment-19508" class="active">Exiv2 Project report...</a></h3>    <div class="submitted">Submitted by digiKam on Tue, 2010-09-07 09:12.</div>
    <div class="content">
     Andreas Huggel, the lead Exiv2 project developer, contributing to Coding Sprint has written a small report about the event. <a href="http://dev.exiv2.org/boards/3/topics/608">Look there</a>         </div>
    <div class="links">» </div>
  </div>
<a id="comment-19516"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/538#comment-19516" class="active">UI animations</a></h3>    <div class="submitted">Submitted by Mark (not verified) on Fri, 2010-09-17 08:47.</div>
    <div class="content">
     <p>Digikam is a damn fine application and I appreciate all the developers' hard work but guys, this is a photo management program. It does not need the bloat of animations and/or a "live" UI. Just like KDE 3.5 didn't need all the eye-candy crap of KDE 4. Make improvements to the core functionality by all means, but don't waste your time and my RAM and CPU cycles on tarting up a UI that works fine as it is.</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-19546"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/538#comment-19546" class="active">keep options, over different interface</a></h3>    <div class="submitted">Submitted by Anonymouser Penguinista (not verified) on Mon, 2010-09-20 20:05.</div>
    <div class="content">
     <p>I have no problems with having more options but Amarok should be able to offer a simple look for newbies, mom and dad and another more involved UI for power users.<br>
All my parents need is to be able to DL their pictures in as little steps as possible which it does wonderfully. The problem is when they want to crop a picture or retouch the redeyes before they get their photos printed. My dad prefers to use Gvenview because those 2-3 functions that they use the most (crop pix and fix redeye) are easy for them to find.</p>
<p>Why not have an evolving UI that could serve the people who need to do the 5 or 10 most basic options (which often in software means the overwhelming majority) and with one click of a button, change to a more complex set of options for power users. Sometimes even power users dont need all the clutter and very often newbies learn enough that they want to let go off the training wheels and use all of the softwares potential.</p>
<p>I like when I use VLC how you can go to settings and view the Simple settings for the most used options or<br>
you can click Advanced (and honestly, Im scared of touching the damn things for fear of screwing up!)</p>
<p>But Digikam and Gvenview have made all our digital adventures a total pleasure and the  switch to Linux much more pleasant and I look forwards to more great stuff from the team.</p>
<p>Its nice to see the blog posts because it reminds users of all the work that goes in free software and even though some of us might become jaded and accustomed to the concept, we still need to take two secs and thank all the developers and others who make free software possible.<br>
My father has been asking more and more about free software, the GPL and his desktop (he cant tell the difference between the different distros, they all look the same to him) and he wants his updates to always appear because it gives him a sense of all the work (which he doesnt understand) that goes into making all the GNU components work.<br>
A post like this one about such a beloved and Just Works (well) app, helps bring a human face to the project.<br>
For the people, by the people.</p>
<p>Even years later, it never ceases to amaze me how it all works together.</p>
<p>All the best to all who help Digikam and KDE be good at what they do.<br>
Your dedication shows in your work.</p>
<p>AP</p>
         </div>
    <div class="links">» </div>
  </div>
</div><a id="comment-19562"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/538#comment-19562" class="active">Thanks</a></h3>    <div class="submitted">Submitted by <a href="http://www.marmaragundem.org" rel="nofollow">hastaneler</a> (not verified) on Tue, 2010-09-28 01:41.</div>
    <div class="content">
     <p>But Digikam and Gvenview have made all our digital adventures a total pleasure and the switch to Linux much more pleasant and I look forwards to more great stuff from the team.</p>
         </div>
    <div class="links">» </div>
  </div>

</div>
