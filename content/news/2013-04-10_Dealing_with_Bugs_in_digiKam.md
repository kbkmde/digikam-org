---
date: "2013-04-10T12:02:00Z"
title: "Dealing with Bugs in digiKam"
author: "Dmitri Popov"
description: "Bugs are inevitable in complex software, and digiKam is no exception. So what should you do when you've discovered a bug in your favorite photo"
category: "news"
aliases: "/node/691"

---

<p>Bugs are inevitable in complex software, and digiKam is no exception. So what should you do when you've discovered a bug in your favorite photo management application? As a non-programmer, the best thing you can do is to file the bug with the KDE bug tracking system (digiKam is managed as part of the KDE project). Submitting bugs can be considered a tedious task,&nbsp;but this greatly helps the developers to improve digiKam, and the KDE bug tracking system makes it relatively easy to file bugs and issues.</p>
<p><a href="http://scribblesandsnaps.com/2013/04/10/dealing-with-bugs-in-digikam/">Continue to read</a></p>

<div class="legacy-comments">

</div>