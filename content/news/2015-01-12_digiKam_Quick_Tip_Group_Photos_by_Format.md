---
date: "2015-01-12T10:38:00Z"
title: "digiKam Quick Tip: Group Photos by Format"
author: "Dmitri Popov"
description: "Using the View -> Group Images -> By Format command, you can group photos in albums by format. This feature can come in useful for"
category: "news"
aliases: "/node/728"

---

<p>Using the <strong>View -&gt; Group Images -&gt; By Format</strong>&nbsp;command, you can group photos in albums by format. This feature can come in useful for managing&nbsp;albums containing photos in different formats: JPEG, TIFF, RAW and video files, etc.</p>
<p><img src="https://scribblesandsnaps.files.wordpress.com/2015/01/group_by_format.png?w=605" alt="group_by_format" width="605" height="446"></p>
<p><a href="http://scribblesandsnaps.com/2015/01/12/digikam-quick-tip-group-photos-by-format/">Continue reading</a></p>

<div class="legacy-comments">

</div>