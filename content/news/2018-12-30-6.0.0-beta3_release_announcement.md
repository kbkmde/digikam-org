---
date: "2018-12-30T00:00:00"
title: "digiKam 6.0.0 beta 3 is released"
author: "digiKam Team"
description: "Dear digiKam fans and users, following the second 6.0.0 beta release published in october,
we are proud to announce the third beta of digiKam 6.0.0."
category: "news"
---

[![](https://c2.staticflickr.com/8/7806/31570477457_4fcc63a31a_c.jpg "digiKam 6.0.0-beta3 editing Hubble photo")](https://www.flickr.com/photos/digikam/31570477457)

Dear digiKam fans and users, following the
[first beta release](https://www.digikam.org/news/2018-10-31-6.0.0-beta2_release_announcement/)
published in October, we are proud to announce the third and last beta of digiKam 6.0.0,
before the final release planed in February 2019.

### Last main components updated

With the new upcoming 6.0.0 final release, we updated the code and all bundles with
last stable versions of [Exiv2 0.27](http://exiv2.dyndns.org/whatsnew.html)
and [Libraw 0.19](https://www.libraw.org/news/libraw-0-19-2-release) shared libraries.

The first one is a main component of digiKam used to interact with file metadata, like
populating database contents, updating item textual information, or handling
XMP side-car for read only files.

The second one is dedicated to process in totality more than 1000 RAW formats, everywhere in
digiKam, as Image Editor, Light Table, Batch Queue Manager and Export tools.
You can find the complete list of supported RAW files in the Help/Supported Raw Camera dialog.

Thanks to Exiv2 and Libraw teams to share and maintain these important low level libraries.

### The Last Main Improvements Before The Final Release

Even if we have been working to consolidate all the implementations, we found time to add new small
improvements with this 6.0.0 beta3 release:

- Under settings collections from Configuration dialog, a refresh button have been added to
[update an existing collection registered in database](https://bugs.kde.org/show_bug.cgi?id=401767)
to a new drive or a new mount path when using a backup disk or a removable device.

- In Album icon-view, a new function have been added to group images together when they were
part of [the same timelapse or burst](https://bugs.kde.org/show_bug.cgi?id=400777).
This will be especially usefull when timelapse photos
are mixed with single-shots, or when importing from a sports camera like Gopro.

- In Batch Queue Manager resize tool, it's now possible to
[apply new image dimmensions using percents](https://bugs.kde.org/show_bug.cgi?id=401676)
relative to original sizes.

### More than 600 Bugs Closed for the upcoming final release

With this release, 40 new files have been closed since 6.0.0 beta2. The total
files closed for next 6.0.0 final release is more than
[The reports already closed](https://bugs.kde.org/buglist.cgi?f1=cf_versionfixedin&limit=0&o1=equals&order=bug_id&product=digikam&v1=6.0.0)
600 files now, and we are now close to the final 6.0.0 release planed in Febrary 2019.

The next stage while January will be to stabilize all implementations. No new features are planed and
the application will be ready for a major update by translator teams about internationalization.

### The Final Words: How to Test This Beta Version

As we already said at 6.0.0 beta1 and beta2 announcements, due to many changes in database
schema, do not use this beta3 in production yet and backup your database files
before testing it. It's recommended to try this beta3 with a small collection of
items first, and to add new photo and video step by step.

Thanks to all users for your supports and help during the beta stage, through the bug reports and mailing list feedbacks.
Thanks also to users who support this project by donations at each releases. This is very appreciated.

digiKam 6.0.0 beta3 source code tarball, Linux 32/64 bits AppImage bundles,
MacOS package, and Windows 32/64 bits installers can be downloaded from
[this repository](http://download.kde.org/unstable/digikam/).

Happy new year 2019 and digiKam testing...
