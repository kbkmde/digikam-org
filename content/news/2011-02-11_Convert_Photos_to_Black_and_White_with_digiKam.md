---
date: "2011-02-11T10:04:00Z"
title: "Convert Photos to Black and White with digiKam"
author: "Dmitri Popov"
description: "If you fancy black and white photography, you'll be pleased to learn that digiKam features a rather powerful tool for converting color photos to black"
category: "news"
aliases: "/node/575"

---

<p>If you fancy black and white photography, you'll be pleased to learn that digiKam features a rather powerful tool for converting color photos to black and white. Turning the currently edited photo to black and white in digiKam is a matter of choosing Color » Black &amp; White. But in most cases, the converted photo needs additional tweaking, and the application offers a few nifty tools to do just that. <a href="http://scribblesandsnaps.wordpress.com/2011/02/11/convert-photos-to-black-and-white-with-digikam/">Continue to read</a></p>

<div class="legacy-comments">

</div>