---
date: "2013-03-11T11:25:00Z"
title: "Basic RAW Processing in digiKam"
author: "Dmitri Popov"
description: "For this project, we'll use a photo of the famous Sagrada Familia cathedral in Barcelona, Spain. The photo was taken with a Canon PowerShot S90"
category: "news"
aliases: "/node/687"

---

<p>For this project, we'll use a photo of the famous <em>Sagrada Familia</em> cathedral in Barcelona, Spain. The photo was taken with a Canon PowerShot S90 camera, and the RAW file exhibits several obvious flaws, including visible barrel distortion, underexposed areas, and noise. In other words, this particular RAW file is perfect for tweaking in digiKam.</p>
<p><img class="size-medium wp-image-3342" alt="Configuring RAW decoding" src="http://scribblesandsnaps.files.wordpress.com/2013/01/digikam-rawdev-rawdecoding.png?w=500" width="500" height="422"></p>
<p><a href="http://scribblesandsnaps.com/2013/03/11/basic-raw-processing-in-digikam/">Continue to read</a></p>

<div class="legacy-comments">

</div>