---
date: "2008-06-17T12:27:00Z"
title: "digiKam has a new Find Duplicates Images Tool"
author: "digiKam"
description: "After my first post about the new Fuzzy Searches tools included in digiKam for KDE4 to find images using sketch or to find similars images,"
category: "news"
aliases: "/node/333"

---

<p>After my first post about the new <a href="http://www.digikam.org/drupal/node/321">Fuzzy Searches</a> tools included in digiKam for KDE4 to find images using sketch or to find similars images, a new search tool is now done to find all duplicates items around all collections. A video demo can be see below:</p>

<object width="500" height="404"><param name="movie" value="http://www.youtube.com/v/NND-bfuG5OM&amp;hl=en"><embed src="http://www.youtube.com/v/NND-bfuG5OM&amp;hl=en" type="application/x-shockwave-flash" width="500" height="404"></object>

<p>This tool have been done in the way to replace the old FindImages kipi-plugins from KDE3. See below the dialog layout of this plugin:</p>

<a href="http://www.flickr.com/photos/digikam/2586314849/" title="finduplicateskipiplugin4 by digiKam team, on Flickr"><img src="http://farm4.static.flickr.com/3268/2586314849_7c26014484_m.jpg" width="240" height="213" alt="finduplicateskipiplugin4"></a>

<p>This last one do not provide a graphical interface fully integrated with digiKam, and usability is not fine. The new tool is not a kipi-plugin. It have been implemented in digiKam core, and use the new Haar wavelets informations stored in database. It's more simple to use and faster. See below a screenshot of digiKam Find Duplicate tool:</p>

<a href="http://www.flickr.com/photos/digikam/2587176220/" title="finduplicatestool by digiKam team, on Flickr"><img src="http://farm4.static.flickr.com/3265/2587176220_855f805f01_m.jpg" width="240" height="192" alt="finduplicatestool"></a>

<p>The tool scan all collections hosted by digiKam in background. It use KIO-Slave, and you can continue to play with digiKam during this stage. It return a map of all reference images which have duplicates items.  All reference images are listed on the left. When you select one item, icon view display all duplicates relevant. You can play with icon view items as well, like with others part of digiKam.</p>

<div class="legacy-comments">

  <a id="comment-17588"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/333#comment-17588" class="active">Awesome feature, keep up the</a></h3>    <div class="submitted">Submitted by tribe (not verified) on Tue, 2008-06-17 17:55.</div>
    <div class="content">
     <p>Awesome feature, keep up the good work!</p>
<p>ps: hardest captcha ever o_O'</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-17623"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/333#comment-17623" class="active">Awesome...</a></h3>    <div class="submitted">Submitted by Xunil (not verified) on Sat, 2008-07-05 11:26.</div>
    <div class="content">
     <p>Awsome! I've been waiting for this feature! But can it do the same things as imgseek?<br>
http://www.imgseek.net/</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-17630"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/333#comment-17630" class="active">Certainly, if you understand</a></h3>    <div class="submitted">Submitted by digiKam on Tue, 2008-07-08 05:16.</div>
    <div class="content">
     <p>Certainly, if you understand how to use the imgSeek interface... </p>
<p>digiKam</p>
         </div>
    <div class="links">» </div>
  </div>
</div></div>
</div>
