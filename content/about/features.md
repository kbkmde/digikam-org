---
date: "2017-03-21"
title: "Features"
author: "digiKam Team"
description: "A non-exhaustive list of digiKam features."
category: "about"
aliases: "/features"
---

![digiKam Pack](/img/content/about/features_digikam_import_timeline.png)

### Summary

The following is a concise overview of features and tools available in digiKam.
The application covers the photographic workflow consisting of these major stages:

* Setup your collections
* Import photos, raw files, and videos
* Organize your collections
* Search in your collections
* Browse, compare, and view items in your collections
* Post-processing, editing, assembling, and transforming your photos
* Share and publish photos

### Setup

* digiKam support multiple collections hosted from different media, as local, removable, or from the network
* All collections are scanned to populate a database with most importants information from your items,
  as camera settings, geolocation, lens, copyright, captions, etc
* Complex queries can be processed to search and found quickly items inside your collections using technical criteria
* Directories and files can be ignored during scan

[![](/img/content/about/features_setup_collections.png "Setup digiKam Collections to Populate Database")](/img/content/about/features_setup_collections.png)

* Database can be stored in local or to a remote server. Sqlite, Mysql, and Mariadb databases are supported.
* Database files are separated by main features, to simplify backups:
  * Core database to store items properties
  * Thumbs database to store all thumbnails with efficient [PGF wavelets compression format](http://www.libpgf.org/)
  * Faces database to store recognition vectors from neural network processing [based on OpenCV](https://opencv.org/)
  * Similarity database to store image finger-prints to search duplicate items

[![](/img/content/about/features_setup_database.png "Setup Database for a Remote Mysql Server")](/img/content/about/features_setup_database.png)

### Import

[![](/img/content/about/features_import_camera.png "Tool to Import Photo from Camera in Action")](/img/content/about/features_import_camera.png)

* Support for all digital camera models that [work with Gphoto2](http://www.gphoto.org/proj/libgphoto2/support.php)
* Support of USB Mass Storage devices
* Import module supports the following actions:
  * Delete images
  * Upload images
  * Lock images
  * Auto-rename pictures during import
  * Auto-rotate pictures during import
  * Auto-creation of albums during import
  * Lossless conversion during import
  * RAW to DNG conversion during import
  * Run customized scripts during import
  * Identification of already downloaded items
  * Show camera information

[![](/img/content/about/features_setup_camera.png "Setup Camera")](/img/content/about/features_setup_camera.png)

### Organize

* Albums are directories to host images, raw files, and videos

[![](/img/content/about/features_album_view.png "Album Contents Show in Icon-View")](/img/content/about/features_album_view.png)

* Albums use hierarchies on disk with sub-albums
* Tags are virtual albums stored on database but using same properties than physical albums
* Tags hierarchies and properties can be organized by a special tool named Tags Manager

[![](/img/content/about/features_tags_manager.png "Tool to Manage Tags Hierarchies and Properties")](/img/content/about/features_tags_manager.png)

* Labels are special tags to mark items for your workflow (rating, color, pick)

[![](/img/content/about/features_labels_menu.png "Icon-View and Labels Contextual Menu")](/img/content/about/features_labels_menu.png)

* Group items together by file-name, by date, time-lapse

[![](/img/content/about/features_group_items.png "Group Items Options From Contextual Menu")](/img/content/about/features_group_items.png)

* Add tags, comments, and advanced textual properties to items

[![](/img/content/about/features_edit_captions.png "Multi-language Captions Editor")](/img/content/about/features_edit_captions.png)

* Sort albums by folder, category, and creation date
* Sort items in albms by name, path, date, file size, or manually

[![](/img/content/about/features_short_items.png "Options to Short Items in Album")](/img/content/about/features_short_items.png)

* Separate icon-view items as flat list, by file format, by album, or by month
* Filter album items by rating, filename, file type, tags, and labels

[![](/img/content/about/features_filter_items.png "Options to Filter Items in Albums")](/img/content/about/features_filter_items.png)

### Search

digiKam provide plenty of tools to search items in your collections using simple criteria based on items properties as:

* By Tags

[![](/img/content/about/features_search_tags.png "Tags Search Tool")](/img/content/about/features_search_tags.png)

* By Labels (rating, color marks, flags)

[![](/img/content/about/features_search_labels.png "Labels Search Tool")](/img/content/about/features_search_labels.png)

* By Dates using calendar

[![](/img/content/about/features_search_calendar.png "Dates Search Tool")](/img/content/about/features_search_calendar.png)

* By Time Range using timeline histogram

[![](/img/content/about/features_search_timeline.png "Time Range Search Tool")](/img/content/about/features_search_timeline.png)

* By Geolocation using world maps

[![](/img/content/about/features_search_geolocation.png "Geolocation Search Tool")](/img/content/about/features_search_geolocation.png)

In addition, more advanced search tools are also provided using post processed properties computed in second stage by end users:

* By Similarity to find duplicates or by drawing a sketch to find items with specific shapes

[![](/img/content/about/features_search_similarity.png "Similarity Search Tool")](/img/content/about/features_search_similarity.png)

* By faces (detection and recognition)

[![](/img/content/about/features_search_faces.png "Faces Search Tool")](/img/content/about/features_search_faces.png)

Finaly, if these methods are not enough to complete a search on your collections, an ultimate very powerful tool can process
complex queries on the database using multiple criteria at the same time. All items properties stored in database can
be used to compose a search.

[![](/img/content/about/features_search_advanced.png "Advanced Search Tool")](/img/content/about/features_search_advanced.png)

### View

Central view can use multiple visualization modes:

* Icon view to display items as thumbnails
* Item preview to display items in full size

[![](/img/content/about/features_preview_mode.png "Central View Switched in Preview Mode With Thumbbar on Top")](/img/content/about/features_preview_mode.png)

* Map view with items position show as overlay

[![](/img/content/about/features_geolocation_mode.png "Central View Switched in World-Map Mode")](/img/content/about/features_geolocation_mode.png)

* Table view to display item with a list as a file manager

[![](/img/content/about/features_tableview_mode.png "Central View Switched in Table-View Mode")](/img/content/about/features_tableview_mode.png)

* Video support as photo: thumbnails, preview, and metadata

[![](/img/content/about/features_video_preview.png "Central View Switched in Video Mode With Thumbbar on Top and XMP Metadata on the Right Side")](/img/content/about/features_video_preview.png)

* Support of RAW pictures using [LibRaw](http://www.libraw.org) included in digiKam core. More than [1000 proprietary
  RAW camera formats are supported](https://www.libraw.org/supported-cameras-snapshot-201903)

[![](/img/content/about/features_raw_options.png "Options to Decode Raw Images")](/img/content/about/features_raw_options.png)

* Easy comparing similar pictures using Light Table:
  * Files displayed side by side
  * Properties displayed side by side
  * Synchronized panning and zooming
  * Navigation by pair
  * Raw files support

[![](/img/content/about/features_light_table.png "Light Table to Compare Details Side by Side 2 Images")](/img/content/about/features_light_table.png)

* All digiKam components support 16 bits color depth pictures (RAW, TIFF, PNG, JPEG2000)
* All digiKam components has a full Color Management support using ICC color profiles

[![](/img/content/about/features_setup_colormanagement.png "Setup Color Management")](/img/content/about/features_setup_colormanagement.png)

* Thumbnail sizes can be adapted to your screen size on the fly and support Hdpi monitor
* Thumbnail can show all main properties from items

[![](/img/content/about/features_thumbnail_options.png "Options to Show Thumbnails in Albums")](/img/content/about/features_thumbnail_options.png)

* Sidebar used everywhere to display
  * Item information
  * Item Metadata
  * Colors information: histogram and ICC profile
  * Geolocation information
  * Captions/tags/labels/date
  * Item versionning information
  * Tag filter information
  * Quick access to post-processing tools

[![](/img/content/about/features_right_sidebar.png "Right Sidebar With Metadata, Geolocation, and Tools view")](/img/content/about/features_right_sidebar.png)

* Fast preview pictures mode embedded into albums interface with zoom and panning features

[![](/img/content/about/features_preview_options.png "Options to Show Preview in Albums")](/img/content/about/features_preview_options.png)

### Post-processing

#### Plugins Architecture

* To simplify development using stand-alone modules, digiKam make use of own plugins:
  * Generic tools to process group of items (assembly, export, import, etc)
  * BQM tools to batch process items on queues following workflow settings
  * Image Editor tools to fix/improve/transform photo

* Plugin architecture allows to create new tools without to touch core implementations
* Plugins are also shared with Showfoto application
* Plugins can be disabled in application

[![](/img/content/about/features_setup_genericplugins.png "Setup Generic Plugins")](/img/content/about/features_setup_genericplugins.png)

#### Generic plugins

* Print creator

[![](/img/content/about/features_printcreator_plugin.png "Plugin to Print Assembled Images")](/img/content/about/features_printcreator_plugin.png)

* Create calendar

[![](/img/content/about/features_calendar_plugin.png "Plugin to Create Calendar From a list of Items")](/img/content/about/features_calendar_plugin.png)

* Adjust time and date

[![](/img/content/about/features_timeadjust_plugin.png "Plugin to Adjust Images Date and Time")](/img/content/about/features_timeadjust_plugin.png)

* Share items with DLNA/Upnp compatible devices

[![](/img/content/about/features_dlna_plugin.png "Plugin to Shate Items with DLNA/Upnp Compatible Devices")](/img/content/about/features_dlna_plugin.png)

* Geolocation editor

[![](/img/content/about/features_geolocation_plugin.png "Plugin to Edit GPS Coordinates")](/img/content/about/features_geolocation_plugin.png)

* Create presentation

[![](/img/content/about/features_presentation_plugin.png "Plugin to Create Prenstation From a List of Items")](/img/content/about/features_presentation_plugin.png)

* Create panorama

[![](/img/content/about/features_panorama_plugin.png "Plugin to Assemble Images as Large Panorama")](/img/content/about/features_panorama_plugin.png)

* Send items by e-mail

[![](/img/content/about/features_sendbymail_plugin.png "Plugin to Sned Items by E-Mail")](/img/content/about/features_sendbymail_plugin.png)

* Edit Pictures Metadata

[![](/img/content/about/features_metadataeditor_plugin.png "Plugin to Edit Exif/Iptc/Xmp metadata from images")](/img/content/about/features_metadataeditor_plugin.png)

* Create video-slideshow

[![](/img/content/about/features_video_slideshow.png "Plugin to Create Video Slideshow")](/img/content/about/features_video_slideshow.png)

* ...and more...

#### Image Editor

* Fast image editor with:
  * Free cropping
  * Rotation
  * Flipping
  * Photo editing without losing metadata
  * Histogram viewer
  * Under and Over Exposure indicator on the canvas
  * Color Management support
  * Export to another image format
  * Zooming and panning
  * Printing images
  * Multiple editor tool preview to compare side by side results
  * 16 bits image color depth support
  * RAW camera image support

[![](/img/content/about/features_editor_rawimport.png "Image Editor Raw Import Tool in Action")](/img/content/about/features_editor_rawimport.png)

#### Image Editor Plugins

[![](/img/content/about/features_setup_editorplugins.png "Setup Image Editor Plugins")](/img/content/about/features_setup_editorplugins.png)

* Hue / Saturation / Luminosity correction: to adjust hue, saturation, and luminosity on photograph

[![](/img/content/about/features_editor_hsl.png "Hue, Saturation, and Luminosity Correction Editor Plugin in Action")](/img/content/about/features_editor_hsl.png)

* Brightness / Contrast / Gamma correction: to adjust brighness, contrast, and gamma on photograph

[![](/img/content/about/features_editor_bcg.png "Brightness, Contrast, and Gamma Correction Editor Plugin in Action")](/img/content/about/features_editor_bcg.png)

* Colors balance: to adjust balance of colors

[![](/img/content/about/features_editor_colorsbalance.png "Colors Balance Correction Editor Plugin in Action")](/img/content/about/features_editor_colorsbalance.png)

* Colors auto-correction tools: to apply Normalize / Equalize / Auto levels / Stretch Contrast histogram filters

[![](/img/content/about/features_editor_autocorrection.png "Auto Colors Correction Editor Plugin in Action")](/img/content/about/features_editor_autocorrection.png)

* Invert colors: to generate positive photograph
* Color-Space conversion: to switch image color space to another one

[![](/img/content/about/features_editor_convertcolorspace.png "Convert Color-Space Editor Plugin in Action")](/img/content/about/features_editor_convertcolorspace.png)

* Red eyes correction: to fix automatically red eyes on photograph

[![](/img/content/about/features_editor_redeyes.png "Automatic Red-Eyes Correction Editor Plugin in Action")](/img/content/about/features_editor_redeyes.png)

* Apply Texture: to apply a decorative texture to a photograph

[![](/img/content/about/features_editor_texture.png "Apply Texture Editor Plugin in Action")](/img/content/about/features_editor_texture.png)

* Simple blur: to apply gaussian blur on image
* Ratio Crop : to cut photograph with proportion aids and composing tools based on Fibonacci rules

[![](/img/content/about/features_editor_ratiocrop.png "Ratio Crop Editor Plugin in Action")](/img/content/about/features_editor_ratiocrop.png)

* Rain Drops: to add a visual effect of raindrops on photograph
* Adjust levels: to adjust the photograph histogram levels manually

[![](/img/content/about/features_editor_adjustlevels.png "Adjust Levels Editor Plugin in Action")](/img/content/about/features_editor_adjustlevels.png)

* Vignetting: to remove or add vignetting on photograph
* Noise Reduction: to filter noise using on wavelet correction

[![](/img/content/about/features_editor_noisereduction.png "Noise Reduction Editor Plugin in Action")](/img/content/about/features_editor_noisereduction.png)

* Liquid Rescale: to change ratio of a picture while keeping the content intact by content aware resizing
* Local Contrast: to generate a pseudo HDR-tonemapping by recover highlights and shadows while keeping local contrast

[![](/img/content/about/features_editor_localcontrast.png "Local Contrast Editor Plugin in Action")](/img/content/about/features_editor_localcontrast.png)

* Distortion FX: to apply distortion special effects on photograph

[![](/img/content/about/features_editor_distortionfx.png "Distortion Effects Editor Plugin in Action")](/img/content/about/features_editor_distortionfx.png)

* Lens Correction: to correct lens spherical aberration on photograph using [Lensfun library](https://lensfun.github.io/)

[![](/img/content/about/features_editor_lenscorrection.png "Lens Correction Editor Plugin in Action")](/img/content/about/features_editor_lenscorrection.png)

* Shear Tool: to shear a photograph horizontally and vertically
* Adjust curves: to adjust the photograph colors using curves

[![](/img/content/about/features_editor_adjustcurves.png "Adjust Curves Editor Plugin in Action")](/img/content/about/features_editor_adjustcurves.png)

* Channel Mixer: to mix the photograph color channels
* White Balance: to adjust white color temperature of photograph

[![](/img/content/about/features_editor_whitebalance.png "White Balance Editor Plugin in Action")](/img/content/about/features_editor_whitebalance.png)

* Perspective Tool: to adjust the photograph perspective
* Photograph Restoration: to reduce photograph artifacts using CImg library

[![](/img/content/about/features_editor_restoration.png "Photograph Restoration Editor Plugin in Action")](/img/content/about/features_editor_restoration.png)

* Hot Pixels Correction: to remove photograph hot pixels generated by a deficient camera
* Free Rotation: to rotate a photograph with a free angle in degrees

[![](/img/content/about/features_editor_freerotation.png "Free Rotation Editor Plugin in Action")](/img/content/about/features_editor_freerotation.png)

* Add Border: to add decorative frame around a photograph
* Insert Text: to insert text over a photograph

[![](/img/content/about/features_editor_inserttext.png "Insert Text Editor Plugin in Action")](/img/content/about/features_editor_inserttext.png)

* Blur FX: to apply blurring special effects on photograph

[![](/img/content/about/features_editor_blurfx.png "Blur Effects Editor Plugin in Action")](/img/content/about/features_editor_blurfx.png)

* Oil Paint: to simulate oil painting on photograph

[![](/img/content/about/features_editor_oilpaint.png "Oil Paint Editor Plugin in Action")](/img/content/about/features_editor_oilpaint.png)

* Emboss: to apply emboss filter on photograph
* Black and White: to simulate black and white and infrared film effect on photograph with possible tonality change

[![](/img/content/about/features_editor_blackandwhite.png "Black and White Editor Plugin in Action")](/img/content/about/features_editor_blackandwhite.png)

* Charcoal: to simulate charcoal drawing on photograph
* Film Grain: to simulate film grain on photograph

[![](/img/content/about/features_editor_filmgrain.png "Film Grain Editor Plugin in Action")](/img/content/about/features_editor_filmgrain.png)

* Color FX: to apply color effect on photograph as Solarize or Lut3D

[![](/img/content/about/features_editor_colorfx.png "Color Effects Editor Plugin in Action")](/img/content/about/features_editor_colorfx.png)

* Sharpen: to apply a filter to refocus a photograph without increase noise

[![](/img/content/about/features_editor_sharpen.png "Sharpen Editor Plugin in Action")](/img/content/about/features_editor_sharpen.png)


#### Batch Queue Manager

* Items to batch process can be hosted in queues
* Each queue has a dedicated settings as target album, Raw decoding, renamings, etc
* BQM Plugins provide same tools than Image Editor
* Plugins can be chained as assinged to queues to speed-up processing

[![](/img/content/about/features_setup_bqmplugins.png "Setup Batch Queue Manager Plugins")](/img/content/about/features_setup_bqmplugins.png)

* Parallelized processing of queues using multi-cores
* Save and restore queue settings as workflow
* RAW image converter
* DNG converter

[![](/img/content/about/features_bqm_settings.png "Batch Queue Manager Settings")](/img/content/about/features_bqm_settings.png)

### Share

![](/img/content/about/features_export_menu.png "Export Menu")

* Export to [Dropbox](https://www.dropbox.com)
* Export to [Flickr](https://www.flickr.com/)
* Export to [Google Photo](https://www.google.com/photos/)
* Export to [Google Drive](https://www.google.com/drive/)
* Export to [Piwigo](http://piwigo.org/)
* Export to [Pinterest](https://www.pinterest.com/)
* Export to [Twitter](https://twitter.com/)
* Export to HTML Gallery
* Export to a local directory or remote computer
* ...and more...

[![](/img/content/about/features_flickrexport_plugin.png "Plugin to Export to Flickr Web-service in Action")](/img/content/about/features_flickrexport_plugin.png)
