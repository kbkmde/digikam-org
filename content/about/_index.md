---
date: "2017-03-22T13:13:45-06:00"
title: "About"
author: "digiKam Team"
description: "Learn about digiKam"
category: "about"
aliases: "/about"
menu: "navbar"
---

### What is digiKam?

digiKam is an advanced open-source digital photo management application that runs on Linux, Windows, and macOS. The application provides a comprehensive 
set of tools for importing, managing, editing, and sharing photos and raw files.

[![](/img/content/about/about_editor_albumview.png "Albums View and Image Editor")](/img/content/about/about_editor_albumview.png)

### Highlights

You can use digiKam's import capabilities to easily transfer photos, raw files, and videos directly from your camera and external storage devices (SD cards, USB disks, etc.). 
The application allows you to configure import settings and rules that process and organize imported items on-the-fly.

digiKam organizes photos, raw files, and videos into albums. But the application also features powerful tagging tools that allow you to assign tags, ratings, and labels to 
photos and raw files. You can then use filtering functionality to quickly find items that match specific criteria.

In addition to filtering functionality, digiKam features powerful searching capabilities that let you search the photo library by a wide range of criteria. 
You can search photos by tags, labels, rating, data, location, and even specific EXIF, IPTC, or XMP metadata. You can also combine several criteria for more advanced searches. 
digiKam rely on [Exiv2](http://www.exiv2.org/) library to handle metadata tag contents from files to populate the photo library.

digiKam can handle raw files, and the application uses the excellent [LibRaw](https://www.libraw.org/) library for decoding raw files. 
The library is actively maintained and regularly updated to include support for the latest camera models.

digiKam can also manage video files for cataloging purpose, and the application uses the couple [FFmpeg](http://ffmpeg.org/)
and [QtAv](http://www.qtav.org/) libraries to extract metadata and play media.

The application provides a comprehensive set of editing tools. This includes basic tools for adjusting colors, cropping, and sharpening 
as well as advanced tools for, curves adjustment, panorama stitching, and much more. A special tool
based on [Lensfun](https://lensfun.github.io/) library permit to apply lens corrections automatically on images.

Extended functionality in digiKam is implemented via a set of tools based of plugins mechanism (named **DPlugins** for digiKam Plugins).
Plugins can be written to import and export contents to remote web-services, add new features to edit image, and batch process photo.

[![](/img/content/about/about_albumview_itemmenu.png "Albums View and Item Menu")](/img/content/about/about_albumview_itemmenu.png)
