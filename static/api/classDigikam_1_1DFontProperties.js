var classDigikam_1_1DFontProperties =
[
    [ "DisplayFlag", "classDigikam_1_1DFontProperties.html#af359b15373605c896cf26287cf0ab5b2", [
      [ "NoDisplayFlags", "classDigikam_1_1DFontProperties.html#af359b15373605c896cf26287cf0ab5b2ace83ba84116e86326bff9e5dee1b0d9b", null ],
      [ "FixedFontsOnly", "classDigikam_1_1DFontProperties.html#af359b15373605c896cf26287cf0ab5b2a62db86785c3438d077770d0671776d54", null ],
      [ "DisplayFrame", "classDigikam_1_1DFontProperties.html#af359b15373605c896cf26287cf0ab5b2a2cd09faa49234ad74cb030615b78bd74", null ],
      [ "ShowDifferences", "classDigikam_1_1DFontProperties.html#af359b15373605c896cf26287cf0ab5b2ab241219ef8791b75d1e3fcc127b7fef1", null ]
    ] ],
    [ "FontColumn", "classDigikam_1_1DFontProperties.html#a1dcc8ca83cf98821f209e0c075fb4631", [
      [ "FamilyList", "classDigikam_1_1DFontProperties.html#a1dcc8ca83cf98821f209e0c075fb4631a7e6a2a56ac0e37ac09962161368df61c", null ],
      [ "StyleList", "classDigikam_1_1DFontProperties.html#a1dcc8ca83cf98821f209e0c075fb4631a3d2bcfe83f4dd793c396fa9425339560", null ],
      [ "SizeList", "classDigikam_1_1DFontProperties.html#a1dcc8ca83cf98821f209e0c075fb4631a3464aea7bd7cd4144a1dcbe21b70ae7b", null ]
    ] ],
    [ "FontDiff", "classDigikam_1_1DFontProperties.html#ac9d2c23ace4cc8583a6b03f786281700", [
      [ "NoFontDiffFlags", "classDigikam_1_1DFontProperties.html#ac9d2c23ace4cc8583a6b03f786281700ab939e664453959ac2c28ad553f44f8b2", null ],
      [ "FontDiffFamily", "classDigikam_1_1DFontProperties.html#ac9d2c23ace4cc8583a6b03f786281700a5b1ecb0375c0cd2df520893317eefb44", null ],
      [ "FontDiffStyle", "classDigikam_1_1DFontProperties.html#ac9d2c23ace4cc8583a6b03f786281700a9a375c9cac6b9da0770cad3bdeef73e0", null ],
      [ "FontDiffSize", "classDigikam_1_1DFontProperties.html#ac9d2c23ace4cc8583a6b03f786281700a9a0ad9c691cb500541349d0f8731dec4", null ],
      [ "AllFontDiffs", "classDigikam_1_1DFontProperties.html#ac9d2c23ace4cc8583a6b03f786281700a294e3f577d01ea0f1bcde41068b12801", null ]
    ] ],
    [ "FontListCriteria", "classDigikam_1_1DFontProperties.html#a5f208e6e415b9e601283d12c79e892c6", [
      [ "FixedWidthFonts", "classDigikam_1_1DFontProperties.html#a5f208e6e415b9e601283d12c79e892c6a2577e5ef963f930afb64f4da38061a04", null ],
      [ "ScalableFonts", "classDigikam_1_1DFontProperties.html#a5f208e6e415b9e601283d12c79e892c6a54864b4e96827253742d88f9d971b4b5", null ],
      [ "SmoothScalableFonts", "classDigikam_1_1DFontProperties.html#a5f208e6e415b9e601283d12c79e892c6a726f3207f4e3182f2c3246edb82dc1e5", null ]
    ] ],
    [ "DFontProperties", "classDigikam_1_1DFontProperties.html#a86fc960318780a44ad8edb19a3853ff2", null ],
    [ "~DFontProperties", "classDigikam_1_1DFontProperties.html#a9a69dfee24142f25ea598fc099f1f493", null ],
    [ "backgroundColor", "classDigikam_1_1DFontProperties.html#aa6b6cd703025784225dc99d8b70cd410", null ],
    [ "color", "classDigikam_1_1DFontProperties.html#ae2dd54ca3d7343925aed0b24471e6ced", null ],
    [ "enableColumn", "classDigikam_1_1DFontProperties.html#aef185da243df77643e954db7c49e69f7", null ],
    [ "font", "classDigikam_1_1DFontProperties.html#a877556c78346501ff13b941a0facb963", null ],
    [ "fontDiffFlags", "classDigikam_1_1DFontProperties.html#a1a4dd4a222cb2614e7e7cc8f503651a8", null ],
    [ "fontSelected", "classDigikam_1_1DFontProperties.html#a5519220d1e2d18d439fda64b08cbc453", null ],
    [ "makeColumnVisible", "classDigikam_1_1DFontProperties.html#aa0e8af2c214a00a1ba9175f12106ea2b", null ],
    [ "sampleText", "classDigikam_1_1DFontProperties.html#abbc586f43461743fe78a91734c066c9e", null ],
    [ "setBackgroundColor", "classDigikam_1_1DFontProperties.html#af75677546f2e60fedc92ab69f5693076", null ],
    [ "setColor", "classDigikam_1_1DFontProperties.html#a07db0a92f4ea4a031e88285a87318ae9", null ],
    [ "setFont", "classDigikam_1_1DFontProperties.html#af82ac3c6dc7c898d1fd9abcd550e2b19", null ],
    [ "setSampleBoxVisible", "classDigikam_1_1DFontProperties.html#ae164920b395590288dfeafd63a98fd4f", null ],
    [ "setSampleText", "classDigikam_1_1DFontProperties.html#aab434496220c94bc4378b7ea5cdf4cb6", null ],
    [ "setSizeIsRelative", "classDigikam_1_1DFontProperties.html#ab53a147425d6795e81047a1c79602529", null ],
    [ "sizeHint", "classDigikam_1_1DFontProperties.html#a30cfcd6a9ac1eb3a60cc2660c027b320", null ],
    [ "sizeIsRelative", "classDigikam_1_1DFontProperties.html#a78943f8a4adc358a93ce652a890e1e4f", null ],
    [ "backgroundColor", "classDigikam_1_1DFontProperties.html#a65200cc23eadec7061948c2fa1ec6036", null ],
    [ "color", "classDigikam_1_1DFontProperties.html#aed5588839e6738c3edf0bbbcf374581e", null ],
    [ "font", "classDigikam_1_1DFontProperties.html#a341b8533efbbb3ec6519999f77904b6d", null ],
    [ "sampleText", "classDigikam_1_1DFontProperties.html#a977f4eeffa3328dd7d602054dcef1000", null ],
    [ "sizeIsRelative", "classDigikam_1_1DFontProperties.html#a670576ebdd5c3806886ed94389a98a2a", null ]
];