var classDigikam_1_1DTextLabelName =
[
    [ "DTextLabelName", "classDigikam_1_1DTextLabelName.html#ab17a834d502eb962c58720df14b1e531", null ],
    [ "~DTextLabelName", "classDigikam_1_1DTextLabelName.html#a07061a43da7958c4e3e558f871c87148", null ],
    [ "adjustedText", "classDigikam_1_1DTextLabelName.html#a808d91a0b119dc0ea7963c5d9008a22e", null ],
    [ "minimumSizeHint", "classDigikam_1_1DTextLabelName.html#ad7094264b03ebf2b1d304b878dcc6fe8", null ],
    [ "setAdjustedText", "classDigikam_1_1DTextLabelName.html#ad868928c7eeee8e41b2c2afd37274ec2", null ],
    [ "setAlignment", "classDigikam_1_1DTextLabelName.html#a39554a9d00b3390c4348e9094dcf5eca", null ],
    [ "setElideMode", "classDigikam_1_1DTextLabelName.html#affa7663c26f07e8bb30abe48180a2c77", null ],
    [ "sizeHint", "classDigikam_1_1DTextLabelName.html#a293e6d780f0d118b702ca1fe76820ceb", null ]
];