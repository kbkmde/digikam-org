var classDigikam_1_1DPluginAuthor =
[
    [ "DPluginAuthor", "classDigikam_1_1DPluginAuthor.html#a8d02ef74885c603645ac92041880ae5a", null ],
    [ "DPluginAuthor", "classDigikam_1_1DPluginAuthor.html#acb5d3ec6f483562cac84163049f2ff7a", null ],
    [ "~DPluginAuthor", "classDigikam_1_1DPluginAuthor.html#a59f58610a75e31722860f8d012e0f160", null ],
    [ "toString", "classDigikam_1_1DPluginAuthor.html#abd58a9d27463fb438eb9956b6d640727", null ],
    [ "email", "classDigikam_1_1DPluginAuthor.html#a6adfe0b802c2e3d949c1440880ad34b5", null ],
    [ "name", "classDigikam_1_1DPluginAuthor.html#a1caa0a07eac7ca2b97a29c3ea5c31db2", null ],
    [ "roles", "classDigikam_1_1DPluginAuthor.html#a4bf3dcfa15fbeb8d7ed5717ad547e508", null ],
    [ "years", "classDigikam_1_1DPluginAuthor.html#a13ffe12c6aaac328586e7a7fb7171156", null ]
];