var dir_c1fd2015d54d9daa4e84a256f8dca728 =
[
    [ "bcgcontainer.cpp", "bcgcontainer_8cpp.html", null ],
    [ "bcgcontainer.h", "bcgcontainer_8h.html", [
      [ "BCGContainer", "classDigikam_1_1BCGContainer.html", "classDigikam_1_1BCGContainer" ]
    ] ],
    [ "bcgfilter.cpp", "bcgfilter_8cpp.html", null ],
    [ "bcgfilter.h", "bcgfilter_8h.html", [
      [ "BCGFilter", "classDigikam_1_1BCGFilter.html", "classDigikam_1_1BCGFilter" ]
    ] ],
    [ "bcgsettings.cpp", "bcgsettings_8cpp.html", null ],
    [ "bcgsettings.h", "bcgsettings_8h.html", [
      [ "BCGSettings", "classDigikam_1_1BCGSettings.html", "classDigikam_1_1BCGSettings" ]
    ] ]
];