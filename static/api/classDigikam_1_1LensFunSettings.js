var classDigikam_1_1LensFunSettings =
[
    [ "LensFunSettings", "classDigikam_1_1LensFunSettings.html#a1ac5076df58fd929569315aada1e2af1", null ],
    [ "~LensFunSettings", "classDigikam_1_1LensFunSettings.html#aabe636edb2458f365b91fe63e37e0ae2", null ],
    [ "assignFilterSettings", "classDigikam_1_1LensFunSettings.html#a4eb477762f8a21b51ad6de07f3f51dc0", null ],
    [ "defaultSettings", "classDigikam_1_1LensFunSettings.html#ac020bfa9922817484e15a83dc2ebb608", null ],
    [ "readSettings", "classDigikam_1_1LensFunSettings.html#adfc4a9c3dfa66f81fd24d554a1a52f7a", null ],
    [ "resetToDefault", "classDigikam_1_1LensFunSettings.html#ad28d98a9dc5d90264aca31f5a34558fd", null ],
    [ "setEnabledCCA", "classDigikam_1_1LensFunSettings.html#a1c4c2720c450870356f95be393597bcc", null ],
    [ "setEnabledDist", "classDigikam_1_1LensFunSettings.html#ac242bd06becbb9eb584f1e90373e53fd", null ],
    [ "setEnabledGeom", "classDigikam_1_1LensFunSettings.html#a0599dcfacb54dd2c0793ceb070edd39b", null ],
    [ "setEnabledVig", "classDigikam_1_1LensFunSettings.html#ac2f03412e6accbea400e7869e7295091", null ],
    [ "setFilterSettings", "classDigikam_1_1LensFunSettings.html#af74a0441572c69065918e70c2d873840", null ],
    [ "settings", "classDigikam_1_1LensFunSettings.html#a174cb17321d6d346f6c793be7b1d2104", null ],
    [ "signalSettingsChanged", "classDigikam_1_1LensFunSettings.html#ae203096d751fc86ce5f703fb173d56e1", null ],
    [ "writeSettings", "classDigikam_1_1LensFunSettings.html#a45a6bd4f50f29678e611ba6b358ffa47", null ]
];