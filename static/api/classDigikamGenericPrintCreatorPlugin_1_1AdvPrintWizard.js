var classDigikamGenericPrintCreatorPlugin_1_1AdvPrintWizard =
[
    [ "AdvPrintWizard", "classDigikamGenericPrintCreatorPlugin_1_1AdvPrintWizard.html#a2416304c931c0dc931cc50f568989801", null ],
    [ "~AdvPrintWizard", "classDigikamGenericPrintCreatorPlugin_1_1AdvPrintWizard.html#a90f215a6cabe91539e11e1ed491ed046", null ],
    [ "iface", "classDigikamGenericPrintCreatorPlugin_1_1AdvPrintWizard.html#a40fb8164ced52e22e5d4f8d0eff60898", null ],
    [ "itemsList", "classDigikamGenericPrintCreatorPlugin_1_1AdvPrintWizard.html#ad211ee925f372c0f6b01896fa44a40e0", null ],
    [ "nextId", "classDigikamGenericPrintCreatorPlugin_1_1AdvPrintWizard.html#a880c56c820718cf5f34a27cb663c8fe7", null ],
    [ "previewPhotos", "classDigikamGenericPrintCreatorPlugin_1_1AdvPrintWizard.html#ab1aaa433baa0e8f6d3311874be98bc93", null ],
    [ "restoreDialogSize", "classDigikamGenericPrintCreatorPlugin_1_1AdvPrintWizard.html#a99a0591ddddda89952710d912c0a8e68", null ],
    [ "saveDialogSize", "classDigikamGenericPrintCreatorPlugin_1_1AdvPrintWizard.html#a37a3b930d03a859f95dfef14004c3448", null ],
    [ "setItemsList", "classDigikamGenericPrintCreatorPlugin_1_1AdvPrintWizard.html#a4a2ebecabf95d979ec838e797432ecea", null ],
    [ "setPlugin", "classDigikamGenericPrintCreatorPlugin_1_1AdvPrintWizard.html#a1aa458a15664294147f3b6cb10c9323f", null ],
    [ "settings", "classDigikamGenericPrintCreatorPlugin_1_1AdvPrintWizard.html#a3fcc6c0313830b4544f1c83624386aad", null ],
    [ "updateCropFrame", "classDigikamGenericPrintCreatorPlugin_1_1AdvPrintWizard.html#a964e353972a8546f5e902e1b152697e9", null ]
];