var classDigikam_1_1DSaveSettingsWidget =
[
    [ "OutputFormat", "classDigikam_1_1DSaveSettingsWidget.html#a75c2b8eb3f8a864d7f50025879f55e59", [
      [ "OUTPUT_PNG", "classDigikam_1_1DSaveSettingsWidget.html#a75c2b8eb3f8a864d7f50025879f55e59ae55a38878ae9915c75bd7c029797afe5", null ],
      [ "OUTPUT_TIFF", "classDigikam_1_1DSaveSettingsWidget.html#a75c2b8eb3f8a864d7f50025879f55e59ac84fc35e98fba80eacef5df1aaf7a683", null ],
      [ "OUTPUT_JPEG", "classDigikam_1_1DSaveSettingsWidget.html#a75c2b8eb3f8a864d7f50025879f55e59ab0b40fcdcf0ad734cbe04bd4f4413a4d", null ],
      [ "OUTPUT_PPM", "classDigikam_1_1DSaveSettingsWidget.html#a75c2b8eb3f8a864d7f50025879f55e59a06d3e711a80bc8507af94e4ca6f12b3f", null ]
    ] ],
    [ "DSaveSettingsWidget", "classDigikam_1_1DSaveSettingsWidget.html#a239887d44ff955361f2f38ccb3285613", null ],
    [ "~DSaveSettingsWidget", "classDigikam_1_1DSaveSettingsWidget.html#ae765acff8bc990587f44fd90d8e8550e", null ],
    [ "conflictRule", "classDigikam_1_1DSaveSettingsWidget.html#a4e4d989046ea26344097a766073d7c85", null ],
    [ "extension", "classDigikam_1_1DSaveSettingsWidget.html#a9570ebb3ce1bca58b2b80740a62c153c", null ],
    [ "fileFormat", "classDigikam_1_1DSaveSettingsWidget.html#aaa07143d0aac78712f8aeadb4281c122", null ],
    [ "readSettings", "classDigikam_1_1DSaveSettingsWidget.html#a2011343d97bb69e5758f7943349e1209", null ],
    [ "resetToDefault", "classDigikam_1_1DSaveSettingsWidget.html#a253ff3fef22461ac5d829d5a042d0825", null ],
    [ "setConflictRule", "classDigikam_1_1DSaveSettingsWidget.html#a82d1bd89627fa7b68a8b535df7726e30", null ],
    [ "setCustomSettingsWidget", "classDigikam_1_1DSaveSettingsWidget.html#a9c47b46f1dbc578e00139cfbb8973207", null ],
    [ "setFileFormat", "classDigikam_1_1DSaveSettingsWidget.html#a069e4411d29dff9d5cfbf43f3e903d5c", null ],
    [ "signalConflictButtonChanged", "classDigikam_1_1DSaveSettingsWidget.html#a2024474b90f058afc7f722d4a3cf6ec8", null ],
    [ "signalSaveFormatChanged", "classDigikam_1_1DSaveSettingsWidget.html#ac2cb581e3b2ec1d684ab3eae22dc0451", null ],
    [ "slotPopulateImageFormat", "classDigikam_1_1DSaveSettingsWidget.html#ad88528aa2e6ff40c8e27259e61338450", null ],
    [ "typeMime", "classDigikam_1_1DSaveSettingsWidget.html#a22a72cf652bf9f3ce4be649e50fef36e", null ],
    [ "writeSettings", "classDigikam_1_1DSaveSettingsWidget.html#a215689bca408ad66748f1fb7062116a8", null ]
];