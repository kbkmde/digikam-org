var structDigikamGenericImgUrPlugin_1_1ImgurTalkerAction =
[
    [ "account", "structDigikamGenericImgUrPlugin_1_1ImgurTalkerAction.html#a5fe97f6af996ad9f5cfeca692276872b", null ],
    [ "description", "structDigikamGenericImgUrPlugin_1_1ImgurTalkerAction.html#a6d02850677c8501041f96051eac41179", null ],
    [ "imgpath", "structDigikamGenericImgUrPlugin_1_1ImgurTalkerAction.html#a3b493e642aedf472912c7552e6bba2d9", null ],
    [ "title", "structDigikamGenericImgUrPlugin_1_1ImgurTalkerAction.html#a04188cbf8e4283ed438d9f8b44f4a0bc", null ],
    [ "type", "structDigikamGenericImgUrPlugin_1_1ImgurTalkerAction.html#a59c54e586131c9a61bdb712bf4cf523a", null ],
    [ "upload", "structDigikamGenericImgUrPlugin_1_1ImgurTalkerAction.html#a08789e7b065017ee5978b4dd09808d0a", null ],
    [ "username", "structDigikamGenericImgUrPlugin_1_1ImgurTalkerAction.html#a6ae216ee8fc4edfcb5f0f4b924512b6c", null ]
];