var dir_6f98a38adefd0b7ae44e66c8e33a1485 =
[
    [ "colorlabelwidget.cpp", "colorlabelwidget_8cpp.html", null ],
    [ "colorlabelwidget.h", "colorlabelwidget_8h.html", [
      [ "ColorLabelMenuAction", "classDigikam_1_1ColorLabelMenuAction.html", "classDigikam_1_1ColorLabelMenuAction" ],
      [ "ColorLabelSelector", "classDigikam_1_1ColorLabelSelector.html", "classDigikam_1_1ColorLabelSelector" ],
      [ "ColorLabelWidget", "classDigikam_1_1ColorLabelWidget.html", "classDigikam_1_1ColorLabelWidget" ]
    ] ],
    [ "picklabelwidget.cpp", "picklabelwidget_8cpp.html", null ],
    [ "picklabelwidget.h", "picklabelwidget_8h.html", [
      [ "PickLabelMenuAction", "classDigikam_1_1PickLabelMenuAction.html", "classDigikam_1_1PickLabelMenuAction" ],
      [ "PickLabelSelector", "classDigikam_1_1PickLabelSelector.html", "classDigikam_1_1PickLabelSelector" ],
      [ "PickLabelWidget", "classDigikam_1_1PickLabelWidget.html", "classDigikam_1_1PickLabelWidget" ]
    ] ],
    [ "ratingwidget.cpp", "ratingwidget_8cpp.html", null ],
    [ "ratingwidget.h", "ratingwidget_8h.html", [
      [ "RatingBox", "classDigikam_1_1RatingBox.html", "classDigikam_1_1RatingBox" ],
      [ "RatingMenuAction", "classDigikam_1_1RatingMenuAction.html", "classDigikam_1_1RatingMenuAction" ],
      [ "RatingWidget", "classDigikam_1_1RatingWidget.html", "classDigikam_1_1RatingWidget" ]
    ] ]
];