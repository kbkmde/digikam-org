var classDigikam_1_1Task =
[
    [ "Task", "classDigikam_1_1Task.html#ab9e3e5dd3ca07daf97893066b0afbefe", null ],
    [ "~Task", "classDigikam_1_1Task.html#a2b8f18d4a5b59f9df3cb7dacf20031d3", null ],
    [ "cancel", "classDigikam_1_1Task.html#ad801a5f7a879375239c3acc000cb91a4", null ],
    [ "run", "classDigikam_1_1Task.html#a7bc4728a201d98f8f6dfe05d71bbb542", null ],
    [ "setItem", "classDigikam_1_1Task.html#aca030091dba3e69d57c98a9490e098ca", null ],
    [ "setSettings", "classDigikam_1_1Task.html#a2653b3b55cb458053f76e4381a7360d0", null ],
    [ "signalDone", "classDigikam_1_1Task.html#aa34d76e1cd1e839039f1b67add81920d", null ],
    [ "signalFinished", "classDigikam_1_1Task.html#aacdf10826c4cc7a39bbcbeed521f1b31", null ],
    [ "signalProgress", "classDigikam_1_1Task.html#a736bc91a70d7892ae9fe8d1bd76b795e", null ],
    [ "signalStarted", "classDigikam_1_1Task.html#afed71b5e01ec11f40bcb99c9f24b9048", null ],
    [ "signalStarting", "classDigikam_1_1Task.html#abc12279dd5e24f7812ff33b7e6bb63c1", null ],
    [ "slotCancel", "classDigikam_1_1Task.html#a6dfde7732514ae116be3b62255481980", null ],
    [ "m_cancel", "classDigikam_1_1Task.html#a7b2f25a3878a3a2bcdde09ef10c4345e", null ]
];