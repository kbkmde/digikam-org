var classDigikamGenericPresentationPlugin_1_1PresentationLoader =
[
    [ "PresentationLoader", "classDigikamGenericPresentationPlugin_1_1PresentationLoader.html#a62541a36d68ebf75ad94e5990d5c1922", null ],
    [ "~PresentationLoader", "classDigikamGenericPresentationPlugin_1_1PresentationLoader.html#a001e7e4ec3ec98e0035dad66c81475c2", null ],
    [ "currFileName", "classDigikamGenericPresentationPlugin_1_1PresentationLoader.html#a21b7b1dee9b0ece213942d2d6fef8671", null ],
    [ "currPath", "classDigikamGenericPresentationPlugin_1_1PresentationLoader.html#aa424e6d21be18040e07c87a3d782375b", null ],
    [ "getCurrent", "classDigikamGenericPresentationPlugin_1_1PresentationLoader.html#a5be715decfdf0a2fefe282894e9af6a9", null ],
    [ "next", "classDigikamGenericPresentationPlugin_1_1PresentationLoader.html#ae8aea5903ce019f75287d5aee748a53c", null ],
    [ "prev", "classDigikamGenericPresentationPlugin_1_1PresentationLoader.html#adb1c651850d0206ee85469631d8d22a7", null ]
];