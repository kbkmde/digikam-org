var dir_bbf7fc332d988157cb7e1cd789b32ae8 =
[
    [ "collectionlocation.cpp", "collectionlocation_8cpp.html", null ],
    [ "collectionlocation.h", "collectionlocation_8h.html", "collectionlocation_8h" ],
    [ "collectionmanager.cpp", "collectionmanager_8cpp.html", null ],
    [ "collectionmanager.h", "collectionmanager_8h.html", [
      [ "CollectionManager", "classDigikam_1_1CollectionManager.html", "classDigikam_1_1CollectionManager" ]
    ] ],
    [ "collectionmanager_album.cpp", "collectionmanager__album_8cpp.html", null ],
    [ "collectionmanager_location.cpp", "collectionmanager__location_8cpp.html", null ],
    [ "collectionmanager_p.cpp", "collectionmanager__p_8cpp.html", null ],
    [ "collectionmanager_p.h", "collectionmanager__p_8h.html", [
      [ "AlbumRootLocation", "classDigikam_1_1AlbumRootLocation.html", "classDigikam_1_1AlbumRootLocation" ],
      [ "ChangingDB", "classDigikam_1_1ChangingDB.html", "classDigikam_1_1ChangingDB" ],
      [ "Private", "classDigikam_1_1CollectionManager_1_1Private.html", "classDigikam_1_1CollectionManager_1_1Private" ],
      [ "SolidVolumeInfo", "classDigikam_1_1SolidVolumeInfo.html", "classDigikam_1_1SolidVolumeInfo" ]
    ] ],
    [ "collectionscanner.cpp", "collectionscanner_8cpp.html", null ],
    [ "collectionscanner.h", "collectionscanner_8h.html", [
      [ "CollectionScanner", "classDigikam_1_1CollectionScanner.html", "classDigikam_1_1CollectionScanner" ]
    ] ],
    [ "collectionscanner_p.cpp", "collectionscanner__p_8cpp.html", "collectionscanner__p_8cpp" ],
    [ "collectionscanner_p.h", "collectionscanner__p_8h.html", "collectionscanner__p_8h" ],
    [ "collectionscanner_scan.cpp", "collectionscanner__scan_8cpp.html", null ],
    [ "collectionscanner_utils.cpp", "collectionscanner__utils_8cpp.html", null ],
    [ "collectionscannerhints.cpp", "collectionscannerhints_8cpp.html", null ],
    [ "collectionscannerhints.h", "collectionscannerhints_8h.html", "collectionscannerhints_8h" ],
    [ "collectionscannerobserver.h", "collectionscannerobserver_8h.html", [
      [ "CollectionScannerObserver", "classDigikam_1_1CollectionScannerObserver.html", "classDigikam_1_1CollectionScannerObserver" ],
      [ "InitializationObserver", "classDigikam_1_1InitializationObserver.html", "classDigikam_1_1InitializationObserver" ]
    ] ]
];