var dir_db206bea550a1337e37d218187ceab11 =
[
    [ "geocoordinates.cpp", "geocoordinates_8cpp.html", "geocoordinates_8cpp" ],
    [ "geocoordinates.h", "geocoordinates_8h.html", "geocoordinates_8h" ],
    [ "geodragdrophandler.cpp", "geodragdrophandler_8cpp.html", null ],
    [ "geodragdrophandler.h", "geodragdrophandler_8h.html", [
      [ "GeoDragDropHandler", "classDigikam_1_1GeoDragDropHandler.html", "classDigikam_1_1GeoDragDropHandler" ]
    ] ],
    [ "geogroupstate.h", "geogroupstate_8h.html", "geogroupstate_8h" ],
    [ "geoifacecommon.cpp", "geoifacecommon_8cpp.html", "geoifacecommon_8cpp" ],
    [ "geoifacecommon.h", "geoifacecommon_8h.html", "geoifacecommon_8h" ],
    [ "geoifacetypes.h", "geoifacetypes_8h.html", "geoifacetypes_8h" ],
    [ "geomodelhelper.cpp", "geomodelhelper_8cpp.html", null ],
    [ "geomodelhelper.h", "geomodelhelper_8h.html", [
      [ "GeoModelHelper", "classDigikam_1_1GeoModelHelper.html", "classDigikam_1_1GeoModelHelper" ]
    ] ],
    [ "gpscommon.cpp", "gpscommon_8cpp.html", "gpscommon_8cpp" ],
    [ "gpscommon.h", "gpscommon_8h.html", "gpscommon_8h" ],
    [ "gpsgeoifacemodelhelper.cpp", "gpsgeoifacemodelhelper_8cpp.html", null ],
    [ "gpsgeoifacemodelhelper.h", "gpsgeoifacemodelhelper_8h.html", [
      [ "GPSGeoIfaceModelHelper", "classDigikam_1_1GPSGeoIfaceModelHelper.html", "classDigikam_1_1GPSGeoIfaceModelHelper" ]
    ] ],
    [ "gpsundocommand.cpp", "gpsundocommand_8cpp.html", null ],
    [ "gpsundocommand.h", "gpsundocommand_8h.html", [
      [ "GPSUndoCommand", "classDigikam_1_1GPSUndoCommand.html", "classDigikam_1_1GPSUndoCommand" ],
      [ "UndoInfo", "classDigikam_1_1GPSUndoCommand_1_1UndoInfo.html", "classDigikam_1_1GPSUndoCommand_1_1UndoInfo" ]
    ] ],
    [ "groupstatecomputer.cpp", "groupstatecomputer_8cpp.html", null ],
    [ "groupstatecomputer.h", "groupstatecomputer_8h.html", [
      [ "GroupStateComputer", "classDigikam_1_1GroupStateComputer.html", "classDigikam_1_1GroupStateComputer" ]
    ] ]
];