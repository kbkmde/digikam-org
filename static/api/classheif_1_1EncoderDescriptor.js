var classheif_1_1EncoderDescriptor =
[
    [ "get_compression_format", "classheif_1_1EncoderDescriptor.html#aa5e109e489361b6e0692cd1edd5e34df", null ],
    [ "get_encoder", "classheif_1_1EncoderDescriptor.html#a47ce894d4f85b3181047e16f105d737e", null ],
    [ "get_id_name", "classheif_1_1EncoderDescriptor.html#add3079356948f6aecfa5eadec9bfcfbe", null ],
    [ "get_name", "classheif_1_1EncoderDescriptor.html#adcce7ec613d87d689517e218e8853894", null ],
    [ "supportes_lossless_compression", "classheif_1_1EncoderDescriptor.html#a4b77d1d0afe18ddcca6da806fd683d81", null ],
    [ "supportes_lossy_compression", "classheif_1_1EncoderDescriptor.html#aca77368b140a14c02ae6dcbfb06b67cb", null ]
];