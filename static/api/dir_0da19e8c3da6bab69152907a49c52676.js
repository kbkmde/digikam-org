var dir_0da19e8c3da6bab69152907a49c52676 =
[
    [ "dngconverteractions.h", "dngconverteractions_8h.html", "dngconverteractions_8h" ],
    [ "dngconverterdialog.cpp", "dngconverterdialog_8cpp.html", null ],
    [ "dngconverterdialog.h", "dngconverterdialog_8h.html", [
      [ "DNGConverterDialog", "classDigikamGenericDNGConverterPlugin_1_1DNGConverterDialog.html", "classDigikamGenericDNGConverterPlugin_1_1DNGConverterDialog" ]
    ] ],
    [ "dngconverterlist.cpp", "dngconverterlist_8cpp.html", null ],
    [ "dngconverterlist.h", "dngconverterlist_8h.html", [
      [ "DNGConverterList", "classDigikamGenericDNGConverterPlugin_1_1DNGConverterList.html", "classDigikamGenericDNGConverterPlugin_1_1DNGConverterList" ],
      [ "DNGConverterListViewItem", "classDigikamGenericDNGConverterPlugin_1_1DNGConverterListViewItem.html", "classDigikamGenericDNGConverterPlugin_1_1DNGConverterListViewItem" ]
    ] ],
    [ "dngconverterplugin.cpp", "dngconverterplugin_8cpp.html", null ],
    [ "dngconverterplugin.h", "dngconverterplugin_8h.html", "dngconverterplugin_8h" ],
    [ "dngconvertertask.cpp", "dngconvertertask_8cpp.html", null ],
    [ "dngconvertertask.h", "dngconvertertask_8h.html", [
      [ "DNGConverterTask", "classDigikamGenericDNGConverterPlugin_1_1DNGConverterTask.html", "classDigikamGenericDNGConverterPlugin_1_1DNGConverterTask" ]
    ] ],
    [ "dngconverterthread.cpp", "dngconverterthread_8cpp.html", null ],
    [ "dngconverterthread.h", "dngconverterthread_8h.html", [
      [ "DNGConverterActionThread", "classDigikamGenericDNGConverterPlugin_1_1DNGConverterActionThread.html", "classDigikamGenericDNGConverterPlugin_1_1DNGConverterActionThread" ]
    ] ]
];