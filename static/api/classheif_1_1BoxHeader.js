var classheif_1_1BoxHeader =
[
    [ "BoxHeader", "classheif_1_1BoxHeader.html#a53e95f4fa56c3ba588be1657b8715386", null ],
    [ "~BoxHeader", "classheif_1_1BoxHeader.html#a34aedc7c99ddd6f4005bf5641df467c3", null ],
    [ "dump", "classheif_1_1BoxHeader.html#a7ced457217dbbcf21187b51e2e91b280", null ],
    [ "get_box_size", "classheif_1_1BoxHeader.html#a7007aba2ce43a233beef58e09b7e181c", null ],
    [ "get_flags", "classheif_1_1BoxHeader.html#a39d39e81b35c6e87a9928e7ddcbd3673", null ],
    [ "get_header_size", "classheif_1_1BoxHeader.html#a7abc183ab5226b5771ab9db93082508a", null ],
    [ "get_short_type", "classheif_1_1BoxHeader.html#a63f22499fada881eef508cf96e9b51a7", null ],
    [ "get_type", "classheif_1_1BoxHeader.html#ab8a2b8029ba44809e9255b4a867c6d9c", null ],
    [ "get_type_string", "classheif_1_1BoxHeader.html#afe8f50ff1d59612204f5f0add1e1fd05", null ],
    [ "get_version", "classheif_1_1BoxHeader.html#ac75c1cfd9aabffe0c361d70c91c7b21c", null ],
    [ "is_full_box_header", "classheif_1_1BoxHeader.html#adecf2125f8dba3b2d1d369c9318c8122", null ],
    [ "parse", "classheif_1_1BoxHeader.html#a5fa307055bd4008d0f525e4c26523456", null ],
    [ "parse_full_box_header", "classheif_1_1BoxHeader.html#ad9050dc3e81b9a63c656753ff83b9a56", null ],
    [ "prepend_header", "classheif_1_1BoxHeader.html#a3fd5d77a13b528eeaba0118bbd828605", null ],
    [ "reserve_box_header_space", "classheif_1_1BoxHeader.html#a053aeb7c2970e8a5344de7c93d6cbf71", null ],
    [ "set_flags", "classheif_1_1BoxHeader.html#ad0e2c3e8d6ef42c9a25cd452dd2511af", null ],
    [ "set_is_full_box", "classheif_1_1BoxHeader.html#a6140d66f43364c6f0a6bf53a16066fe2", null ],
    [ "set_short_type", "classheif_1_1BoxHeader.html#a181d3407828fba6fa48c40d4edeb2400", null ],
    [ "set_version", "classheif_1_1BoxHeader.html#ab1a9ccf0766e5db004e6bf7f70f142c0", null ]
];