var dir_8a5873af1f5762d752181f2d03c5cfa5 =
[
    [ "similaritydb.cpp", "similaritydb_8cpp.html", null ],
    [ "similaritydb.h", "similaritydb_8h.html", "similaritydb_8h" ],
    [ "similaritydbaccess.cpp", "similaritydbaccess_8cpp.html", null ],
    [ "similaritydbaccess.h", "similaritydbaccess_8h.html", [
      [ "SimilarityDbAccess", "classDigikam_1_1SimilarityDbAccess.html", "classDigikam_1_1SimilarityDbAccess" ]
    ] ],
    [ "similaritydbbackend.cpp", "similaritydbbackend_8cpp.html", null ],
    [ "similaritydbbackend.h", "similaritydbbackend_8h.html", [
      [ "SimilarityDbBackend", "classDigikam_1_1SimilarityDbBackend.html", "classDigikam_1_1SimilarityDbBackend" ]
    ] ],
    [ "similaritydbschemaupdater.cpp", "similaritydbschemaupdater_8cpp.html", null ],
    [ "similaritydbschemaupdater.h", "similaritydbschemaupdater_8h.html", [
      [ "SimilarityDbSchemaUpdater", "classDigikam_1_1SimilarityDbSchemaUpdater.html", "classDigikam_1_1SimilarityDbSchemaUpdater" ]
    ] ]
];