var classDigikamEditorChannelMixerToolPlugin_1_1ChannelMixerTool =
[
    [ "RenderingMode", "classDigikamEditorChannelMixerToolPlugin_1_1ChannelMixerTool.html#ac3d6fd9ffa7c089539b893a7a58be6b2", [
      [ "NoneRendering", "classDigikamEditorChannelMixerToolPlugin_1_1ChannelMixerTool.html#ac3d6fd9ffa7c089539b893a7a58be6b2adbde621cb0749a02dd6a0a295fabbd44", null ],
      [ "PreviewRendering", "classDigikamEditorChannelMixerToolPlugin_1_1ChannelMixerTool.html#ac3d6fd9ffa7c089539b893a7a58be6b2ab66447d37e1ba796bfad3849f5fbf725", null ],
      [ "FinalRendering", "classDigikamEditorChannelMixerToolPlugin_1_1ChannelMixerTool.html#ac3d6fd9ffa7c089539b893a7a58be6b2ac4f7158b77330e3c765a11ced16f031c", null ]
    ] ],
    [ "ChannelMixerTool", "classDigikamEditorChannelMixerToolPlugin_1_1ChannelMixerTool.html#a56a88ba697a482d578073ebfc1a648cc", null ],
    [ "~ChannelMixerTool", "classDigikamEditorChannelMixerToolPlugin_1_1ChannelMixerTool.html#a2f37c49db787e8e8cecd425920b06597", null ],
    [ "analyser", "classDigikamEditorChannelMixerToolPlugin_1_1ChannelMixerTool.html#ac5f059e8e6f16354639aaff6511d9c59", null ],
    [ "analyserCompleted", "classDigikamEditorChannelMixerToolPlugin_1_1ChannelMixerTool.html#a2f780143587b25066940ceae39b5ed58", null ],
    [ "cancelClicked", "classDigikamEditorChannelMixerToolPlugin_1_1ChannelMixerTool.html#a079c4e8e0189f2b08ac97c57d39d4e1d", null ],
    [ "deleteFilterInstance", "classDigikamEditorChannelMixerToolPlugin_1_1ChannelMixerTool.html#a76cad493ae538f89dd9df7f42f1cb3fb", null ],
    [ "exposureSettingsChanged", "classDigikamEditorChannelMixerToolPlugin_1_1ChannelMixerTool.html#a77604957de501c1286831c44c32691f3", null ],
    [ "filter", "classDigikamEditorChannelMixerToolPlugin_1_1ChannelMixerTool.html#a483e012282390f2aae585612cc233adf", null ],
    [ "finalRendering", "classDigikamEditorChannelMixerToolPlugin_1_1ChannelMixerTool.html#a5bd206334d2f6e89e1b7900c944d7c03", null ],
    [ "ICCSettingsChanged", "classDigikamEditorChannelMixerToolPlugin_1_1ChannelMixerTool.html#a9220394e71620f7262b306d2ceda49fd", null ],
    [ "init", "classDigikamEditorChannelMixerToolPlugin_1_1ChannelMixerTool.html#ade313efb59416fe168e9d494a8261714", null ],
    [ "okClicked", "classDigikamEditorChannelMixerToolPlugin_1_1ChannelMixerTool.html#aaf16461888d12351963e1892317b8d02", null ],
    [ "plugin", "classDigikamEditorChannelMixerToolPlugin_1_1ChannelMixerTool.html#ac9f258dfdcc8e161b2e3e04945640053", null ],
    [ "renderingFinished", "classDigikamEditorChannelMixerToolPlugin_1_1ChannelMixerTool.html#a0bec48a86dafaa7ad7179768ced6eaf2", null ],
    [ "renderingMode", "classDigikamEditorChannelMixerToolPlugin_1_1ChannelMixerTool.html#adb3cf4a66b58fe11279cc443302a9661", null ],
    [ "setAnalyser", "classDigikamEditorChannelMixerToolPlugin_1_1ChannelMixerTool.html#aa085c2dc38420de813a896f74173b6d1", null ],
    [ "setBackgroundColor", "classDigikamEditorChannelMixerToolPlugin_1_1ChannelMixerTool.html#a07c8dabdc2fa11e3fbf45a5de6e3084a", null ],
    [ "setBusy", "classDigikamEditorChannelMixerToolPlugin_1_1ChannelMixerTool.html#adc9455ddc0cc3e74ffd177fa85a89c2c", null ],
    [ "setFilter", "classDigikamEditorChannelMixerToolPlugin_1_1ChannelMixerTool.html#a2e62e2ce1707123ebc1f6b790080e378", null ],
    [ "setInitPreview", "classDigikamEditorChannelMixerToolPlugin_1_1ChannelMixerTool.html#ac253001bb85c9329a687fc21298e680d", null ],
    [ "setPlugin", "classDigikamEditorChannelMixerToolPlugin_1_1ChannelMixerTool.html#af1c52946b840a515d1e15c05a077e0e6", null ],
    [ "setPreviewModeMask", "classDigikamEditorChannelMixerToolPlugin_1_1ChannelMixerTool.html#aaf540040a7c43ac693eed06537d8c55f", null ],
    [ "setProgressMessage", "classDigikamEditorChannelMixerToolPlugin_1_1ChannelMixerTool.html#a9350e3f48796b382e658d256fd5fa666", null ],
    [ "setToolCategory", "classDigikamEditorChannelMixerToolPlugin_1_1ChannelMixerTool.html#a9a2b760ae7d24d126b5e8b8295a804bf", null ],
    [ "setToolHelp", "classDigikamEditorChannelMixerToolPlugin_1_1ChannelMixerTool.html#a5a58fa2bcc13672c3fa3f372e006a416", null ],
    [ "setToolIcon", "classDigikamEditorChannelMixerToolPlugin_1_1ChannelMixerTool.html#aab748c0d89f3791f1096867dcc03a0ca", null ],
    [ "setToolInfoMessage", "classDigikamEditorChannelMixerToolPlugin_1_1ChannelMixerTool.html#aafb8d4048e28c142cdec61af4e9f791f", null ],
    [ "setToolName", "classDigikamEditorChannelMixerToolPlugin_1_1ChannelMixerTool.html#ab4f48edf807c06660bba77d411672336", null ],
    [ "setToolSettings", "classDigikamEditorChannelMixerToolPlugin_1_1ChannelMixerTool.html#a81a16e17809e0c1517c09cff1134041e", null ],
    [ "setToolVersion", "classDigikamEditorChannelMixerToolPlugin_1_1ChannelMixerTool.html#abd5232a8268bb211859bba4acd386277", null ],
    [ "setToolView", "classDigikamEditorChannelMixerToolPlugin_1_1ChannelMixerTool.html#a22889920743aafd0773c877a25f69f2e", null ],
    [ "slotAbort", "classDigikamEditorChannelMixerToolPlugin_1_1ChannelMixerTool.html#a41de685d774c6a146ad8078933b27adf", null ],
    [ "slotAnalyserFinished", "classDigikamEditorChannelMixerToolPlugin_1_1ChannelMixerTool.html#af856e4ce609759e7c36dcc815a93d196", null ],
    [ "slotAnalyserStarted", "classDigikamEditorChannelMixerToolPlugin_1_1ChannelMixerTool.html#a117e0b69a814c8f79710b16497c0c0dc", null ],
    [ "slotApplyTool", "classDigikamEditorChannelMixerToolPlugin_1_1ChannelMixerTool.html#acb6a259870c46dcaf95ff3ea0d583e83", null ],
    [ "slotCancel", "classDigikamEditorChannelMixerToolPlugin_1_1ChannelMixerTool.html#a0c449a1ea7e9fdc01e4383117b8c6463", null ],
    [ "slotChannelChanged", "classDigikamEditorChannelMixerToolPlugin_1_1ChannelMixerTool.html#a475dbb46a84bc63ebf9cb6eeb9863b2a", null ],
    [ "slotCloseTool", "classDigikamEditorChannelMixerToolPlugin_1_1ChannelMixerTool.html#a2baf799e8d8ae25fd947696fe99263bb", null ],
    [ "slotFilterFinished", "classDigikamEditorChannelMixerToolPlugin_1_1ChannelMixerTool.html#ab2be25a913731793eb9209422a7f6508", null ],
    [ "slotFilterStarted", "classDigikamEditorChannelMixerToolPlugin_1_1ChannelMixerTool.html#a9ce953ad6bc30cf8e86729abe65c08f1", null ],
    [ "slotInit", "classDigikamEditorChannelMixerToolPlugin_1_1ChannelMixerTool.html#a0cecdbcbea20574ce8ccd6fcc6127a8d", null ],
    [ "slotOk", "classDigikamEditorChannelMixerToolPlugin_1_1ChannelMixerTool.html#ace3e7bea6fd42a19edf0e08c0692ed3a", null ],
    [ "slotPreview", "classDigikamEditorChannelMixerToolPlugin_1_1ChannelMixerTool.html#aab459857669608b0544bf312b561f6bc", null ],
    [ "slotPreviewModeChanged", "classDigikamEditorChannelMixerToolPlugin_1_1ChannelMixerTool.html#a5d112b1773806c67813deb6ef764787e", null ],
    [ "slotProgress", "classDigikamEditorChannelMixerToolPlugin_1_1ChannelMixerTool.html#a2d12fadb5f6fc61d799bfe049bb22558", null ],
    [ "slotScaleChanged", "classDigikamEditorChannelMixerToolPlugin_1_1ChannelMixerTool.html#ac2385c79f4cf636bc1c615dc23ec657a", null ],
    [ "slotTimer", "classDigikamEditorChannelMixerToolPlugin_1_1ChannelMixerTool.html#a5cef4546adc836a3fd9bfea6aad8e1d2", null ],
    [ "slotUpdateSpotInfo", "classDigikamEditorChannelMixerToolPlugin_1_1ChannelMixerTool.html#abf7a46f52815a5ead35007caa77a3ff7", null ],
    [ "toolCategory", "classDigikamEditorChannelMixerToolPlugin_1_1ChannelMixerTool.html#a96deea8a2ee1155e014bfef257b84d3f", null ],
    [ "toolHelp", "classDigikamEditorChannelMixerToolPlugin_1_1ChannelMixerTool.html#aa3a4ca4085bc733beb0a577929a535c2", null ],
    [ "toolIcon", "classDigikamEditorChannelMixerToolPlugin_1_1ChannelMixerTool.html#ac8e1df7974d5dbfd448a1d4d87b4273f", null ],
    [ "toolName", "classDigikamEditorChannelMixerToolPlugin_1_1ChannelMixerTool.html#a18c70c25e0103b592fd74c5dcdf30a21", null ],
    [ "toolSettings", "classDigikamEditorChannelMixerToolPlugin_1_1ChannelMixerTool.html#a20fa60db7f77d9955314bf52900b42a2", null ],
    [ "toolVersion", "classDigikamEditorChannelMixerToolPlugin_1_1ChannelMixerTool.html#a3deae60f1ffeea29811b8fe75783f0a4", null ],
    [ "toolView", "classDigikamEditorChannelMixerToolPlugin_1_1ChannelMixerTool.html#aeb9bb1a5bcab1ee44299230a1c935a58", null ]
];