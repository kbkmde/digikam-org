var dir_804354ab758058040e3d5a40c7b4684a =
[
    [ "yfauth", "dir_6ab51b448814461f3bee82724f3cd296.html", "dir_6ab51b448814461f3bee82724f3cd296" ],
    [ "yfalbum.cpp", "yfalbum_8cpp.html", "yfalbum_8cpp" ],
    [ "yfalbum.h", "yfalbum_8h.html", "yfalbum_8h" ],
    [ "yfnewalbumdlg.cpp", "yfnewalbumdlg_8cpp.html", null ],
    [ "yfnewalbumdlg.h", "yfnewalbumdlg_8h.html", [
      [ "YFNewAlbumDlg", "classDigikamGenericYFPlugin_1_1YFNewAlbumDlg.html", "classDigikamGenericYFPlugin_1_1YFNewAlbumDlg" ]
    ] ],
    [ "yfphoto.cpp", "yfphoto_8cpp.html", "yfphoto_8cpp" ],
    [ "yfphoto.h", "yfphoto_8h.html", "yfphoto_8h" ],
    [ "yfplugin.cpp", "yfplugin_8cpp.html", null ],
    [ "yfplugin.h", "yfplugin_8h.html", "yfplugin_8h" ],
    [ "yftalker.cpp", "yftalker_8cpp.html", null ],
    [ "yftalker.h", "yftalker_8h.html", [
      [ "YFTalker", "classDigikamGenericYFPlugin_1_1YFTalker.html", "classDigikamGenericYFPlugin_1_1YFTalker" ]
    ] ],
    [ "yfwidget.cpp", "yfwidget_8cpp.html", null ],
    [ "yfwidget.h", "yfwidget_8h.html", [
      [ "YFWidget", "classDigikamGenericYFPlugin_1_1YFWidget.html", "classDigikamGenericYFPlugin_1_1YFWidget" ]
    ] ],
    [ "yfwindow.cpp", "yfwindow_8cpp.html", null ],
    [ "yfwindow.h", "yfwindow_8h.html", [
      [ "YFWindow", "classDigikamGenericYFPlugin_1_1YFWindow.html", "classDigikamGenericYFPlugin_1_1YFWindow" ]
    ] ]
];