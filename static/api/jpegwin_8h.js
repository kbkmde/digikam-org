var jpegwin_8h =
[
    [ "digikam_source_mgr", "structDigikam_1_1JPEGUtils_1_1digikam__source__mgr.html", "structDigikam_1_1JPEGUtils_1_1digikam__source__mgr" ],
    [ "fill_input_buffer", "jpegwin_8h.html#a12fb1931afda411f6d2cb42e71da638a", null ],
    [ "init_source", "jpegwin_8h.html#a682a9b5df93a1a2c2fcf6a895328444f", null ],
    [ "jpeg_memory_src", "jpegwin_8h.html#a8b7574de419e947429295f1a38583cbe", null ],
    [ "skip_input_data", "jpegwin_8h.html#a5c900838f0a9592c1dcefac7d79af9d1", null ],
    [ "term_source", "jpegwin_8h.html#a3d169f0f42c1fe9baf54b01d3491000f", null ]
];