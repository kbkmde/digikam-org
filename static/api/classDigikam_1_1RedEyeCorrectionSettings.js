var classDigikam_1_1RedEyeCorrectionSettings =
[
    [ "RedEyeCorrectionSettings", "classDigikam_1_1RedEyeCorrectionSettings.html#a5436b6ec4379b1025119d50a76c89d3f", null ],
    [ "~RedEyeCorrectionSettings", "classDigikam_1_1RedEyeCorrectionSettings.html#a4aea574505dda37f7ccd7b4bebd4d4a5", null ],
    [ "defaultSettings", "classDigikam_1_1RedEyeCorrectionSettings.html#ae25eaff5aef6213e5d6cf46fff942095", null ],
    [ "readSettings", "classDigikam_1_1RedEyeCorrectionSettings.html#a3e21fdae981766908c72c7606fc33c01", null ],
    [ "resetToDefault", "classDigikam_1_1RedEyeCorrectionSettings.html#ac4254da0d66e043d92e9cff4a5a6d8ed", null ],
    [ "setSettings", "classDigikam_1_1RedEyeCorrectionSettings.html#a25f10892999c23d3f2888e631dc5c800", null ],
    [ "settings", "classDigikam_1_1RedEyeCorrectionSettings.html#ab9452c8a9971d1783b2cbc015d4cf388", null ],
    [ "signalSettingsChanged", "classDigikam_1_1RedEyeCorrectionSettings.html#a6310d91ca37ad88bef96c1e79cfce24e", null ],
    [ "writeSettings", "classDigikam_1_1RedEyeCorrectionSettings.html#a573065d178577b385c1ac2202de0b371", null ]
];