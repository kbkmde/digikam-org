var dir_00c9f9bdadc695d9fbf3e6831dba2a08 =
[
    [ "camerafolderitem.cpp", "camerafolderitem_8cpp.html", null ],
    [ "camerafolderitem.h", "camerafolderitem_8h.html", [
      [ "CameraFolderItem", "classDigikam_1_1CameraFolderItem.html", "classDigikam_1_1CameraFolderItem" ]
    ] ],
    [ "importcategorydrawer.cpp", "importcategorydrawer_8cpp.html", null ],
    [ "importcategorydrawer.h", "importcategorydrawer_8h.html", [
      [ "ImportCategoryDrawer", "classDigikam_1_1ImportCategoryDrawer.html", "classDigikam_1_1ImportCategoryDrawer" ]
    ] ],
    [ "importdelegate.cpp", "importdelegate_8cpp.html", null ],
    [ "importdelegate.h", "importdelegate_8h.html", [
      [ "ImportDelegate", "classDigikam_1_1ImportDelegate.html", "classDigikam_1_1ImportDelegate" ],
      [ "ImportNormalDelegate", "classDigikam_1_1ImportNormalDelegate.html", "classDigikam_1_1ImportNormalDelegate" ],
      [ "ImportThumbnailDelegate", "classDigikam_1_1ImportThumbnailDelegate.html", "classDigikam_1_1ImportThumbnailDelegate" ]
    ] ],
    [ "importdelegate_p.h", "importdelegate__p_8h.html", [
      [ "ImportDelegatePrivate", "classDigikam_1_1ImportDelegate_1_1ImportDelegatePrivate.html", "classDigikam_1_1ImportDelegate_1_1ImportDelegatePrivate" ],
      [ "ImportNormalDelegatePrivate", "classDigikam_1_1ImportNormalDelegatePrivate.html", "classDigikam_1_1ImportNormalDelegatePrivate" ],
      [ "ImportThumbnailDelegatePrivate", "classDigikam_1_1ImportThumbnailDelegatePrivate.html", "classDigikam_1_1ImportThumbnailDelegatePrivate" ]
    ] ],
    [ "importoverlays.cpp", "importoverlays_8cpp.html", null ],
    [ "importoverlays.h", "importoverlays_8h.html", "importoverlays_8h" ],
    [ "importtooltipfiller.cpp", "importtooltipfiller_8cpp.html", null ],
    [ "importtooltipfiller.h", "importtooltipfiller_8h.html", "importtooltipfiller_8h" ],
    [ "itemviewimportdelegate.cpp", "itemviewimportdelegate_8cpp.html", null ],
    [ "itemviewimportdelegate.h", "itemviewimportdelegate_8h.html", [
      [ "ItemViewImportDelegate", "classDigikam_1_1ItemViewImportDelegate.html", "classDigikam_1_1ItemViewImportDelegate" ]
    ] ],
    [ "itemviewimportdelegate_p.h", "itemviewimportdelegate__p_8h.html", [
      [ "ItemViewImportDelegatePrivate", "classDigikam_1_1ItemViewImportDelegatePrivate.html", "classDigikam_1_1ItemViewImportDelegatePrivate" ]
    ] ]
];