var classDigikam_1_1IccSettings =
[
    [ "Private", "classDigikam_1_1IccSettings_1_1Private.html", "classDigikam_1_1IccSettings_1_1Private" ],
    [ "allProfiles", "classDigikam_1_1IccSettings.html#a80e44cbdc6c1464476c22c2ca2ffc64d", null ],
    [ "displayProfiles", "classDigikam_1_1IccSettings.html#a6c0937b2d335939dc2a7440fcd5c28d3", null ],
    [ "inputProfiles", "classDigikam_1_1IccSettings.html#a9feb9f9d0010e958c66a018538d03d8e", null ],
    [ "isEnabled", "classDigikam_1_1IccSettings.html#a3be4e762134093d5ca816e240bc7698a", null ],
    [ "loadAllProfilesProperties", "classDigikam_1_1IccSettings.html#a176c78eaf1e978a2120dcb75be0cc7ec", null ],
    [ "monitorProfile", "classDigikam_1_1IccSettings.html#aa74305f0bd97598530261bc143202c75", null ],
    [ "monitorProfileFromSystem", "classDigikam_1_1IccSettings.html#acbaed2581171647d3bcef759d027060c", null ],
    [ "outputProfiles", "classDigikam_1_1IccSettings.html#aeda6dff3d0f52e7ccf5bfbe2c40c4ae2", null ],
    [ "profilesForDescription", "classDigikam_1_1IccSettings.html#ab858646f116274ed5559c98d1e91db50", null ],
    [ "setIccPath", "classDigikam_1_1IccSettings.html#ad9a64940e895660f1d6800974145425a", null ],
    [ "setSettings", "classDigikam_1_1IccSettings.html#a94480aa46d23383ec8ae5289421a464d", null ],
    [ "settings", "classDigikam_1_1IccSettings.html#aa20b7a1010532516372aca58e3b79df7", null ],
    [ "setUseManagedPreviews", "classDigikam_1_1IccSettings.html#a817ec76a3d72f2d3fc3e151ed74f35f9", null ],
    [ "setUseManagedView", "classDigikam_1_1IccSettings.html#ab59b309ef7040644301495408425cbe4", null ],
    [ "signalICCSettingsChanged", "classDigikam_1_1IccSettings.html#a11b969820300eb570bae46c5511c6ab4", null ],
    [ "signalSettingsChanged", "classDigikam_1_1IccSettings.html#aedd818c04a92fb1a626025c2242002ad", null ],
    [ "useManagedPreviews", "classDigikam_1_1IccSettings.html#a835d1f51f649345595a64ce2bb639fd7", null ],
    [ "workspaceProfiles", "classDigikam_1_1IccSettings.html#a844c23ea49c24c70da478f45c6a74e01", null ],
    [ "IccSettingsCreator", "classDigikam_1_1IccSettings.html#a48de05d1ed7ea2cbbc8ab6624f7bfd3f", null ],
    [ "Private", "classDigikam_1_1IccSettings.html#ac96b60d37bd806132da680e187dc2288", null ]
];