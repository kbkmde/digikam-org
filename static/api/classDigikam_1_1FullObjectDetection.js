var classDigikam_1_1FullObjectDetection =
[
    [ "FullObjectDetection", "classDigikam_1_1FullObjectDetection.html#ac1a606b967639b9cdd61541a3384befd", null ],
    [ "FullObjectDetection", "classDigikam_1_1FullObjectDetection.html#aa15716cbb70e3ca2e016f8410d2c19fc", null ],
    [ "FullObjectDetection", "classDigikam_1_1FullObjectDetection.html#a597aad2396a58cac661abf4c3f0c196c", null ],
    [ "get_rect", "classDigikam_1_1FullObjectDetection.html#abcc0ce926aa8f94f9b367763c739d3e0", null ],
    [ "get_rect", "classDigikam_1_1FullObjectDetection.html#a5e51e4b98659e1f26b76026e56310d62", null ],
    [ "num_parts", "classDigikam_1_1FullObjectDetection.html#a943ee2acff25f3c979923ed43f64c7de", null ],
    [ "part", "classDigikam_1_1FullObjectDetection.html#ab5f323f7581fb1a07e362cd8365c5e44", null ],
    [ "part", "classDigikam_1_1FullObjectDetection.html#a6c2ea1916019593f4f1f72d8ecd21442", null ]
];