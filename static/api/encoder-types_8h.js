var encoder_types_8h =
[
    [ "CTBTreeMatrix", "classCTBTreeMatrix.html", "classCTBTreeMatrix" ],
    [ "enc_cb", "classenc__cb.html", "classenc__cb" ],
    [ "enc_node", "classenc__node.html", "classenc__node" ],
    [ "enc_pb_inter", "structenc__pb__inter.html", "structenc__pb__inter" ],
    [ "enc_tb", "classenc__tb.html", "classenc__tb" ],
    [ "PixelAccessor", "classPixelAccessor.html", "classPixelAccessor" ],
    [ "small_image_buffer", "classsmall__image__buffer.html", "classsmall__image__buffer" ],
    [ "childX", "encoder-types_8h.html#ad3757f5dcdff99d7fbbc38f8134865c8", null ],
    [ "childY", "encoder-types_8h.html#a705ac07efcc09fc48dd30004f3c2b20d", null ]
];