var classShowFoto_1_1ShowfotoStackViewList =
[
    [ "StackViewRole", "classShowFoto_1_1ShowfotoStackViewList.html#a5214f3af88138c8dc19a058ebff886b2", [
      [ "FileName", "classShowFoto_1_1ShowfotoStackViewList.html#a5214f3af88138c8dc19a058ebff886b2a19050a0cfb2a524e9bcae9e7da405d9a", null ],
      [ "FileSize", "classShowFoto_1_1ShowfotoStackViewList.html#a5214f3af88138c8dc19a058ebff886b2a7cece840138168fab525b4474893d19e", null ],
      [ "FileType", "classShowFoto_1_1ShowfotoStackViewList.html#a5214f3af88138c8dc19a058ebff886b2a5310a7ba9c762735e5570d82d71e3c78", null ],
      [ "FileDate", "classShowFoto_1_1ShowfotoStackViewList.html#a5214f3af88138c8dc19a058ebff886b2afa7cbb8ea4c28bfd7885b35216acadcb", null ]
    ] ],
    [ "ThumbnailSize", "classShowFoto_1_1ShowfotoStackViewList.html#a1a8b1261207ef08b23e478d6f194d7cd", [
      [ "SizeSmall", "classShowFoto_1_1ShowfotoStackViewList.html#a1a8b1261207ef08b23e478d6f194d7cdaefd1c45c423f9f745fe53080f64404b7", null ],
      [ "SizeMedium", "classShowFoto_1_1ShowfotoStackViewList.html#a1a8b1261207ef08b23e478d6f194d7cdab7fdda868b5455cc7430306a071a5b9e", null ],
      [ "SizeLarge", "classShowFoto_1_1ShowfotoStackViewList.html#a1a8b1261207ef08b23e478d6f194d7cdab44f76e7692bdb5809cad08882c15187", null ],
      [ "SizeHuge", "classShowFoto_1_1ShowfotoStackViewList.html#a1a8b1261207ef08b23e478d6f194d7cdab29b13b8279483344a875e32145a7100", null ]
    ] ],
    [ "ShowfotoStackViewList", "classShowFoto_1_1ShowfotoStackViewList.html#adc0189d92fd0d982273a7b7dc1b417f3", null ],
    [ "~ShowfotoStackViewList", "classShowFoto_1_1ShowfotoStackViewList.html#ae56e070297b8891dd0fdc17d76a88e73", null ],
    [ "currentUrl", "classShowFoto_1_1ShowfotoStackViewList.html#aeb272bef5dec7493eaa442050e1a99b0", null ],
    [ "infoFromIndex", "classShowFoto_1_1ShowfotoStackViewList.html#a5a62ad8be63e67c33958f494f394a669", null ],
    [ "setThumbbar", "classShowFoto_1_1ShowfotoStackViewList.html#a6a52c17de6cf49626257dcfd0570a8b2", null ],
    [ "signalAddFavorite", "classShowFoto_1_1ShowfotoStackViewList.html#a90482e40b47af32ce7bf39d333687d30", null ],
    [ "signalClearItemsList", "classShowFoto_1_1ShowfotoStackViewList.html#acad9af73e07a88f773193b7c292f84d5", null ],
    [ "signalItemListChanged", "classShowFoto_1_1ShowfotoStackViewList.html#af6f6c57c7f9f8a9ed03c2d8c08a228cf", null ],
    [ "signalRemoveItemInfos", "classShowFoto_1_1ShowfotoStackViewList.html#ad5c585735c5ad3b82e79878f742803cf", null ],
    [ "signalShowfotoItemInfoActivated", "classShowFoto_1_1ShowfotoStackViewList.html#aa5a28a931d71941a3eb728de5f65520e", null ],
    [ "slotIconSizeChanged", "classShowFoto_1_1ShowfotoStackViewList.html#a227e91e67ee0974cd18ffe8b787afc81", null ],
    [ "sortOrder", "classShowFoto_1_1ShowfotoStackViewList.html#ac8a9d851c4183dbc69456eb9f9cce1c8", null ],
    [ "sortRole", "classShowFoto_1_1ShowfotoStackViewList.html#a7659bdeadb62f6dcb3b8bc3aa38ee56a", null ],
    [ "urls", "classShowFoto_1_1ShowfotoStackViewList.html#ac23778249d451844856bf4bf4ed7056d", null ]
];