var encoder_params_8h =
[
    [ "encoder_params", "structencoder__params.html", "structencoder__params" ],
    [ "option_MEMode", "classoption__MEMode.html", "classoption__MEMode" ],
    [ "option_SOP_Structure", "classoption__SOP__Structure.html", "classoption__SOP__Structure" ],
    [ "IntraPredSearch", "encoder-params_8h.html#ab3ab038001baa57f926fa0eec7cedd68", [
      [ "IntraPredSearch_Complete", "encoder-params_8h.html#ab3ab038001baa57f926fa0eec7cedd68a4d5bc78f2b355ea646ecd4c99f624704", null ]
    ] ],
    [ "MEMode", "encoder-params_8h.html#a507687766a392e06aa3d0e82890dbc35", [
      [ "MEMode_Test", "encoder-params_8h.html#a507687766a392e06aa3d0e82890dbc35a46f6b6313625b8fc25964c29a86f324c", null ],
      [ "MEMode_Search", "encoder-params_8h.html#a507687766a392e06aa3d0e82890dbc35a61b9b41f20cd7f6ec010fad5f558496b", null ]
    ] ],
    [ "RateControlMethod", "encoder-params_8h.html#a8048035a2daada8a80aa25dd04d54d79", [
      [ "RateControlMethod_ConstantQP", "encoder-params_8h.html#a8048035a2daada8a80aa25dd04d54d79ac4cf1afb2827e83468bbc9bc2b7891ea", null ],
      [ "RateControlMethod_ConstantLambda", "encoder-params_8h.html#a8048035a2daada8a80aa25dd04d54d79a59421be4121fcc2a8acb66a540ad15ad", null ]
    ] ],
    [ "SOP_Structure", "encoder-params_8h.html#abe1c78946ea7f52cffe4b13885eba99b", [
      [ "SOP_Intra", "encoder-params_8h.html#abe1c78946ea7f52cffe4b13885eba99babb272b062e268a1895d0d284cfd10201", null ],
      [ "SOP_LowDelay", "encoder-params_8h.html#abe1c78946ea7f52cffe4b13885eba99ba2eed446d032d71dcd5c454199bf6b345", null ]
    ] ]
];