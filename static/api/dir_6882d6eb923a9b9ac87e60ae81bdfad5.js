var dir_6882d6eb923a9b9ac87e60ae81bdfad5 =
[
    [ "videodecoder.cpp", "videodecoder_8cpp.html", null ],
    [ "videodecoder.h", "videodecoder_8h.html", [
      [ "VideoDecoder", "classDigikam_1_1VideoDecoder.html", "classDigikam_1_1VideoDecoder" ]
    ] ],
    [ "videodecoder_p.cpp", "videodecoder__p_8cpp.html", null ],
    [ "videodecoder_p.h", "videodecoder__p_8h.html", [
      [ "Private", "classDigikam_1_1VideoDecoder_1_1Private.html", "classDigikam_1_1VideoDecoder_1_1Private" ]
    ] ],
    [ "videostripfilter.cpp", "videostripfilter_8cpp.html", null ],
    [ "videostripfilter.h", "videostripfilter_8h.html", [
      [ "VideoStripFilter", "classDigikam_1_1VideoStripFilter.html", "classDigikam_1_1VideoStripFilter" ]
    ] ],
    [ "videothumbnailer.cpp", "videothumbnailer_8cpp.html", null ],
    [ "videothumbnailer.h", "videothumbnailer_8h.html", [
      [ "VideoThumbnailer", "classDigikam_1_1VideoThumbnailer.html", "classDigikam_1_1VideoThumbnailer" ]
    ] ],
    [ "videothumbwriter.cpp", "videothumbwriter_8cpp.html", null ],
    [ "videothumbwriter.h", "videothumbwriter_8h.html", [
      [ "VideoFrame", "classDigikam_1_1VideoFrame.html", "classDigikam_1_1VideoFrame" ],
      [ "VideoThumbWriter", "classDigikam_1_1VideoThumbWriter.html", "classDigikam_1_1VideoThumbWriter" ]
    ] ]
];