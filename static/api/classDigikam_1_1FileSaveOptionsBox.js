var classDigikam_1_1FileSaveOptionsBox =
[
    [ "FileSaveOptionsBox", "classDigikam_1_1FileSaveOptionsBox.html#a9bc9596a9e3c289c8c1a25845d6fe768", null ],
    [ "~FileSaveOptionsBox", "classDigikam_1_1FileSaveOptionsBox.html#aa2f5dafbad95fa477f2af15650834359", null ],
    [ "applySettings", "classDigikam_1_1FileSaveOptionsBox.html#abebceceeb966a4f33d8ec35036089b36", null ],
    [ "discoverFormat", "classDigikam_1_1FileSaveOptionsBox.html#a2bec826f6d14eabd323eba3da8c254f7", null ],
    [ "setImageFileFormat", "classDigikam_1_1FileSaveOptionsBox.html#a54ffa4a7547dfc60543d80c1a8ed5fd1", null ]
];