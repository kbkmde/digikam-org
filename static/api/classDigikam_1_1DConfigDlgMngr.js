var classDigikam_1_1DConfigDlgMngr =
[
    [ "DConfigDlgMngr", "classDigikam_1_1DConfigDlgMngr.html#add2859c495da495d40d2c24f59f903f9", null ],
    [ "~DConfigDlgMngr", "classDigikam_1_1DConfigDlgMngr.html#afc8f1d96c77fba9d8fa36b9574bea481", null ],
    [ "addWidget", "classDigikam_1_1DConfigDlgMngr.html#a92ee5fb83ee9320e0f3af79d26e1da32", null ],
    [ "getCustomProperty", "classDigikam_1_1DConfigDlgMngr.html#ac2d60259095b5b32ec17bb36ac6d762a", null ],
    [ "getCustomPropertyChangedSignal", "classDigikam_1_1DConfigDlgMngr.html#af48b21a20ab5254ac4d141efc14f7f94", null ],
    [ "getUserProperty", "classDigikam_1_1DConfigDlgMngr.html#ac5cc267894087eed044457708fc51627", null ],
    [ "getUserPropertyChangedSignal", "classDigikam_1_1DConfigDlgMngr.html#a38a7f005f9de1fcae94040c57d0469be", null ],
    [ "hasChanged", "classDigikam_1_1DConfigDlgMngr.html#a5c5ee8b7c3a105a056de2df130a142b3", null ],
    [ "init", "classDigikam_1_1DConfigDlgMngr.html#a0eeff2348cf0022149485d64f7f3333f", null ],
    [ "isDefault", "classDigikam_1_1DConfigDlgMngr.html#a54ab0fe7c254d9d84e5beb9bc267bf6e", null ],
    [ "parseChildren", "classDigikam_1_1DConfigDlgMngr.html#a662695437ec82ea9d45f1404def2d96a", null ],
    [ "property", "classDigikam_1_1DConfigDlgMngr.html#a8107c35638abe0446e90fe189f4bfb7f", null ],
    [ "setProperty", "classDigikam_1_1DConfigDlgMngr.html#a32341fabc0c30bb3e5d0bd3fa10fc310", null ],
    [ "settingsChanged", "classDigikam_1_1DConfigDlgMngr.html#a264e57ab84d669ec044492817e10bb75", null ],
    [ "settingsChanged", "classDigikam_1_1DConfigDlgMngr.html#a611ab61d57a0152ee20ce429fd83b2f5", null ],
    [ "setupWidget", "classDigikam_1_1DConfigDlgMngr.html#a5761f0aac4fe668b6903ab50932e117c", null ],
    [ "updateSettings", "classDigikam_1_1DConfigDlgMngr.html#a3b79225a13fa6a0d26ed672e16f64920", null ],
    [ "updateWidgets", "classDigikam_1_1DConfigDlgMngr.html#a7a01017b25ad3cdf7494b270ccb31e1d", null ],
    [ "updateWidgetsDefault", "classDigikam_1_1DConfigDlgMngr.html#ad5ca67e996d6bf31a5ed7c492d254c07", null ],
    [ "widgetModified", "classDigikam_1_1DConfigDlgMngr.html#a05de09eeca31bd5920b876c832a79653", null ]
];