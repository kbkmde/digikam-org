var namespaceDigikamGenericDNGConverterPlugin =
[
    [ "DNGConverterActionData", "classDigikamGenericDNGConverterPlugin_1_1DNGConverterActionData.html", "classDigikamGenericDNGConverterPlugin_1_1DNGConverterActionData" ],
    [ "DNGConverterActionThread", "classDigikamGenericDNGConverterPlugin_1_1DNGConverterActionThread.html", "classDigikamGenericDNGConverterPlugin_1_1DNGConverterActionThread" ],
    [ "DNGConverterDialog", "classDigikamGenericDNGConverterPlugin_1_1DNGConverterDialog.html", "classDigikamGenericDNGConverterPlugin_1_1DNGConverterDialog" ],
    [ "DNGConverterList", "classDigikamGenericDNGConverterPlugin_1_1DNGConverterList.html", "classDigikamGenericDNGConverterPlugin_1_1DNGConverterList" ],
    [ "DNGConverterListViewItem", "classDigikamGenericDNGConverterPlugin_1_1DNGConverterListViewItem.html", "classDigikamGenericDNGConverterPlugin_1_1DNGConverterListViewItem" ],
    [ "DNGConverterPlugin", "classDigikamGenericDNGConverterPlugin_1_1DNGConverterPlugin.html", "classDigikamGenericDNGConverterPlugin_1_1DNGConverterPlugin" ],
    [ "DNGConverterTask", "classDigikamGenericDNGConverterPlugin_1_1DNGConverterTask.html", "classDigikamGenericDNGConverterPlugin_1_1DNGConverterTask" ],
    [ "DNGConverterAction", "namespaceDigikamGenericDNGConverterPlugin.html#a973d39362c08f3b46e502615f60c89ed", [
      [ "NONE", "namespaceDigikamGenericDNGConverterPlugin.html#a973d39362c08f3b46e502615f60c89eda0f6f9db07384471e319e35b8755b827a", null ],
      [ "IDENTIFY", "namespaceDigikamGenericDNGConverterPlugin.html#a973d39362c08f3b46e502615f60c89eda222cecf912baaae3948428bb1a0e4c1c", null ],
      [ "PROCESS", "namespaceDigikamGenericDNGConverterPlugin.html#a973d39362c08f3b46e502615f60c89eda544697836ba26d4044b358cf1f65d1cd", null ]
    ] ]
];