var digikam__globals__bundles_8cpp =
[
    [ "adjustedEnvironmentForAppImage", "digikam__globals__bundles_8cpp.html#abdbde7171265f488ecf60770fa654c94", null ],
    [ "installQtTranslationFiles", "digikam__globals__bundles_8cpp.html#aef7aab1fa676680d50879a50d4487ca9", null ],
    [ "isRunningInAppImageBundle", "digikam__globals__bundles_8cpp.html#aaa58e5ff23ca7fd3ec8e3089f199a52d", null ],
    [ "loadEcmQtTranslationFiles", "digikam__globals__bundles_8cpp.html#ab7938f4c91236db6a89f715edd5473db", null ],
    [ "loadStdQtTranslationFiles", "digikam__globals__bundles_8cpp.html#a5ca2ce891ae5f543ceeea52e1a54543b", null ],
    [ "macOSBundlePrefix", "digikam__globals__bundles_8cpp.html#afd8b206f3bb25f68d9472f0dc001929c", null ],
    [ "tryInitDrMingw", "digikam__globals__bundles_8cpp.html#a4433d9e68f7da24d6a629aaf206bf913", null ],
    [ "unloadQtTranslationFiles", "digikam__globals__bundles_8cpp.html#aed07d6302e8ed66ede19f0256af281c4", null ]
];