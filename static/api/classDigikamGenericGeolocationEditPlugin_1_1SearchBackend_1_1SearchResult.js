var classDigikamGenericGeolocationEditPlugin_1_1SearchBackend_1_1SearchResult =
[
    [ "List", "classDigikamGenericGeolocationEditPlugin_1_1SearchBackend_1_1SearchResult.html#a3d3e273a4a11833bdced684051a97f1f", null ],
    [ "SearchResult", "classDigikamGenericGeolocationEditPlugin_1_1SearchBackend_1_1SearchResult.html#a53bc68b37b488d760098b6f06d412799", null ],
    [ "boundingBox", "classDigikamGenericGeolocationEditPlugin_1_1SearchBackend_1_1SearchResult.html#a376f2fcdb7009acf8dd04b189e8bba84", null ],
    [ "coordinates", "classDigikamGenericGeolocationEditPlugin_1_1SearchBackend_1_1SearchResult.html#a6870364f805926979215d8c874409244", null ],
    [ "internalId", "classDigikamGenericGeolocationEditPlugin_1_1SearchBackend_1_1SearchResult.html#afd1b439df61d5ad350ccccb1724daa7d", null ],
    [ "name", "classDigikamGenericGeolocationEditPlugin_1_1SearchBackend_1_1SearchResult.html#a6c58ff02e632146bcd984ec359d98336", null ]
];