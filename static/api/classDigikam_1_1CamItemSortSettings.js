var classDigikam_1_1CamItemSortSettings =
[
    [ "CategorizationMode", "classDigikam_1_1CamItemSortSettings.html#a81b2849b036f2d1de0e7a70f62151eb6", [
      [ "NoCategories", "classDigikam_1_1CamItemSortSettings.html#a81b2849b036f2d1de0e7a70f62151eb6a218fcc785056fab9ac1d836932759ec4", null ],
      [ "CategoryByFolder", "classDigikam_1_1CamItemSortSettings.html#a81b2849b036f2d1de0e7a70f62151eb6af3b3701f4a202c96c85e302fe92326ac", null ],
      [ "CategoryByFormat", "classDigikam_1_1CamItemSortSettings.html#a81b2849b036f2d1de0e7a70f62151eb6a44685a0a9e1a29acdfdce3622f2e1a6a", null ],
      [ "CategoryByDate", "classDigikam_1_1CamItemSortSettings.html#a81b2849b036f2d1de0e7a70f62151eb6a58047227f2395f69df1fc5d39d0c654b", null ]
    ] ],
    [ "SortOrder", "classDigikam_1_1CamItemSortSettings.html#abd3bb9456dfc598344ae656b3b109868", [
      [ "AscendingOrder", "classDigikam_1_1CamItemSortSettings.html#abd3bb9456dfc598344ae656b3b109868a8c322d06d9d9f88d2a3bdca03b35b823", null ],
      [ "DescendingOrder", "classDigikam_1_1CamItemSortSettings.html#abd3bb9456dfc598344ae656b3b109868a7be0b90736e97fe5e7ee3a6fd63aba5d", null ],
      [ "DefaultOrder", "classDigikam_1_1CamItemSortSettings.html#abd3bb9456dfc598344ae656b3b109868abe4fc60e4eb0f8648ccdb7bfd7899cd3", null ]
    ] ],
    [ "SortRole", "classDigikam_1_1CamItemSortSettings.html#adb8b088faec254d586e3166e13fd8cda", [
      [ "SortByFileName", "classDigikam_1_1CamItemSortSettings.html#adb8b088faec254d586e3166e13fd8cdaabb1e16eb959aa0f20d1f0496dff841c8", null ],
      [ "SortByFilePath", "classDigikam_1_1CamItemSortSettings.html#adb8b088faec254d586e3166e13fd8cdaa712f2e6efc7339f0b48fe69b6641732b", null ],
      [ "SortByCreationDate", "classDigikam_1_1CamItemSortSettings.html#adb8b088faec254d586e3166e13fd8cdaabee63548b776acf27d4d9d73370f1fc4", null ],
      [ "SortByFileSize", "classDigikam_1_1CamItemSortSettings.html#adb8b088faec254d586e3166e13fd8cdaa31c235d1b048fc7a9574e16953d4f0d2", null ],
      [ "SortByDownloadState", "classDigikam_1_1CamItemSortSettings.html#adb8b088faec254d586e3166e13fd8cdaa0ad34fc0d7a31c6f6dd2df839d1e1631", null ],
      [ "SortByRating", "classDigikam_1_1CamItemSortSettings.html#adb8b088faec254d586e3166e13fd8cdaa2e4c59bbffab23d6395c400c1d05ec89", null ]
    ] ],
    [ "CamItemSortSettings", "classDigikam_1_1CamItemSortSettings.html#a2717a06d73af7ec74bf562d3ae7c35e8", null ],
    [ "~CamItemSortSettings", "classDigikam_1_1CamItemSortSettings.html#ab488e9a2bf7579774ee969a736da1503", null ],
    [ "compare", "classDigikam_1_1CamItemSortSettings.html#ab5ae30d669191a47795763da937ef5b8", null ],
    [ "compare", "classDigikam_1_1CamItemSortSettings.html#a21e0e6f560f87d08a5f3ad42b59cd878", null ],
    [ "compareCategories", "classDigikam_1_1CamItemSortSettings.html#a164674cef8e63d445d064cae10b2352a", null ],
    [ "isCategorized", "classDigikam_1_1CamItemSortSettings.html#a7f87ee92a18b756848e9ca3715fe6c2d", null ],
    [ "lessThan", "classDigikam_1_1CamItemSortSettings.html#a06ccc7c2396d9fdf1299d378420475d2", null ],
    [ "lessThan", "classDigikam_1_1CamItemSortSettings.html#a876f2fa74247aed48eb28c8c51c40afe", null ],
    [ "operator==", "classDigikam_1_1CamItemSortSettings.html#ab468c61c3ebff137db5f4b93e618a68e", null ],
    [ "setCategorizationMode", "classDigikam_1_1CamItemSortSettings.html#a6fb4125872090c15a881edfcb596ad99", null ],
    [ "setCategorizationSortOrder", "classDigikam_1_1CamItemSortSettings.html#ab62ba8a251bee80ca079593f87615222", null ],
    [ "setSortOrder", "classDigikam_1_1CamItemSortSettings.html#a813d97decb9600be7edb8f48eca25244", null ],
    [ "setSortRole", "classDigikam_1_1CamItemSortSettings.html#a049c098e1ac12729bcc1889c985f3d96", null ],
    [ "setStringTypeNatural", "classDigikam_1_1CamItemSortSettings.html#a2e8f645aa2dafe907d5bb3fe122d772e", null ],
    [ "categorizationCaseSensitivity", "classDigikam_1_1CamItemSortSettings.html#aad82ead9d64a5f1d69b1e7d72b470f11", null ],
    [ "categorizationMode", "classDigikam_1_1CamItemSortSettings.html#a9909c2e3124e38c4e38af078ce082026", null ],
    [ "categorizationSortOrder", "classDigikam_1_1CamItemSortSettings.html#af0806f26cbaadc2263ef31c12c159554", null ],
    [ "currentCategorizationSortOrder", "classDigikam_1_1CamItemSortSettings.html#ae055938685ee0240fc0ba77ba57c7b4a", null ],
    [ "currentSortOrder", "classDigikam_1_1CamItemSortSettings.html#a02e79f353ba212c21d464d37027d3fce", null ],
    [ "sortCaseSensitivity", "classDigikam_1_1CamItemSortSettings.html#ac9f637f1323ad2055d9690ba1039fcc4", null ],
    [ "sortOrder", "classDigikam_1_1CamItemSortSettings.html#a79c81fb218d704946eb1e573c26b70b6", null ],
    [ "sortRole", "classDigikam_1_1CamItemSortSettings.html#aa2c40106e8308668b3d796ba9bf4db72", null ],
    [ "strTypeNatural", "classDigikam_1_1CamItemSortSettings.html#afe9f34592ebcdd0ecb4162567ff1e7c3", null ]
];