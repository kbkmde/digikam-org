var geoifacecommon_8h =
[
    [ "GeoIfaceCluster", "classDigikam_1_1GeoIfaceCluster.html", "classDigikam_1_1GeoIfaceCluster" ],
    [ "GeoIfaceGlobalObject", "classDigikam_1_1GeoIfaceGlobalObject.html", "classDigikam_1_1GeoIfaceGlobalObject" ],
    [ "GeoIfaceInternalWidgetInfo", "classDigikam_1_1GeoIfaceInternalWidgetInfo.html", "classDigikam_1_1GeoIfaceInternalWidgetInfo" ],
    [ "GeoIfaceSharedData", "classDigikam_1_1GeoIfaceSharedData.html", "classDigikam_1_1GeoIfaceSharedData" ],
    [ "GEOIFACE_ASSERT", "geoifacecommon_8h.html#abe4ae4f529e2110cf50327fc46e4955e", null ],
    [ "GeoIface_assert", "geoifacecommon_8h.html#a3824540999643c4a6158a72960fbf6fd", null ],
    [ "GeoIfaceHelperNormalizeBounds", "geoifacecommon_8h.html#a8c82b04a37b6159612c15c312c48e4bc", null ],
    [ "GeoIfaceHelperParseBoundsString", "geoifacecommon_8h.html#aa831d43bc0f7ac799766de4a0a27e78f", null ],
    [ "GeoIfaceHelperParseLatLonString", "geoifacecommon_8h.html#af859f9d365d1784a0938de806f723f8b", null ],
    [ "GeoIfaceHelperParseXYStringToPoint", "geoifacecommon_8h.html#a15f7c20e4a4926ee0db399386fa90998", null ],
    [ "QPointSquareDistance", "geoifacecommon_8h.html#a6ed63f14ea7b9ca060f1a220f13a3fa5", null ],
    [ "GeoIfaceMinMarkerGroupingRadius", "geoifacecommon_8h.html#a00696d2cf30623ff00021a69cb211f2b", null ],
    [ "GeoIfaceMinThumbnailGroupingRadius", "geoifacecommon_8h.html#ae5b4ce24de5abae26af7947ba00800a2", null ],
    [ "GeoIfaceMinThumbnailSize", "geoifacecommon_8h.html#a14cab434d6bd60651b70213ee07575d5", null ]
];