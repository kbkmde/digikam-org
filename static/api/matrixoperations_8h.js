var matrixoperations_8h =
[
    [ "cvmattostdmat", "matrixoperations_8h.html#a2372763569b814f2b5ffa6eb3c352500", null ],
    [ "determinant", "matrixoperations_8h.html#ae2453b81277e9db5eaad104fb0dab08c", null ],
    [ "inv2", "matrixoperations_8h.html#a0eb70b39a13d5debb41aac5f3439a0e2", null ],
    [ "pinv", "matrixoperations_8h.html#a0823230708278198c642010be10eeed2", null ],
    [ "pythag", "matrixoperations_8h.html#aaaa5c20a27b17692d5364119f1a0b529", null ],
    [ "signdlib", "matrixoperations_8h.html#ad92bfcf837c08ffd5df698fc31397c60", null ],
    [ "stdmattocvmat", "matrixoperations_8h.html#ae7ba3a3b16e4bb3ee128745d205d0237", null ],
    [ "svd", "matrixoperations_8h.html#aff873e7667c885f617d7ff5cf6010aef", null ],
    [ "svd3", "matrixoperations_8h.html#aebb305f68ee337a6d163637bc3528e5f", null ],
    [ "trace", "matrixoperations_8h.html#ad602782b79983c56c16d0c3a68be0900", null ],
    [ "transpose", "matrixoperations_8h.html#a9ec792589fefee39e6199b7f00e847d4", null ]
];