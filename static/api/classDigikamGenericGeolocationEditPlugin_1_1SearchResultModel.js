var classDigikamGenericGeolocationEditPlugin_1_1SearchResultModel =
[
    [ "SearchResultItem", "classDigikamGenericGeolocationEditPlugin_1_1SearchResultModel_1_1SearchResultItem.html", "classDigikamGenericGeolocationEditPlugin_1_1SearchResultModel_1_1SearchResultItem" ],
    [ "SearchResultModel", "classDigikamGenericGeolocationEditPlugin_1_1SearchResultModel.html#a11f23aede8efc1845c620249c156192d", null ],
    [ "~SearchResultModel", "classDigikamGenericGeolocationEditPlugin_1_1SearchResultModel.html#a18baefac4bd759bde228aac57d5d02dc", null ],
    [ "addResults", "classDigikamGenericGeolocationEditPlugin_1_1SearchResultModel.html#af08a0b1e725a748c27b5a94377e28869", null ],
    [ "clearResults", "classDigikamGenericGeolocationEditPlugin_1_1SearchResultModel.html#a5065aec757b48a121411ef12da3dcbb7", null ],
    [ "columnCount", "classDigikamGenericGeolocationEditPlugin_1_1SearchResultModel.html#a9b00ee0a7876d1ad3572a474e0cfa86a", null ],
    [ "data", "classDigikamGenericGeolocationEditPlugin_1_1SearchResultModel.html#aa3af71c9550530f5089efe3cc7e56351", null ],
    [ "flags", "classDigikamGenericGeolocationEditPlugin_1_1SearchResultModel.html#a617e36c2283c68b12f594a240247d561", null ],
    [ "getMarkerIcon", "classDigikamGenericGeolocationEditPlugin_1_1SearchResultModel.html#a1288177972c77cfa217eab03e4f76334", null ],
    [ "headerData", "classDigikamGenericGeolocationEditPlugin_1_1SearchResultModel.html#a290d735b92ebe55d77345cd32a9e8275", null ],
    [ "index", "classDigikamGenericGeolocationEditPlugin_1_1SearchResultModel.html#a9952ef5fbbf8c5f13ef0c31f33cfd17d", null ],
    [ "parent", "classDigikamGenericGeolocationEditPlugin_1_1SearchResultModel.html#a5af0e06e95121b8dd206dab12f45694a", null ],
    [ "removeRowsByIndexes", "classDigikamGenericGeolocationEditPlugin_1_1SearchResultModel.html#a7277f3a479c4f0e1d7922f69bc68e746", null ],
    [ "removeRowsBySelection", "classDigikamGenericGeolocationEditPlugin_1_1SearchResultModel.html#ac76fd720faa402a3e9e4405ef3c923a4", null ],
    [ "resultItem", "classDigikamGenericGeolocationEditPlugin_1_1SearchResultModel.html#ae7a895bbb1f54fbeaf67d1a60768e9d9", null ],
    [ "rowCount", "classDigikamGenericGeolocationEditPlugin_1_1SearchResultModel.html#a93b361ac41954962e08a08d4be49b226", null ],
    [ "setData", "classDigikamGenericGeolocationEditPlugin_1_1SearchResultModel.html#afa36c87def5735809edde79b403e598d", null ],
    [ "setHeaderData", "classDigikamGenericGeolocationEditPlugin_1_1SearchResultModel.html#aa49f06f70f49df8980709de517456aaa", null ],
    [ "setSelectionModel", "classDigikamGenericGeolocationEditPlugin_1_1SearchResultModel.html#a0703c80cc3292012af26a7f6d90fa368", null ]
];