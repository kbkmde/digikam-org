var classDigikam_1_1VersionManager =
[
    [ "FileNameType", "classDigikam_1_1VersionManager.html#a05c961bbcd6d4c636a5cad2145319b31", [
      [ "CurrentVersionName", "classDigikam_1_1VersionManager.html#a05c961bbcd6d4c636a5cad2145319b31adbac22f0170d4ee459f30349c70a6175", null ],
      [ "NewVersionName", "classDigikam_1_1VersionManager.html#a05c961bbcd6d4c636a5cad2145319b31a4ac6a8c2b4b815ff38b2df1cbef719ab", null ]
    ] ],
    [ "VersionManager", "classDigikam_1_1VersionManager.html#a18e6107bd494859a9fbda28c7e34882f", null ],
    [ "~VersionManager", "classDigikam_1_1VersionManager.html#ad2cfe9da65c93a85ae18a7c77a5e5f33", null ],
    [ "namingScheme", "classDigikam_1_1VersionManager.html#a0b4ba6799123830b3784aea7c1ff3eb2", null ],
    [ "operation", "classDigikam_1_1VersionManager.html#aa6448e6d82d7bf1376c7db113d5b822f", null ],
    [ "operationNewVersionAs", "classDigikam_1_1VersionManager.html#aeeb661cecbc9e7333cab55f34a897a62", null ],
    [ "operationNewVersionInFormat", "classDigikam_1_1VersionManager.html#ac0d7bc35d79da30a0ed4a868a6df2da3", null ],
    [ "setNamingScheme", "classDigikam_1_1VersionManager.html#a57d602dda06439e6a9e4f09e1e0cceb3", null ],
    [ "setSettings", "classDigikam_1_1VersionManager.html#af53f8f3b3c2dd64a70530b571d2f7055", null ],
    [ "settings", "classDigikam_1_1VersionManager.html#ad71e663c7485dbd41a011744f6dd9594", null ],
    [ "toplevelDirectory", "classDigikam_1_1VersionManager.html#a68a503a34f8f8b215020226a25f1357b", null ],
    [ "workspaceFileFormats", "classDigikam_1_1VersionManager.html#ab1eb6fad03b2cd6feeb7350e40a3334c", null ]
];