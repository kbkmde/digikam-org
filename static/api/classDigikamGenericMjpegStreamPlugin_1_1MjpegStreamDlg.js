var classDigikamGenericMjpegStreamPlugin_1_1MjpegStreamDlg =
[
    [ "Private", "classDigikamGenericMjpegStreamPlugin_1_1MjpegStreamDlg_1_1Private.html", "classDigikamGenericMjpegStreamPlugin_1_1MjpegStreamDlg_1_1Private" ],
    [ "MjpegStreamDlg", "classDigikamGenericMjpegStreamPlugin_1_1MjpegStreamDlg.html#af7ed281a72fc8d18791485b419df9672", null ],
    [ "~MjpegStreamDlg", "classDigikamGenericMjpegStreamPlugin_1_1MjpegStreamDlg.html#a62cf5119e368b7adea64949f53e8313d", null ],
    [ "restoreDialogSize", "classDigikamGenericMjpegStreamPlugin_1_1MjpegStreamDlg.html#a5da66f5eb64468e9524230138e09c2a2", null ],
    [ "saveDialogSize", "classDigikamGenericMjpegStreamPlugin_1_1MjpegStreamDlg.html#a5496bc5fd80e57edd35e652dbb8361ae", null ],
    [ "setPlugin", "classDigikamGenericMjpegStreamPlugin_1_1MjpegStreamDlg.html#a8f9473a19990a8001d98cde6286d1059", null ],
    [ "m_buttons", "classDigikamGenericMjpegStreamPlugin_1_1MjpegStreamDlg.html#a06170b9f45856493f30cb0edd368a3c0", null ]
];