var dir_0398e3362116e42a05e8a97e1ec8979c =
[
    [ "colorcorrectiondlg.cpp", "colorcorrectiondlg_8cpp.html", null ],
    [ "colorcorrectiondlg.h", "colorcorrectiondlg_8h.html", [
      [ "ColorCorrectionDlg", "classDigikam_1_1ColorCorrectionDlg.html", "classDigikam_1_1ColorCorrectionDlg" ]
    ] ],
    [ "softproofdialog.cpp", "softproofdialog_8cpp.html", null ],
    [ "softproofdialog.h", "softproofdialog_8h.html", [
      [ "SoftProofDialog", "classDigikam_1_1SoftProofDialog.html", "classDigikam_1_1SoftProofDialog" ]
    ] ],
    [ "versioningpromptusersavedlg.cpp", "versioningpromptusersavedlg_8cpp.html", null ],
    [ "versioningpromptusersavedlg.h", "versioningpromptusersavedlg_8h.html", [
      [ "VersioningPromptUserSaveDialog", "classDigikam_1_1VersioningPromptUserSaveDialog.html", "classDigikam_1_1VersioningPromptUserSaveDialog" ]
    ] ]
];