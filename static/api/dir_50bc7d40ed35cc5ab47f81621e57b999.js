var dir_50bc7d40ed35cc5ab47f81621e57b999 =
[
    [ "subjectedit.cpp", "subjectedit_8cpp.html", null ],
    [ "subjectedit.h", "subjectedit_8h.html", [
      [ "SubjectEdit", "classDigikam_1_1SubjectEdit.html", "classDigikam_1_1SubjectEdit" ]
    ] ],
    [ "templatelist.cpp", "templatelist_8cpp.html", null ],
    [ "templatelist.h", "templatelist_8h.html", [
      [ "TemplateList", "classDigikam_1_1TemplateList.html", "classDigikam_1_1TemplateList" ],
      [ "TemplateListItem", "classDigikam_1_1TemplateListItem.html", "classDigikam_1_1TemplateListItem" ]
    ] ],
    [ "templatemanager.cpp", "templatemanager_8cpp.html", null ],
    [ "templatemanager.h", "templatemanager_8h.html", [
      [ "TemplateManager", "classDigikam_1_1TemplateManager.html", "classDigikam_1_1TemplateManager" ]
    ] ],
    [ "templatepanel.cpp", "templatepanel_8cpp.html", null ],
    [ "templatepanel.h", "templatepanel_8h.html", [
      [ "TemplatePanel", "classDigikam_1_1TemplatePanel.html", "classDigikam_1_1TemplatePanel" ]
    ] ],
    [ "templateselector.cpp", "templateselector_8cpp.html", null ],
    [ "templateselector.h", "templateselector_8h.html", [
      [ "TemplateSelector", "classDigikam_1_1TemplateSelector.html", "classDigikam_1_1TemplateSelector" ]
    ] ],
    [ "templateviewer.cpp", "templateviewer_8cpp.html", null ],
    [ "templateviewer.h", "templateviewer_8h.html", [
      [ "TemplateViewer", "classDigikam_1_1TemplateViewer.html", "classDigikam_1_1TemplateViewer" ]
    ] ]
];