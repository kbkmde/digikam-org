var dir_652b560a3e90a6a151bd3154fe0f7919 =
[
    [ "calintropage.cpp", "calintropage_8cpp.html", null ],
    [ "calintropage.h", "calintropage_8h.html", [
      [ "CalIntroPage", "classDigikamGenericCalendarPlugin_1_1CalIntroPage.html", "classDigikamGenericCalendarPlugin_1_1CalIntroPage" ]
    ] ],
    [ "calmonthwidget.cpp", "calmonthwidget_8cpp.html", null ],
    [ "calmonthwidget.h", "calmonthwidget_8h.html", [
      [ "CalMonthWidget", "classDigikamGenericCalendarPlugin_1_1CalMonthWidget.html", "classDigikamGenericCalendarPlugin_1_1CalMonthWidget" ]
    ] ],
    [ "caltemplate.cpp", "caltemplate_8cpp.html", null ],
    [ "caltemplate.h", "caltemplate_8h.html", [
      [ "CalTemplate", "classDigikamGenericCalendarPlugin_1_1CalTemplate.html", "classDigikamGenericCalendarPlugin_1_1CalTemplate" ]
    ] ],
    [ "calwidget.cpp", "calwidget_8cpp.html", null ],
    [ "calwidget.h", "calwidget_8h.html", [
      [ "CalWidget", "classDigikamGenericCalendarPlugin_1_1CalWidget.html", "classDigikamGenericCalendarPlugin_1_1CalWidget" ]
    ] ],
    [ "calwizard.cpp", "calwizard_8cpp.html", null ],
    [ "calwizard.h", "calwizard_8h.html", [
      [ "CalWizard", "classDigikamGenericCalendarPlugin_1_1CalWizard.html", "classDigikamGenericCalendarPlugin_1_1CalWizard" ]
    ] ]
];