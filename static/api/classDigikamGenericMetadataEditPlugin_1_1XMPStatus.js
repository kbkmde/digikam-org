var classDigikamGenericMetadataEditPlugin_1_1XMPStatus =
[
    [ "XMPStatus", "classDigikamGenericMetadataEditPlugin_1_1XMPStatus.html#ad700f08ff7d45a61fba76e2f28f356ed", null ],
    [ "~XMPStatus", "classDigikamGenericMetadataEditPlugin_1_1XMPStatus.html#a6241245b6e44b0c143d5b23571947d46", null ],
    [ "applyMetadata", "classDigikamGenericMetadataEditPlugin_1_1XMPStatus.html#ac6d0f5757cceef7ec33f5e282a56a3ca", null ],
    [ "readMetadata", "classDigikamGenericMetadataEditPlugin_1_1XMPStatus.html#a73701fd00dab0c2ec5f426023b952d6a", null ],
    [ "signalModified", "classDigikamGenericMetadataEditPlugin_1_1XMPStatus.html#a4e98db47434cba8798873305fff45c81", null ]
];