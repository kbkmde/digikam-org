var classDigikam_1_1LocalContrastSettings =
[
    [ "LocalContrastSettings", "classDigikam_1_1LocalContrastSettings.html#af77d7fa8f4ff455377150525b305080c", null ],
    [ "~LocalContrastSettings", "classDigikam_1_1LocalContrastSettings.html#a3f3b52c29e5f5a10670aa2ba7e26da52", null ],
    [ "defaultSettings", "classDigikam_1_1LocalContrastSettings.html#a1f0389e7bf3d8a53ac12c21916fd04d1", null ],
    [ "loadSettings", "classDigikam_1_1LocalContrastSettings.html#a842d12962eadd3f8977efad670e7aa65", null ],
    [ "readSettings", "classDigikam_1_1LocalContrastSettings.html#a6466984d83ada2cc9c1d06515306b1fa", null ],
    [ "resetToDefault", "classDigikam_1_1LocalContrastSettings.html#afeabc6651d6181775db76e8a5fdc0772", null ],
    [ "saveAsSettings", "classDigikam_1_1LocalContrastSettings.html#a3b84e68d7619b38f728eea089a208800", null ],
    [ "setSettings", "classDigikam_1_1LocalContrastSettings.html#aa14374bf6e61bd7baeea2ff2d1a06134", null ],
    [ "settings", "classDigikam_1_1LocalContrastSettings.html#ae310dfa1b1e4008260d1cb3b9f96707e", null ],
    [ "signalSettingsChanged", "classDigikam_1_1LocalContrastSettings.html#af657e281454fe70f01e72d9fe65cc8f5", null ],
    [ "writeSettings", "classDigikam_1_1LocalContrastSettings.html#a6e6d988e494d0bace3b1e870abed38fe", null ]
];