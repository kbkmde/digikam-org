var classDigikamEditorBorderToolPlugin_1_1BorderToolPlugin =
[
    [ "BorderToolPlugin", "classDigikamEditorBorderToolPlugin_1_1BorderToolPlugin.html#adb58ff1b216c2253d52916fec5f72794", null ],
    [ "~BorderToolPlugin", "classDigikamEditorBorderToolPlugin_1_1BorderToolPlugin.html#ab700fb0ff4f7118b8f07ba23e5392c54", null ],
    [ "actions", "classDigikamEditorBorderToolPlugin_1_1BorderToolPlugin.html#af244555adac28e4baec9e5938182f914", null ],
    [ "addAction", "classDigikamEditorBorderToolPlugin_1_1BorderToolPlugin.html#a07e25f4a176a823feb70e6dd80e6b3f7", null ],
    [ "authors", "classDigikamEditorBorderToolPlugin_1_1BorderToolPlugin.html#ab819bc4e99882d5ceb2e6814305e1b86", null ],
    [ "categories", "classDigikamEditorBorderToolPlugin_1_1BorderToolPlugin.html#a35d45cfa520c713de9348130b0a4d26c", null ],
    [ "cleanUp", "classDigikamEditorBorderToolPlugin_1_1BorderToolPlugin.html#a586144ad9625ffa1f503ad74e341c639", null ],
    [ "count", "classDigikamEditorBorderToolPlugin_1_1BorderToolPlugin.html#a8199ffdd14c09124360cb4b2ba08e1f9", null ],
    [ "description", "classDigikamEditorBorderToolPlugin_1_1BorderToolPlugin.html#a8dcca8aca45c812ee67b66622212d26a", null ],
    [ "details", "classDigikamEditorBorderToolPlugin_1_1BorderToolPlugin.html#a0a869e32b25b62e6b232bc301010d7a8", null ],
    [ "extraAboutData", "classDigikamEditorBorderToolPlugin_1_1BorderToolPlugin.html#a3895819eddf64a8800a5b0f155f114b1", null ],
    [ "extraAboutDataTitle", "classDigikamEditorBorderToolPlugin_1_1BorderToolPlugin.html#a5ab9e1bce54d762f0e65acc11069896b", null ],
    [ "findActionByName", "classDigikamEditorBorderToolPlugin_1_1BorderToolPlugin.html#a7c27ed94fba2b6e4e1eeb63aca55096a", null ],
    [ "hasVisibilityProperty", "classDigikamEditorBorderToolPlugin_1_1BorderToolPlugin.html#a996af3ec25ecb4c44788435b8ca5d079", null ],
    [ "icon", "classDigikamEditorBorderToolPlugin_1_1BorderToolPlugin.html#a8c2f672765fad5631be2c73ab072a930", null ],
    [ "ifaceIid", "classDigikamEditorBorderToolPlugin_1_1BorderToolPlugin.html#a2a64b992e50d15b4c67f6112b6bcaef8", null ],
    [ "iid", "classDigikamEditorBorderToolPlugin_1_1BorderToolPlugin.html#a6e8d836a7543d60a42986d29a118f453", null ],
    [ "infoIface", "classDigikamEditorBorderToolPlugin_1_1BorderToolPlugin.html#a7aa1f0e7d5365c77f9dd44b7b0dd42da", null ],
    [ "libraryFileName", "classDigikamEditorBorderToolPlugin_1_1BorderToolPlugin.html#a149a7b768dc09157dc9cb2dc118d2285", null ],
    [ "name", "classDigikamEditorBorderToolPlugin_1_1BorderToolPlugin.html#ab2423869c66b1a6d4cb77daba52de037", null ],
    [ "pluginAuthors", "classDigikamEditorBorderToolPlugin_1_1BorderToolPlugin.html#a98546b3aa26a43b6695a25643b73725e", null ],
    [ "setLibraryFileName", "classDigikamEditorBorderToolPlugin_1_1BorderToolPlugin.html#afde82d7d92a1d75dbae094a66c669057", null ],
    [ "setShouldLoaded", "classDigikamEditorBorderToolPlugin_1_1BorderToolPlugin.html#a27e50fb1f2756122f15cc289f7e02c7b", null ],
    [ "setup", "classDigikamEditorBorderToolPlugin_1_1BorderToolPlugin.html#a50dc792233e70bd194c94442d58eeaa4", null ],
    [ "setVisible", "classDigikamEditorBorderToolPlugin_1_1BorderToolPlugin.html#a8140de1f7aa50c20fd137921f0ad63a8", null ],
    [ "shouldLoaded", "classDigikamEditorBorderToolPlugin_1_1BorderToolPlugin.html#a0806aedffe128e70253d910c1dbf5690", null ],
    [ "version", "classDigikamEditorBorderToolPlugin_1_1BorderToolPlugin.html#a71a6f035204fb005960edf5d285884a9", null ]
];