var classDigikam_1_1CIETongueWidget =
[
    [ "CIETongueWidget", "classDigikam_1_1CIETongueWidget.html#ad665786c1ffaf20a2d3b5da0f61a94ad", null ],
    [ "~CIETongueWidget", "classDigikam_1_1CIETongueWidget.html#aef67d4ee98b28708370cc7bedd8f347e", null ],
    [ "colorByCoord", "classDigikam_1_1CIETongueWidget.html#a3c7af2699118c177a7c12e9024d6a514", null ],
    [ "drawLabels", "classDigikam_1_1CIETongueWidget.html#a54670b85e8794a9cff3d7f6f79360325", null ],
    [ "drawSmallElipse", "classDigikam_1_1CIETongueWidget.html#a298d8c53db8a249239acee3d44cb1c07", null ],
    [ "drawTongueAxis", "classDigikam_1_1CIETongueWidget.html#a291a3928d29609395d93471c794e56fe", null ],
    [ "drawTongueGrid", "classDigikam_1_1CIETongueWidget.html#ae69ec47937c817d573fbd24b31124496", null ],
    [ "fillTongue", "classDigikam_1_1CIETongueWidget.html#ab8bf9e41c05097c82897358d53b61111", null ],
    [ "grids", "classDigikam_1_1CIETongueWidget.html#aa917389cdac48fb5397a060a7210839a", null ],
    [ "loadingFailed", "classDigikam_1_1CIETongueWidget.html#ad553a5a0600660f7b3f15dbf7e237d98", null ],
    [ "loadingStarted", "classDigikam_1_1CIETongueWidget.html#aac47c57d53091e54fffe757b7f208aef", null ],
    [ "outlineTongue", "classDigikam_1_1CIETongueWidget.html#a9d45166e4003189e6f2c3ea2e4fbbaa2", null ],
    [ "paintEvent", "classDigikam_1_1CIETongueWidget.html#a978ba4a461eb585dfe20410d1ed10787", null ],
    [ "resizeEvent", "classDigikam_1_1CIETongueWidget.html#afef19931752697095cd4ef75b1c92100", null ],
    [ "setProfileData", "classDigikam_1_1CIETongueWidget.html#a845d81dca912ee8be62ae0f4f40e09a3", null ],
    [ "setProfileFromFile", "classDigikam_1_1CIETongueWidget.html#a0fc6428d13fc7e820212a295c59ea55a", null ],
    [ "uncalibratedColor", "classDigikam_1_1CIETongueWidget.html#acf87c1cdfd419d3ee2be9337f0d972d1", null ]
];