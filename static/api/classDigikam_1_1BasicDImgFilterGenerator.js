var classDigikam_1_1BasicDImgFilterGenerator =
[
    [ "BasicDImgFilterGenerator", "classDigikam_1_1BasicDImgFilterGenerator.html#a84e01c32cd56e0058da4802960386739", null ],
    [ "createFilter", "classDigikam_1_1BasicDImgFilterGenerator.html#a5d7b8df17cc748e318a2ad8b24d09258", null ],
    [ "displayableName", "classDigikam_1_1BasicDImgFilterGenerator.html#aad46f9b00aaa3303d0e3e8732e90642c", null ],
    [ "isSupported", "classDigikam_1_1BasicDImgFilterGenerator.html#ab4c045dd1812873f9ee259f7ae665ab2", null ],
    [ "isSupported", "classDigikam_1_1BasicDImgFilterGenerator.html#a629497ac473aa51e99721ac7cfce9114", null ],
    [ "supportedFilters", "classDigikam_1_1BasicDImgFilterGenerator.html#a9a78218524e7a2b15f3adf7e8e8deed1", null ],
    [ "supportedVersions", "classDigikam_1_1BasicDImgFilterGenerator.html#a80e2ccf3b2cac15214aef94f8c132545", null ]
];