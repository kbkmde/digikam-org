var classDigikamGenericPresentationPlugin_1_1PresentationAudioListItem =
[
    [ "PresentationAudioListItem", "classDigikamGenericPresentationPlugin_1_1PresentationAudioListItem.html#a884d8220a02a26f1809a034a86d88ea7", null ],
    [ "~PresentationAudioListItem", "classDigikamGenericPresentationPlugin_1_1PresentationAudioListItem.html#a6775afc2f979686f2824f15a4ed27d6b", null ],
    [ "artist", "classDigikamGenericPresentationPlugin_1_1PresentationAudioListItem.html#a2421c4cf2f4f321a1b952c67a12c0040", null ],
    [ "setName", "classDigikamGenericPresentationPlugin_1_1PresentationAudioListItem.html#a45f3137730e7e8ddd7b47e30765f3040", null ],
    [ "signalTotalTimeReady", "classDigikamGenericPresentationPlugin_1_1PresentationAudioListItem.html#a6bedf3555427e53e282f75cf4f5770e4", null ],
    [ "title", "classDigikamGenericPresentationPlugin_1_1PresentationAudioListItem.html#a2a9168bac4c30814a822419fbda197b2", null ],
    [ "totalTime", "classDigikamGenericPresentationPlugin_1_1PresentationAudioListItem.html#ab9d2e5555640a5048aadfbac9d425396", null ],
    [ "url", "classDigikamGenericPresentationPlugin_1_1PresentationAudioListItem.html#a64d472c900e857b35d019720488e2840", null ]
];