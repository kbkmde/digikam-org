var classDigikamBqmSharpenPlugin_1_1Sharpen =
[
    [ "BatchToolGroup", "classDigikamBqmSharpenPlugin_1_1Sharpen.html#afa76b46ac346747b289ce17be3124a72", [
      [ "BaseTool", "classDigikamBqmSharpenPlugin_1_1Sharpen.html#afa76b46ac346747b289ce17be3124a72abf7d05254a90fb96b64257b37ab2571c", null ],
      [ "CustomTool", "classDigikamBqmSharpenPlugin_1_1Sharpen.html#afa76b46ac346747b289ce17be3124a72a3e0af80bcff0ed3b2a81c1994ebf2d50", null ],
      [ "ColorTool", "classDigikamBqmSharpenPlugin_1_1Sharpen.html#afa76b46ac346747b289ce17be3124a72a678db3327b06483d6eec8601a6b65457", null ],
      [ "EnhanceTool", "classDigikamBqmSharpenPlugin_1_1Sharpen.html#afa76b46ac346747b289ce17be3124a72ac99e79b29944cded7f1466dad3f31c22", null ],
      [ "TransformTool", "classDigikamBqmSharpenPlugin_1_1Sharpen.html#afa76b46ac346747b289ce17be3124a72a5abc81bbd353db5e71868a59ec402d3f", null ],
      [ "DecorateTool", "classDigikamBqmSharpenPlugin_1_1Sharpen.html#afa76b46ac346747b289ce17be3124a72ac275940dc7d00089f0a46924d40413ac", null ],
      [ "FiltersTool", "classDigikamBqmSharpenPlugin_1_1Sharpen.html#afa76b46ac346747b289ce17be3124a72a3f77943d1787b72f8a1c3d5a9a04d4db", null ],
      [ "ConvertTool", "classDigikamBqmSharpenPlugin_1_1Sharpen.html#afa76b46ac346747b289ce17be3124a72a87f1c29bf0d78ea00d5ea67a99dc063d", null ],
      [ "MetadataTool", "classDigikamBqmSharpenPlugin_1_1Sharpen.html#afa76b46ac346747b289ce17be3124a72abb96e2d5f48eeda7bb30755e25cd6756", null ]
    ] ],
    [ "Sharpen", "classDigikamBqmSharpenPlugin_1_1Sharpen.html#a7b9332481458d1fce5e3a645eb54f793", null ],
    [ "~Sharpen", "classDigikamBqmSharpenPlugin_1_1Sharpen.html#a7d478bdd4cbe381b118b9fe976fa58ef", null ],
    [ "apply", "classDigikamBqmSharpenPlugin_1_1Sharpen.html#a36d8541d1b9a820534e87902b54d088c", null ],
    [ "applyFilter", "classDigikamBqmSharpenPlugin_1_1Sharpen.html#a90a5d88988617961068fb2242dbf88d7", null ],
    [ "applyFilter", "classDigikamBqmSharpenPlugin_1_1Sharpen.html#a561e9e51ca9cadf1fecdbf147b5e0588", null ],
    [ "applyFilterChangedProperties", "classDigikamBqmSharpenPlugin_1_1Sharpen.html#ac596446d1ca3d457b7a8a6c9f360d763", null ],
    [ "cancel", "classDigikamBqmSharpenPlugin_1_1Sharpen.html#a41b4ff36f19166534ce42462d39ceff3", null ],
    [ "clone", "classDigikamBqmSharpenPlugin_1_1Sharpen.html#ad4196a107e46f690e7507ac0f96cefcd", null ],
    [ "defaultSettings", "classDigikamBqmSharpenPlugin_1_1Sharpen.html#a6386b6377b642d205b20c3728ff8a820", null ],
    [ "deleteSettingsWidget", "classDigikamBqmSharpenPlugin_1_1Sharpen.html#abd1e5b945bfc28740f7cd79534d3dabf", null ],
    [ "errorDescription", "classDigikamBqmSharpenPlugin_1_1Sharpen.html#a9a6bd8dccc7132093bebf21c0368d8ef", null ],
    [ "getBranchHistory", "classDigikamBqmSharpenPlugin_1_1Sharpen.html#aa423a1039987bba7bd0d9369eb233957", null ],
    [ "getNeedResetExifOrientation", "classDigikamBqmSharpenPlugin_1_1Sharpen.html#a8d1951cc07e2e31fd6763a8f2abfd4c7", null ],
    [ "getResetExifOrientationAllowed", "classDigikamBqmSharpenPlugin_1_1Sharpen.html#a8fba6bed21021f2e8b9d34c5dbe54605", null ],
    [ "image", "classDigikamBqmSharpenPlugin_1_1Sharpen.html#a07a6d7854d84caae798143ba79fd777f", null ],
    [ "imageData", "classDigikamBqmSharpenPlugin_1_1Sharpen.html#a23eb9f26cab4ebe516e1047b99ec02e9", null ],
    [ "imageInfo", "classDigikamBqmSharpenPlugin_1_1Sharpen.html#a74fcc66bd46387a69238cd0b49376d32", null ],
    [ "inputUrl", "classDigikamBqmSharpenPlugin_1_1Sharpen.html#aaed484c6d693e73ce09d405803355d04", null ],
    [ "ioFileSettings", "classDigikamBqmSharpenPlugin_1_1Sharpen.html#a29b94a1be7c1548b30032809a9e0e91a", null ],
    [ "isCancelled", "classDigikamBqmSharpenPlugin_1_1Sharpen.html#aec1749bf7cedd5261dcb7caf710b91b3", null ],
    [ "isLastChainedTool", "classDigikamBqmSharpenPlugin_1_1Sharpen.html#af9c31ea2af0d09924370a483161b1dde", null ],
    [ "isRawFile", "classDigikamBqmSharpenPlugin_1_1Sharpen.html#afd910fab457aa527e72634f2de834c48", null ],
    [ "loadToDImg", "classDigikamBqmSharpenPlugin_1_1Sharpen.html#a070c2544bad70f9d7e1f85688de9c27f", null ],
    [ "outputSuffix", "classDigikamBqmSharpenPlugin_1_1Sharpen.html#aeee8ce2604a87f131d75e9bea8a595aa", null ],
    [ "outputUrl", "classDigikamBqmSharpenPlugin_1_1Sharpen.html#a4703fb10c14dd3b84edba248fed832bd", null ],
    [ "plugin", "classDigikamBqmSharpenPlugin_1_1Sharpen.html#a4c4ac6dcb7580e4e23edaf66a713c888", null ],
    [ "rawDecodingSettings", "classDigikamBqmSharpenPlugin_1_1Sharpen.html#a285e355bed564ca3d4651a6c264bb5d0", null ],
    [ "registerSettingsWidget", "classDigikamBqmSharpenPlugin_1_1Sharpen.html#a93c9d707d42c8f5fd97d07abf3180e9a", null ],
    [ "savefromDImg", "classDigikamBqmSharpenPlugin_1_1Sharpen.html#a71045c04fa4bb019c021c9dacd32348d", null ],
    [ "setBranchHistory", "classDigikamBqmSharpenPlugin_1_1Sharpen.html#a5003b8ab574560903b0dcffa408d139a", null ],
    [ "setDRawDecoderSettings", "classDigikamBqmSharpenPlugin_1_1Sharpen.html#a6a3fc7a2cd0c1890543b5a0f3b8d5f59", null ],
    [ "setErrorDescription", "classDigikamBqmSharpenPlugin_1_1Sharpen.html#a73db6ff94e50240e68594c8635413659", null ],
    [ "setImageData", "classDigikamBqmSharpenPlugin_1_1Sharpen.html#a8547dbb306a9811d0e7242dec09ac68b", null ],
    [ "setInputUrl", "classDigikamBqmSharpenPlugin_1_1Sharpen.html#a57e6cc4aa683fa7a83e2ffe3d4651a1b", null ],
    [ "setIOFileSettings", "classDigikamBqmSharpenPlugin_1_1Sharpen.html#a282091b29cea96aeae8bdb20f6aa73d0", null ],
    [ "setItemInfo", "classDigikamBqmSharpenPlugin_1_1Sharpen.html#a9c057fdf245570ab92de8133ce134d70", null ],
    [ "setLastChainedTool", "classDigikamBqmSharpenPlugin_1_1Sharpen.html#ae76700064b9ffacb124a8163dc693546", null ],
    [ "setNeedResetExifOrientation", "classDigikamBqmSharpenPlugin_1_1Sharpen.html#aa34b534ee277b0d6af52d0821423b5ee", null ],
    [ "setOutputUrl", "classDigikamBqmSharpenPlugin_1_1Sharpen.html#a9774274355899f382590dc68d782497a", null ],
    [ "setOutputUrlFromInputUrl", "classDigikamBqmSharpenPlugin_1_1Sharpen.html#aacfa495346a2334b263b8c6a425d740a", null ],
    [ "setPlugin", "classDigikamBqmSharpenPlugin_1_1Sharpen.html#a3da22fb32151df2f75e1073bb42626db", null ],
    [ "setRawLoadingRules", "classDigikamBqmSharpenPlugin_1_1Sharpen.html#ac3f13be06dcb2e9be6af7486154e1ab0", null ],
    [ "setResetExifOrientationAllowed", "classDigikamBqmSharpenPlugin_1_1Sharpen.html#abfbf72c25a9b65b9fb4b572d46fca3ef", null ],
    [ "setSaveAsNewVersion", "classDigikamBqmSharpenPlugin_1_1Sharpen.html#a6d9b7644e60bda3e0f033aa3e9c4f8f5", null ],
    [ "setSettings", "classDigikamBqmSharpenPlugin_1_1Sharpen.html#a0a6c7c5630bd3a81fe65a351114f2b84", null ],
    [ "settings", "classDigikamBqmSharpenPlugin_1_1Sharpen.html#a101d327668f9d9f7cc9695b1b9bf0d20", null ],
    [ "settingsWidget", "classDigikamBqmSharpenPlugin_1_1Sharpen.html#acc1ce64746976144f211e2d537d3b851", null ],
    [ "setToolDescription", "classDigikamBqmSharpenPlugin_1_1Sharpen.html#a1c9ffb404b2597cdfc08515b3e9f86e7", null ],
    [ "setToolIcon", "classDigikamBqmSharpenPlugin_1_1Sharpen.html#a78786c22c95ece3d7ad4abd8b1e7b6d3", null ],
    [ "setToolIconName", "classDigikamBqmSharpenPlugin_1_1Sharpen.html#af339b08111d419eea4a8515c45f3accc", null ],
    [ "setToolTitle", "classDigikamBqmSharpenPlugin_1_1Sharpen.html#a4337c17e1bc828dc77eb8dba74099db0", null ],
    [ "setWorkingUrl", "classDigikamBqmSharpenPlugin_1_1Sharpen.html#aae015f01fb686cfec93c764ca2c06013", null ],
    [ "signalAssignSettings2Widget", "classDigikamBqmSharpenPlugin_1_1Sharpen.html#af44500f1c3827c0e643f6494ef49b660", null ],
    [ "signalSettingsChanged", "classDigikamBqmSharpenPlugin_1_1Sharpen.html#a77b20102fb03bdb2e388c306df43b044", null ],
    [ "signalVisible", "classDigikamBqmSharpenPlugin_1_1Sharpen.html#acaa2b52161bee3aa9d24db22ff2d72b1", null ],
    [ "slotResetSettingsToDefault", "classDigikamBqmSharpenPlugin_1_1Sharpen.html#ab6fc0970f527dadbddc041ef1d9a1d1c", null ],
    [ "slotSettingsChanged", "classDigikamBqmSharpenPlugin_1_1Sharpen.html#a3a0cda57c6cb05452e3a1af200c098ea", null ],
    [ "toolDescription", "classDigikamBqmSharpenPlugin_1_1Sharpen.html#a2f87e45801f6e53bdbdec85c52240a60", null ],
    [ "toolGroup", "classDigikamBqmSharpenPlugin_1_1Sharpen.html#a28d9efb0e9c27c7f53ecf9c0b13a2d26", null ],
    [ "toolGroupToString", "classDigikamBqmSharpenPlugin_1_1Sharpen.html#a7a2ffb5cdde6950f6505525546639cba", null ],
    [ "toolIcon", "classDigikamBqmSharpenPlugin_1_1Sharpen.html#a847551f8c091a7b598e5782348bafbd8", null ],
    [ "toolTitle", "classDigikamBqmSharpenPlugin_1_1Sharpen.html#add18d9118dc64a85e1b2c7f86c871f86", null ],
    [ "toolVersion", "classDigikamBqmSharpenPlugin_1_1Sharpen.html#ac65a61bd8560a8e2a39ea9b9f75eb669", null ],
    [ "workingUrl", "classDigikamBqmSharpenPlugin_1_1Sharpen.html#ac84787fb1a697cdec8c21758896f49a3", null ],
    [ "m_settingsWidget", "classDigikamBqmSharpenPlugin_1_1Sharpen.html#a50a1ed7dcd42d42698fe50fa16eb4868", null ]
];