var namespaceDigikamGenericJAlbumPlugin =
[
    [ "JAlbumFinalPage", "classDigikamGenericJAlbumPlugin_1_1JAlbumFinalPage.html", "classDigikamGenericJAlbumPlugin_1_1JAlbumFinalPage" ],
    [ "JAlbumGenerator", "classDigikamGenericJAlbumPlugin_1_1JAlbumGenerator.html", "classDigikamGenericJAlbumPlugin_1_1JAlbumGenerator" ],
    [ "JAlbumIntroPage", "classDigikamGenericJAlbumPlugin_1_1JAlbumIntroPage.html", "classDigikamGenericJAlbumPlugin_1_1JAlbumIntroPage" ],
    [ "JalbumJar", "classDigikamGenericJAlbumPlugin_1_1JalbumJar.html", "classDigikamGenericJAlbumPlugin_1_1JalbumJar" ],
    [ "JalbumJava", "classDigikamGenericJAlbumPlugin_1_1JalbumJava.html", "classDigikamGenericJAlbumPlugin_1_1JalbumJava" ],
    [ "JAlbumOutputPage", "classDigikamGenericJAlbumPlugin_1_1JAlbumOutputPage.html", "classDigikamGenericJAlbumPlugin_1_1JAlbumOutputPage" ],
    [ "JAlbumPlugin", "classDigikamGenericJAlbumPlugin_1_1JAlbumPlugin.html", "classDigikamGenericJAlbumPlugin_1_1JAlbumPlugin" ],
    [ "JAlbumSelectionPage", "classDigikamGenericJAlbumPlugin_1_1JAlbumSelectionPage.html", "classDigikamGenericJAlbumPlugin_1_1JAlbumSelectionPage" ],
    [ "JAlbumSettings", "classDigikamGenericJAlbumPlugin_1_1JAlbumSettings.html", "classDigikamGenericJAlbumPlugin_1_1JAlbumSettings" ],
    [ "JAlbumWizard", "classDigikamGenericJAlbumPlugin_1_1JAlbumWizard.html", "classDigikamGenericJAlbumPlugin_1_1JAlbumWizard" ],
    [ "operator<<", "namespaceDigikamGenericJAlbumPlugin.html#aa806c364a5bb00635362dc41d21fdaa9", null ]
];