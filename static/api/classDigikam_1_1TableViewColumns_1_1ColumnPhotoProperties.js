var classDigikam_1_1TableViewColumns_1_1ColumnPhotoProperties =
[
    [ "ColumnCompareResult", "classDigikam_1_1TableViewColumns_1_1ColumnPhotoProperties.html#a0714c73efa682e4bcc6dac00989cabd7", [
      [ "CmpEqual", "classDigikam_1_1TableViewColumns_1_1ColumnPhotoProperties.html#a0714c73efa682e4bcc6dac00989cabd7ab1fd7950c9141af71b6d915d7619da20", null ],
      [ "CmpABiggerB", "classDigikam_1_1TableViewColumns_1_1ColumnPhotoProperties.html#a0714c73efa682e4bcc6dac00989cabd7a3b46fa13fd837bb5e8303e15579e9c0a", null ],
      [ "CmpALessB", "classDigikam_1_1TableViewColumns_1_1ColumnPhotoProperties.html#a0714c73efa682e4bcc6dac00989cabd7af64727fba2a786f83c4032b9ac4e2ac7", null ]
    ] ],
    [ "ColumnFlag", "classDigikam_1_1TableViewColumns_1_1ColumnPhotoProperties.html#acf85d0f13e2a9d163ab4fbeed5c223e4", [
      [ "ColumnNoFlags", "classDigikam_1_1TableViewColumns_1_1ColumnPhotoProperties.html#acf85d0f13e2a9d163ab4fbeed5c223e4a3a1867e93424ceda7439df444b42b7a8", null ],
      [ "ColumnCustomPainting", "classDigikam_1_1TableViewColumns_1_1ColumnPhotoProperties.html#acf85d0f13e2a9d163ab4fbeed5c223e4a2496490a69825be1607d673758561fea", null ],
      [ "ColumnCustomSorting", "classDigikam_1_1TableViewColumns_1_1ColumnPhotoProperties.html#acf85d0f13e2a9d163ab4fbeed5c223e4ab775074b18540b93dbbe923cc7977b0c", null ],
      [ "ColumnHasConfigurationWidget", "classDigikam_1_1TableViewColumns_1_1ColumnPhotoProperties.html#acf85d0f13e2a9d163ab4fbeed5c223e4a22debcee2a26f5a6a7fda09fdd1e3c0c", null ]
    ] ],
    [ "SubColumn", "classDigikam_1_1TableViewColumns_1_1ColumnPhotoProperties.html#aeeb961d349989590baae8cf965f52b18", [
      [ "SubColumnCameraMaker", "classDigikam_1_1TableViewColumns_1_1ColumnPhotoProperties.html#aeeb961d349989590baae8cf965f52b18a50135358a985bd01c6aeddd1b2128b31", null ],
      [ "SubColumnCameraModel", "classDigikam_1_1TableViewColumns_1_1ColumnPhotoProperties.html#aeeb961d349989590baae8cf965f52b18a992c0d0a3cb7f39117fea90b05ebbc31", null ],
      [ "SubColumnLens", "classDigikam_1_1TableViewColumns_1_1ColumnPhotoProperties.html#aeeb961d349989590baae8cf965f52b18a579f4707e036e631eb1bc4ba35eb3892", null ],
      [ "SubColumnAperture", "classDigikam_1_1TableViewColumns_1_1ColumnPhotoProperties.html#aeeb961d349989590baae8cf965f52b18af8aaadefe1d5ec26de31bc7902b09d0c", null ],
      [ "SubColumnFocal", "classDigikam_1_1TableViewColumns_1_1ColumnPhotoProperties.html#aeeb961d349989590baae8cf965f52b18a9579c5f449e99c558861429a0377b123", null ],
      [ "SubColumnExposure", "classDigikam_1_1TableViewColumns_1_1ColumnPhotoProperties.html#aeeb961d349989590baae8cf965f52b18a1debae3fa18839d54122ef4b38b25ddb", null ],
      [ "SubColumnSensitivity", "classDigikam_1_1TableViewColumns_1_1ColumnPhotoProperties.html#aeeb961d349989590baae8cf965f52b18ab1bcf0145e3e483a4cf84096645265d7", null ],
      [ "SubColumnModeProgram", "classDigikam_1_1TableViewColumns_1_1ColumnPhotoProperties.html#aeeb961d349989590baae8cf965f52b18affec87833c8d9f9f02d2711285a622a7", null ],
      [ "SubColumnFlash", "classDigikam_1_1TableViewColumns_1_1ColumnPhotoProperties.html#aeeb961d349989590baae8cf965f52b18a40f2876a4d8cee980fa66c678aa85917", null ],
      [ "SubColumnWhiteBalance", "classDigikam_1_1TableViewColumns_1_1ColumnPhotoProperties.html#aeeb961d349989590baae8cf965f52b18a0c56c3b022505e06dc279e250764911c", null ]
    ] ],
    [ "ColumnPhotoProperties", "classDigikam_1_1TableViewColumns_1_1ColumnPhotoProperties.html#a211947612637a1892e45948fa4f1286b", null ],
    [ "~ColumnPhotoProperties", "classDigikam_1_1TableViewColumns_1_1ColumnPhotoProperties.html#a0d39ac1818400a9734c9f367e0f117c4", null ],
    [ "columnAffectedByChangeset", "classDigikam_1_1TableViewColumns_1_1ColumnPhotoProperties.html#ac275f599c98cde40ce903a5de6aaf6f4", null ],
    [ "compare", "classDigikam_1_1TableViewColumns_1_1ColumnPhotoProperties.html#a907e3e3916419fadc77ed26a17ac256d", null ],
    [ "data", "classDigikam_1_1TableViewColumns_1_1ColumnPhotoProperties.html#a840a02f3525674a26409a5f1edb250f5", null ],
    [ "getColumnFlags", "classDigikam_1_1TableViewColumns_1_1ColumnPhotoProperties.html#a4430bfdab453e181e4fc775d6d9ad2dc", null ],
    [ "getConfiguration", "classDigikam_1_1TableViewColumns_1_1ColumnPhotoProperties.html#ace668cd97cd6d0b584332b803a0ec665", null ],
    [ "getConfigurationWidget", "classDigikam_1_1TableViewColumns_1_1ColumnPhotoProperties.html#ad37242de38af743995fc760351a1c6bc", null ],
    [ "getTitle", "classDigikam_1_1TableViewColumns_1_1ColumnPhotoProperties.html#a5c721e9a3d6309fd13ce56119753684d", null ],
    [ "paint", "classDigikam_1_1TableViewColumns_1_1ColumnPhotoProperties.html#ae53896a52f61680c4263e571e19eec7f", null ],
    [ "setConfiguration", "classDigikam_1_1TableViewColumns_1_1ColumnPhotoProperties.html#a4af16825234c1ea6ba8aebc133c92af4", null ],
    [ "signalAllDataChanged", "classDigikam_1_1TableViewColumns_1_1ColumnPhotoProperties.html#ab288b91b167fc9f40490e3d41dee38ee", null ],
    [ "signalDataChanged", "classDigikam_1_1TableViewColumns_1_1ColumnPhotoProperties.html#ac52d1ec1e5839d96de9e1b365582fdfc", null ],
    [ "sizeHint", "classDigikam_1_1TableViewColumns_1_1ColumnPhotoProperties.html#a3a5f8ab59b570eb69d51b0d01c452190", null ],
    [ "updateThumbnailSize", "classDigikam_1_1TableViewColumns_1_1ColumnPhotoProperties.html#ac4696688718ef915e4fb096ed8a2efe3", null ],
    [ "configuration", "classDigikam_1_1TableViewColumns_1_1ColumnPhotoProperties.html#a1e0c6be1da4fa29ddecaf0bc07a87a37", null ],
    [ "s", "classDigikam_1_1TableViewColumns_1_1ColumnPhotoProperties.html#a90a53ac037c5230322f608a687680efa", null ]
];