var classDigikam_1_1AlbumManager =
[
    [ "Private", "classDigikam_1_1AlbumManager_1_1Private.html", "classDigikam_1_1AlbumManager_1_1Private" ],
    [ "addFakeConnection", "classDigikam_1_1AlbumManager.html#aa2aa12520de75891bd574164c7a3fa8c", null ],
    [ "albumTitles", "classDigikam_1_1AlbumManager.html#ac81d57bcb55258039acb22611f209533", null ],
    [ "allDAlbums", "classDigikam_1_1AlbumManager.html#ac8e4c2220588caa165e64393cd970e83", null ],
    [ "allPAlbums", "classDigikam_1_1AlbumManager.html#a25125b65c3d41e9a2b280f2a4bfe6825", null ],
    [ "allSAlbums", "classDigikam_1_1AlbumManager.html#a5c0d8eb562cde835547a9f42b7f6ec4f", null ],
    [ "allTAlbums", "classDigikam_1_1AlbumManager.html#aeb530ebdc9854a06c41eac77b63af74f", null ],
    [ "changeDatabase", "classDigikam_1_1AlbumManager.html#a983e92836288fb796f69569f73e3d568", null ],
    [ "cleanUp", "classDigikam_1_1AlbumManager.html#ae6cd83354bf61fb2b745d772d2f92879", null ],
    [ "clearCurrentAlbums", "classDigikam_1_1AlbumManager.html#ae045707f527370e4db2fc9f548cc224d", null ],
    [ "createPAlbum", "classDigikam_1_1AlbumManager.html#a1f9c2df8ac7174777c89e94a833b92e0", null ],
    [ "createPAlbum", "classDigikam_1_1AlbumManager.html#a57ac41112f2b61d9130fa171d216ee4c", null ],
    [ "createPAlbum", "classDigikam_1_1AlbumManager.html#a901381e784fd568d53768e3c14069467", null ],
    [ "createSAlbum", "classDigikam_1_1AlbumManager.html#af31ae7025228ef49ea2994645d0b5967", null ],
    [ "createTAlbum", "classDigikam_1_1AlbumManager.html#a7321dbe155b613bd6812b22d14ff74c5", null ],
    [ "currentAlbums", "classDigikam_1_1AlbumManager.html#ab6ea6ff95e81b3989f10ef7aa1de53fc", null ],
    [ "currentPAlbum", "classDigikam_1_1AlbumManager.html#a65fd86dc312bc4d6bced2cdebb49c05b", null ],
    [ "currentTAlbums", "classDigikam_1_1AlbumManager.html#ac5d128bbf92ecf90295ade3ebf26c256", null ],
    [ "databaseEqual", "classDigikam_1_1AlbumManager.html#a7bb6887cbb3a4a7333c6e50dc6dfedca", null ],
    [ "deleteSAlbum", "classDigikam_1_1AlbumManager.html#a44dd9c84e422fd88f31e47e99e98f08e", null ],
    [ "deleteTAlbum", "classDigikam_1_1AlbumManager.html#a106cbfdbf556b1611f52ef0eaffa04c3", null ],
    [ "findAlbum", "classDigikam_1_1AlbumManager.html#a28d1fbb4c7dc7e9f512be79df4ae2ad5", null ],
    [ "findAlbum", "classDigikam_1_1AlbumManager.html#a8ea589f35a771d5d79aa11d31d1b0f37", null ],
    [ "findDAlbum", "classDigikam_1_1AlbumManager.html#acf21a3bd8d7bafb1b99ecb6bc89d716f", null ],
    [ "findOrCreateTAlbums", "classDigikam_1_1AlbumManager.html#acacf6990e07e6c3f8e442858652cc701", null ],
    [ "findPAlbum", "classDigikam_1_1AlbumManager.html#a718e75ca9922d6e0e3d4079bf923a578", null ],
    [ "findPAlbum", "classDigikam_1_1AlbumManager.html#a21cdb6a9d230223bf9c33c890616f892", null ],
    [ "findSAlbum", "classDigikam_1_1AlbumManager.html#a79c6a164b4a27c335fc5e832657ef8ae", null ],
    [ "findSAlbum", "classDigikam_1_1AlbumManager.html#a046596a9d4d6d2e0a34e300b51eeb17d", null ],
    [ "findSAlbumsBySearchType", "classDigikam_1_1AlbumManager.html#a89c08cdd55bd4a1e9ad4f9765a124048", null ],
    [ "findTagsWithProperty", "classDigikam_1_1AlbumManager.html#ac7af0bf74886e45c330fb7dd42ec28d5", null ],
    [ "findTagsWithProperty", "classDigikam_1_1AlbumManager.html#af2c8268cfece83f49a8802d42f9a6236", null ],
    [ "findTAlbum", "classDigikam_1_1AlbumManager.html#aaf2fef6c62452700d9d81304ffb70357", null ],
    [ "findTAlbum", "classDigikam_1_1AlbumManager.html#aa657654260ba8b91655f3dc4d8ea09af", null ],
    [ "getDAlbumsCount", "classDigikam_1_1AlbumManager.html#a2a203013e0e2b6d240769259b6a2608d", null ],
    [ "getFaceCount", "classDigikam_1_1AlbumManager.html#ab21b72b3554c395f1b2cbc1ec346dae5", null ],
    [ "getItemFromAlbum", "classDigikam_1_1AlbumManager.html#ad971e1a23817e1190fc7746e09fa4830", null ],
    [ "getPAlbumsCount", "classDigikam_1_1AlbumManager.html#a582c5bc65820e70d23c8057c5ee00835", null ],
    [ "getRecentlyAssignedTags", "classDigikam_1_1AlbumManager.html#ac467d193be96c6d95b50b00c75507961", null ],
    [ "getTAlbumsCount", "classDigikam_1_1AlbumManager.html#a6359d3c7d548ae3df92089b2d9b66930", null ],
    [ "getUnconfirmedFaceCount", "classDigikam_1_1AlbumManager.html#aa3ef4df221d217dbba3e70fa144948a1", null ],
    [ "isMovingAlbum", "classDigikam_1_1AlbumManager.html#a2b44045f7690c5768a69cdcb68b964b4", null ],
    [ "isShowingOnlyAvailableAlbums", "classDigikam_1_1AlbumManager.html#afaa8e7f10b6244835f9b6b18b5d94d0e", null ],
    [ "mergeTAlbum", "classDigikam_1_1AlbumManager.html#ad6754fb5a1e738d954fb33736a823db4", null ],
    [ "moveTAlbum", "classDigikam_1_1AlbumManager.html#aa1f9e53efca7b02d66d0264072c7944e", null ],
    [ "prepareItemCounts", "classDigikam_1_1AlbumManager.html#af0212cb85c1dcf431a528c2436411228", null ],
    [ "refresh", "classDigikam_1_1AlbumManager.html#a7a4075a1e38438479ea9dc1ecc71ac7c", null ],
    [ "removeFakeConnection", "classDigikam_1_1AlbumManager.html#a9199404d16402299dad453a97227cadc", null ],
    [ "removeWatchedPAlbums", "classDigikam_1_1AlbumManager.html#a86717b53ee9ff281d770e77a61f07d15", null ],
    [ "renamePAlbum", "classDigikam_1_1AlbumManager.html#ae6b48fd95da2430021de2f4d1c6aa4b1", null ],
    [ "renameTAlbum", "classDigikam_1_1AlbumManager.html#a7bc8be471713c31c77fc22863cf16ab8", null ],
    [ "setCurrentAlbums", "classDigikam_1_1AlbumManager.html#a791067842edf85aa51c6580ce1e4e1f1", null ],
    [ "setDatabase", "classDigikam_1_1AlbumManager.html#ad9fbb576c7659226acc95edea6058619", null ],
    [ "setShowOnlyAvailableAlbums", "classDigikam_1_1AlbumManager.html#a177ccfcb75a9128d67871e4e39b7232f", null ],
    [ "signalAlbumAboutToBeAdded", "classDigikam_1_1AlbumManager.html#a3f7c762d56e5387ca318227a7dda2cc1", null ],
    [ "signalAlbumAboutToBeDeleted", "classDigikam_1_1AlbumManager.html#aef780005c859ca24f04e17b4092aed7e", null ],
    [ "signalAlbumAboutToBeMoved", "classDigikam_1_1AlbumManager.html#a13c3272d64ab852aab85194b4e0c65f4", null ],
    [ "signalAlbumAdded", "classDigikam_1_1AlbumManager.html#adb3f22df2990c8a48f58aa0815154417", null ],
    [ "signalAlbumCurrentChanged", "classDigikam_1_1AlbumManager.html#a7b377238f62f094a664effda17bd403b", null ],
    [ "signalAlbumDeleted", "classDigikam_1_1AlbumManager.html#a458f1c968c034a92093dc51d5ad71fe2", null ],
    [ "signalAlbumHasBeenDeleted", "classDigikam_1_1AlbumManager.html#a26ce79de6f7178727743757ef226366e", null ],
    [ "signalAlbumIconChanged", "classDigikam_1_1AlbumManager.html#a9299f692c3cbe111db64b90bac61f9b2", null ],
    [ "signalAlbumMoved", "classDigikam_1_1AlbumManager.html#a8670e7421a519c4f3ce926c6b7ec5b6c", null ],
    [ "signalAlbumNewPath", "classDigikam_1_1AlbumManager.html#ab7937eece3bcc4c485ef110f97f68403", null ],
    [ "signalAlbumRenamed", "classDigikam_1_1AlbumManager.html#ab07cbe4b039520036472c29bc3ec9883", null ],
    [ "signalAlbumsCleared", "classDigikam_1_1AlbumManager.html#a4eb44fb4dd5112a3497cc4215e415c9b", null ],
    [ "signalAlbumsUpdated", "classDigikam_1_1AlbumManager.html#acc37f701607d0420f1aeb51c9a19b87c", null ],
    [ "signalAllAlbumsLoaded", "classDigikam_1_1AlbumManager.html#a0ccd523a7c7b56e2cb2f2fa419ec61ef", null ],
    [ "signalAllDAlbumsLoaded", "classDigikam_1_1AlbumManager.html#aaff459eb89645482b880b3fdc04525bf", null ],
    [ "signalDAlbumsDirty", "classDigikam_1_1AlbumManager.html#a5d12c228da6ee3efa745ec3e911d9b4f", null ],
    [ "signalDatesHashDirty", "classDigikam_1_1AlbumManager.html#a6c1434401c80ffe5dd89690c6e5024f2", null ],
    [ "signalFaceCountsDirty", "classDigikam_1_1AlbumManager.html#a6d777876ea6169c28b96704d603f300d", null ],
    [ "signalPAlbumsDirty", "classDigikam_1_1AlbumManager.html#a589a4eae635d919be8fd57fc014db72e", null ],
    [ "signalSearchUpdated", "classDigikam_1_1AlbumManager.html#aeac25cc4a628aacc50f50fbe850c3557", null ],
    [ "signalShowOnlyAvailableAlbumsChanged", "classDigikam_1_1AlbumManager.html#a6ea1fbe8e3e073539949f28087b1111e", null ],
    [ "signalTagPropertiesChanged", "classDigikam_1_1AlbumManager.html#a7c2ad49961f8615f408016c36f235cd0", null ],
    [ "signalTAlbumsDirty", "classDigikam_1_1AlbumManager.html#a3690573d6a429884d84b8b206c241006", null ],
    [ "signalUpdateDuplicatesAlbums", "classDigikam_1_1AlbumManager.html#a449d1cf73498b7339ee9b4ae35c3efe0", null ],
    [ "startScan", "classDigikam_1_1AlbumManager.html#a58d5e7dcc38a6d2837959ed21fddc3c7", null ],
    [ "subTags", "classDigikam_1_1AlbumManager.html#ab69d4d5dd21f3dd9186a13827232732e", null ],
    [ "tagNames", "classDigikam_1_1AlbumManager.html#a673e18ac8effe3afc8b33d43e0046023", null ],
    [ "tagNames", "classDigikam_1_1AlbumManager.html#a6928493d14d929ccb953d29d8079892e", null ],
    [ "tagPaths", "classDigikam_1_1AlbumManager.html#a31ec5456753805fced16508082276563", null ],
    [ "tagPaths", "classDigikam_1_1AlbumManager.html#a33021c794210eb6ac5e2fb84e5047583", null ],
    [ "updatePAlbumIcon", "classDigikam_1_1AlbumManager.html#a5044cd95bfdbdadcd6923419af3c7a05", null ],
    [ "updateSAlbum", "classDigikam_1_1AlbumManager.html#a94f7610e0d9312b4c0eda57526178ed1", null ],
    [ "updateTAlbumIcon", "classDigikam_1_1AlbumManager.html#af447da4636719e1a89e0eb6583ce71ce", null ],
    [ "Album", "classDigikam_1_1AlbumManager.html#a8e3a19ea7fcd3a2fcf1536a8d7dea22f", null ],
    [ "AlbumManagerCreator", "classDigikam_1_1AlbumManager.html#a1a03b0dcb3770fb0fa346e08bff4efa6", null ],
    [ "AlbumPointer", "classDigikam_1_1AlbumManager.html#aa3d20f60947cceba4115e24d95898ce8", null ]
];