var dir_92f3183d4148fbc9a0b16396fbc74922 =
[
    [ "advprintalbumspage.cpp", "advprintalbumspage_8cpp.html", null ],
    [ "advprintalbumspage.h", "advprintalbumspage_8h.html", [
      [ "AdvPrintAlbumsPage", "classDigikamGenericPrintCreatorPlugin_1_1AdvPrintAlbumsPage.html", "classDigikamGenericPrintCreatorPlugin_1_1AdvPrintAlbumsPage" ]
    ] ],
    [ "advprintcaptionpage.cpp", "advprintcaptionpage_8cpp.html", null ],
    [ "advprintcaptionpage.h", "advprintcaptionpage_8h.html", [
      [ "AdvPrintCaptionPage", "classDigikamGenericPrintCreatorPlugin_1_1AdvPrintCaptionPage.html", "classDigikamGenericPrintCreatorPlugin_1_1AdvPrintCaptionPage" ]
    ] ],
    [ "advprintcropframe.cpp", "advprintcropframe_8cpp.html", null ],
    [ "advprintcropframe.h", "advprintcropframe_8h.html", [
      [ "AdvPrintCropFrame", "classDigikamGenericPrintCreatorPlugin_1_1AdvPrintCropFrame.html", "classDigikamGenericPrintCreatorPlugin_1_1AdvPrintCropFrame" ]
    ] ],
    [ "advprintcroppage.cpp", "advprintcroppage_8cpp.html", null ],
    [ "advprintcroppage.h", "advprintcroppage_8h.html", [
      [ "AdvPrintCropPage", "classDigikamGenericPrintCreatorPlugin_1_1AdvPrintCropPage.html", "classDigikamGenericPrintCreatorPlugin_1_1AdvPrintCropPage" ]
    ] ],
    [ "advprintcustomdlg.cpp", "advprintcustomdlg_8cpp.html", null ],
    [ "advprintcustomdlg.h", "advprintcustomdlg_8h.html", [
      [ "AdvPrintCustomLayoutDlg", "classDigikamGenericPrintCreatorPlugin_1_1AdvPrintCustomLayoutDlg.html", "classDigikamGenericPrintCreatorPlugin_1_1AdvPrintCustomLayoutDlg" ]
    ] ],
    [ "advprintfinalpage.cpp", "advprintfinalpage_8cpp.html", null ],
    [ "advprintfinalpage.h", "advprintfinalpage_8h.html", [
      [ "AdvPrintFinalPage", "classDigikamGenericPrintCreatorPlugin_1_1AdvPrintFinalPage.html", "classDigikamGenericPrintCreatorPlugin_1_1AdvPrintFinalPage" ]
    ] ],
    [ "advprintintropage.cpp", "advprintintropage_8cpp.html", null ],
    [ "advprintintropage.h", "advprintintropage_8h.html", [
      [ "AdvPrintIntroPage", "classDigikamGenericPrintCreatorPlugin_1_1AdvPrintIntroPage.html", "classDigikamGenericPrintCreatorPlugin_1_1AdvPrintIntroPage" ]
    ] ],
    [ "advprintoutputpage.cpp", "advprintoutputpage_8cpp.html", null ],
    [ "advprintoutputpage.h", "advprintoutputpage_8h.html", [
      [ "AdvPrintOutputPage", "classDigikamGenericPrintCreatorPlugin_1_1AdvPrintOutputPage.html", "classDigikamGenericPrintCreatorPlugin_1_1AdvPrintOutputPage" ]
    ] ],
    [ "advprintphotopage.cpp", "advprintphotopage_8cpp.html", null ],
    [ "advprintphotopage.h", "advprintphotopage_8h.html", [
      [ "AdvPrintPhotoPage", "classDigikamGenericPrintCreatorPlugin_1_1AdvPrintPhotoPage.html", "classDigikamGenericPrintCreatorPlugin_1_1AdvPrintPhotoPage" ]
    ] ],
    [ "advprintwizard.cpp", "advprintwizard_8cpp.html", null ],
    [ "advprintwizard.h", "advprintwizard_8h.html", [
      [ "AdvPrintWizard", "classDigikamGenericPrintCreatorPlugin_1_1AdvPrintWizard.html", "classDigikamGenericPrintCreatorPlugin_1_1AdvPrintWizard" ]
    ] ]
];