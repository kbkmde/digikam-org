var classDigikam_1_1DImgFilterManager =
[
    [ "addGenerator", "classDigikam_1_1DImgFilterManager.html#ae228d14f61ca2c01e586686c20d41b04", null ],
    [ "createFilter", "classDigikam_1_1DImgFilterManager.html#af3f84212db6a7785865fb2204f38738e", null ],
    [ "displayableName", "classDigikam_1_1DImgFilterManager.html#acbe5ed27811b7839cb3766b2b727ffb3", null ],
    [ "filterIcon", "classDigikam_1_1DImgFilterManager.html#a1924b6ff2c4155a14f29a6d5c6367cf0", null ],
    [ "filterIcon", "classDigikam_1_1DImgFilterManager.html#a56056b83480daea9adb3d3541bf2df74", null ],
    [ "i18nDisplayableName", "classDigikam_1_1DImgFilterManager.html#aa4a920d15b2f4d2466855f438ec01594", null ],
    [ "i18nDisplayableName", "classDigikam_1_1DImgFilterManager.html#adb8b785d8d61883e93d5f43df171902a", null ],
    [ "isRawConversion", "classDigikam_1_1DImgFilterManager.html#aaf301ecb8adc879ab79a20864f53853f", null ],
    [ "isSupported", "classDigikam_1_1DImgFilterManager.html#a8f173dda0275d18f4f6fb95bf9249a39", null ],
    [ "isSupported", "classDigikam_1_1DImgFilterManager.html#ac1295d516eb4f532986dbcfff2b6bb28", null ],
    [ "removeGenerator", "classDigikam_1_1DImgFilterManager.html#a4a2c0bfaf376e25716b36226ea58ee64", null ],
    [ "supportedFilters", "classDigikam_1_1DImgFilterManager.html#ad6d6b5bedd252aeacbb3e5a0ffa18819", null ],
    [ "supportedVersions", "classDigikam_1_1DImgFilterManager.html#a7621889b614e8bf3d218b071e091b7cb", null ],
    [ "DImgFilterManagerCreator", "classDigikam_1_1DImgFilterManager.html#a3ffb89e4c2b1c53cace798e094e11a85", null ]
];