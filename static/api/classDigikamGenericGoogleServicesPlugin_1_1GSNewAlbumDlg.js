var classDigikamGenericGoogleServicesPlugin_1_1GSNewAlbumDlg =
[
    [ "GSNewAlbumDlg", "classDigikamGenericGoogleServicesPlugin_1_1GSNewAlbumDlg.html#a165c505d4ea30eee1937f2118dc6d435", null ],
    [ "~GSNewAlbumDlg", "classDigikamGenericGoogleServicesPlugin_1_1GSNewAlbumDlg.html#a583faf6e4cb025755e0a0ff17ea51acd", null ],
    [ "addToMainLayout", "classDigikamGenericGoogleServicesPlugin_1_1GSNewAlbumDlg.html#add6de4ea3bfd2840ec32de4b3ec3e62d", null ],
    [ "getAlbumBox", "classDigikamGenericGoogleServicesPlugin_1_1GSNewAlbumDlg.html#a2a796c4f2807c1e8ed6cf40affa5afa0", null ],
    [ "getAlbumProperties", "classDigikamGenericGoogleServicesPlugin_1_1GSNewAlbumDlg.html#a0c0dbb81fb729a3a17ffb57295f32fdc", null ],
    [ "getBaseAlbumProperties", "classDigikamGenericGoogleServicesPlugin_1_1GSNewAlbumDlg.html#ab06d1922af37d55a29df704167231037", null ],
    [ "getButtonBox", "classDigikamGenericGoogleServicesPlugin_1_1GSNewAlbumDlg.html#a17db77c1fc6054529b012766af159b55", null ],
    [ "getDateTimeEdit", "classDigikamGenericGoogleServicesPlugin_1_1GSNewAlbumDlg.html#af7f30d95ea761e4c49e7970894690591", null ],
    [ "getDescEdit", "classDigikamGenericGoogleServicesPlugin_1_1GSNewAlbumDlg.html#a48787b212858a75b47331abf230f4412", null ],
    [ "getLocEdit", "classDigikamGenericGoogleServicesPlugin_1_1GSNewAlbumDlg.html#a3578365a1d3f6662decfb76b077611d2", null ],
    [ "getMainWidget", "classDigikamGenericGoogleServicesPlugin_1_1GSNewAlbumDlg.html#a12076a20f061f220d81a951734d29a67", null ],
    [ "getTitleEdit", "classDigikamGenericGoogleServicesPlugin_1_1GSNewAlbumDlg.html#a356426352dc81b5419852be6b742397b", null ],
    [ "hideDateTime", "classDigikamGenericGoogleServicesPlugin_1_1GSNewAlbumDlg.html#a7d7d1c674a00240ed5628a621dd1471d", null ],
    [ "hideDesc", "classDigikamGenericGoogleServicesPlugin_1_1GSNewAlbumDlg.html#a90e69c3d0fbae903d3e3db5268aa8df1", null ],
    [ "hideLocation", "classDigikamGenericGoogleServicesPlugin_1_1GSNewAlbumDlg.html#a335987a85da0b9b0bd083f3b85250370", null ]
];