var classShowFoto_1_1ShowfotoStackViewFavoriteItem =
[
    [ "FavoriteType", "classShowFoto_1_1ShowfotoStackViewFavoriteItem.html#a95597c1e559874f93022176bd8295cb6", [
      [ "FavoriteRoot", "classShowFoto_1_1ShowfotoStackViewFavoriteItem.html#a95597c1e559874f93022176bd8295cb6a04ac1a762cd019e3b98611448a139d85", null ],
      [ "FavoriteFolder", "classShowFoto_1_1ShowfotoStackViewFavoriteItem.html#a95597c1e559874f93022176bd8295cb6a9a576392519089994bdf399c818b6a1c", null ],
      [ "FavoriteItem", "classShowFoto_1_1ShowfotoStackViewFavoriteItem.html#a95597c1e559874f93022176bd8295cb6a23d6af00e5be45d5e2a6bb9f4ef5eba9", null ]
    ] ],
    [ "ShowfotoStackViewFavoriteItem", "classShowFoto_1_1ShowfotoStackViewFavoriteItem.html#a5656f821b73fb183db9effab00540ea1", null ],
    [ "ShowfotoStackViewFavoriteItem", "classShowFoto_1_1ShowfotoStackViewFavoriteItem.html#a338e33f6c03eb164ad28c824659d5b9e", null ],
    [ "~ShowfotoStackViewFavoriteItem", "classShowFoto_1_1ShowfotoStackViewFavoriteItem.html#a61f536c4633dd130a92811d58c76f631", null ],
    [ "currentUrl", "classShowFoto_1_1ShowfotoStackViewFavoriteItem.html#a27349b3a0e7f38b829e2f8531d86726a", null ],
    [ "date", "classShowFoto_1_1ShowfotoStackViewFavoriteItem.html#a18681efb9a6d3db5c41075e146579aa1", null ],
    [ "description", "classShowFoto_1_1ShowfotoStackViewFavoriteItem.html#af479749f666f7a731597af37b73d5dc2", null ],
    [ "favoriteType", "classShowFoto_1_1ShowfotoStackViewFavoriteItem.html#a2b2a0a48f3885a13a924219c77f4823a", null ],
    [ "hierarchy", "classShowFoto_1_1ShowfotoStackViewFavoriteItem.html#ab15dfd007554368d4785e6b074eb8030", null ],
    [ "name", "classShowFoto_1_1ShowfotoStackViewFavoriteItem.html#a2832a762d2ba43d5699462a095fae8f4", null ],
    [ "setCurrentUrl", "classShowFoto_1_1ShowfotoStackViewFavoriteItem.html#ad630a4e534d6ae09e0d4af19d9daff0a", null ],
    [ "setDate", "classShowFoto_1_1ShowfotoStackViewFavoriteItem.html#a7cc9bc35ad19fdea2b4585395a013c79", null ],
    [ "setDescription", "classShowFoto_1_1ShowfotoStackViewFavoriteItem.html#ad0aef7f91374b66f29af9a416197604c", null ],
    [ "setFavoriteType", "classShowFoto_1_1ShowfotoStackViewFavoriteItem.html#a5d94e750d1a6ba3bf34bdb2f269c1717", null ],
    [ "setHierarchy", "classShowFoto_1_1ShowfotoStackViewFavoriteItem.html#a7f2756d089debc3d01b01e4c1b20d557", null ],
    [ "setName", "classShowFoto_1_1ShowfotoStackViewFavoriteItem.html#ac960bf9e4fa2230090b528481660d8ce", null ],
    [ "setUrls", "classShowFoto_1_1ShowfotoStackViewFavoriteItem.html#a2b80b4de4c7520ec0edb28a46ff7cbc6", null ],
    [ "urls", "classShowFoto_1_1ShowfotoStackViewFavoriteItem.html#a765e9461f08f207104c4927bf0614c89", null ],
    [ "urlsToPaths", "classShowFoto_1_1ShowfotoStackViewFavoriteItem.html#a72bffbf66710bebc1fdbfd8e0fcdeacb", null ]
];