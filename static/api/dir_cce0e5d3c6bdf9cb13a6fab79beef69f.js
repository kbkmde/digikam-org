var dir_cce0e5d3c6bdf9cb13a6fab79beef69f =
[
    [ "imageshackitem.h", "imageshackitem_8h.html", [
      [ "ImageShackGallery", "classDigikamGenericImageShackPlugin_1_1ImageShackGallery.html", "classDigikamGenericImageShackPlugin_1_1ImageShackGallery" ],
      [ "ImageShackPhoto", "classDigikamGenericImageShackPlugin_1_1ImageShackPhoto.html", "classDigikamGenericImageShackPlugin_1_1ImageShackPhoto" ]
    ] ],
    [ "imageshackmpform.cpp", "imageshackmpform_8cpp.html", null ],
    [ "imageshackmpform.h", "imageshackmpform_8h.html", [
      [ "ImageShackMPForm", "classDigikamGenericImageShackPlugin_1_1ImageShackMPForm.html", "classDigikamGenericImageShackPlugin_1_1ImageShackMPForm" ]
    ] ],
    [ "imageshacknewalbumdlg.cpp", "imageshacknewalbumdlg_8cpp.html", null ],
    [ "imageshacknewalbumdlg.h", "imageshacknewalbumdlg_8h.html", [
      [ "ImageShackNewAlbumDlg", "classDigikamGenericImageShackPlugin_1_1ImageShackNewAlbumDlg.html", "classDigikamGenericImageShackPlugin_1_1ImageShackNewAlbumDlg" ]
    ] ],
    [ "imageshackplugin.cpp", "imageshackplugin_8cpp.html", null ],
    [ "imageshackplugin.h", "imageshackplugin_8h.html", "imageshackplugin_8h" ],
    [ "imageshacksession.cpp", "imageshacksession_8cpp.html", null ],
    [ "imageshacksession.h", "imageshacksession_8h.html", [
      [ "ImageShackSession", "classDigikamGenericImageShackPlugin_1_1ImageShackSession.html", "classDigikamGenericImageShackPlugin_1_1ImageShackSession" ]
    ] ],
    [ "imageshacktalker.cpp", "imageshacktalker_8cpp.html", null ],
    [ "imageshacktalker.h", "imageshacktalker_8h.html", [
      [ "ImageShackTalker", "classDigikamGenericImageShackPlugin_1_1ImageShackTalker.html", "classDigikamGenericImageShackPlugin_1_1ImageShackTalker" ]
    ] ],
    [ "imageshackwidget.cpp", "imageshackwidget_8cpp.html", null ],
    [ "imageshackwidget.h", "imageshackwidget_8h.html", [
      [ "ImageShackWidget", "classDigikamGenericImageShackPlugin_1_1ImageShackWidget.html", "classDigikamGenericImageShackPlugin_1_1ImageShackWidget" ]
    ] ],
    [ "imageshackwidget_p.h", "imageshackwidget__p_8h.html", [
      [ "Private", "classDigikamGenericImageShackPlugin_1_1ImageShackWidget_1_1Private.html", "classDigikamGenericImageShackPlugin_1_1ImageShackWidget_1_1Private" ]
    ] ],
    [ "imageshackwindow.cpp", "imageshackwindow_8cpp.html", null ],
    [ "imageshackwindow.h", "imageshackwindow_8h.html", [
      [ "ImageShackWindow", "classDigikamGenericImageShackPlugin_1_1ImageShackWindow.html", "classDigikamGenericImageShackPlugin_1_1ImageShackWindow" ]
    ] ]
];