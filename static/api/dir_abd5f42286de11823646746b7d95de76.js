var dir_abd5f42286de11823646746b7d95de76 =
[
    [ "searchbackend.cpp", "searchbackend_8cpp.html", null ],
    [ "searchbackend.h", "searchbackend_8h.html", [
      [ "SearchBackend", "classDigikamGenericGeolocationEditPlugin_1_1SearchBackend.html", "classDigikamGenericGeolocationEditPlugin_1_1SearchBackend" ],
      [ "SearchResult", "classDigikamGenericGeolocationEditPlugin_1_1SearchBackend_1_1SearchResult.html", "classDigikamGenericGeolocationEditPlugin_1_1SearchBackend_1_1SearchResult" ]
    ] ],
    [ "searchresultmodel.cpp", "searchresultmodel_8cpp.html", null ],
    [ "searchresultmodel.h", "searchresultmodel_8h.html", [
      [ "SearchResultModel", "classDigikamGenericGeolocationEditPlugin_1_1SearchResultModel.html", "classDigikamGenericGeolocationEditPlugin_1_1SearchResultModel" ],
      [ "SearchResultItem", "classDigikamGenericGeolocationEditPlugin_1_1SearchResultModel_1_1SearchResultItem.html", "classDigikamGenericGeolocationEditPlugin_1_1SearchResultModel_1_1SearchResultItem" ]
    ] ],
    [ "searchresultmodelhelper.cpp", "searchresultmodelhelper_8cpp.html", null ],
    [ "searchresultmodelhelper.h", "searchresultmodelhelper_8h.html", [
      [ "SearchResultModelHelper", "classDigikamGenericGeolocationEditPlugin_1_1SearchResultModelHelper.html", "classDigikamGenericGeolocationEditPlugin_1_1SearchResultModelHelper" ]
    ] ],
    [ "searchwidget.cpp", "searchwidget_8cpp.html", null ],
    [ "searchwidget.h", "searchwidget_8h.html", [
      [ "SearchWidget", "classDigikamGenericGeolocationEditPlugin_1_1SearchWidget.html", "classDigikamGenericGeolocationEditPlugin_1_1SearchWidget" ]
    ] ]
];