var dir_3a6be7597e29ab7c2c1c2ff9c3aa4f51 =
[
    [ "versionfileoperation.cpp", "versionfileoperation_8cpp.html", null ],
    [ "versionfileoperation.h", "versionfileoperation_8h.html", [
      [ "VersionFileInfo", "classDigikam_1_1VersionFileInfo.html", "classDigikam_1_1VersionFileInfo" ],
      [ "VersionFileOperation", "classDigikam_1_1VersionFileOperation.html", "classDigikam_1_1VersionFileOperation" ]
    ] ],
    [ "versionmanager.cpp", "versionmanager_8cpp.html", null ],
    [ "versionmanager.h", "versionmanager_8h.html", [
      [ "VersionManager", "classDigikam_1_1VersionManager.html", "classDigikam_1_1VersionManager" ]
    ] ],
    [ "versionmanagersettings.cpp", "versionmanagersettings_8cpp.html", null ],
    [ "versionmanagersettings.h", "versionmanagersettings_8h.html", [
      [ "VersionManagerSettings", "classDigikam_1_1VersionManagerSettings.html", "classDigikam_1_1VersionManagerSettings" ]
    ] ],
    [ "versionnamingscheme.cpp", "versionnamingscheme_8cpp.html", null ],
    [ "versionnamingscheme.h", "versionnamingscheme_8h.html", [
      [ "DefaultVersionNamingScheme", "classDigikam_1_1DefaultVersionNamingScheme.html", "classDigikam_1_1DefaultVersionNamingScheme" ],
      [ "VersionNamingScheme", "classDigikam_1_1VersionNamingScheme.html", "classDigikam_1_1VersionNamingScheme" ]
    ] ]
];