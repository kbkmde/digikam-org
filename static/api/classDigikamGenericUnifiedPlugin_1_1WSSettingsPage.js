var classDigikamGenericUnifiedPlugin_1_1WSSettingsPage =
[
    [ "WSSettingsPage", "classDigikamGenericUnifiedPlugin_1_1WSSettingsPage.html#a3ce2147d58c000fb2e4c496786c3fe30", null ],
    [ "~WSSettingsPage", "classDigikamGenericUnifiedPlugin_1_1WSSettingsPage.html#a34d9a11d6e635dadba41b231452b2264", null ],
    [ "assistant", "classDigikamGenericUnifiedPlugin_1_1WSSettingsPage.html#a681293182dfaf2919429a4a5aa30dd04", null ],
    [ "id", "classDigikamGenericUnifiedPlugin_1_1WSSettingsPage.html#a84aa8a0e37d7ae49efc832413764ffa1", null ],
    [ "initializePage", "classDigikamGenericUnifiedPlugin_1_1WSSettingsPage.html#a92d1a535719984a98144e1453c931b63", null ],
    [ "isComplete", "classDigikamGenericUnifiedPlugin_1_1WSSettingsPage.html#a77413f486f6d4a405359e095fb0a07a3", null ],
    [ "removePageWidget", "classDigikamGenericUnifiedPlugin_1_1WSSettingsPage.html#a40cc4abca8695016a1d788f3df1cdd0d", null ],
    [ "setComplete", "classDigikamGenericUnifiedPlugin_1_1WSSettingsPage.html#a9e97ef81648ea3f5e33c5a08ae5f0ff0", null ],
    [ "setLeftBottomPix", "classDigikamGenericUnifiedPlugin_1_1WSSettingsPage.html#a04095257f4a957050e91cd4666f53316", null ],
    [ "setLeftBottomPix", "classDigikamGenericUnifiedPlugin_1_1WSSettingsPage.html#a2e56503c5324f5551edb8fb1f1156530", null ],
    [ "setPageWidget", "classDigikamGenericUnifiedPlugin_1_1WSSettingsPage.html#a2815f8463990300af07fe29cbd090308", null ],
    [ "setShowLeftView", "classDigikamGenericUnifiedPlugin_1_1WSSettingsPage.html#a67975edf6041a574e674576a29d606a1", null ],
    [ "validatePage", "classDigikamGenericUnifiedPlugin_1_1WSSettingsPage.html#aad06b8d4e6dcdea9589e34ca64ff994c", null ]
];