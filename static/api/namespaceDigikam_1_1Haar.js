var namespaceDigikam_1_1Haar =
[
    [ "Calculator", "classDigikam_1_1Haar_1_1Calculator.html", "classDigikam_1_1Haar_1_1Calculator" ],
    [ "ImageData", "classDigikam_1_1Haar_1_1ImageData.html", "classDigikam_1_1Haar_1_1ImageData" ],
    [ "SignatureData", "classDigikam_1_1Haar_1_1SignatureData.html", "classDigikam_1_1Haar_1_1SignatureData" ],
    [ "SignatureMap", "classDigikam_1_1Haar_1_1SignatureMap.html", "classDigikam_1_1Haar_1_1SignatureMap" ],
    [ "WeightBin", "classDigikam_1_1Haar_1_1WeightBin.html", "classDigikam_1_1Haar_1_1WeightBin" ],
    [ "Weights", "classDigikam_1_1Haar_1_1Weights.html", "classDigikam_1_1Haar_1_1Weights" ],
    [ "Idx", "namespaceDigikam_1_1Haar.html#ad2cf61f795f7cd1979a2d1a231942f2c", null ],
    [ "Unit", "namespaceDigikam_1_1Haar.html#ad7ade49d2eafef3f8363263c0d36447c", null ],
    [ "valqueue", "namespaceDigikam_1_1Haar.html#a17bb9fa4e51bad15c090de55fb8643ea", null ]
];