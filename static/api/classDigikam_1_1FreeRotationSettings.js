var classDigikam_1_1FreeRotationSettings =
[
    [ "FreeRotationSettings", "classDigikam_1_1FreeRotationSettings.html#ac74c56029318ea4152cfa95def8d9526", null ],
    [ "~FreeRotationSettings", "classDigikam_1_1FreeRotationSettings.html#a940fcebe5edc350afa1b6b4ba9216f91", null ],
    [ "defaultSettings", "classDigikam_1_1FreeRotationSettings.html#abc0e83cc5c61b4cacdb555a0e0be66d5", null ],
    [ "readSettings", "classDigikam_1_1FreeRotationSettings.html#a31b612f346e8a4bc5a9366d7244d83e5", null ],
    [ "resetToDefault", "classDigikam_1_1FreeRotationSettings.html#a8e07e6b951f1c409a029213e2541a267", null ],
    [ "setSettings", "classDigikam_1_1FreeRotationSettings.html#a2133899929025c899e2dc869c8de82d2", null ],
    [ "settings", "classDigikam_1_1FreeRotationSettings.html#a40f49d7c2d7e50bd55a4d24458de5740", null ],
    [ "signalSettingsChanged", "classDigikam_1_1FreeRotationSettings.html#ad5255d99e60f5e6f3149a2c0845b15a8", null ],
    [ "writeSettings", "classDigikam_1_1FreeRotationSettings.html#a4fcc65afa99721c50fb7253c4edc0ec4", null ]
];