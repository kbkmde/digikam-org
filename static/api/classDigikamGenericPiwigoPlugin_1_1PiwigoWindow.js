var classDigikamGenericPiwigoPlugin_1_1PiwigoWindow =
[
    [ "PiwigoWindow", "classDigikamGenericPiwigoPlugin_1_1PiwigoWindow.html#ad322198c72870d95d576cdd867800707", null ],
    [ "~PiwigoWindow", "classDigikamGenericPiwigoPlugin_1_1PiwigoWindow.html#a23cb4e3d672daa271c92ed16538e9b5d", null ],
    [ "addButton", "classDigikamGenericPiwigoPlugin_1_1PiwigoWindow.html#a338ad1ca1d39489c83775e3286302c59", null ],
    [ "cancelClicked", "classDigikamGenericPiwigoPlugin_1_1PiwigoWindow.html#a6f9919249e2e8aa237fabb2ed72e75a2", null ],
    [ "restoreDialogSize", "classDigikamGenericPiwigoPlugin_1_1PiwigoWindow.html#a5da66f5eb64468e9524230138e09c2a2", null ],
    [ "saveDialogSize", "classDigikamGenericPiwigoPlugin_1_1PiwigoWindow.html#a5496bc5fd80e57edd35e652dbb8361ae", null ],
    [ "setMainWidget", "classDigikamGenericPiwigoPlugin_1_1PiwigoWindow.html#ae956aceb6ae24079791e2f491957d446", null ],
    [ "setPlugin", "classDigikamGenericPiwigoPlugin_1_1PiwigoWindow.html#a8f9473a19990a8001d98cde6286d1059", null ],
    [ "setRejectButtonMode", "classDigikamGenericPiwigoPlugin_1_1PiwigoWindow.html#a32667efc4f6a02e644886729952fd354", null ],
    [ "startButton", "classDigikamGenericPiwigoPlugin_1_1PiwigoWindow.html#a34e7c6deb1458c37066862375a487600", null ],
    [ "m_buttons", "classDigikamGenericPiwigoPlugin_1_1PiwigoWindow.html#a06170b9f45856493f30cb0edd368a3c0", null ]
];