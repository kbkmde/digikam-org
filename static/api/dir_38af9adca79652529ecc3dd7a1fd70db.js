var dir_38af9adca79652529ecc3dd7a1fd70db =
[
    [ "editorstackview.cpp", "editorstackview_8cpp.html", null ],
    [ "editorstackview.h", "editorstackview_8h.html", [
      [ "EditorStackView", "classDigikam_1_1EditorStackView.html", "classDigikam_1_1EditorStackView" ]
    ] ],
    [ "editortool.cpp", "editortool_8cpp.html", null ],
    [ "editortool.h", "editortool_8h.html", [
      [ "EditorTool", "classDigikam_1_1EditorTool.html", "classDigikam_1_1EditorTool" ],
      [ "EditorToolThreaded", "classDigikam_1_1EditorToolThreaded.html", "classDigikam_1_1EditorToolThreaded" ]
    ] ],
    [ "editortooliface.cpp", "editortooliface_8cpp.html", null ],
    [ "editortooliface.h", "editortooliface_8h.html", [
      [ "EditorToolIface", "classDigikam_1_1EditorToolIface.html", "classDigikam_1_1EditorToolIface" ]
    ] ],
    [ "editortoolsettings.cpp", "editortoolsettings_8cpp.html", null ],
    [ "editortoolsettings.h", "editortoolsettings_8h.html", [
      [ "EditorToolSettings", "classDigikam_1_1EditorToolSettings.html", "classDigikam_1_1EditorToolSettings" ]
    ] ],
    [ "editorwindow.cpp", "editorwindow_8cpp.html", null ],
    [ "editorwindow.h", "editorwindow_8h.html", [
      [ "EditorWindow", "classDigikam_1_1EditorWindow.html", "classDigikam_1_1EditorWindow" ]
    ] ],
    [ "editorwindow_p.h", "editorwindow__p_8h.html", [
      [ "Private", "classDigikam_1_1EditorWindow_1_1Private.html", "classDigikam_1_1EditorWindow_1_1Private" ]
    ] ],
    [ "imageiface.cpp", "imageiface_8cpp.html", null ],
    [ "imageiface.h", "imageiface_8h.html", [
      [ "ImageIface", "classDigikam_1_1ImageIface.html", "classDigikam_1_1ImageIface" ]
    ] ]
];