var dir_7394af9ad2aeba2a23b236239a7f0f1c =
[
    [ "histogrambox.cpp", "histogrambox_8cpp.html", null ],
    [ "histogrambox.h", "histogrambox_8h.html", [
      [ "HistogramBox", "classDigikam_1_1HistogramBox.html", "classDigikam_1_1HistogramBox" ]
    ] ],
    [ "histogrampainter.cpp", "histogrampainter_8cpp.html", "histogrampainter_8cpp" ],
    [ "histogrampainter.h", "histogrampainter_8h.html", [
      [ "HistogramPainter", "classDigikam_1_1HistogramPainter.html", "classDigikam_1_1HistogramPainter" ]
    ] ],
    [ "histogramwidget.cpp", "histogramwidget_8cpp.html", null ],
    [ "histogramwidget.h", "histogramwidget_8h.html", [
      [ "HistogramWidget", "classDigikam_1_1HistogramWidget.html", "classDigikam_1_1HistogramWidget" ]
    ] ],
    [ "imagehistogram.cpp", "imagehistogram_8cpp.html", null ],
    [ "imagehistogram.h", "imagehistogram_8h.html", [
      [ "ImageHistogram", "classDigikam_1_1ImageHistogram.html", "classDigikam_1_1ImageHistogram" ]
    ] ],
    [ "imagelevels.cpp", "imagelevels_8cpp.html", "imagelevels_8cpp" ],
    [ "imagelevels.h", "imagelevels_8h.html", [
      [ "ImageLevels", "classDigikam_1_1ImageLevels.html", "classDigikam_1_1ImageLevels" ]
    ] ],
    [ "levelsfilter.cpp", "levelsfilter_8cpp.html", null ],
    [ "levelsfilter.h", "levelsfilter_8h.html", [
      [ "LevelsContainer", "classDigikam_1_1LevelsContainer.html", "classDigikam_1_1LevelsContainer" ],
      [ "LevelsFilter", "classDigikam_1_1LevelsFilter.html", "classDigikam_1_1LevelsFilter" ]
    ] ]
];