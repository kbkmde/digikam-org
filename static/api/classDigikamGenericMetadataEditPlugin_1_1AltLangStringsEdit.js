var classDigikamGenericMetadataEditPlugin_1_1AltLangStringsEdit =
[
    [ "AltLangStringsEdit", "classDigikamGenericMetadataEditPlugin_1_1AltLangStringsEdit.html#acf17b7f274adb60476356dc84d293aef", null ],
    [ "~AltLangStringsEdit", "classDigikamGenericMetadataEditPlugin_1_1AltLangStringsEdit.html#a316a7e154ad47bef5dfed61a8283ed9d", null ],
    [ "asDefaultAltLang", "classDigikamGenericMetadataEditPlugin_1_1AltLangStringsEdit.html#a2be9a21efce9eb5e43863e49ce63b3c7", null ],
    [ "defaultAltLang", "classDigikamGenericMetadataEditPlugin_1_1AltLangStringsEdit.html#a17bcdc199d374fe2dd52b89d3022746f", null ],
    [ "getValues", "classDigikamGenericMetadataEditPlugin_1_1AltLangStringsEdit.html#a5ad9de31490c1d4443ed7be4281f3e36", null ],
    [ "isValid", "classDigikamGenericMetadataEditPlugin_1_1AltLangStringsEdit.html#a5f5fd98734d90a8da7efe44244b34358", null ],
    [ "setValid", "classDigikamGenericMetadataEditPlugin_1_1AltLangStringsEdit.html#a941cc930cd49db79eadb1da891a27566", null ],
    [ "setValues", "classDigikamGenericMetadataEditPlugin_1_1AltLangStringsEdit.html#ab9435c1d12084e108a69bcf3c59de559", null ],
    [ "signalDefaultLanguageEnabled", "classDigikamGenericMetadataEditPlugin_1_1AltLangStringsEdit.html#afb66fa1b26c853efbd126732888f5069", null ],
    [ "signalModified", "classDigikamGenericMetadataEditPlugin_1_1AltLangStringsEdit.html#ae779cacadf9a3a57168ea2f6234d2a3a", null ],
    [ "signalToggled", "classDigikamGenericMetadataEditPlugin_1_1AltLangStringsEdit.html#a404e7b5129e981a2f052df7a5c75a28d", null ]
];