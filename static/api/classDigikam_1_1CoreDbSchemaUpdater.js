var classDigikam_1_1CoreDbSchemaUpdater =
[
    [ "CoreDbSchemaUpdater", "classDigikam_1_1CoreDbSchemaUpdater.html#a645a5ed31b2e5e08872b97ce228bf124", null ],
    [ "~CoreDbSchemaUpdater", "classDigikam_1_1CoreDbSchemaUpdater.html#aab3d1e1052cf5d51ecc9b353b212f609", null ],
    [ "getLastErrorMessage", "classDigikam_1_1CoreDbSchemaUpdater.html#ac7641439aa296bdf8885643bbe52eaab", null ],
    [ "setCoreDbAccess", "classDigikam_1_1CoreDbSchemaUpdater.html#ad40099454e2a692e766c96aa154d212e", null ],
    [ "setObserver", "classDigikam_1_1CoreDbSchemaUpdater.html#a7114c51aa1f3ec8a3256566951c60933", null ],
    [ "update", "classDigikam_1_1CoreDbSchemaUpdater.html#a0a96935c1e29f49a8e6c8d9137af36c0", null ],
    [ "updateUniqueHash", "classDigikam_1_1CoreDbSchemaUpdater.html#a7782f1adba267f228e9b40b1059b5acf", null ]
];