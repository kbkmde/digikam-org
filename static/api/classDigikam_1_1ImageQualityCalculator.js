var classDigikam_1_1ImageQualityCalculator =
[
    [ "ResultDetection", "structDigikam_1_1ImageQualityCalculator_1_1ResultDetection.html", "structDigikam_1_1ImageQualityCalculator_1_1ResultDetection" ],
    [ "ImageQualityCalculator", "classDigikam_1_1ImageQualityCalculator.html#a6dcd4e9d101dfdf4637f61605d8025b9", null ],
    [ "~ImageQualityCalculator", "classDigikam_1_1ImageQualityCalculator.html#ac3b23b15e277ef811a22b503ed57c5bb", null ],
    [ "addDetectionResult", "classDigikam_1_1ImageQualityCalculator.html#af265c0fcd36f703e70e9b5aa9388ae15", null ],
    [ "calculateQuality", "classDigikam_1_1ImageQualityCalculator.html#a08ed6eeaa8d2a77d31dcf5b673ff441e", null ]
];