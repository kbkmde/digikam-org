var dir_dec403e4d80c57f953fc8cfc42016359 =
[
    [ "autocroptask.cpp", "autocroptask_8cpp.html", null ],
    [ "autocroptask.h", "autocroptask_8h.html", [
      [ "AutoCropTask", "classDigikamGenericPanoramaPlugin_1_1AutoCropTask.html", "classDigikamGenericPanoramaPlugin_1_1AutoCropTask" ]
    ] ],
    [ "commandtask.cpp", "commandtask_8cpp.html", null ],
    [ "commandtask.h", "commandtask_8h.html", [
      [ "CommandTask", "classDigikamGenericPanoramaPlugin_1_1CommandTask.html", "classDigikamGenericPanoramaPlugin_1_1CommandTask" ]
    ] ],
    [ "compilemksteptask.cpp", "compilemksteptask_8cpp.html", null ],
    [ "compilemksteptask.h", "compilemksteptask_8h.html", [
      [ "CompileMKStepTask", "classDigikamGenericPanoramaPlugin_1_1CompileMKStepTask.html", "classDigikamGenericPanoramaPlugin_1_1CompileMKStepTask" ]
    ] ],
    [ "compilemktask.cpp", "compilemktask_8cpp.html", null ],
    [ "compilemktask.h", "compilemktask_8h.html", [
      [ "CompileMKTask", "classDigikamGenericPanoramaPlugin_1_1CompileMKTask.html", "classDigikamGenericPanoramaPlugin_1_1CompileMKTask" ]
    ] ],
    [ "copyfilestask.cpp", "copyfilestask_8cpp.html", null ],
    [ "copyfilestask.h", "copyfilestask_8h.html", [
      [ "CopyFilesTask", "classDigikamGenericPanoramaPlugin_1_1CopyFilesTask.html", "classDigikamGenericPanoramaPlugin_1_1CopyFilesTask" ]
    ] ],
    [ "cpcleantask.cpp", "cpcleantask_8cpp.html", null ],
    [ "cpcleantask.h", "cpcleantask_8h.html", [
      [ "CpCleanTask", "classDigikamGenericPanoramaPlugin_1_1CpCleanTask.html", "classDigikamGenericPanoramaPlugin_1_1CpCleanTask" ]
    ] ],
    [ "cpfindtask.cpp", "cpfindtask_8cpp.html", null ],
    [ "cpfindtask.h", "cpfindtask_8h.html", [
      [ "CpFindTask", "classDigikamGenericPanoramaPlugin_1_1CpFindTask.html", "classDigikamGenericPanoramaPlugin_1_1CpFindTask" ]
    ] ],
    [ "createfinalptotask.cpp", "createfinalptotask_8cpp.html", null ],
    [ "createfinalptotask.h", "createfinalptotask_8h.html", [
      [ "CreateFinalPtoTask", "classDigikamGenericPanoramaPlugin_1_1CreateFinalPtoTask.html", "classDigikamGenericPanoramaPlugin_1_1CreateFinalPtoTask" ]
    ] ],
    [ "createmktask.cpp", "createmktask_8cpp.html", null ],
    [ "createmktask.h", "createmktask_8h.html", [
      [ "CreateMKTask", "classDigikamGenericPanoramaPlugin_1_1CreateMKTask.html", "classDigikamGenericPanoramaPlugin_1_1CreateMKTask" ]
    ] ],
    [ "createpreviewtask.cpp", "createpreviewtask_8cpp.html", null ],
    [ "createpreviewtask.h", "createpreviewtask_8h.html", [
      [ "CreatePreviewTask", "classDigikamGenericPanoramaPlugin_1_1CreatePreviewTask.html", "classDigikamGenericPanoramaPlugin_1_1CreatePreviewTask" ]
    ] ],
    [ "createptotask.cpp", "createptotask_8cpp.html", null ],
    [ "createptotask.h", "createptotask_8h.html", [
      [ "CreatePtoTask", "classDigikamGenericPanoramaPlugin_1_1CreatePtoTask.html", "classDigikamGenericPanoramaPlugin_1_1CreatePtoTask" ]
    ] ],
    [ "huginexecutortask.cpp", "huginexecutortask_8cpp.html", null ],
    [ "huginexecutortask.h", "huginexecutortask_8h.html", [
      [ "HuginExecutorTask", "classDigikamGenericPanoramaPlugin_1_1HuginExecutorTask.html", "classDigikamGenericPanoramaPlugin_1_1HuginExecutorTask" ]
    ] ],
    [ "optimisationtask.cpp", "optimisationtask_8cpp.html", null ],
    [ "optimisationtask.h", "optimisationtask_8h.html", [
      [ "OptimisationTask", "classDigikamGenericPanoramaPlugin_1_1OptimisationTask.html", "classDigikamGenericPanoramaPlugin_1_1OptimisationTask" ]
    ] ],
    [ "panotask.cpp", "panotask_8cpp.html", null ],
    [ "panotask.h", "panotask_8h.html", [
      [ "PanoTask", "classDigikamGenericPanoramaPlugin_1_1PanoTask.html", "classDigikamGenericPanoramaPlugin_1_1PanoTask" ]
    ] ],
    [ "panotasks.h", "panotasks_8h.html", null ],
    [ "preprocesstask.cpp", "preprocesstask_8cpp.html", null ],
    [ "preprocesstask.h", "preprocesstask_8h.html", [
      [ "PreProcessTask", "classDigikamGenericPanoramaPlugin_1_1PreProcessTask.html", "classDigikamGenericPanoramaPlugin_1_1PreProcessTask" ]
    ] ]
];