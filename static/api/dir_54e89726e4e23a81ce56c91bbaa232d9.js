var dir_54e89726e4e23a81ce56c91bbaa232d9 =
[
    [ "database", "dir_a156ff54dcd22042f55f8c885e9bb11f.html", "dir_a156ff54dcd22042f55f8c885e9bb11f" ],
    [ "cameranameoption.cpp", "cameranameoption_8cpp.html", null ],
    [ "cameranameoption.h", "cameranameoption_8h.html", [
      [ "CameraNameOption", "classDigikam_1_1CameraNameOption.html", "classDigikam_1_1CameraNameOption" ]
    ] ],
    [ "dateoption.cpp", "dateoption_8cpp.html", null ],
    [ "dateoption.h", "dateoption_8h.html", [
      [ "DateFormat", "classDigikam_1_1DateFormat.html", "classDigikam_1_1DateFormat" ],
      [ "DateOption", "classDigikam_1_1DateOption.html", "classDigikam_1_1DateOption" ],
      [ "DateOptionDialog", "classDigikam_1_1DateOptionDialog.html", "classDigikam_1_1DateOptionDialog" ]
    ] ],
    [ "directorynameoption.cpp", "directorynameoption_8cpp.html", null ],
    [ "directorynameoption.h", "directorynameoption_8h.html", [
      [ "DirectoryNameOption", "classDigikam_1_1DirectoryNameOption.html", "classDigikam_1_1DirectoryNameOption" ]
    ] ],
    [ "filepropertiesoption.cpp", "filepropertiesoption_8cpp.html", null ],
    [ "filepropertiesoption.h", "filepropertiesoption_8h.html", [
      [ "FilePropertiesOption", "classDigikam_1_1FilePropertiesOption.html", "classDigikam_1_1FilePropertiesOption" ]
    ] ],
    [ "metadataoption.cpp", "metadataoption_8cpp.html", null ],
    [ "metadataoption.h", "metadataoption_8h.html", [
      [ "MetadataOption", "classDigikam_1_1MetadataOption.html", "classDigikam_1_1MetadataOption" ],
      [ "MetadataOptionDialog", "classDigikam_1_1MetadataOptionDialog.html", "classDigikam_1_1MetadataOptionDialog" ]
    ] ],
    [ "sequencenumberoption.cpp", "sequencenumberoption_8cpp.html", null ],
    [ "sequencenumberoption.h", "sequencenumberoption_8h.html", [
      [ "SequenceNumberDialog", "classDigikam_1_1SequenceNumberDialog.html", "classDigikam_1_1SequenceNumberDialog" ],
      [ "SequenceNumberOption", "classDigikam_1_1SequenceNumberOption.html", "classDigikam_1_1SequenceNumberOption" ]
    ] ]
];