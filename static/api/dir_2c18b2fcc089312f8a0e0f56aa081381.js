var dir_2c18b2fcc089312f8a0e0f56aa081381 =
[
    [ "assignnameoverlay.cpp", "assignnameoverlay_8cpp.html", null ],
    [ "assignnameoverlay.h", "assignnameoverlay_8h.html", [
      [ "AssignNameOverlay", "classDigikam_1_1AssignNameOverlay.html", "classDigikam_1_1AssignNameOverlay" ]
    ] ],
    [ "facerejectionoverlay.cpp", "facerejectionoverlay_8cpp.html", null ],
    [ "facerejectionoverlay.h", "facerejectionoverlay_8h.html", [
      [ "FaceRejectionOverlay", "classDigikam_1_1FaceRejectionOverlay.html", "classDigikam_1_1FaceRejectionOverlay" ],
      [ "FaceRejectionOverlayButton", "classDigikam_1_1FaceRejectionOverlayButton.html", "classDigikam_1_1FaceRejectionOverlayButton" ]
    ] ],
    [ "groupindicatoroverlay.cpp", "groupindicatoroverlay_8cpp.html", null ],
    [ "groupindicatoroverlay.h", "groupindicatoroverlay_8h.html", [
      [ "GroupIndicatorOverlay", "classDigikam_1_1GroupIndicatorOverlay.html", "classDigikam_1_1GroupIndicatorOverlay" ],
      [ "GroupIndicatorOverlayWidget", "classDigikam_1_1GroupIndicatorOverlayWidget.html", "classDigikam_1_1GroupIndicatorOverlayWidget" ]
    ] ],
    [ "itemcoordinatesoverlay.cpp", "itemcoordinatesoverlay_8cpp.html", null ],
    [ "itemcoordinatesoverlay.h", "itemcoordinatesoverlay_8h.html", [
      [ "CoordinatesOverlayWidget", "classDigikam_1_1CoordinatesOverlayWidget.html", "classDigikam_1_1CoordinatesOverlayWidget" ],
      [ "ItemCoordinatesOverlay", "classDigikam_1_1ItemCoordinatesOverlay.html", "classDigikam_1_1ItemCoordinatesOverlay" ]
    ] ],
    [ "itemfullscreenoverlay.cpp", "itemfullscreenoverlay_8cpp.html", null ],
    [ "itemfullscreenoverlay.h", "itemfullscreenoverlay_8h.html", [
      [ "ItemFullScreenOverlay", "classDigikam_1_1ItemFullScreenOverlay.html", "classDigikam_1_1ItemFullScreenOverlay" ],
      [ "ItemFullScreenOverlayButton", "classDigikam_1_1ItemFullScreenOverlayButton.html", "classDigikam_1_1ItemFullScreenOverlayButton" ]
    ] ],
    [ "itemratingoverlay.cpp", "itemratingoverlay_8cpp.html", null ],
    [ "itemratingoverlay.h", "itemratingoverlay_8h.html", [
      [ "ItemRatingOverlay", "classDigikam_1_1ItemRatingOverlay.html", "classDigikam_1_1ItemRatingOverlay" ]
    ] ],
    [ "itemrotationoverlay.cpp", "itemrotationoverlay_8cpp.html", null ],
    [ "itemrotationoverlay.h", "itemrotationoverlay_8h.html", "itemrotationoverlay_8h" ],
    [ "itemselectionoverlay.cpp", "itemselectionoverlay_8cpp.html", null ],
    [ "itemselectionoverlay.h", "itemselectionoverlay_8h.html", [
      [ "ItemSelectionOverlay", "classDigikam_1_1ItemSelectionOverlay.html", "classDigikam_1_1ItemSelectionOverlay" ],
      [ "ItemSelectionOverlayButton", "classDigikam_1_1ItemSelectionOverlayButton.html", "classDigikam_1_1ItemSelectionOverlayButton" ]
    ] ]
];