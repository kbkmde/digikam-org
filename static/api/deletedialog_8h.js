var deletedialog_8h =
[
    [ "DeleteDialog", "classDigikam_1_1DeleteDialog.html", "classDigikam_1_1DeleteDialog" ],
    [ "DeleteItem", "classDigikam_1_1DeleteItem.html", "classDigikam_1_1DeleteItem" ],
    [ "DeleteItemList", "classDigikam_1_1DeleteItemList.html", "classDigikam_1_1DeleteItemList" ],
    [ "DeleteWidget", "classDigikam_1_1DeleteWidget.html", "classDigikam_1_1DeleteWidget" ],
    [ "DeleteMode", "deletedialog_8h.html#a228226fa80a476233c29ca3208c80908", [
      [ "NoChoiceTrash", "deletedialog_8h.html#a228226fa80a476233c29ca3208c80908ac8391504bd36ea042b5623241140773e", null ],
      [ "NoChoiceDeletePermanently", "deletedialog_8h.html#a228226fa80a476233c29ca3208c80908a5ec8f64dc273274e3a10f3e506d95fb8", null ],
      [ "UserPreference", "deletedialog_8h.html#a228226fa80a476233c29ca3208c80908ace051bffda9aacad9d5c38bde338f5cc", null ],
      [ "UseTrash", "deletedialog_8h.html#a228226fa80a476233c29ca3208c80908a23be9092b83db12699a80333801a6cbd", null ],
      [ "DeletePermanently", "deletedialog_8h.html#a228226fa80a476233c29ca3208c80908ab116dbf190ec0c8f5bcec7750d3c432d", null ]
    ] ],
    [ "ListMode", "deletedialog_8h.html#acd0b96f1006cd58f88e871594d27cf90", [
      [ "Files", "deletedialog_8h.html#acd0b96f1006cd58f88e871594d27cf90a872e300e10a2356b253cce3d4ed42e97", null ],
      [ "Albums", "deletedialog_8h.html#acd0b96f1006cd58f88e871594d27cf90a7d4c36b5f15597c5af9f586d43c1bdf8", null ],
      [ "Subalbums", "deletedialog_8h.html#acd0b96f1006cd58f88e871594d27cf90a42b80294a6f383bc90888177b92b3bbd", null ]
    ] ]
];