var classDigikam_1_1DTextLabelValue =
[
    [ "DTextLabelValue", "classDigikam_1_1DTextLabelValue.html#a312cfb8f92efc96762753ce0baa5fceb", null ],
    [ "~DTextLabelValue", "classDigikam_1_1DTextLabelValue.html#a0e811c1d440b2a14d9e130033fd672a1", null ],
    [ "adjustedText", "classDigikam_1_1DTextLabelValue.html#a808d91a0b119dc0ea7963c5d9008a22e", null ],
    [ "minimumSizeHint", "classDigikam_1_1DTextLabelValue.html#ad7094264b03ebf2b1d304b878dcc6fe8", null ],
    [ "setAdjustedText", "classDigikam_1_1DTextLabelValue.html#ad868928c7eeee8e41b2c2afd37274ec2", null ],
    [ "setAlignment", "classDigikam_1_1DTextLabelValue.html#a39554a9d00b3390c4348e9094dcf5eca", null ],
    [ "setElideMode", "classDigikam_1_1DTextLabelValue.html#affa7663c26f07e8bb30abe48180a2c77", null ],
    [ "sizeHint", "classDigikam_1_1DTextLabelValue.html#a293e6d780f0d118b702ca1fe76820ceb", null ]
];