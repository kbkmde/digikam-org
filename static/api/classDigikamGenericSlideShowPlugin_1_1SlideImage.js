var classDigikamGenericSlideShowPlugin_1_1SlideImage =
[
    [ "SlideImage", "classDigikamGenericSlideShowPlugin_1_1SlideImage.html#a71316db3ad6ce030813528dc303f3f86", null ],
    [ "~SlideImage", "classDigikamGenericSlideShowPlugin_1_1SlideImage.html#af33cfbb4f00f8ce7460fe616fb6e764c", null ],
    [ "setLoadUrl", "classDigikamGenericSlideShowPlugin_1_1SlideImage.html#a7a40d09f91f1d2fed828c5964556ae8f", null ],
    [ "setPreloadUrl", "classDigikamGenericSlideShowPlugin_1_1SlideImage.html#ad38c6fb4905bae11f427cc5c0814e1f6", null ],
    [ "setPreviewSettings", "classDigikamGenericSlideShowPlugin_1_1SlideImage.html#a3db09fa5f033adf4c1dabbece06806d8", null ],
    [ "signalImageLoaded", "classDigikamGenericSlideShowPlugin_1_1SlideImage.html#af3ed2cb333b85254bd0479bcad9846dc", null ]
];