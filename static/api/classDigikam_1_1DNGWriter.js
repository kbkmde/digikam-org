var classDigikam_1_1DNGWriter =
[
    [ "Private", "classDigikam_1_1DNGWriter_1_1Private.html", "classDigikam_1_1DNGWriter_1_1Private" ],
    [ "ConvertError", "classDigikam_1_1DNGWriter.html#a49b6e960e65e1c78394a83a553923610", [
      [ "PROCESS_CONTINUE", "classDigikam_1_1DNGWriter.html#a49b6e960e65e1c78394a83a553923610ac5ad19043ca122679e39e17ed46197d3", null ],
      [ "PROCESS_COMPLETE", "classDigikam_1_1DNGWriter.html#a49b6e960e65e1c78394a83a553923610a813e441008b87104737296bfa82969c2", null ],
      [ "PROCESS_FAILED", "classDigikam_1_1DNGWriter.html#a49b6e960e65e1c78394a83a553923610a26569b6090d8533fb40e3d3bb1f07c47", null ],
      [ "PROCESS_CANCELED", "classDigikam_1_1DNGWriter.html#a49b6e960e65e1c78394a83a553923610a9dba5dfa12c8e8ae7bef2a6e1f083ba6", null ],
      [ "FILE_NOT_SUPPORTED", "classDigikam_1_1DNGWriter.html#a49b6e960e65e1c78394a83a553923610ac2fbb9f7f8b48f74ceb6e6fc37344ea5", null ],
      [ "DNG_SDK_INTERNAL_ERROR", "classDigikam_1_1DNGWriter.html#a49b6e960e65e1c78394a83a553923610a62f770f0853c3059f35b0f24764d679d", null ]
    ] ],
    [ "JPEGPreview", "classDigikam_1_1DNGWriter.html#aa4c24136abbcbdf717d343695f435c41", [
      [ "NONE", "classDigikam_1_1DNGWriter.html#aa4c24136abbcbdf717d343695f435c41a6ed0a9ac66ddbf28e33ce690ed2a4681", null ],
      [ "MEDIUM", "classDigikam_1_1DNGWriter.html#aa4c24136abbcbdf717d343695f435c41ab6b411cb020aacf2d4ab915f05a6002c", null ],
      [ "FULL_SIZE", "classDigikam_1_1DNGWriter.html#aa4c24136abbcbdf717d343695f435c41a1ce1c58a2f06338f2a6b768cdc342098", null ]
    ] ],
    [ "DNGWriter", "classDigikam_1_1DNGWriter.html#ab670bf66af481d1a7c023420cc549ec3", null ],
    [ "~DNGWriter", "classDigikam_1_1DNGWriter.html#a9156d010a27cc20867bd023acf4a2184", null ],
    [ "backupOriginalRawFile", "classDigikam_1_1DNGWriter.html#a7a477392ae540a8f0bb5bcf25f6a2186", null ],
    [ "cancel", "classDigikam_1_1DNGWriter.html#afd5d0800bcb5341d5a5840e32c793fc6", null ],
    [ "compressLossLess", "classDigikam_1_1DNGWriter.html#aa69d0499b990da5e68dff1e7cf8ae760", null ],
    [ "convert", "classDigikam_1_1DNGWriter.html#ad43f2fdd32ea9b6bdac43efbd7c8ec13", null ],
    [ "inputFile", "classDigikam_1_1DNGWriter.html#ad68e8d81c769c25e52caf43f6afc117f", null ],
    [ "outputFile", "classDigikam_1_1DNGWriter.html#a353de1fe44d172c7ebbb1f9b03000b6d", null ],
    [ "previewMode", "classDigikam_1_1DNGWriter.html#a59a4b34ee2cbcd5f9f88f5fa03d076b3", null ],
    [ "reset", "classDigikam_1_1DNGWriter.html#ac09fae7e86f22b26ebaf6a152f1e19a9", null ],
    [ "setBackupOriginalRawFile", "classDigikam_1_1DNGWriter.html#afc4b28eb99cc0390b37f2dbcaf8d272e", null ],
    [ "setCompressLossLess", "classDigikam_1_1DNGWriter.html#a26c38f7ab84005f17efb03ddf8349f01", null ],
    [ "setInputFile", "classDigikam_1_1DNGWriter.html#a330f440a342183e95cc51d0a0c4973c1", null ],
    [ "setOutputFile", "classDigikam_1_1DNGWriter.html#a658ad72409145d88be64e813d713d548", null ],
    [ "setPreviewMode", "classDigikam_1_1DNGWriter.html#a5caf6e23e5697b23834ba45e61cfa089", null ],
    [ "setUpdateFileDate", "classDigikam_1_1DNGWriter.html#a923a641e965bfc46b1197c7e9a2bad12", null ],
    [ "updateFileDate", "classDigikam_1_1DNGWriter.html#ab34fc8347e798d7bbb663e37358ee8ea", null ]
];