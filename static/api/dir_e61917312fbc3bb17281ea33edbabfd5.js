var dir_e61917312fbc3bb17281ea33edbabfd5 =
[
    [ "parsetagstring.h", "parsetagstring_8h.html", "parsetagstring_8h" ],
    [ "rginfo.cpp", "rginfo_8cpp.html", null ],
    [ "rginfo.h", "rginfo_8h.html", [
      [ "RGInfo", "classDigikam_1_1RGInfo.html", "classDigikam_1_1RGInfo" ]
    ] ],
    [ "rgtagmodel.cpp", "rgtagmodel_8cpp.html", "rgtagmodel_8cpp" ],
    [ "rgtagmodel.h", "rgtagmodel_8h.html", [
      [ "RGTagModel", "classDigikam_1_1RGTagModel.html", "classDigikam_1_1RGTagModel" ]
    ] ],
    [ "rgwidget.cpp", "rgwidget_8cpp.html", null ],
    [ "rgwidget.h", "rgwidget_8h.html", [
      [ "RGWidget", "classDigikam_1_1RGWidget.html", "classDigikam_1_1RGWidget" ]
    ] ],
    [ "simpletreemodel.cpp", "simpletreemodel_8cpp.html", null ],
    [ "simpletreemodel.h", "simpletreemodel_8h.html", [
      [ "SimpleTreeModel", "classDigikam_1_1SimpleTreeModel.html", "classDigikam_1_1SimpleTreeModel" ],
      [ "Item", "classDigikam_1_1SimpleTreeModel_1_1Item.html", "classDigikam_1_1SimpleTreeModel_1_1Item" ]
    ] ],
    [ "treebranch.h", "treebranch_8h.html", [
      [ "TreeBranch", "classDigikam_1_1TreeBranch.html", "classDigikam_1_1TreeBranch" ]
    ] ]
];