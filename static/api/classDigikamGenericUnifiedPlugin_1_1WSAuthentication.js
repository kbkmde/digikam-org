var classDigikamGenericUnifiedPlugin_1_1WSAuthentication =
[
    [ "WSAuthentication", "classDigikamGenericUnifiedPlugin_1_1WSAuthentication.html#a24fcfe130e35473d29e55b280bab37d8", null ],
    [ "~WSAuthentication", "classDigikamGenericUnifiedPlugin_1_1WSAuthentication.html#af9fda6adc7329bf1533d9dd6b3e904b6", null ],
    [ "authenticate", "classDigikamGenericUnifiedPlugin_1_1WSAuthentication.html#a90380e846b4c27d588ce1c0687a82dd1", null ],
    [ "authenticated", "classDigikamGenericUnifiedPlugin_1_1WSAuthentication.html#acc7c07423c7b2b21be6291fe32571144", null ],
    [ "cancelTalker", "classDigikamGenericUnifiedPlugin_1_1WSAuthentication.html#af5db6c77e5f1da29ed075f8abcf9f5bc", null ],
    [ "createTalker", "classDigikamGenericUnifiedPlugin_1_1WSAuthentication.html#a09213108b5a6d89b7ae6508face0f79f", null ],
    [ "getImageCaption", "classDigikamGenericUnifiedPlugin_1_1WSAuthentication.html#a8882c1e554aca9bcae0a032667daf040", null ],
    [ "numberItemsUpload", "classDigikamGenericUnifiedPlugin_1_1WSAuthentication.html#aaf766df431d694f729ee77105d1b07e8", null ],
    [ "prepareForUpload", "classDigikamGenericUnifiedPlugin_1_1WSAuthentication.html#a47798cd3f8acaa15738d0e831f4b4fea", null ],
    [ "reauthenticate", "classDigikamGenericUnifiedPlugin_1_1WSAuthentication.html#aa6ea47221727998804a0e2b025588ea3", null ],
    [ "signalAuthenticationComplete", "classDigikamGenericUnifiedPlugin_1_1WSAuthentication.html#add13bc3163afdb2879d6ffa2787e12f7", null ],
    [ "signalCloseBrowser", "classDigikamGenericUnifiedPlugin_1_1WSAuthentication.html#af3dd5dba93adc9f29acc27fdf2060ed7", null ],
    [ "signalCreateAlbumDone", "classDigikamGenericUnifiedPlugin_1_1WSAuthentication.html#ad45a7232c7b5d59c773453231e577c5c", null ],
    [ "signalDone", "classDigikamGenericUnifiedPlugin_1_1WSAuthentication.html#a690b5ffa584592c6775a67f2fd508555", null ],
    [ "signalListAlbumsDone", "classDigikamGenericUnifiedPlugin_1_1WSAuthentication.html#a70354801c715ef969b410b6d8269d136", null ],
    [ "signalMessage", "classDigikamGenericUnifiedPlugin_1_1WSAuthentication.html#a6fd53da48c5f6d5c3ac16f35eb193f2f", null ],
    [ "signalOpenBrowser", "classDigikamGenericUnifiedPlugin_1_1WSAuthentication.html#abac00585c67b59b06ab1f95565ea8d6f", null ],
    [ "signalProgress", "classDigikamGenericUnifiedPlugin_1_1WSAuthentication.html#adfa76b5f90a036112406be15d4cbdbf7", null ],
    [ "signalResponseTokenReceived", "classDigikamGenericUnifiedPlugin_1_1WSAuthentication.html#a3babda3c59550d63da61cfb91688a026", null ],
    [ "slotAddPhotoDone", "classDigikamGenericUnifiedPlugin_1_1WSAuthentication.html#ab874c6e823c2fa24aba6bfe8b39518bc", null ],
    [ "slotCancel", "classDigikamGenericUnifiedPlugin_1_1WSAuthentication.html#a4aa31250707dac511b0271f97e226b55", null ],
    [ "slotListAlbumsDone", "classDigikamGenericUnifiedPlugin_1_1WSAuthentication.html#a491cf8f215b2fafbb03e709ba3ce3c45", null ],
    [ "slotListAlbumsRequest", "classDigikamGenericUnifiedPlugin_1_1WSAuthentication.html#a70c0907e8663daff053e1f51b4161255", null ],
    [ "slotNewAlbumRequest", "classDigikamGenericUnifiedPlugin_1_1WSAuthentication.html#a923fe80cc29fdc5071877ebf5127d41a", null ],
    [ "startTransfer", "classDigikamGenericUnifiedPlugin_1_1WSAuthentication.html#a73c0f397a6a3d167485b533797fc5773", null ],
    [ "uploadNextPhoto", "classDigikamGenericUnifiedPlugin_1_1WSAuthentication.html#a674fb262726e4ca5d33d85c6fa114d56", null ],
    [ "webserviceName", "classDigikamGenericUnifiedPlugin_1_1WSAuthentication.html#acc46ccff6835946a43237e9db1518926", null ]
];