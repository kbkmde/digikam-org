var dir_337de85c1bc2db5aa487c839f549ef3c =
[
    [ "dhistoryview.cpp", "dhistoryview_8cpp.html", null ],
    [ "dhistoryview.h", "dhistoryview_8h.html", [
      [ "DHistoryView", "classDigikam_1_1DHistoryView.html", "classDigikam_1_1DHistoryView" ]
    ] ],
    [ "dlogoaction.cpp", "dlogoaction_8cpp.html", null ],
    [ "dlogoaction.h", "dlogoaction_8h.html", [
      [ "DLogoAction", "classDigikam_1_1DLogoAction.html", "classDigikam_1_1DLogoAction" ]
    ] ],
    [ "dprogresswdg.cpp", "dprogresswdg_8cpp.html", null ],
    [ "dprogresswdg.h", "dprogresswdg_8h.html", [
      [ "DProgressWdg", "classDigikam_1_1DProgressWdg.html", "classDigikam_1_1DProgressWdg" ]
    ] ],
    [ "dworkingpixmap.cpp", "dworkingpixmap_8cpp.html", null ],
    [ "dworkingpixmap.h", "dworkingpixmap_8h.html", [
      [ "DWorkingPixmap", "classDigikam_1_1DWorkingPixmap.html", "classDigikam_1_1DWorkingPixmap" ]
    ] ],
    [ "overlaywidget.cpp", "overlaywidget_8cpp.html", null ],
    [ "overlaywidget.h", "overlaywidget_8h.html", [
      [ "OverlayWidget", "classDigikam_1_1OverlayWidget.html", "classDigikam_1_1OverlayWidget" ]
    ] ],
    [ "progressmanager.cpp", "progressmanager_8cpp.html", null ],
    [ "progressmanager.h", "progressmanager_8h.html", [
      [ "ProgressItem", "classDigikam_1_1ProgressItem.html", "classDigikam_1_1ProgressItem" ],
      [ "ProgressManager", "classDigikam_1_1ProgressManager.html", "classDigikam_1_1ProgressManager" ]
    ] ],
    [ "progressview.cpp", "progressview_8cpp.html", null ],
    [ "progressview.h", "progressview_8h.html", [
      [ "ProgressView", "classDigikam_1_1ProgressView.html", "classDigikam_1_1ProgressView" ],
      [ "TransactionItem", "classDigikam_1_1TransactionItem.html", "classDigikam_1_1TransactionItem" ],
      [ "TransactionItemView", "classDigikam_1_1TransactionItemView.html", "classDigikam_1_1TransactionItemView" ]
    ] ],
    [ "statusbarprogresswidget.cpp", "statusbarprogresswidget_8cpp.html", null ],
    [ "statusbarprogresswidget.h", "statusbarprogresswidget_8h.html", [
      [ "StatusbarProgressWidget", "classDigikam_1_1StatusbarProgressWidget.html", "classDigikam_1_1StatusbarProgressWidget" ]
    ] ],
    [ "statusprogressbar.cpp", "statusprogressbar_8cpp.html", null ],
    [ "statusprogressbar.h", "statusprogressbar_8h.html", [
      [ "StatusProgressBar", "classDigikam_1_1StatusProgressBar.html", "classDigikam_1_1StatusProgressBar" ]
    ] ],
    [ "workingwidget.cpp", "workingwidget_8cpp.html", null ],
    [ "workingwidget.h", "workingwidget_8h.html", [
      [ "WorkingWidget", "classDigikam_1_1WorkingWidget.html", "classDigikam_1_1WorkingWidget" ]
    ] ]
];