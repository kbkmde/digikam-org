var classDigikam_1_1DNNFaceDetectorBase =
[
    [ "DNNFaceDetectorBase", "classDigikam_1_1DNNFaceDetectorBase.html#a480f03d2d5b988b014bfd8fe1a77e5b5", null ],
    [ "DNNFaceDetectorBase", "classDigikam_1_1DNNFaceDetectorBase.html#a485ffbbd9a296a69d8f015732c92f43c", null ],
    [ "~DNNFaceDetectorBase", "classDigikam_1_1DNNFaceDetectorBase.html#a4f6533a17a6e584295c3558dd2f6ae87", null ],
    [ "correctBbox", "classDigikam_1_1DNNFaceDetectorBase.html#a0fef3b769989f7e1f752dbf7074697c4", null ],
    [ "detectFaces", "classDigikam_1_1DNNFaceDetectorBase.html#a5bf57d3c4d090abd710936c6694f3df8", null ],
    [ "nnInputSizeRequired", "classDigikam_1_1DNNFaceDetectorBase.html#a10c2f7b450ebc0a6b0b631e333f4f10d", null ],
    [ "selectBbox", "classDigikam_1_1DNNFaceDetectorBase.html#ac162bff0abe3bb46f644651ae04d7026", null ],
    [ "inputImageSize", "classDigikam_1_1DNNFaceDetectorBase.html#a57327c3e3bc01d71b2c155cb51f10488", null ],
    [ "meanValToSubtract", "classDigikam_1_1DNNFaceDetectorBase.html#a800befeba470c89ae6e2b9cad13bf6f3", null ],
    [ "mutex", "classDigikam_1_1DNNFaceDetectorBase.html#add749bb2513feabbf51fbf7a8d368fbf", null ],
    [ "net", "classDigikam_1_1DNNFaceDetectorBase.html#a05c55414cc0784bebc3925a6547f8fef", null ],
    [ "scaleFactor", "classDigikam_1_1DNNFaceDetectorBase.html#a3c902bace9d612c1990c1eeeec51b22f", null ]
];