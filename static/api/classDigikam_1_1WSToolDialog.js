var classDigikam_1_1WSToolDialog =
[
    [ "WSToolDialog", "classDigikam_1_1WSToolDialog.html#ad6e6ca7daa82abb21ca3539689a4e61f", null ],
    [ "~WSToolDialog", "classDigikam_1_1WSToolDialog.html#a1ce89238ba6b2b3b7dd9eba3f62c697c", null ],
    [ "addButton", "classDigikam_1_1WSToolDialog.html#a338ad1ca1d39489c83775e3286302c59", null ],
    [ "cancelClicked", "classDigikam_1_1WSToolDialog.html#a6f9919249e2e8aa237fabb2ed72e75a2", null ],
    [ "restoreDialogSize", "classDigikam_1_1WSToolDialog.html#a5da66f5eb64468e9524230138e09c2a2", null ],
    [ "saveDialogSize", "classDigikam_1_1WSToolDialog.html#a5496bc5fd80e57edd35e652dbb8361ae", null ],
    [ "setMainWidget", "classDigikam_1_1WSToolDialog.html#ae956aceb6ae24079791e2f491957d446", null ],
    [ "setPlugin", "classDigikam_1_1WSToolDialog.html#a8f9473a19990a8001d98cde6286d1059", null ],
    [ "setRejectButtonMode", "classDigikam_1_1WSToolDialog.html#a32667efc4f6a02e644886729952fd354", null ],
    [ "startButton", "classDigikam_1_1WSToolDialog.html#a34e7c6deb1458c37066862375a487600", null ],
    [ "m_buttons", "classDigikam_1_1WSToolDialog.html#a06170b9f45856493f30cb0edd368a3c0", null ]
];