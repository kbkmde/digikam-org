var dir_293a087082ed9b74abb1e6b117890064 =
[
    [ "advprintphoto.cpp", "advprintphoto_8cpp.html", null ],
    [ "advprintphoto.h", "advprintphoto_8h.html", [
      [ "AdvPrintAdditionalInfo", "classDigikamGenericPrintCreatorPlugin_1_1AdvPrintAdditionalInfo.html", "classDigikamGenericPrintCreatorPlugin_1_1AdvPrintAdditionalInfo" ],
      [ "AdvPrintCaptionInfo", "classDigikamGenericPrintCreatorPlugin_1_1AdvPrintCaptionInfo.html", "classDigikamGenericPrintCreatorPlugin_1_1AdvPrintCaptionInfo" ],
      [ "AdvPrintPhoto", "classDigikamGenericPrintCreatorPlugin_1_1AdvPrintPhoto.html", "classDigikamGenericPrintCreatorPlugin_1_1AdvPrintPhoto" ],
      [ "AdvPrintPhotoSize", "classDigikamGenericPrintCreatorPlugin_1_1AdvPrintPhotoSize.html", "classDigikamGenericPrintCreatorPlugin_1_1AdvPrintPhotoSize" ]
    ] ],
    [ "advprintsettings.cpp", "advprintsettings_8cpp.html", null ],
    [ "advprintsettings.h", "advprintsettings_8h.html", [
      [ "AdvPrintSettings", "classDigikamGenericPrintCreatorPlugin_1_1AdvPrintSettings.html", "classDigikamGenericPrintCreatorPlugin_1_1AdvPrintSettings" ]
    ] ],
    [ "advprinttask.cpp", "advprinttask_8cpp.html", null ],
    [ "advprinttask.h", "advprinttask_8h.html", [
      [ "AdvPrintTask", "classDigikamGenericPrintCreatorPlugin_1_1AdvPrintTask.html", "classDigikamGenericPrintCreatorPlugin_1_1AdvPrintTask" ]
    ] ],
    [ "advprintthread.cpp", "advprintthread_8cpp.html", null ],
    [ "advprintthread.h", "advprintthread_8h.html", [
      [ "AdvPrintThread", "classDigikamGenericPrintCreatorPlugin_1_1AdvPrintThread.html", "classDigikamGenericPrintCreatorPlugin_1_1AdvPrintThread" ]
    ] ]
];