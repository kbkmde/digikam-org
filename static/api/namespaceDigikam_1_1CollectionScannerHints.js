var namespaceDigikam_1_1CollectionScannerHints =
[
    [ "Album", "classDigikam_1_1CollectionScannerHints_1_1Album.html", "classDigikam_1_1CollectionScannerHints_1_1Album" ],
    [ "DstPath", "classDigikam_1_1CollectionScannerHints_1_1DstPath.html", "classDigikam_1_1CollectionScannerHints_1_1DstPath" ],
    [ "Item", "classDigikam_1_1CollectionScannerHints_1_1Item.html", "classDigikam_1_1CollectionScannerHints_1_1Item" ],
    [ "qHash", "namespaceDigikam_1_1CollectionScannerHints.html#a8fda9fd2e80e1d7e805e5e48a287e67e", null ],
    [ "qHash", "namespaceDigikam_1_1CollectionScannerHints.html#a9da597118d5dc02da57050ead23793aa", null ],
    [ "qHash", "namespaceDigikam_1_1CollectionScannerHints.html#a1e8ccabb6a9d0632d9322391dca50ca6", null ]
];