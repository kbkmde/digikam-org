var classDigikam_1_1SimilarityDb =
[
    [ "copySimilarityAttributes", "classDigikam_1_1SimilarityDb.html#a314b4daa5d8c88c6c8f4cd00cba25e49", null ],
    [ "getDirtyOrMissingFingerprints", "classDigikam_1_1SimilarityDb.html#a5062c10408ef0dd6bab93a3b6761b806", null ],
    [ "getDirtyOrMissingFingerprintURLs", "classDigikam_1_1SimilarityDb.html#aa27f7215fb0093088dda0565c96f1b1f", null ],
    [ "getImageSimilarity", "classDigikam_1_1SimilarityDb.html#a69f45486030596925ade9153a599bb42", null ],
    [ "getImageSimilarityAlgorithms", "classDigikam_1_1SimilarityDb.html#acc1e36f6d586bda787ff9e3a5caa2c76", null ],
    [ "getLegacySetting", "classDigikam_1_1SimilarityDb.html#a5070b94ef547ee85e759f7ed9b5ea329", null ],
    [ "getSetting", "classDigikam_1_1SimilarityDb.html#a49bafc515107735ea8484b678bba5267", null ],
    [ "hasDirtyOrMissingFingerprint", "classDigikam_1_1SimilarityDb.html#a9f349172080222bea88b3d7b50a8402b", null ],
    [ "hasFingerprint", "classDigikam_1_1SimilarityDb.html#a1961f9052c39cc6947ae9e7411fbd2ad", null ],
    [ "hasFingerprints", "classDigikam_1_1SimilarityDb.html#a8b4da887d0803f534bc320c81cd80974", null ],
    [ "hasFingerprints", "classDigikam_1_1SimilarityDb.html#a4ef3e2784d568e77e3a42eca80814b83", null ],
    [ "integrityCheck", "classDigikam_1_1SimilarityDb.html#a5faed3137e36c74a9e06ea20b2148bb7", null ],
    [ "registeredImageIds", "classDigikam_1_1SimilarityDb.html#a327d354baae487bde65dfd9ebea4f5a2", null ],
    [ "removeImageFingerprint", "classDigikam_1_1SimilarityDb.html#a239f1958488fc6e9852ba847108bc40d", null ],
    [ "removeImageSimilarity", "classDigikam_1_1SimilarityDb.html#a83a721db991ac7e3c3618ffdde298b5f", null ],
    [ "removeImageSimilarity", "classDigikam_1_1SimilarityDb.html#a0a1092afe6c2c8a44070ddf78003abc9", null ],
    [ "setImageSimilarity", "classDigikam_1_1SimilarityDb.html#ada7da8b655c99c29e0329026afb8cdf3", null ],
    [ "setSetting", "classDigikam_1_1SimilarityDb.html#a197cd0969f0bf0f25cd5206c04ac356d", null ],
    [ "vacuum", "classDigikam_1_1SimilarityDb.html#a3314453908b309807342026f62149949", null ],
    [ "SimilarityDbAccess", "classDigikam_1_1SimilarityDb.html#a7f575d99cb646c18a927072459696fc4", null ]
];