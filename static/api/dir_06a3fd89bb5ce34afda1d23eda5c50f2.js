var dir_06a3fd89bb5ce34afda1d23eda5c50f2 =
[
    [ "albumpropsedit.cpp", "albumpropsedit_8cpp.html", null ],
    [ "albumpropsedit.h", "albumpropsedit_8h.html", [
      [ "AlbumPropsEdit", "classDigikam_1_1AlbumPropsEdit.html", "classDigikam_1_1AlbumPropsEdit" ]
    ] ],
    [ "albumselectcombobox.cpp", "albumselectcombobox_8cpp.html", null ],
    [ "albumselectcombobox.h", "albumselectcombobox_8h.html", [
      [ "AbstractAlbumTreeViewSelectComboBox", "classDigikam_1_1AbstractAlbumTreeViewSelectComboBox.html", "classDigikam_1_1AbstractAlbumTreeViewSelectComboBox" ],
      [ "AlbumSelectComboBox", "classDigikam_1_1AlbumSelectComboBox.html", "classDigikam_1_1AlbumSelectComboBox" ],
      [ "AlbumTreeViewSelectComboBox", "classDigikam_1_1AlbumTreeViewSelectComboBox.html", "classDigikam_1_1AlbumTreeViewSelectComboBox" ],
      [ "TagTreeViewSelectComboBox", "classDigikam_1_1TagTreeViewSelectComboBox.html", "classDigikam_1_1TagTreeViewSelectComboBox" ]
    ] ],
    [ "albumselectdialog.cpp", "albumselectdialog_8cpp.html", null ],
    [ "albumselectdialog.h", "albumselectdialog_8h.html", [
      [ "AlbumSelectDialog", "classDigikam_1_1AlbumSelectDialog.html", "classDigikam_1_1AlbumSelectDialog" ]
    ] ],
    [ "albumselectors.cpp", "albumselectors_8cpp.html", null ],
    [ "albumselectors.h", "albumselectors_8h.html", [
      [ "AlbumSelectors", "classDigikam_1_1AlbumSelectors.html", "classDigikam_1_1AlbumSelectors" ]
    ] ],
    [ "albumselecttabs.cpp", "albumselecttabs_8cpp.html", null ],
    [ "albumselecttabs.h", "albumselecttabs_8h.html", [
      [ "AlbumSelectTabs", "classDigikam_1_1AlbumSelectTabs.html", "classDigikam_1_1AlbumSelectTabs" ]
    ] ],
    [ "albumselectwidget.cpp", "albumselectwidget_8cpp.html", null ],
    [ "albumselectwidget.h", "albumselectwidget_8h.html", [
      [ "AlbumSelectTreeView", "classDigikam_1_1AlbumSelectTreeView.html", "classDigikam_1_1AlbumSelectTreeView" ],
      [ "AlbumSelectWidget", "classDigikam_1_1AlbumSelectWidget.html", "classDigikam_1_1AlbumSelectWidget" ]
    ] ]
];