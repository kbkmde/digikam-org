var classShowFoto_1_1ShowfotoSetupMetadata =
[
    [ "MetadataTab", "classShowFoto_1_1ShowfotoSetupMetadata.html#a6422aa7bda15498d0f859ab2e2ba078c", [
      [ "Behavior", "classShowFoto_1_1ShowfotoSetupMetadata.html#a6422aa7bda15498d0f859ab2e2ba078ca64a9171459b50c2440baff2513833e78", null ],
      [ "ExifViewer", "classShowFoto_1_1ShowfotoSetupMetadata.html#a6422aa7bda15498d0f859ab2e2ba078ca3958633ea180b1be35a8530dd1b66794", null ],
      [ "MakernotesViewer", "classShowFoto_1_1ShowfotoSetupMetadata.html#a6422aa7bda15498d0f859ab2e2ba078ca26d32d6a939967670c90d65cd8d84749", null ],
      [ "IptcViewer", "classShowFoto_1_1ShowfotoSetupMetadata.html#a6422aa7bda15498d0f859ab2e2ba078ca385ff89f59e2cb0632355ab08b349ca0", null ],
      [ "XmpViewer", "classShowFoto_1_1ShowfotoSetupMetadata.html#a6422aa7bda15498d0f859ab2e2ba078ca646a2db54961e75fbfa0af11827df3af", null ],
      [ "ExifTool", "classShowFoto_1_1ShowfotoSetupMetadata.html#a6422aa7bda15498d0f859ab2e2ba078ca4deeab40f559b043b68bdb46b1a82162", null ]
    ] ],
    [ "ShowfotoSetupMetadata", "classShowFoto_1_1ShowfotoSetupMetadata.html#a7a2cc48224a328bd9e1c5b38581f52a7", null ],
    [ "~ShowfotoSetupMetadata", "classShowFoto_1_1ShowfotoSetupMetadata.html#ace97fb21251ef9feeb9b73297f360b79", null ],
    [ "applySettings", "classShowFoto_1_1ShowfotoSetupMetadata.html#a25b4560710746c9b776f0345b86b8e7e", null ],
    [ "setActiveTab", "classShowFoto_1_1ShowfotoSetupMetadata.html#acc9aee1bc2f4f2cc32c2aa4acc2b49a7", null ]
];