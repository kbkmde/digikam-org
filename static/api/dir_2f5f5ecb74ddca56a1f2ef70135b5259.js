var dir_2f5f5ecb74ddca56a1f2ef70135b5259 =
[
    [ "autocrop", "dir_a63065a13fb30a7307e5e63c46cd0482.html", "dir_a63065a13fb30a7307e5e63c46cd0482" ],
    [ "contentawareresize", "dir_de8d9ce42b3a37619dbfe0cc9d1fd6bb.html", "dir_de8d9ce42b3a37619dbfe0cc9d1fd6bb" ],
    [ "freerotation", "dir_92e6913b6f41ea64daf2b5a3e117f0c8.html", "dir_92e6913b6f41ea64daf2b5a3e117f0c8" ],
    [ "perspective", "dir_34d5a3e5a770220ca2e4025239cabe53.html", "dir_34d5a3e5a770220ca2e4025239cabe53" ],
    [ "ratiocrop", "dir_88ae4e383c8216ce59e3d5a5a7f11dba.html", "dir_88ae4e383c8216ce59e3d5a5a7f11dba" ],
    [ "resize", "dir_e476dbc05a01e6c986a2d7c21612cd1e.html", "dir_e476dbc05a01e6c986a2d7c21612cd1e" ],
    [ "shear", "dir_27cf23c531204e4af182bb58b6bf081b.html", "dir_27cf23c531204e4af182bb58b6bf081b" ]
];