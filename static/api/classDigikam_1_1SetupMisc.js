var classDigikam_1_1SetupMisc =
[
    [ "MiscTab", "classDigikam_1_1SetupMisc.html#a6a8c93ef778e64d85ea7204d524dd83b", [
      [ "Behaviour", "classDigikam_1_1SetupMisc.html#a6a8c93ef778e64d85ea7204d524dd83ba9289e201988d746092c9991fc097d56c", null ],
      [ "Appearance", "classDigikam_1_1SetupMisc.html#a6a8c93ef778e64d85ea7204d524dd83ba92f6704d4347767d245cc5c9521b7eca", null ],
      [ "Grouping", "classDigikam_1_1SetupMisc.html#a6a8c93ef778e64d85ea7204d524dd83ba7540e120f29ba348284f812b1bf7e539", null ],
      [ "System", "classDigikam_1_1SetupMisc.html#a6a8c93ef778e64d85ea7204d524dd83bad8f0b238d0325fe5d4c62396fbad1580", null ]
    ] ],
    [ "SetupMisc", "classDigikam_1_1SetupMisc.html#a42c87736238f53e75863d48a22908c6e", null ],
    [ "~SetupMisc", "classDigikam_1_1SetupMisc.html#af9e7db75f90872db0b9762e7c4d973de", null ],
    [ "applySettings", "classDigikam_1_1SetupMisc.html#aeba2fa9aad66120f2c10138382d1469a", null ],
    [ "checkSettings", "classDigikam_1_1SetupMisc.html#a8da8ec94ea6cb884502b5fbeb0751c4a", null ]
];