var classDigikam_1_1IccProfilesSettings =
[
    [ "IccProfilesSettings", "classDigikam_1_1IccProfilesSettings.html#a92c79da8b741b70e072deb2813df3f68", null ],
    [ "~IccProfilesSettings", "classDigikam_1_1IccProfilesSettings.html#ab41630bf8d7b71768b69fe45bb02f3ad", null ],
    [ "childEvent", "classDigikam_1_1IccProfilesSettings.html#a579b04e3ec4519b0ebbaafb6256cdf15", null ],
    [ "currentProfile", "classDigikam_1_1IccProfilesSettings.html#ac568000db21c38d4d7468112b475a747", null ],
    [ "defaultProfile", "classDigikam_1_1IccProfilesSettings.html#ac0723dd11f3c83e5f29bde916dffac17", null ],
    [ "minimumSizeHint", "classDigikam_1_1IccProfilesSettings.html#a25789a423f4bb2a6458949f1135a5b84", null ],
    [ "readSettings", "classDigikam_1_1IccProfilesSettings.html#a012b714f1fcfba23e200f97880eb5a19", null ],
    [ "resetToDefault", "classDigikam_1_1IccProfilesSettings.html#a94ea2ec2a300ab4e9bdaf44484decd23", null ],
    [ "setContentsMargins", "classDigikam_1_1IccProfilesSettings.html#ae1da4ee33a0f131ea0f1855813d4fb86", null ],
    [ "setContentsMargins", "classDigikam_1_1IccProfilesSettings.html#ae5fcb8ec12518ec7a9dff86dcf027e84", null ],
    [ "setCurrentProfile", "classDigikam_1_1IccProfilesSettings.html#a191a5865bfd870662136ec512da3bd20", null ],
    [ "setSpacing", "classDigikam_1_1IccProfilesSettings.html#a7c13f7e941510af04789d6097c878ee1", null ],
    [ "setStretchFactor", "classDigikam_1_1IccProfilesSettings.html#a6a79fbab0ad275840da007964ea8b5a0", null ],
    [ "signalSettingsChanged", "classDigikam_1_1IccProfilesSettings.html#adefd4cad78deeceb31f33d41528a0a7d", null ],
    [ "sizeHint", "classDigikam_1_1IccProfilesSettings.html#adfd68279bc71f4b8e91011a8ed733f96", null ],
    [ "writeSettings", "classDigikam_1_1IccProfilesSettings.html#a4b5201ba981cda5d19dad5381fcf2372", null ]
];