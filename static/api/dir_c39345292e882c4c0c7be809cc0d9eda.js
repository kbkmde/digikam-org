var dir_c39345292e882c4c0c7be809cc0d9eda =
[
    [ "timeadjustdialog.cpp", "timeadjustdialog_8cpp.html", null ],
    [ "timeadjustdialog.h", "timeadjustdialog_8h.html", [
      [ "TimeAdjustDialog", "classDigikamGenericTimeAdjustPlugin_1_1TimeAdjustDialog.html", "classDigikamGenericTimeAdjustPlugin_1_1TimeAdjustDialog" ]
    ] ],
    [ "timeadjustlist.cpp", "timeadjustlist_8cpp.html", null ],
    [ "timeadjustlist.h", "timeadjustlist_8h.html", [
      [ "TimeAdjustList", "classDigikamGenericTimeAdjustPlugin_1_1TimeAdjustList.html", "classDigikamGenericTimeAdjustPlugin_1_1TimeAdjustList" ]
    ] ],
    [ "timeadjustplugin.cpp", "generic_2metadata_2timeadjust_2timeadjustplugin_8cpp.html", null ],
    [ "timeadjustplugin.h", "generic_2metadata_2timeadjust_2timeadjustplugin_8h.html", "generic_2metadata_2timeadjust_2timeadjustplugin_8h" ],
    [ "timeadjusttask.cpp", "timeadjusttask_8cpp.html", null ],
    [ "timeadjusttask.h", "timeadjusttask_8h.html", [
      [ "TimeAdjustTask", "classDigikamGenericTimeAdjustPlugin_1_1TimeAdjustTask.html", "classDigikamGenericTimeAdjustPlugin_1_1TimeAdjustTask" ],
      [ "TimePreviewTask", "classDigikamGenericTimeAdjustPlugin_1_1TimePreviewTask.html", "classDigikamGenericTimeAdjustPlugin_1_1TimePreviewTask" ]
    ] ],
    [ "timeadjustthread.cpp", "timeadjustthread_8cpp.html", null ],
    [ "timeadjustthread.h", "timeadjustthread_8h.html", [
      [ "TimeAdjustThread", "classDigikamGenericTimeAdjustPlugin_1_1TimeAdjustThread.html", "classDigikamGenericTimeAdjustPlugin_1_1TimeAdjustThread" ]
    ] ]
];