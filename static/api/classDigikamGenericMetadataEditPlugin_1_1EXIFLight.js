var classDigikamGenericMetadataEditPlugin_1_1EXIFLight =
[
    [ "EXIFLight", "classDigikamGenericMetadataEditPlugin_1_1EXIFLight.html#ab37b5d662771efb33ac597998782bd38", null ],
    [ "~EXIFLight", "classDigikamGenericMetadataEditPlugin_1_1EXIFLight.html#aa3882feb971b589cc6d1627e11447ff2", null ],
    [ "applyMetadata", "classDigikamGenericMetadataEditPlugin_1_1EXIFLight.html#a47b0974d477b58d79e650b03aa89c436", null ],
    [ "readMetadata", "classDigikamGenericMetadataEditPlugin_1_1EXIFLight.html#a2c0616bf69ebdf19e50e0f8c83449582", null ],
    [ "signalModified", "classDigikamGenericMetadataEditPlugin_1_1EXIFLight.html#a66545a8907fb17fa1a3f598efb26587a", null ]
];