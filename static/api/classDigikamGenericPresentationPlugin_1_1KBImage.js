var classDigikamGenericPresentationPlugin_1_1KBImage =
[
    [ "KBImage", "classDigikamGenericPresentationPlugin_1_1KBImage.html#ad39adf6b24682d096be9ffb00c0455a2", null ],
    [ "~KBImage", "classDigikamGenericPresentationPlugin_1_1KBImage.html#a5e26a8dc7b889728a7dc30fd97f67b07", null ],
    [ "m_aspect", "classDigikamGenericPresentationPlugin_1_1KBImage.html#aeb2305018d979bc5810e821f1d382bee", null ],
    [ "m_opacity", "classDigikamGenericPresentationPlugin_1_1KBImage.html#a7e2246867279d07775f0d8293e24059d", null ],
    [ "m_paint", "classDigikamGenericPresentationPlugin_1_1KBImage.html#a1503fb509c7389ec95114198600d223a", null ],
    [ "m_pos", "classDigikamGenericPresentationPlugin_1_1KBImage.html#a17a5e5619153921a033953bf6f7f326a", null ],
    [ "m_texture", "classDigikamGenericPresentationPlugin_1_1KBImage.html#af39b256d42be0a772649c7ee0af40c9b", null ],
    [ "m_viewTrans", "classDigikamGenericPresentationPlugin_1_1KBImage.html#a60fb282cf393bee85659c2a9346d8405", null ]
];