var classDigikam_1_1MetadataHub =
[
    [ "Status", "classDigikam_1_1MetadataHub.html#a0637b31271ed4e18a451ab3da96cebf2", [
      [ "MetadataInvalid", "classDigikam_1_1MetadataHub.html#a0637b31271ed4e18a451ab3da96cebf2a8b32267423d2f673b8018885d8e0281a", null ],
      [ "MetadataAvailable", "classDigikam_1_1MetadataHub.html#a0637b31271ed4e18a451ab3da96cebf2a62387748102d46c8d9333d9dd5fdfcf5", null ]
    ] ],
    [ "WriteComponents", "classDigikam_1_1MetadataHub.html#ad8d82a42d5a167f16d40e0bb1c5e6ca4", [
      [ "WRITE_DATETIME", "classDigikam_1_1MetadataHub.html#ad8d82a42d5a167f16d40e0bb1c5e6ca4ab488e03d7c73819d8e268ab90bd062f9", null ],
      [ "WRITE_TITLE", "classDigikam_1_1MetadataHub.html#ad8d82a42d5a167f16d40e0bb1c5e6ca4a92e384566914f2718f39b9d5835b87f0", null ],
      [ "WRITE_COMMENTS", "classDigikam_1_1MetadataHub.html#ad8d82a42d5a167f16d40e0bb1c5e6ca4ad51f7cb1abdc3ed92fc018bc2f8dc34f", null ],
      [ "WRITE_PICKLABEL", "classDigikam_1_1MetadataHub.html#ad8d82a42d5a167f16d40e0bb1c5e6ca4ae08b631df395387b6556ffd139d5bd2d", null ],
      [ "WRITE_COLORLABEL", "classDigikam_1_1MetadataHub.html#ad8d82a42d5a167f16d40e0bb1c5e6ca4aacc3174161bb845ed1d965852ff8dfb0", null ],
      [ "WRITE_RATING", "classDigikam_1_1MetadataHub.html#ad8d82a42d5a167f16d40e0bb1c5e6ca4a4ee42d29e9d9eeb439f6e445c48acace", null ],
      [ "WRITE_TEMPLATE", "classDigikam_1_1MetadataHub.html#ad8d82a42d5a167f16d40e0bb1c5e6ca4a4d6c88a326b3ab3c8e5eae1d3268cf83", null ],
      [ "WRITE_TAGS", "classDigikam_1_1MetadataHub.html#ad8d82a42d5a167f16d40e0bb1c5e6ca4aae61540280ffcde57582c0a79d60e350", null ],
      [ "WRITE_POSITION", "classDigikam_1_1MetadataHub.html#ad8d82a42d5a167f16d40e0bb1c5e6ca4ad358f7a7716aea17df61c421559ee543", null ],
      [ "WRITE_ALL", "classDigikam_1_1MetadataHub.html#ad8d82a42d5a167f16d40e0bb1c5e6ca4adfbcdddaa8be526a86698f151897ccec", null ]
    ] ],
    [ "WriteMode", "classDigikam_1_1MetadataHub.html#a1ed2b0aff371d708f307aacd94befe1a", [
      [ "FullWrite", "classDigikam_1_1MetadataHub.html#a1ed2b0aff371d708f307aacd94befe1aa86a5f0185d6fe3379e91b39071799023", null ],
      [ "FullWriteIfChanged", "classDigikam_1_1MetadataHub.html#a1ed2b0aff371d708f307aacd94befe1aacf33e3b92917d32a9e7e3fd3b709787e", null ],
      [ "PartialWrite", "classDigikam_1_1MetadataHub.html#a1ed2b0aff371d708f307aacd94befe1aa3abe3985dcaacfaf9549d296f6311149", null ]
    ] ],
    [ "MetadataHub", "classDigikam_1_1MetadataHub.html#a95eb3a5a04b45da259d9ad18dd2eb98c", null ],
    [ "~MetadataHub", "classDigikam_1_1MetadataHub.html#a3293cefb9095e99ce90053cf1f4d9f36", null ],
    [ "applyChangeNotifications", "classDigikam_1_1MetadataHub.html#ac30d3a2e88ded3dc0207931c9a4440bb", null ],
    [ "cleanupTags", "classDigikam_1_1MetadataHub.html#a059886c782e6a75f76fd52ba5025729e", null ],
    [ "getFaceTags", "classDigikam_1_1MetadataHub.html#a697faa186724c9ed90236fbdd4256e5a", null ],
    [ "load", "classDigikam_1_1MetadataHub.html#ad9f108f820089f69ec4656c86d458101", null ],
    [ "load", "classDigikam_1_1MetadataHub.html#a4ef9a5f8e5be6527573f5476ca30b760", null ],
    [ "loadFaceTags", "classDigikam_1_1MetadataHub.html#a1632fcc38d408b772b5a61de5689ace9", null ],
    [ "loadTags", "classDigikam_1_1MetadataHub.html#ab741be6c77b12fcc3bbd10d5ebe70985", null ],
    [ "loadTags", "classDigikam_1_1MetadataHub.html#afdc5c5eceb66e94831e88fbad1f419f1", null ],
    [ "notifyTagDeleted", "classDigikam_1_1MetadataHub.html#a881e98324a98d73c0dc93d7d16de8c4e", null ],
    [ "reset", "classDigikam_1_1MetadataHub.html#a10e70072ce45c5f5fb87f2245b40b9bb", null ],
    [ "setFaceTags", "classDigikam_1_1MetadataHub.html#a36c1e5705215210a5b6601dbd9363616", null ],
    [ "willWriteMetadata", "classDigikam_1_1MetadataHub.html#a71f484e1cd18a0ad3df69d1124e190cf", null ],
    [ "write", "classDigikam_1_1MetadataHub.html#a72d7f80ce14c756b2279c0326271e2ee", null ],
    [ "write", "classDigikam_1_1MetadataHub.html#a2b8db41d24a4f5b1516b6b7dc0dd2690", null ],
    [ "write", "classDigikam_1_1MetadataHub.html#a065ebff5b645853fc608f7762e0506d7", null ],
    [ "writeTags", "classDigikam_1_1MetadataHub.html#ace9bf321f629398a1ae43df843768743", null ],
    [ "writeTags", "classDigikam_1_1MetadataHub.html#ae9160826e047b4a80121577fb4141a44", null ],
    [ "writeToBaloo", "classDigikam_1_1MetadataHub.html#a609eb666a9741212c22b6b85e641cb3b", null ],
    [ "writeToMetadata", "classDigikam_1_1MetadataHub.html#a611e19b8641f27073724202b8f1d2faf", null ]
];