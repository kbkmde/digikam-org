var classDigikamGenericFileCopyPlugin_1_1FCContainer =
[
    [ "FileCopyType", "classDigikamGenericFileCopyPlugin_1_1FCContainer.html#a7915df5ca6af62a52ccda1d0394153be", [
      [ "CopyFile", "classDigikamGenericFileCopyPlugin_1_1FCContainer.html#a7915df5ca6af62a52ccda1d0394153bea908bb48d44e15d27872bdc452189bffa", null ],
      [ "FullSymLink", "classDigikamGenericFileCopyPlugin_1_1FCContainer.html#a7915df5ca6af62a52ccda1d0394153bea7273e57f1905faa827948ba1041dc610", null ],
      [ "RelativeSymLink", "classDigikamGenericFileCopyPlugin_1_1FCContainer.html#a7915df5ca6af62a52ccda1d0394153bead6465f2bfcc2bd6971698d5e07bee7fc", null ]
    ] ],
    [ "ImageFormat", "classDigikamGenericFileCopyPlugin_1_1FCContainer.html#a5f917007e2fffbf6db1eb75e5e095ebb", [
      [ "JPEG", "classDigikamGenericFileCopyPlugin_1_1FCContainer.html#a5f917007e2fffbf6db1eb75e5e095ebba52da5013e12dcc25ee9b7079dfcbe329", null ],
      [ "PNG", "classDigikamGenericFileCopyPlugin_1_1FCContainer.html#a5f917007e2fffbf6db1eb75e5e095ebba1f470c9497ffc493872f990aa21e749e", null ]
    ] ],
    [ "FCContainer", "classDigikamGenericFileCopyPlugin_1_1FCContainer.html#af3f36aca2137e15de530bc859b3cc9f5", null ],
    [ "~FCContainer", "classDigikamGenericFileCopyPlugin_1_1FCContainer.html#a3e02657654a2ea619d8952a1f847fb89", null ],
    [ "albumPath", "classDigikamGenericFileCopyPlugin_1_1FCContainer.html#a412451db384e72c788c1b811daa20224", null ],
    [ "behavior", "classDigikamGenericFileCopyPlugin_1_1FCContainer.html#a1b9a0e8354f9a126e5d2c743b8505735", null ],
    [ "changeImageProperties", "classDigikamGenericFileCopyPlugin_1_1FCContainer.html#aa317489a36d0f7e1ef781643811da3f1", null ],
    [ "destUrl", "classDigikamGenericFileCopyPlugin_1_1FCContainer.html#ad9ceb6dcbb5fb04dd30d06b9562f673a", null ],
    [ "iface", "classDigikamGenericFileCopyPlugin_1_1FCContainer.html#ac152492f5f5be9638d5d510ee126898a", null ],
    [ "imageCompression", "classDigikamGenericFileCopyPlugin_1_1FCContainer.html#aed7e138f8aa2839f8287ffe139646355", null ],
    [ "imageFormat", "classDigikamGenericFileCopyPlugin_1_1FCContainer.html#a54d0827300ca8bf3ad0ac45e229430db", null ],
    [ "imageResize", "classDigikamGenericFileCopyPlugin_1_1FCContainer.html#a5517133c06847c68102fe396d3dac220", null ],
    [ "overwrite", "classDigikamGenericFileCopyPlugin_1_1FCContainer.html#a598dfa59316a537669577998cc408ce2", null ],
    [ "removeMetadata", "classDigikamGenericFileCopyPlugin_1_1FCContainer.html#afb88b94f83a5cc0998ab526b25de8a76", null ]
];