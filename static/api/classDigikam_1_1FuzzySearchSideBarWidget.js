var classDigikam_1_1FuzzySearchSideBarWidget =
[
    [ "StateSavingDepth", "classDigikam_1_1FuzzySearchSideBarWidget.html#abdf8ad03070bc560242e56575909d6b6", [
      [ "INSTANCE", "classDigikam_1_1FuzzySearchSideBarWidget.html#abdf8ad03070bc560242e56575909d6b6a535f22ead553f373020dce094f0ae735", null ],
      [ "DIRECT_CHILDREN", "classDigikam_1_1FuzzySearchSideBarWidget.html#abdf8ad03070bc560242e56575909d6b6a402f3f077dcfc1835196020f1986fb55", null ],
      [ "RECURSIVE", "classDigikam_1_1FuzzySearchSideBarWidget.html#abdf8ad03070bc560242e56575909d6b6a1d31a55830067a26a4c25eabb48f6d4f", null ]
    ] ],
    [ "FuzzySearchSideBarWidget", "classDigikam_1_1FuzzySearchSideBarWidget.html#afa3ea644f05af6e13f65fb95b5219123", null ],
    [ "~FuzzySearchSideBarWidget", "classDigikam_1_1FuzzySearchSideBarWidget.html#a866b624cdbedf847b38d1909f8842b90", null ],
    [ "applySettings", "classDigikam_1_1FuzzySearchSideBarWidget.html#a687bab596eb4a4b062939be3cf4d5a89", null ],
    [ "changeAlbumFromHistory", "classDigikam_1_1FuzzySearchSideBarWidget.html#a27cc217eb4eb3d1f7c328281539a86d9", null ],
    [ "doLoadState", "classDigikam_1_1FuzzySearchSideBarWidget.html#a49c1feb21963bb096cfa654d305ba78a", null ],
    [ "doSaveState", "classDigikam_1_1FuzzySearchSideBarWidget.html#a46f62a5b51cc3ba34997049d03e68d40", null ],
    [ "entryName", "classDigikam_1_1FuzzySearchSideBarWidget.html#a226ce9b9faa055120ba386b85509bcdf", null ],
    [ "getCaption", "classDigikam_1_1FuzzySearchSideBarWidget.html#a772f7721877435a499ae83a7465cb7c9", null ],
    [ "getConfigGroup", "classDigikam_1_1FuzzySearchSideBarWidget.html#a3dce4d65e29a07ea09d2e7b4746f8350", null ],
    [ "getIcon", "classDigikam_1_1FuzzySearchSideBarWidget.html#ab23aa4d70b330941a9928d3cbebe6c45", null ],
    [ "getStateSavingDepth", "classDigikam_1_1FuzzySearchSideBarWidget.html#a340090725416a1b9030c67d7d6a77d6e", null ],
    [ "loadState", "classDigikam_1_1FuzzySearchSideBarWidget.html#a884a7c5ec2c82c6b61519a6c035577c2", null ],
    [ "newDuplicatesSearch", "classDigikam_1_1FuzzySearchSideBarWidget.html#a8958826791c31b97e63c369523cdaf5d", null ],
    [ "newDuplicatesSearch", "classDigikam_1_1FuzzySearchSideBarWidget.html#a220ab49685baaaf74e45f5b44e1f7e7c", null ],
    [ "newSimilarSearch", "classDigikam_1_1FuzzySearchSideBarWidget.html#a9edecbc25155d7a4abe17bf7c369cd60", null ],
    [ "requestActiveTab", "classDigikam_1_1FuzzySearchSideBarWidget.html#a247452550423658b7ca3f5ba97fb1f07", null ],
    [ "saveState", "classDigikam_1_1FuzzySearchSideBarWidget.html#a0db6b9c2daff1b521bce63f78d8f1652", null ],
    [ "setActive", "classDigikam_1_1FuzzySearchSideBarWidget.html#a919b5a6c515065512154c96c15facf16", null ],
    [ "setConfigGroup", "classDigikam_1_1FuzzySearchSideBarWidget.html#aa37c59ac6e91ba60c3c0dd14cc6fa71e", null ],
    [ "setEntryPrefix", "classDigikam_1_1FuzzySearchSideBarWidget.html#ac289161ea3144b78c348c283cbb7777d", null ],
    [ "setStateSavingDepth", "classDigikam_1_1FuzzySearchSideBarWidget.html#a1023eaabd6d22bcfcd83b0c7f5c47e9b", null ],
    [ "signalActive", "classDigikam_1_1FuzzySearchSideBarWidget.html#a5abef0a34691568ca66e93e393338ca2", null ],
    [ "signalNotificationError", "classDigikam_1_1FuzzySearchSideBarWidget.html#a12e1b990cb7a7f652ce6f10df1ccfc11", null ]
];