var classDigikam_1_1OpenCVDNNFaceRecognizer_1_1Private =
[
    [ "ParallelRecognizer", "classDigikam_1_1OpenCVDNNFaceRecognizer_1_1Private_1_1ParallelRecognizer.html", "classDigikam_1_1OpenCVDNNFaceRecognizer_1_1Private_1_1ParallelRecognizer" ],
    [ "ParallelTrainer", "classDigikam_1_1OpenCVDNNFaceRecognizer_1_1Private_1_1ParallelTrainer.html", "classDigikam_1_1OpenCVDNNFaceRecognizer_1_1Private_1_1ParallelTrainer" ],
    [ "Private", "classDigikam_1_1OpenCVDNNFaceRecognizer_1_1Private.html#acbe8b8f14fc643fb6fdbeec18e1c46c9", null ],
    [ "~Private", "classDigikam_1_1OpenCVDNNFaceRecognizer_1_1Private.html#a8f60f821a3bafcc40e342a313133eb00", null ],
    [ "insertData", "classDigikam_1_1OpenCVDNNFaceRecognizer_1_1Private.html#adab950f6408d32f38b6405962be550be", null ],
    [ "predictDb", "classDigikam_1_1OpenCVDNNFaceRecognizer_1_1Private.html#a99200321f16e6f7136343f6a38865589", null ],
    [ "predictKDTree", "classDigikam_1_1OpenCVDNNFaceRecognizer_1_1Private.html#a1b3ab6fe4e4a3599c87362597a2762a3", null ],
    [ "predictKNN", "classDigikam_1_1OpenCVDNNFaceRecognizer_1_1Private.html#abd530d970f91000963d2799c850f04f3", null ],
    [ "predictSVM", "classDigikam_1_1OpenCVDNNFaceRecognizer_1_1Private.html#ad58a15f6efb8a572cb55381e57e7782d", null ],
    [ "trainKNN", "classDigikam_1_1OpenCVDNNFaceRecognizer_1_1Private.html#ae1404ef0ae092a9fddf6ef9669200940", null ],
    [ "trainSVM", "classDigikam_1_1OpenCVDNNFaceRecognizer_1_1Private.html#adcacf97299b94141f1280d066eb84b8b", null ],
    [ "extractors", "classDigikam_1_1OpenCVDNNFaceRecognizer_1_1Private.html#a2d8bc3f023dc55b4714bdb67bc6cc3ed", null ],
    [ "kNeighbors", "classDigikam_1_1OpenCVDNNFaceRecognizer_1_1Private.html#adb8295b8cf06268ee8bed3e3547fbf34", null ],
    [ "knn", "classDigikam_1_1OpenCVDNNFaceRecognizer_1_1Private.html#aa59e934f923b8e461a9c157a85aac9bc", null ],
    [ "method", "classDigikam_1_1OpenCVDNNFaceRecognizer_1_1Private.html#a212dd25ce89cbc729687a0653c3ddb42", null ],
    [ "newDataAdded", "classDigikam_1_1OpenCVDNNFaceRecognizer_1_1Private.html#a794e90f94a223badb8793d4dea3ce73f", null ],
    [ "svm", "classDigikam_1_1OpenCVDNNFaceRecognizer_1_1Private.html#a94d28d7ed1b03ee99918ee81fa54be96", null ],
    [ "threshold", "classDigikam_1_1OpenCVDNNFaceRecognizer_1_1Private.html#aeddccea5e4ac3831132a53a5319a60c9", null ],
    [ "tree", "classDigikam_1_1OpenCVDNNFaceRecognizer_1_1Private.html#a6a42d68d7dd9e5b8fd91be079e548fc2", null ]
];