var classDigikam_1_1ColorLabelSelector =
[
    [ "ColorLabelSelector", "classDigikam_1_1ColorLabelSelector.html#a74fe429dcd857884012f092c0d1b68e1", null ],
    [ "~ColorLabelSelector", "classDigikam_1_1ColorLabelSelector.html#a09b2e43b79e202da3d0db232f9cae16f", null ],
    [ "colorLabel", "classDigikam_1_1ColorLabelSelector.html#aad4943e2b8bb73dd5bcf232ae55ec95f", null ],
    [ "colorLabelWidget", "classDigikam_1_1ColorLabelSelector.html#a63f0a6641c3b961598006fb8155e6731", null ],
    [ "setColorLabel", "classDigikam_1_1ColorLabelSelector.html#a38f2f0211ad93fdcf6ca2bd096162607", null ],
    [ "signalColorLabelChanged", "classDigikam_1_1ColorLabelSelector.html#a9a42e73e6f6be275e33ebc384e87e8ea", null ]
];