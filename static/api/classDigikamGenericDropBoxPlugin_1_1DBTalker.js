var classDigikamGenericDropBoxPlugin_1_1DBTalker =
[
    [ "DBTalker", "classDigikamGenericDropBoxPlugin_1_1DBTalker.html#a111ad7b53f55b45dcfdd47d1f2fd629f", null ],
    [ "~DBTalker", "classDigikamGenericDropBoxPlugin_1_1DBTalker.html#a9ae5fb73c4fab9298ad9e24b3cca77ad", null ],
    [ "addPhoto", "classDigikamGenericDropBoxPlugin_1_1DBTalker.html#a060c0e64d6a11ecacad917f7ec38d4dc", null ],
    [ "authenticated", "classDigikamGenericDropBoxPlugin_1_1DBTalker.html#a5156ed404421c65ab4b4562191dbbf36", null ],
    [ "cancel", "classDigikamGenericDropBoxPlugin_1_1DBTalker.html#a32789ab9e4c2e908a79e320f79a872c0", null ],
    [ "createFolder", "classDigikamGenericDropBoxPlugin_1_1DBTalker.html#a589032b9f22d29b74dd7c37313208f85", null ],
    [ "getUserName", "classDigikamGenericDropBoxPlugin_1_1DBTalker.html#a9076a390afc3decaac35abc5da59f6c7", null ],
    [ "link", "classDigikamGenericDropBoxPlugin_1_1DBTalker.html#a69fce72379d496dca7207259c434c43d", null ],
    [ "listFolders", "classDigikamGenericDropBoxPlugin_1_1DBTalker.html#a361519b52505d7a6f15f0ffd60f5e32c", null ],
    [ "reauthenticate", "classDigikamGenericDropBoxPlugin_1_1DBTalker.html#a74ecbc1ded2ea6d3d9ca20093b922f4d", null ],
    [ "signalAddPhotoFailed", "classDigikamGenericDropBoxPlugin_1_1DBTalker.html#aed29e803ddaa78dde1d24f8705a32446", null ],
    [ "signalAddPhotoSucceeded", "classDigikamGenericDropBoxPlugin_1_1DBTalker.html#a12ab5f53aa00e3288aa1ee4825dfcb4b", null ],
    [ "signalBusy", "classDigikamGenericDropBoxPlugin_1_1DBTalker.html#a8eb8d8d4929873a0c3d3b1327f8515e7", null ],
    [ "signalCreateFolderFailed", "classDigikamGenericDropBoxPlugin_1_1DBTalker.html#a76943bda632f3687ad7ba344179c627e", null ],
    [ "signalCreateFolderSucceeded", "classDigikamGenericDropBoxPlugin_1_1DBTalker.html#ab607b9ca6fbdc126e69ad922004ed700", null ],
    [ "signalLinkingFailed", "classDigikamGenericDropBoxPlugin_1_1DBTalker.html#a0cd04527f865920d54c761155e707402", null ],
    [ "signalLinkingSucceeded", "classDigikamGenericDropBoxPlugin_1_1DBTalker.html#abe4513ea6ca1aaaf5373b80209393fd3", null ],
    [ "signalListAlbumsDone", "classDigikamGenericDropBoxPlugin_1_1DBTalker.html#a9f3358f172671604098e2fbc4c58fc2d", null ],
    [ "signalListAlbumsFailed", "classDigikamGenericDropBoxPlugin_1_1DBTalker.html#ab63fcd9d5175e8f891204f689c5b19da", null ],
    [ "signalSetUserName", "classDigikamGenericDropBoxPlugin_1_1DBTalker.html#a084687724acac2fed5359c4eeef647dc", null ],
    [ "unLink", "classDigikamGenericDropBoxPlugin_1_1DBTalker.html#a965ae843542c86ee8bfae6f41df32838", null ]
];