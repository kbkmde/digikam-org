var classDigikam_1_1DRawDecoderSettings =
[
    [ "DecodingQuality", "classDigikam_1_1DRawDecoderSettings.html#a6cdd9d3cf382d21b731ebeca49735247", [
      [ "BILINEAR", "classDigikam_1_1DRawDecoderSettings.html#a6cdd9d3cf382d21b731ebeca49735247a2c19228a33bdbcd4a2d956a1ab4b2513", null ],
      [ "VNG", "classDigikam_1_1DRawDecoderSettings.html#a6cdd9d3cf382d21b731ebeca49735247a55c49199cb8201d9fb5ba2b74e7c291d", null ],
      [ "PPG", "classDigikam_1_1DRawDecoderSettings.html#a6cdd9d3cf382d21b731ebeca49735247a71aa9e07921753520f003ffce1e0b587", null ],
      [ "AHD", "classDigikam_1_1DRawDecoderSettings.html#a6cdd9d3cf382d21b731ebeca49735247a4c6eb3c1d2237ba468b5889d2f73cb41", null ],
      [ "DCB", "classDigikam_1_1DRawDecoderSettings.html#a6cdd9d3cf382d21b731ebeca49735247a425410d5e83d093a5655c2bd6ca9e460", null ],
      [ "DHT", "classDigikam_1_1DRawDecoderSettings.html#a6cdd9d3cf382d21b731ebeca49735247a0001caaadd7d81feef8bbf343cc20f11", null ],
      [ "AAHD", "classDigikam_1_1DRawDecoderSettings.html#a6cdd9d3cf382d21b731ebeca49735247a52eb672ef126d8a152da0bca370894d6", null ]
    ] ],
    [ "InputColorSpace", "classDigikam_1_1DRawDecoderSettings.html#a65eb6c5dc7b21e2521096f8fa70d2c27", [
      [ "NOINPUTCS", "classDigikam_1_1DRawDecoderSettings.html#a65eb6c5dc7b21e2521096f8fa70d2c27abbd5b11a4c123f85f8c1563c244d1856", null ],
      [ "EMBEDDED", "classDigikam_1_1DRawDecoderSettings.html#a65eb6c5dc7b21e2521096f8fa70d2c27af212977959819bb13bdd2245fd3899cc", null ],
      [ "CUSTOMINPUTCS", "classDigikam_1_1DRawDecoderSettings.html#a65eb6c5dc7b21e2521096f8fa70d2c27ac033b5d591ba4e13c4191ee51e07e784", null ]
    ] ],
    [ "NoiseReduction", "classDigikam_1_1DRawDecoderSettings.html#a0537ecf756aaff351c53c905eeb142f9", [
      [ "NONR", "classDigikam_1_1DRawDecoderSettings.html#a0537ecf756aaff351c53c905eeb142f9a4e1865f5335ba9b2027330e5a118c4e4", null ],
      [ "WAVELETSNR", "classDigikam_1_1DRawDecoderSettings.html#a0537ecf756aaff351c53c905eeb142f9a5911932f907a0cadd68cb62facd73ee5", null ],
      [ "FBDDNR", "classDigikam_1_1DRawDecoderSettings.html#a0537ecf756aaff351c53c905eeb142f9aeb8c67522523084e6238541dfe89f1cf", null ]
    ] ],
    [ "OutputColorSpace", "classDigikam_1_1DRawDecoderSettings.html#ac0c4dde7ea4a7a9f1719e65fa4b2227a", [
      [ "RAWCOLOR", "classDigikam_1_1DRawDecoderSettings.html#ac0c4dde7ea4a7a9f1719e65fa4b2227aaa842c89c2c3c51e4c765fecce3479c7f", null ],
      [ "SRGB", "classDigikam_1_1DRawDecoderSettings.html#ac0c4dde7ea4a7a9f1719e65fa4b2227aafadd8291236b43f0c3bb35299a9c32e7", null ],
      [ "ADOBERGB", "classDigikam_1_1DRawDecoderSettings.html#ac0c4dde7ea4a7a9f1719e65fa4b2227aa88d74b8b5808133fbc6eca99a3c8d584", null ],
      [ "WIDEGAMMUT", "classDigikam_1_1DRawDecoderSettings.html#ac0c4dde7ea4a7a9f1719e65fa4b2227aad2ac3f3e1d3b2609a60ea981eb68ae80", null ],
      [ "PROPHOTO", "classDigikam_1_1DRawDecoderSettings.html#ac0c4dde7ea4a7a9f1719e65fa4b2227aa830c66e50ec442c9f4a2a19efd7e50cc", null ],
      [ "CUSTOMOUTPUTCS", "classDigikam_1_1DRawDecoderSettings.html#ac0c4dde7ea4a7a9f1719e65fa4b2227aacba9e6eba7bcf1c70a99ab374a62c182", null ]
    ] ],
    [ "WhiteBalance", "classDigikam_1_1DRawDecoderSettings.html#a32cfa670b1d52d86e60f2ed61ac41309", [
      [ "NONE", "classDigikam_1_1DRawDecoderSettings.html#a32cfa670b1d52d86e60f2ed61ac41309a137b133cfa55a5718ef68aeee82f467d", null ],
      [ "CAMERA", "classDigikam_1_1DRawDecoderSettings.html#a32cfa670b1d52d86e60f2ed61ac41309a794a945c76a10fcd2306356f4cb930e6", null ],
      [ "AUTO", "classDigikam_1_1DRawDecoderSettings.html#a32cfa670b1d52d86e60f2ed61ac41309ae469d7540e56d92e2330f8acae0eaaf6", null ],
      [ "CUSTOM", "classDigikam_1_1DRawDecoderSettings.html#a32cfa670b1d52d86e60f2ed61ac41309ac7726e7be85f75debe4a091c480b29eb", null ],
      [ "AERA", "classDigikam_1_1DRawDecoderSettings.html#a32cfa670b1d52d86e60f2ed61ac41309a1b16f4ab59b7ecee9b9e432c8dbaba80", null ]
    ] ],
    [ "DRawDecoderSettings", "classDigikam_1_1DRawDecoderSettings.html#a406ea5ca8931e672d88ccced8a5c7713", null ],
    [ "DRawDecoderSettings", "classDigikam_1_1DRawDecoderSettings.html#ad9f28d1f36707e8ad16120ed1109dec7", null ],
    [ "~DRawDecoderSettings", "classDigikam_1_1DRawDecoderSettings.html#a174ff48a1105614b28917f25afb18761", null ],
    [ "operator=", "classDigikam_1_1DRawDecoderSettings.html#a3209ad91e1025961d24c44fa2c2aa047", null ],
    [ "operator==", "classDigikam_1_1DRawDecoderSettings.html#ae6c14357d3104c1811c541e54544d4f4", null ],
    [ "optimizeTimeLoading", "classDigikam_1_1DRawDecoderSettings.html#af6b6c4184f1aba03d786929f7e20e0af", null ],
    [ "autoBrightness", "classDigikam_1_1DRawDecoderSettings.html#af3fc59610a1956cf491375c784735848", null ],
    [ "blackPoint", "classDigikam_1_1DRawDecoderSettings.html#a796b6490f7346173de53d46647049558", null ],
    [ "brightness", "classDigikam_1_1DRawDecoderSettings.html#a5ad5936ec685c756a8e7bca4b2b26802", null ],
    [ "customWhiteBalance", "classDigikam_1_1DRawDecoderSettings.html#a6a240e51a9019abe4b97f44ee919bdc7", null ],
    [ "customWhiteBalanceGreen", "classDigikam_1_1DRawDecoderSettings.html#a150a39447583ca9e4a7a07cecf7f125d", null ],
    [ "dcbEnhanceFl", "classDigikam_1_1DRawDecoderSettings.html#a0b089a0fa78ccea8355a4277a4c745f9", null ],
    [ "dcbIterations", "classDigikam_1_1DRawDecoderSettings.html#a3e96c0a0edc84480b62f5c6ef5479645", null ],
    [ "deadPixelMap", "classDigikam_1_1DRawDecoderSettings.html#aee5035d4e90a072b1e7a38939bfb014b", null ],
    [ "DontStretchPixels", "classDigikam_1_1DRawDecoderSettings.html#a67b2c1c1a5fcf765768e81ae5dbed34b", null ],
    [ "enableBlackPoint", "classDigikam_1_1DRawDecoderSettings.html#a63c1df4e5110ae6d1aea1700ac69fd3b", null ],
    [ "enableWhitePoint", "classDigikam_1_1DRawDecoderSettings.html#a80729fdff79cbacbcf980e590261dea3", null ],
    [ "expoCorrection", "classDigikam_1_1DRawDecoderSettings.html#a4b041e77c9c3a43bf5d147cc72a926cc", null ],
    [ "expoCorrectionHighlight", "classDigikam_1_1DRawDecoderSettings.html#afe8fd55a289e561142cfbe33e40df098", null ],
    [ "expoCorrectionShift", "classDigikam_1_1DRawDecoderSettings.html#aebf468c00312ce1f83f952aa927fee20", null ],
    [ "fixColorsHighlights", "classDigikam_1_1DRawDecoderSettings.html#ad4697d2e601826f7a7a8b5fe4b63344c", null ],
    [ "halfSizeColorImage", "classDigikam_1_1DRawDecoderSettings.html#ac038f3743e648c69f8ef4d131482614f", null ],
    [ "inputColorSpace", "classDigikam_1_1DRawDecoderSettings.html#a8ffb0f6317f96756c07a1dcd9e90ec2c", null ],
    [ "inputProfile", "classDigikam_1_1DRawDecoderSettings.html#aa49c69c41bd8ced0aace8370343006fc", null ],
    [ "medianFilterPasses", "classDigikam_1_1DRawDecoderSettings.html#a3beb5f989c2934d598e3a28eac74f88e", null ],
    [ "NRThreshold", "classDigikam_1_1DRawDecoderSettings.html#a47563dd2da41dcb3a3444af688a382ce", null ],
    [ "NRType", "classDigikam_1_1DRawDecoderSettings.html#a924b6570dacc1638e472d3daa6af6aa9", null ],
    [ "outputColorSpace", "classDigikam_1_1DRawDecoderSettings.html#a54fdde09e57e14eb7cbe6f26cd85841e", null ],
    [ "outputProfile", "classDigikam_1_1DRawDecoderSettings.html#a644bfc5ea5802400d5955fe0676fbafd", null ],
    [ "RAWQuality", "classDigikam_1_1DRawDecoderSettings.html#a15942aa12b196b90ecf0e8b403e94c18", null ],
    [ "RGBInterpolate4Colors", "classDigikam_1_1DRawDecoderSettings.html#a8815eca533953ec35875c3934e0d194e", null ],
    [ "sixteenBitsImage", "classDigikam_1_1DRawDecoderSettings.html#afc166dcf88b1e6eb78d839e195946968", null ],
    [ "unclipColors", "classDigikam_1_1DRawDecoderSettings.html#a760222321cb1cf841b3002afb3331f4e", null ],
    [ "whiteBalance", "classDigikam_1_1DRawDecoderSettings.html#a22a64181fbccc4c8e47c86a3d814be07", null ],
    [ "whiteBalanceArea", "classDigikam_1_1DRawDecoderSettings.html#a2a178c0c420d99f3cb9cbf995b6fa01a", null ],
    [ "whitePoint", "classDigikam_1_1DRawDecoderSettings.html#a84ce9952d9c5a7d3a6168c9ae51aee1e", null ]
];