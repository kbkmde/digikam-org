var classDigikamGenericPresentationPlugin_1_1KBImageLoader =
[
    [ "KBImageLoader", "classDigikamGenericPresentationPlugin_1_1KBImageLoader.html#a241506d39d0e6ac5faf9b44d8f260fa5", null ],
    [ "~KBImageLoader", "classDigikamGenericPresentationPlugin_1_1KBImageLoader.html#a5e19417d6476be81399e90b43b9f97e1", null ],
    [ "grabImage", "classDigikamGenericPresentationPlugin_1_1KBImageLoader.html#a84f96fc41d02162afb61054a518f6c8f", null ],
    [ "image", "classDigikamGenericPresentationPlugin_1_1KBImageLoader.html#a8a20d4948f57bdce80d820cd3c6eb152", null ],
    [ "imageAspect", "classDigikamGenericPresentationPlugin_1_1KBImageLoader.html#aef88c4ada46fd4152f5f99c8d7f953f1", null ],
    [ "invalidateCurrentImageName", "classDigikamGenericPresentationPlugin_1_1KBImageLoader.html#a65f2fe949dea673200c13bb572fbf17e", null ],
    [ "loadImage", "classDigikamGenericPresentationPlugin_1_1KBImageLoader.html#a537f2435d17ad83818613d8c5ef884b7", null ],
    [ "quit", "classDigikamGenericPresentationPlugin_1_1KBImageLoader.html#ab75cadf767b5c5f6d79619fa533d5e51", null ],
    [ "ready", "classDigikamGenericPresentationPlugin_1_1KBImageLoader.html#af5b4bcdaf1968ca9c510641d9e1ea7ce", null ],
    [ "requestNewImage", "classDigikamGenericPresentationPlugin_1_1KBImageLoader.html#afdbfc1643bd91c929c481aa920fb0431", null ],
    [ "run", "classDigikamGenericPresentationPlugin_1_1KBImageLoader.html#a7ea66bdb58beae91c8fc8c0e201e67bf", null ],
    [ "ungrabImage", "classDigikamGenericPresentationPlugin_1_1KBImageLoader.html#ac9c1a5567e66c755a8ac35b700ba50e5", null ]
];