var classDigikam_1_1KMemoryInfo =
[
    [ "MemoryDetail", "classDigikam_1_1KMemoryInfo.html#a976962a85db63b700bac864e225a8d5a", [
      [ "TotalRam", "classDigikam_1_1KMemoryInfo.html#a976962a85db63b700bac864e225a8d5aa7e777c93e6b793ab7efa841c677028ff", null ],
      [ "AvailableRam", "classDigikam_1_1KMemoryInfo.html#a976962a85db63b700bac864e225a8d5aa9cba413fc1d6ab1c2f4b3cb3198440d5", null ],
      [ "TotalSwap", "classDigikam_1_1KMemoryInfo.html#a976962a85db63b700bac864e225a8d5aa02eeb2d217af2837c7bd67d0c08b51a1", null ],
      [ "AvailableSwap", "classDigikam_1_1KMemoryInfo.html#a976962a85db63b700bac864e225a8d5aad99a6481a55a06c6516ba5b6e66bbd2b", null ],
      [ "AvailableMemory", "classDigikam_1_1KMemoryInfo.html#a976962a85db63b700bac864e225a8d5aaee372793dc853a103893a05f6aaa8d55", null ]
    ] ],
    [ "KMemoryInfo", "classDigikam_1_1KMemoryInfo.html#a5636c9b4fed23611c6a3cdc4d934c496", null ],
    [ "KMemoryInfo", "classDigikam_1_1KMemoryInfo.html#a61b18c1cf804471fc12bbb48a264c11c", null ],
    [ "~KMemoryInfo", "classDigikam_1_1KMemoryInfo.html#a740cf56a0febf9c4f1892a2e1a7d569d", null ],
    [ "bytes", "classDigikam_1_1KMemoryInfo.html#a0378e574068649450c23b2ca57fa2ac0", null ],
    [ "isValid", "classDigikam_1_1KMemoryInfo.html#a42d55ced10e798e281931898fbd4e32a", null ],
    [ "kilobytes", "classDigikam_1_1KMemoryInfo.html#a5af6e15b092a3b3b6403f4a46bbe6ecd", null ],
    [ "lastUpdate", "classDigikam_1_1KMemoryInfo.html#a0fb89ab6ed537fc850ff7701b3f45fc5", null ],
    [ "megabytes", "classDigikam_1_1KMemoryInfo.html#a168b084dab0331eabf67fd336a652800", null ],
    [ "operator=", "classDigikam_1_1KMemoryInfo.html#a3bb1e3ebbb1c3e1b5461fc3a42d43cd0", null ],
    [ "update", "classDigikam_1_1KMemoryInfo.html#a88f05e10d81d71b71b65bfe8d6b23fd0", null ]
];