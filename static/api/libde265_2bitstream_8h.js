var libde265_2bitstream_8h =
[
    [ "bitreader", "structbitreader.html", "structbitreader" ],
    [ "MAX_UVLC_LEADING_ZEROS", "libde265_2bitstream_8h.html#a4eed465257f470f6974dc65a386022e0", null ],
    [ "UVLC_ERROR", "libde265_2bitstream_8h.html#ac06283b42020ce61991522d41f94f801", null ],
    [ "bitreader_init", "libde265_2bitstream_8h.html#a75f9522ab30d6b357db855b26bcfe081", null ],
    [ "bitreader_refill", "libde265_2bitstream_8h.html#a1533026fa97b9b461e1a5e078c21dc9b", null ],
    [ "check_rbsp_trailing_bits", "libde265_2bitstream_8h.html#a9853adfed4f2b2a35c178f3086e4f0ad", null ],
    [ "get_bits", "libde265_2bitstream_8h.html#a88d3020d961ba89f620c15fee374db03", null ],
    [ "get_bits_fast", "libde265_2bitstream_8h.html#acbdd29a97b6f4cd99674f6dca9569497", null ],
    [ "get_svlc", "libde265_2bitstream_8h.html#ae2e4595ee484b8ec5f030da13d0cd03b", null ],
    [ "get_uvlc", "libde265_2bitstream_8h.html#a5b4b438652688efef53a18fd5463b34c", null ],
    [ "next_bit", "libde265_2bitstream_8h.html#aa9cf1e72c29f1c2e93f387f41a26e789", null ],
    [ "next_bit_norefill", "libde265_2bitstream_8h.html#ab7e859321fa4d0368d3fb566a8623075", null ],
    [ "peek_bits", "libde265_2bitstream_8h.html#a87a7872536d05c23e0aa316965acd148", null ],
    [ "prepare_for_CABAC", "libde265_2bitstream_8h.html#acc85457e429b4a43e296c727468ffb71", null ],
    [ "skip_bits", "libde265_2bitstream_8h.html#a4a559b3ffbdef51e366c05216838fbec", null ],
    [ "skip_bits_fast", "libde265_2bitstream_8h.html#a55adf0870bca8c38bf2b04c7d1a2d4fb", null ],
    [ "skip_to_byte_boundary", "libde265_2bitstream_8h.html#aaa05d3de265ba8cf732effb31f7d2e68", null ]
];