var classDigikamGenericSlideShowPlugin_1_1SetupSlideShowDialog =
[
    [ "SetupSlideShowDialog", "classDigikamGenericSlideShowPlugin_1_1SetupSlideShowDialog.html#ace436581d4c50ff22f95e3ef73380f77", null ],
    [ "~SetupSlideShowDialog", "classDigikamGenericSlideShowPlugin_1_1SetupSlideShowDialog.html#a251754002f0a9f65c3e9c8406dd7f4e7", null ],
    [ "restoreDialogSize", "classDigikamGenericSlideShowPlugin_1_1SetupSlideShowDialog.html#a5da66f5eb64468e9524230138e09c2a2", null ],
    [ "saveDialogSize", "classDigikamGenericSlideShowPlugin_1_1SetupSlideShowDialog.html#a5496bc5fd80e57edd35e652dbb8361ae", null ],
    [ "setPlugin", "classDigikamGenericSlideShowPlugin_1_1SetupSlideShowDialog.html#a8f9473a19990a8001d98cde6286d1059", null ],
    [ "slotApplySettings", "classDigikamGenericSlideShowPlugin_1_1SetupSlideShowDialog.html#a90a3371bc223d476854408b96a7b0e7a", null ],
    [ "slotSetUnchecked", "classDigikamGenericSlideShowPlugin_1_1SetupSlideShowDialog.html#a0e34b6ed063fd982e296f92091afed20", null ],
    [ "m_buttons", "classDigikamGenericSlideShowPlugin_1_1SetupSlideShowDialog.html#a06170b9f45856493f30cb0edd368a3c0", null ]
];