var itemdelegateoverlay_8h =
[
    [ "AbstractWidgetDelegateOverlay", "classDigikam_1_1AbstractWidgetDelegateOverlay.html", "classDigikam_1_1AbstractWidgetDelegateOverlay" ],
    [ "HoverButtonDelegateOverlay", "classDigikam_1_1HoverButtonDelegateOverlay.html", "classDigikam_1_1HoverButtonDelegateOverlay" ],
    [ "ItemDelegateOverlay", "classDigikam_1_1ItemDelegateOverlay.html", "classDigikam_1_1ItemDelegateOverlay" ],
    [ "ItemDelegateOverlayContainer", "classDigikam_1_1ItemDelegateOverlayContainer.html", "classDigikam_1_1ItemDelegateOverlayContainer" ],
    [ "PersistentWidgetDelegateOverlay", "classDigikam_1_1PersistentWidgetDelegateOverlay.html", "classDigikam_1_1PersistentWidgetDelegateOverlay" ],
    [ "REQUIRE_DELEGATE", "itemdelegateoverlay_8h.html#aed8d79308e627d1bacd5b2a6ddbe15db", null ]
];