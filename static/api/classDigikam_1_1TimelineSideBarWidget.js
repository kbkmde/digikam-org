var classDigikam_1_1TimelineSideBarWidget =
[
    [ "StateSavingDepth", "classDigikam_1_1TimelineSideBarWidget.html#abdf8ad03070bc560242e56575909d6b6", [
      [ "INSTANCE", "classDigikam_1_1TimelineSideBarWidget.html#abdf8ad03070bc560242e56575909d6b6a535f22ead553f373020dce094f0ae735", null ],
      [ "DIRECT_CHILDREN", "classDigikam_1_1TimelineSideBarWidget.html#abdf8ad03070bc560242e56575909d6b6a402f3f077dcfc1835196020f1986fb55", null ],
      [ "RECURSIVE", "classDigikam_1_1TimelineSideBarWidget.html#abdf8ad03070bc560242e56575909d6b6a1d31a55830067a26a4c25eabb48f6d4f", null ]
    ] ],
    [ "TimelineSideBarWidget", "classDigikam_1_1TimelineSideBarWidget.html#a106cbdd27bfc3f405407e34e739ea014", null ],
    [ "~TimelineSideBarWidget", "classDigikam_1_1TimelineSideBarWidget.html#a8c21087fbe9941d52c3ed31a745d0043", null ],
    [ "applySettings", "classDigikam_1_1TimelineSideBarWidget.html#a4fa012e5f0170c7ab41a3daa1fc545be", null ],
    [ "changeAlbumFromHistory", "classDigikam_1_1TimelineSideBarWidget.html#ab005bb281d1a45ffafa28dafcd1e9028", null ],
    [ "doLoadState", "classDigikam_1_1TimelineSideBarWidget.html#a47af99f20f9a579d217225be08b10ee0", null ],
    [ "doSaveState", "classDigikam_1_1TimelineSideBarWidget.html#a5539be53a0a069cd698e881f66aa4746", null ],
    [ "entryName", "classDigikam_1_1TimelineSideBarWidget.html#a226ce9b9faa055120ba386b85509bcdf", null ],
    [ "getCaption", "classDigikam_1_1TimelineSideBarWidget.html#a1c329b09db2416051d39e5d6e3d59dfb", null ],
    [ "getConfigGroup", "classDigikam_1_1TimelineSideBarWidget.html#a3dce4d65e29a07ea09d2e7b4746f8350", null ],
    [ "getIcon", "classDigikam_1_1TimelineSideBarWidget.html#a1dfbdf72186bffce9027d63697d4389e", null ],
    [ "getStateSavingDepth", "classDigikam_1_1TimelineSideBarWidget.html#a340090725416a1b9030c67d7d6a77d6e", null ],
    [ "loadState", "classDigikam_1_1TimelineSideBarWidget.html#a884a7c5ec2c82c6b61519a6c035577c2", null ],
    [ "requestActiveTab", "classDigikam_1_1TimelineSideBarWidget.html#a247452550423658b7ca3f5ba97fb1f07", null ],
    [ "saveState", "classDigikam_1_1TimelineSideBarWidget.html#a0db6b9c2daff1b521bce63f78d8f1652", null ],
    [ "setActive", "classDigikam_1_1TimelineSideBarWidget.html#adfa8087cfdade78f873c83020f90bf98", null ],
    [ "setConfigGroup", "classDigikam_1_1TimelineSideBarWidget.html#aa37c59ac6e91ba60c3c0dd14cc6fa71e", null ],
    [ "setEntryPrefix", "classDigikam_1_1TimelineSideBarWidget.html#ac289161ea3144b78c348c283cbb7777d", null ],
    [ "setStateSavingDepth", "classDigikam_1_1TimelineSideBarWidget.html#a1023eaabd6d22bcfcd83b0c7f5c47e9b", null ],
    [ "signalNotificationError", "classDigikam_1_1TimelineSideBarWidget.html#a12e1b990cb7a7f652ce6f10df1ccfc11", null ]
];