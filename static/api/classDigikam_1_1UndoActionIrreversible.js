var classDigikam_1_1UndoActionIrreversible =
[
    [ "UndoActionIrreversible", "classDigikam_1_1UndoActionIrreversible.html#a6ba3039d67f5de5c7c5ede24bcb718cc", null ],
    [ "~UndoActionIrreversible", "classDigikam_1_1UndoActionIrreversible.html#a4f005fbf1dbdcd35e1d28cc6ae6343e2", null ],
    [ "fileOriginData", "classDigikam_1_1UndoActionIrreversible.html#a832a7070cde0b021738d3d59b497324b", null ],
    [ "fileOriginResolvedHistory", "classDigikam_1_1UndoActionIrreversible.html#af3932452421bbf2532063d836e1a412f", null ],
    [ "getMetadata", "classDigikam_1_1UndoActionIrreversible.html#adbfd2809dfe36bd4c09246f02f9ad0e9", null ],
    [ "getTitle", "classDigikam_1_1UndoActionIrreversible.html#a68fcd30401a224212d7d6cbd97be72f0", null ],
    [ "hasFileOriginData", "classDigikam_1_1UndoActionIrreversible.html#ae7699bd585c82d1beb91d85a34b42e17", null ],
    [ "setFileOriginData", "classDigikam_1_1UndoActionIrreversible.html#ab6dac017863266442c9d910f18ad5bab", null ],
    [ "setMetadata", "classDigikam_1_1UndoActionIrreversible.html#a2d9079762b333054ab52818a1b66fca1", null ],
    [ "setTitle", "classDigikam_1_1UndoActionIrreversible.html#a099f5350e231a4196a2198d22833d565", null ]
];