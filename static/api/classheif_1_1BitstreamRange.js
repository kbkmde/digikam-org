var classheif_1_1BitstreamRange =
[
    [ "BitstreamRange", "classheif_1_1BitstreamRange.html#a789f53825009b8afa9e77066c08e281f", null ],
    [ "eof", "classheif_1_1BitstreamRange.html#a02bd33a8122d1f0a923dcce5c980d4f5", null ],
    [ "error", "classheif_1_1BitstreamRange.html#ae50e3ab58efbf3313c8e5a00b14d8061", null ],
    [ "get_error", "classheif_1_1BitstreamRange.html#a8b407c6e72c194567028b7aa36e7e19a", null ],
    [ "get_istream", "classheif_1_1BitstreamRange.html#a6c9aad96710ac866c91174f9bc8d82a5", null ],
    [ "get_nesting_level", "classheif_1_1BitstreamRange.html#a2af541aa8caf71e9a00ee04f2b07605e", null ],
    [ "get_remaining_bytes", "classheif_1_1BitstreamRange.html#a518d6c0d293f0a645a353c86d34ec682", null ],
    [ "prepare_read", "classheif_1_1BitstreamRange.html#aba9e255a786db8405bf65bfeaf154bd4", null ],
    [ "read16", "classheif_1_1BitstreamRange.html#a8bbe399a318902db57a0f33fabf4ae3b", null ],
    [ "read32", "classheif_1_1BitstreamRange.html#ac7c4bbbcd5f57347534e792d6f59d2c9", null ],
    [ "read8", "classheif_1_1BitstreamRange.html#a36695c5819ff93cb166357a0d1359216", null ],
    [ "read_string", "classheif_1_1BitstreamRange.html#ad0ce8c1987c1005f3d3124da5f19bd19", null ],
    [ "set_eof_while_reading", "classheif_1_1BitstreamRange.html#a71be4b22f0611ad40a4d0bb47a14182f", null ],
    [ "skip_to_end_of_box", "classheif_1_1BitstreamRange.html#a1b62eacc91a7ea73b11effab46974ff4", null ],
    [ "skip_to_end_of_file", "classheif_1_1BitstreamRange.html#a307456f8c47381cae73c5b2edf9f6151", null ],
    [ "wait_for_available_bytes", "classheif_1_1BitstreamRange.html#a2ffb2fae4b1890a8b3f8eafece7abe53", null ],
    [ "wait_until_range_is_available", "classheif_1_1BitstreamRange.html#a7be5ee52340c2e2817b8434db12c899e", null ]
];