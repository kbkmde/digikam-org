var dir_d4728ad618f7bd96939b78bb6e571e8f =
[
    [ "tableview.cpp", "tableview_8cpp.html", null ],
    [ "tableview.h", "tableview_8h.html", [
      [ "TableView", "classDigikam_1_1TableView.html", "classDigikam_1_1TableView" ]
    ] ],
    [ "tableview_column_audiovideo.cpp", "tableview__column__audiovideo_8cpp.html", null ],
    [ "tableview_column_audiovideo.h", "tableview__column__audiovideo_8h.html", [
      [ "ColumnAudioVideoProperties", "classDigikam_1_1TableViewColumns_1_1ColumnAudioVideoProperties.html", "classDigikam_1_1TableViewColumns_1_1ColumnAudioVideoProperties" ]
    ] ],
    [ "tableview_column_configuration_dialog.cpp", "tableview__column__configuration__dialog_8cpp.html", null ],
    [ "tableview_column_configuration_dialog.h", "tableview__column__configuration__dialog_8h.html", [
      [ "TableViewConfigurationDialog", "classDigikam_1_1TableViewConfigurationDialog.html", "classDigikam_1_1TableViewConfigurationDialog" ]
    ] ],
    [ "tableview_column_digikam.cpp", "tableview__column__digikam_8cpp.html", null ],
    [ "tableview_column_digikam.h", "tableview__column__digikam_8h.html", [
      [ "ColumnDigikamProperties", "classDigikam_1_1TableViewColumns_1_1ColumnDigikamProperties.html", "classDigikam_1_1TableViewColumns_1_1ColumnDigikamProperties" ]
    ] ],
    [ "tableview_column_file.cpp", "tableview__column__file_8cpp.html", null ],
    [ "tableview_column_file.h", "tableview__column__file_8h.html", [
      [ "ColumnFileConfigurationWidget", "classDigikam_1_1TableViewColumns_1_1ColumnFileConfigurationWidget.html", "classDigikam_1_1TableViewColumns_1_1ColumnFileConfigurationWidget" ],
      [ "ColumnFileProperties", "classDigikam_1_1TableViewColumns_1_1ColumnFileProperties.html", "classDigikam_1_1TableViewColumns_1_1ColumnFileProperties" ]
    ] ],
    [ "tableview_column_geo.cpp", "tableview__column__geo_8cpp.html", null ],
    [ "tableview_column_geo.h", "tableview__column__geo_8h.html", [
      [ "ColumnGeoConfigurationWidget", "classDigikam_1_1TableViewColumns_1_1ColumnGeoConfigurationWidget.html", "classDigikam_1_1TableViewColumns_1_1ColumnGeoConfigurationWidget" ],
      [ "ColumnGeoProperties", "classDigikam_1_1TableViewColumns_1_1ColumnGeoProperties.html", "classDigikam_1_1TableViewColumns_1_1ColumnGeoProperties" ]
    ] ],
    [ "tableview_column_item.cpp", "tableview__column__item_8cpp.html", null ],
    [ "tableview_column_item.h", "tableview__column__item_8h.html", [
      [ "ColumnItemProperties", "classDigikam_1_1TableViewColumns_1_1ColumnItemProperties.html", "classDigikam_1_1TableViewColumns_1_1ColumnItemProperties" ]
    ] ],
    [ "tableview_column_photo.cpp", "tableview__column__photo_8cpp.html", null ],
    [ "tableview_column_photo.h", "tableview__column__photo_8h.html", [
      [ "ColumnPhotoConfigurationWidget", "classDigikam_1_1TableViewColumns_1_1ColumnPhotoConfigurationWidget.html", "classDigikam_1_1TableViewColumns_1_1ColumnPhotoConfigurationWidget" ],
      [ "ColumnPhotoProperties", "classDigikam_1_1TableViewColumns_1_1ColumnPhotoProperties.html", "classDigikam_1_1TableViewColumns_1_1ColumnPhotoProperties" ]
    ] ],
    [ "tableview_column_thumbnail.cpp", "tableview__column__thumbnail_8cpp.html", null ],
    [ "tableview_column_thumbnail.h", "tableview__column__thumbnail_8h.html", [
      [ "ColumnThumbnail", "classDigikam_1_1TableViewColumns_1_1ColumnThumbnail.html", "classDigikam_1_1TableViewColumns_1_1ColumnThumbnail" ]
    ] ],
    [ "tableview_columnfactory.cpp", "tableview__columnfactory_8cpp.html", null ],
    [ "tableview_columnfactory.h", "tableview__columnfactory_8h.html", [
      [ "TableViewColumn", "classDigikam_1_1TableViewColumn.html", "classDigikam_1_1TableViewColumn" ],
      [ "TableViewColumnConfiguration", "classDigikam_1_1TableViewColumnConfiguration.html", "classDigikam_1_1TableViewColumnConfiguration" ],
      [ "TableViewColumnConfigurationWidget", "classDigikam_1_1TableViewColumnConfigurationWidget.html", "classDigikam_1_1TableViewColumnConfigurationWidget" ],
      [ "TableViewColumnDescription", "classDigikam_1_1TableViewColumnDescription.html", "classDigikam_1_1TableViewColumnDescription" ],
      [ "TableViewColumnFactory", "classDigikam_1_1TableViewColumnFactory.html", "classDigikam_1_1TableViewColumnFactory" ],
      [ "TableViewColumnProfile", "classDigikam_1_1TableViewColumnProfile.html", "classDigikam_1_1TableViewColumnProfile" ]
    ] ],
    [ "tableview_columns.h", "tableview__columns_8h.html", null ],
    [ "tableview_model.cpp", "tableview__model_8cpp.html", "tableview__model_8cpp" ],
    [ "tableview_model.h", "tableview__model_8h.html", [
      [ "TableViewModel", "classDigikam_1_1TableViewModel.html", "classDigikam_1_1TableViewModel" ],
      [ "Item", "classDigikam_1_1TableViewModel_1_1Item.html", "classDigikam_1_1TableViewModel_1_1Item" ]
    ] ],
    [ "tableview_selection_model_syncer.cpp", "tableview__selection__model__syncer_8cpp.html", null ],
    [ "tableview_selection_model_syncer.h", "tableview__selection__model__syncer_8h.html", [
      [ "TableViewSelectionModelSyncer", "classDigikam_1_1TableViewSelectionModelSyncer.html", "classDigikam_1_1TableViewSelectionModelSyncer" ]
    ] ],
    [ "tableview_shared.cpp", "tableview__shared_8cpp.html", null ],
    [ "tableview_shared.h", "tableview__shared_8h.html", [
      [ "TableViewShared", "classDigikam_1_1TableViewShared.html", "classDigikam_1_1TableViewShared" ]
    ] ],
    [ "tableview_treeview.cpp", "tableview__treeview_8cpp.html", null ],
    [ "tableview_treeview.h", "tableview__treeview_8h.html", [
      [ "TableViewTreeView", "classDigikam_1_1TableViewTreeView.html", "classDigikam_1_1TableViewTreeView" ]
    ] ],
    [ "tableview_treeview_delegate.cpp", "tableview__treeview__delegate_8cpp.html", null ],
    [ "tableview_treeview_delegate.h", "tableview__treeview__delegate_8h.html", [
      [ "TableViewItemDelegate", "classDigikam_1_1TableViewItemDelegate.html", "classDigikam_1_1TableViewItemDelegate" ]
    ] ]
];