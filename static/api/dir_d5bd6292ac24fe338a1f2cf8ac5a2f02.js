var dir_d5bd6292ac24fe338a1f2cf8ac5a2f02 =
[
    [ "boxitem.h", "boxitem_8h.html", [
      [ "BOXFolder", "classDigikamGenericBoxPlugin_1_1BOXFolder.html", "classDigikamGenericBoxPlugin_1_1BOXFolder" ],
      [ "BOXPhoto", "classDigikamGenericBoxPlugin_1_1BOXPhoto.html", "classDigikamGenericBoxPlugin_1_1BOXPhoto" ]
    ] ],
    [ "boxnewalbumdlg.cpp", "boxnewalbumdlg_8cpp.html", null ],
    [ "boxnewalbumdlg.h", "boxnewalbumdlg_8h.html", [
      [ "BOXNewAlbumDlg", "classDigikamGenericBoxPlugin_1_1BOXNewAlbumDlg.html", "classDigikamGenericBoxPlugin_1_1BOXNewAlbumDlg" ]
    ] ],
    [ "boxplugin.cpp", "boxplugin_8cpp.html", null ],
    [ "boxplugin.h", "boxplugin_8h.html", "boxplugin_8h" ],
    [ "boxtalker.cpp", "boxtalker_8cpp.html", null ],
    [ "boxtalker.h", "boxtalker_8h.html", [
      [ "BOXTalker", "classDigikamGenericBoxPlugin_1_1BOXTalker.html", "classDigikamGenericBoxPlugin_1_1BOXTalker" ]
    ] ],
    [ "boxwidget.cpp", "boxwidget_8cpp.html", null ],
    [ "boxwidget.h", "boxwidget_8h.html", [
      [ "BOXWidget", "classDigikamGenericBoxPlugin_1_1BOXWidget.html", "classDigikamGenericBoxPlugin_1_1BOXWidget" ]
    ] ],
    [ "boxwindow.cpp", "boxwindow_8cpp.html", null ],
    [ "boxwindow.h", "boxwindow_8h.html", [
      [ "BOXWindow", "classDigikamGenericBoxPlugin_1_1BOXWindow.html", "classDigikamGenericBoxPlugin_1_1BOXWindow" ]
    ] ]
];