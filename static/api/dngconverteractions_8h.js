var dngconverteractions_8h =
[
    [ "DNGConverterActionData", "classDigikamGenericDNGConverterPlugin_1_1DNGConverterActionData.html", "classDigikamGenericDNGConverterPlugin_1_1DNGConverterActionData" ],
    [ "DNGConverterAction", "dngconverteractions_8h.html#a973d39362c08f3b46e502615f60c89ed", [
      [ "NONE", "dngconverteractions_8h.html#a973d39362c08f3b46e502615f60c89eda0f6f9db07384471e319e35b8755b827a", null ],
      [ "IDENTIFY", "dngconverteractions_8h.html#a973d39362c08f3b46e502615f60c89eda222cecf912baaae3948428bb1a0e4c1c", null ],
      [ "PROCESS", "dngconverteractions_8h.html#a973d39362c08f3b46e502615f60c89eda544697836ba26d4044b358cf1f65d1cd", null ]
    ] ]
];