var classDigikam_1_1SetupAlbumView =
[
    [ "AlbumTab", "classDigikam_1_1SetupAlbumView.html#ad41cc9c01d1d2cce706ecce10c28d189", [
      [ "IconView", "classDigikam_1_1SetupAlbumView.html#ad41cc9c01d1d2cce706ecce10c28d189a90b257eff28aeeabf565c74f9a1c79d6", null ],
      [ "FolderView", "classDigikam_1_1SetupAlbumView.html#ad41cc9c01d1d2cce706ecce10c28d189a661b706879d95d244315ac87c1615f40", null ],
      [ "Preview", "classDigikam_1_1SetupAlbumView.html#ad41cc9c01d1d2cce706ecce10c28d189a3b30cbd9663b6ab7ac4b6b4b12b94541", null ],
      [ "FullScreen", "classDigikam_1_1SetupAlbumView.html#ad41cc9c01d1d2cce706ecce10c28d189aa676e6545b20d1ad31047f0b3c2d5318", null ],
      [ "MimeType", "classDigikam_1_1SetupAlbumView.html#ad41cc9c01d1d2cce706ecce10c28d189a1bab232b6eb69ce5695ebf651cb46025", null ],
      [ "Category", "classDigikam_1_1SetupAlbumView.html#ad41cc9c01d1d2cce706ecce10c28d189a4d8be83b6c59f26c580159a950940f8e", null ]
    ] ],
    [ "SetupAlbumView", "classDigikam_1_1SetupAlbumView.html#a820d2f8902837f6a8ac97101b70ad8c1", null ],
    [ "~SetupAlbumView", "classDigikam_1_1SetupAlbumView.html#aeb90094e75c4e088141259cca90aa378", null ],
    [ "applySettings", "classDigikam_1_1SetupAlbumView.html#aa4d27be667cd0dc074ad279307a6ef18", null ],
    [ "useLargeThumbsHasChanged", "classDigikam_1_1SetupAlbumView.html#a774effe075140b97492b7e45353f8ca6", null ]
];