var dir_7c9111403b6b0166556dc53b19391963 =
[
    [ "onlineversionchecker.cpp", "onlineversionchecker_8cpp.html", null ],
    [ "onlineversionchecker.h", "onlineversionchecker_8h.html", [
      [ "OnlineVersionChecker", "classDigikam_1_1OnlineVersionChecker.html", "classDigikam_1_1OnlineVersionChecker" ]
    ] ],
    [ "onlineversiondlg.cpp", "onlineversiondlg_8cpp.html", null ],
    [ "onlineversiondlg.h", "onlineversiondlg_8h.html", [
      [ "OnlineVersionDlg", "classDigikam_1_1OnlineVersionDlg.html", "classDigikam_1_1OnlineVersionDlg" ]
    ] ],
    [ "onlineversiondwnl.cpp", "onlineversiondwnl_8cpp.html", null ],
    [ "onlineversiondwnl.h", "onlineversiondwnl_8h.html", [
      [ "OnlineVersionDwnl", "classDigikam_1_1OnlineVersionDwnl.html", "classDigikam_1_1OnlineVersionDwnl" ]
    ] ]
];