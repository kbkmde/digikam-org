var dir_f93091bb9f910b5e088b31e9ec0e5094 =
[
    [ "albummanager.cpp", "albummanager_8cpp.html", null ],
    [ "albummanager.h", "albummanager_8h.html", [
      [ "AlbumManager", "classDigikam_1_1AlbumManager.html", "classDigikam_1_1AlbumManager" ]
    ] ],
    [ "albummanager_album.cpp", "albummanager__album_8cpp.html", null ],
    [ "albummanager_collection.cpp", "albummanager__collection_8cpp.html", null ],
    [ "albummanager_dalbum.cpp", "albummanager__dalbum_8cpp.html", null ],
    [ "albummanager_database.cpp", "albummanager__database_8cpp.html", null ],
    [ "albummanager_falbum.cpp", "albummanager__falbum_8cpp.html", null ],
    [ "albummanager_p.cpp", "albummanager__p_8cpp.html", null ],
    [ "albummanager_p.h", "albummanager__p_8h.html", "albummanager__p_8h" ],
    [ "albummanager_palbum.cpp", "albummanager__palbum_8cpp.html", null ],
    [ "albummanager_salbum.cpp", "albummanager__salbum_8cpp.html", null ],
    [ "albummanager_talbum.cpp", "albummanager__talbum_8cpp.html", null ]
];