var classDigikam_1_1TrackManager_1_1Track =
[
    [ "List", "classDigikam_1_1TrackManager_1_1Track.html#ab42dc3627d811a4d1553d9be0ea9b920", null ],
    [ "Flags", "classDigikam_1_1TrackManager_1_1Track.html#a5800c1349d5382b0c811879d60ad73fc", [
      [ "FlagVisible", "classDigikam_1_1TrackManager_1_1Track.html#a5800c1349d5382b0c811879d60ad73fca3fb94c04da6a309580932b9125445fe0", null ],
      [ "FlagDefault", "classDigikam_1_1TrackManager_1_1Track.html#a5800c1349d5382b0c811879d60ad73fca740d057e5e808442c340d691a9a4af0e", null ]
    ] ],
    [ "Track", "classDigikam_1_1TrackManager_1_1Track.html#acd936e55651bf3368dab047298aa8aba", null ],
    [ "color", "classDigikam_1_1TrackManager_1_1Track.html#a95e9213553992297d76635cc92188643", null ],
    [ "flags", "classDigikam_1_1TrackManager_1_1Track.html#ac5cb4dcb46008f176a6f5291e03bc6ce", null ],
    [ "id", "classDigikam_1_1TrackManager_1_1Track.html#af3dea08ef5aef8a8ff12fe4ca51ae01a", null ],
    [ "points", "classDigikam_1_1TrackManager_1_1Track.html#ab587d78b294ea5063b457071aa020811", null ],
    [ "url", "classDigikam_1_1TrackManager_1_1Track.html#a2754c0f49a7031909c11d62b690a804b", null ]
];