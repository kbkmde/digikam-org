var classDigikam_1_1SearchModificationHelper =
[
    [ "SearchModificationHelper", "classDigikam_1_1SearchModificationHelper.html#abcabc1df2b35f1c55bc200aa581ab487", null ],
    [ "~SearchModificationHelper", "classDigikam_1_1SearchModificationHelper.html#a486a717190ee9887f6734a1244c8029d", null ],
    [ "createFuzzySearchFromDropped", "classDigikam_1_1SearchModificationHelper.html#a642bac838517af5c7f0723034990ae68", null ],
    [ "createFuzzySearchFromImage", "classDigikam_1_1SearchModificationHelper.html#aacdf020db869f32a059032cccc71dcad", null ],
    [ "createFuzzySearchFromSketch", "classDigikam_1_1SearchModificationHelper.html#a8c24455e884e84eb488ad85495b1fb01", null ],
    [ "slotCreateFuzzySearchFromDropped", "classDigikam_1_1SearchModificationHelper.html#a69ddb0c717dbc8ec89fa7182afd0ea48", null ],
    [ "slotCreateFuzzySearchFromImage", "classDigikam_1_1SearchModificationHelper.html#a0af24b423a9045b550d0dc8762c30ffe", null ],
    [ "slotCreateFuzzySearchFromSketch", "classDigikam_1_1SearchModificationHelper.html#ad944ec13a272729581195d34975fa6f7", null ],
    [ "slotCreateTimeLineSearch", "classDigikam_1_1SearchModificationHelper.html#aaea501d2b12c9ffda76cfca267837c51", null ],
    [ "slotSearchDelete", "classDigikam_1_1SearchModificationHelper.html#a2b919b122b3834d678dc3bb80598c474", null ],
    [ "slotSearchRename", "classDigikam_1_1SearchModificationHelper.html#ae167e4e138421bc020833488eed988ec", null ]
];