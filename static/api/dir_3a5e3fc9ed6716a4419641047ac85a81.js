var dir_3a5e3fc9ed6716a4419641047ac85a81 =
[
    [ "setupeditor.cpp", "setupeditor_8cpp.html", null ],
    [ "setupeditor.h", "setupeditor_8h.html", [
      [ "SetupEditor", "classDigikam_1_1SetupEditor.html", "classDigikam_1_1SetupEditor" ]
    ] ],
    [ "setupeditoriface.cpp", "setupeditoriface_8cpp.html", null ],
    [ "setupeditoriface.h", "setupeditoriface_8h.html", [
      [ "SetupEditorIface", "classDigikam_1_1SetupEditorIface.html", "classDigikam_1_1SetupEditorIface" ]
    ] ],
    [ "setupiofiles.cpp", "setupiofiles_8cpp.html", null ],
    [ "setupiofiles.h", "setupiofiles_8h.html", [
      [ "SetupIOFiles", "classDigikam_1_1SetupIOFiles.html", "classDigikam_1_1SetupIOFiles" ]
    ] ],
    [ "setupraw.cpp", "setupraw_8cpp.html", null ],
    [ "setupraw.h", "setupraw_8h.html", [
      [ "SetupRaw", "classDigikam_1_1SetupRaw.html", "classDigikam_1_1SetupRaw" ]
    ] ],
    [ "setupversioning.cpp", "setupversioning_8cpp.html", null ],
    [ "setupversioning.h", "setupversioning_8h.html", [
      [ "SetupVersioning", "classDigikam_1_1SetupVersioning.html", "classDigikam_1_1SetupVersioning" ]
    ] ]
];