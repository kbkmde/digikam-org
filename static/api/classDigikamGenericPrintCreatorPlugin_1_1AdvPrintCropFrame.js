var classDigikamGenericPrintCreatorPlugin_1_1AdvPrintCropFrame =
[
    [ "AdvPrintCropFrame", "classDigikamGenericPrintCreatorPlugin_1_1AdvPrintCropFrame.html#a11b682f75eb459591d8dc7a7c41f91cd", null ],
    [ "~AdvPrintCropFrame", "classDigikamGenericPrintCreatorPlugin_1_1AdvPrintCropFrame.html#a8b67f9dcf318d0e64ba7380d5f7b0891", null ],
    [ "color", "classDigikamGenericPrintCreatorPlugin_1_1AdvPrintCropFrame.html#a5869befb096e09522a21bab8f901e7c2", null ],
    [ "drawCropRectangle", "classDigikamGenericPrintCreatorPlugin_1_1AdvPrintCropFrame.html#a973c362196119518749ad07ad06e466d", null ],
    [ "init", "classDigikamGenericPrintCreatorPlugin_1_1AdvPrintCropFrame.html#ad0f446e2ce9c851a2b59c3388ead87cc", null ],
    [ "keyReleaseEvent", "classDigikamGenericPrintCreatorPlugin_1_1AdvPrintCropFrame.html#a42e3838cc78654acfeacb02e6e93c271", null ],
    [ "mouseMoveEvent", "classDigikamGenericPrintCreatorPlugin_1_1AdvPrintCropFrame.html#a51371c829cda7ece1448eb732d2f1780", null ],
    [ "mousePressEvent", "classDigikamGenericPrintCreatorPlugin_1_1AdvPrintCropFrame.html#a42a3e07acbf46fde1bee58564f158fae", null ],
    [ "mouseReleaseEvent", "classDigikamGenericPrintCreatorPlugin_1_1AdvPrintCropFrame.html#ab27a362b6afa73baefe4a5441c2dcc1c", null ],
    [ "paintEvent", "classDigikamGenericPrintCreatorPlugin_1_1AdvPrintCropFrame.html#ab074fa19d60080f80d0e9f2a7a3ea713", null ],
    [ "setColor", "classDigikamGenericPrintCreatorPlugin_1_1AdvPrintCropFrame.html#a0860395475cd63a6fe0c01b20b0ce63c", null ]
];