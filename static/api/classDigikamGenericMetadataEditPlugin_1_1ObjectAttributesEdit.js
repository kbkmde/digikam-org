var classDigikamGenericMetadataEditPlugin_1_1ObjectAttributesEdit =
[
    [ "ObjectAttributesEdit", "classDigikamGenericMetadataEditPlugin_1_1ObjectAttributesEdit.html#a53586640b529ba8145ad43e4161e2377", null ],
    [ "~ObjectAttributesEdit", "classDigikamGenericMetadataEditPlugin_1_1ObjectAttributesEdit.html#abbca3e2af55d84b6b32239391f78b281", null ],
    [ "getValues", "classDigikamGenericMetadataEditPlugin_1_1ObjectAttributesEdit.html#a307ff70ccc60f750a2d84dd4578ad70a", null ],
    [ "isValid", "classDigikamGenericMetadataEditPlugin_1_1ObjectAttributesEdit.html#a7dc1fff5ed4ead9b7f347aae3fd3ae9c", null ],
    [ "setValid", "classDigikamGenericMetadataEditPlugin_1_1ObjectAttributesEdit.html#a62073d196caae1f83bc1a492cf1556ea", null ],
    [ "setValues", "classDigikamGenericMetadataEditPlugin_1_1ObjectAttributesEdit.html#ac2c4f66d78d0784e03442092836fbf4f", null ],
    [ "signalModified", "classDigikamGenericMetadataEditPlugin_1_1ObjectAttributesEdit.html#a9896e3e44ec8351ae2072f5775192523", null ],
    [ "valueEdit", "classDigikamGenericMetadataEditPlugin_1_1ObjectAttributesEdit.html#a4a9470669cfe3f514117f961a9ebd813", null ]
];