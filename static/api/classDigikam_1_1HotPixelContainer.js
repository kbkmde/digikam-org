var classDigikam_1_1HotPixelContainer =
[
    [ "Direction", "classDigikam_1_1HotPixelContainer.html#a2a8f418ca0022e58f92aebe6e6e676fb", [
      [ "TWODIM_DIRECTION", "classDigikam_1_1HotPixelContainer.html#a2a8f418ca0022e58f92aebe6e6e676fbad58102375325efd332cd5388a9b12fe6", null ],
      [ "VERTICAL_DIRECTION", "classDigikam_1_1HotPixelContainer.html#a2a8f418ca0022e58f92aebe6e6e676fba2074f669a7a2945b840c93b7ce565700", null ],
      [ "HORIZONTAL_DIRECTION", "classDigikam_1_1HotPixelContainer.html#a2a8f418ca0022e58f92aebe6e6e676fbaa9a9ad225665b78cd54fb05d0abb329d", null ]
    ] ],
    [ "InterpolationMethod", "classDigikam_1_1HotPixelContainer.html#a88c6c556c4a424298666431c21bf8bc5", [
      [ "AVERAGE_INTERPOLATION", "classDigikam_1_1HotPixelContainer.html#a88c6c556c4a424298666431c21bf8bc5ad1b1eb2157ef01dc23201736e78d7953", null ],
      [ "LINEAR_INTERPOLATION", "classDigikam_1_1HotPixelContainer.html#a88c6c556c4a424298666431c21bf8bc5ad6c09cb4b86be94d0ee8bf359804fc1e", null ],
      [ "QUADRATIC_INTERPOLATION", "classDigikam_1_1HotPixelContainer.html#a88c6c556c4a424298666431c21bf8bc5aba8994d62e10507810007fa70b9ffef2", null ],
      [ "CUBIC_INTERPOLATION", "classDigikam_1_1HotPixelContainer.html#a88c6c556c4a424298666431c21bf8bc5a3f4886965223c692e5cddce72d661ce8", null ]
    ] ],
    [ "HotPixelContainer", "classDigikam_1_1HotPixelContainer.html#aa55ea4a824a605d5a8f5805b2fbe8b50", null ],
    [ "~HotPixelContainer", "classDigikam_1_1HotPixelContainer.html#ac3a5d984deacab86c0f6e9b8ccb4fce0", null ],
    [ "isDefault", "classDigikam_1_1HotPixelContainer.html#a17dbb962233d2ab79594a6874ea05bec", null ],
    [ "operator==", "classDigikam_1_1HotPixelContainer.html#a0c12001294828f9dd5ae9487678ed056", null ],
    [ "writeToFilterAction", "classDigikam_1_1HotPixelContainer.html#a6d561f5c33e457f1bc38750fe6e88917", null ],
    [ "blackFrameUrl", "classDigikam_1_1HotPixelContainer.html#a0a6224128704a06076ffb9dbfd159fae", null ],
    [ "filterMethod", "classDigikam_1_1HotPixelContainer.html#a837769ccd8f41b77f4748f3a71042586", null ],
    [ "hotPixelsList", "classDigikam_1_1HotPixelContainer.html#af3e0c7c87cef88b7a29ef214c2185b96", null ]
];