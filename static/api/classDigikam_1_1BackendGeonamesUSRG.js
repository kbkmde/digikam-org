var classDigikam_1_1BackendGeonamesUSRG =
[
    [ "BackendGeonamesUSRG", "classDigikam_1_1BackendGeonamesUSRG.html#a3430b82a4b444d7cc72ca4dfbf154253", null ],
    [ "~BackendGeonamesUSRG", "classDigikam_1_1BackendGeonamesUSRG.html#a84510e3bc3b7e891eab1ebc912f2e4fb", null ],
    [ "backendName", "classDigikam_1_1BackendGeonamesUSRG.html#ad7308725b640f61302a63fc5868bb999", null ],
    [ "callRGBackend", "classDigikam_1_1BackendGeonamesUSRG.html#aadfe910013c9e8405b8f12db407bf5f6", null ],
    [ "cancelRequests", "classDigikam_1_1BackendGeonamesUSRG.html#aa9c64eafebb18e9d4e71187ad78373b1", null ],
    [ "getErrorMessage", "classDigikam_1_1BackendGeonamesUSRG.html#a29a520cdddb8aa0b97190c355ccec1b9", null ],
    [ "makeQMapFromXML", "classDigikam_1_1BackendGeonamesUSRG.html#a8f29558f54aa7b3008ffc14683375861", null ],
    [ "signalRGReady", "classDigikam_1_1BackendGeonamesUSRG.html#a6ee2a6e36a160142ffa2fe82b07c4695", null ]
];