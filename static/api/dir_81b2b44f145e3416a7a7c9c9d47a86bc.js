var dir_81b2b44f145e3416a7a7c9c9d47a86bc =
[
    [ "assignedlist.cpp", "assignedlist_8cpp.html", "assignedlist_8cpp" ],
    [ "assignedlist.h", "assignedlist_8h.html", [
      [ "AssignedListView", "classDigikam_1_1AssignedListView.html", "classDigikam_1_1AssignedListView" ],
      [ "AssignedListViewItem", "classDigikam_1_1AssignedListViewItem.html", "classDigikam_1_1AssignedListViewItem" ]
    ] ],
    [ "queuelist.cpp", "queuelist_8cpp.html", null ],
    [ "queuelist.h", "queuelist_8h.html", [
      [ "QueueListView", "classDigikam_1_1QueueListView.html", "classDigikam_1_1QueueListView" ],
      [ "QueueListViewItem", "classDigikam_1_1QueueListViewItem.html", "classDigikam_1_1QueueListViewItem" ]
    ] ],
    [ "queuepool.cpp", "queuepool_8cpp.html", null ],
    [ "queuepool.h", "queuepool_8h.html", [
      [ "QueuePool", "classDigikam_1_1QueuePool.html", "classDigikam_1_1QueuePool" ],
      [ "QueuePoolBar", "classDigikam_1_1QueuePoolBar.html", "classDigikam_1_1QueuePoolBar" ]
    ] ],
    [ "queuesettingsview.cpp", "queuesettingsview_8cpp.html", null ],
    [ "queuesettingsview.h", "queuesettingsview_8h.html", [
      [ "QueueSettingsView", "classDigikam_1_1QueueSettingsView.html", "classDigikam_1_1QueueSettingsView" ]
    ] ],
    [ "queuetooltip.cpp", "queuetooltip_8cpp.html", null ],
    [ "queuetooltip.h", "queuetooltip_8h.html", [
      [ "QueueToolTip", "classDigikam_1_1QueueToolTip.html", "classDigikam_1_1QueueToolTip" ]
    ] ],
    [ "toolsettingsview.cpp", "toolsettingsview_8cpp.html", null ],
    [ "toolsettingsview.h", "toolsettingsview_8h.html", [
      [ "ToolSettingsView", "classDigikam_1_1ToolSettingsView.html", "classDigikam_1_1ToolSettingsView" ]
    ] ],
    [ "toolslistview.cpp", "toolslistview_8cpp.html", null ],
    [ "toolslistview.h", "toolslistview_8h.html", [
      [ "ToolListViewGroup", "classDigikam_1_1ToolListViewGroup.html", "classDigikam_1_1ToolListViewGroup" ],
      [ "ToolListViewItem", "classDigikam_1_1ToolListViewItem.html", "classDigikam_1_1ToolListViewItem" ],
      [ "ToolsListView", "classDigikam_1_1ToolsListView.html", "classDigikam_1_1ToolsListView" ]
    ] ],
    [ "toolsview.cpp", "toolsview_8cpp.html", null ],
    [ "toolsview.h", "toolsview_8h.html", [
      [ "ToolsView", "classDigikam_1_1ToolsView.html", "classDigikam_1_1ToolsView" ]
    ] ],
    [ "workflowdlg.cpp", "workflowdlg_8cpp.html", null ],
    [ "workflowdlg.h", "workflowdlg_8h.html", [
      [ "WorkflowDlg", "classDigikam_1_1WorkflowDlg.html", "classDigikam_1_1WorkflowDlg" ]
    ] ],
    [ "workflowlist.cpp", "workflowlist_8cpp.html", null ],
    [ "workflowlist.h", "workflowlist_8h.html", [
      [ "WorkflowItem", "classDigikam_1_1WorkflowItem.html", "classDigikam_1_1WorkflowItem" ],
      [ "WorkflowList", "classDigikam_1_1WorkflowList.html", "classDigikam_1_1WorkflowList" ]
    ] ]
];