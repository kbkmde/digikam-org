var dir_2d4d3e9b3c6b441882018d6e4fd8f3be =
[
    [ "album.cpp", "album_8cpp.html", null ],
    [ "album.h", "album_8h.html", "album_8h" ],
    [ "albumhistory.cpp", "albumhistory_8cpp.html", "albumhistory_8cpp" ],
    [ "albumhistory.h", "albumhistory_8h.html", [
      [ "AlbumHistory", "classDigikam_1_1AlbumHistory.html", "classDigikam_1_1AlbumHistory" ]
    ] ],
    [ "albummodificationhelper.cpp", "albummodificationhelper_8cpp.html", null ],
    [ "albummodificationhelper.h", "albummodificationhelper_8h.html", [
      [ "AlbumModificationHelper", "classDigikam_1_1AlbumModificationHelper.html", "classDigikam_1_1AlbumModificationHelper" ]
    ] ],
    [ "albumparser.cpp", "albumparser_8cpp.html", null ],
    [ "albumparser.h", "albumparser_8h.html", [
      [ "AlbumParser", "classDigikam_1_1AlbumParser.html", "classDigikam_1_1AlbumParser" ]
    ] ],
    [ "albumpointer.h", "albumpointer_8h.html", [
      [ "AlbumPointer", "classDigikam_1_1AlbumPointer.html", "classDigikam_1_1AlbumPointer" ],
      [ "AlbumPointerList", "classDigikam_1_1AlbumPointerList.html", "classDigikam_1_1AlbumPointerList" ]
    ] ],
    [ "albumthumbnailloader.cpp", "albumthumbnailloader_8cpp.html", "albumthumbnailloader_8cpp" ],
    [ "albumthumbnailloader.h", "albumthumbnailloader_8h.html", [
      [ "AlbumThumbnailLoader", "classDigikam_1_1AlbumThumbnailLoader.html", "classDigikam_1_1AlbumThumbnailLoader" ]
    ] ],
    [ "albumwatch.cpp", "albumwatch_8cpp.html", null ],
    [ "albumwatch.h", "albumwatch_8h.html", [
      [ "AlbumWatch", "classDigikam_1_1AlbumWatch.html", "classDigikam_1_1AlbumWatch" ]
    ] ]
];