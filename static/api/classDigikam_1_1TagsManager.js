var classDigikam_1_1TagsManager =
[
    [ "StateSavingDepth", "classDigikam_1_1TagsManager.html#abdf8ad03070bc560242e56575909d6b6", [
      [ "INSTANCE", "classDigikam_1_1TagsManager.html#abdf8ad03070bc560242e56575909d6b6a535f22ead553f373020dce094f0ae735", null ],
      [ "DIRECT_CHILDREN", "classDigikam_1_1TagsManager.html#abdf8ad03070bc560242e56575909d6b6a402f3f077dcfc1835196020f1986fb55", null ],
      [ "RECURSIVE", "classDigikam_1_1TagsManager.html#abdf8ad03070bc560242e56575909d6b6a1d31a55830067a26a4c25eabb48f6d4f", null ]
    ] ],
    [ "TagsManager", "classDigikam_1_1TagsManager.html#afe77614be097dd10624d11d4c8ac1b7a", null ],
    [ "~TagsManager", "classDigikam_1_1TagsManager.html#ab031faaff001b0f6795d72982ebdb341", null ],
    [ "closeEvent", "classDigikam_1_1TagsManager.html#acce4b8cddc26b75017ca56e628377ae7", null ],
    [ "doLoadState", "classDigikam_1_1TagsManager.html#acbb1116331b447062cbea98990346ecf", null ],
    [ "doSaveState", "classDigikam_1_1TagsManager.html#a55dae654ebebdc1dea76f6066c618c2b", null ],
    [ "entryName", "classDigikam_1_1TagsManager.html#a226ce9b9faa055120ba386b85509bcdf", null ],
    [ "getConfigGroup", "classDigikam_1_1TagsManager.html#a3dce4d65e29a07ea09d2e7b4746f8350", null ],
    [ "getStateSavingDepth", "classDigikam_1_1TagsManager.html#a340090725416a1b9030c67d7d6a77d6e", null ],
    [ "loadState", "classDigikam_1_1TagsManager.html#a884a7c5ec2c82c6b61519a6c035577c2", null ],
    [ "saveState", "classDigikam_1_1TagsManager.html#a0db6b9c2daff1b521bce63f78d8f1652", null ],
    [ "setConfigGroup", "classDigikam_1_1TagsManager.html#aa37c59ac6e91ba60c3c0dd14cc6fa71e", null ],
    [ "setEntryPrefix", "classDigikam_1_1TagsManager.html#ac289161ea3144b78c348c283cbb7777d", null ],
    [ "setStateSavingDepth", "classDigikam_1_1TagsManager.html#a1023eaabd6d22bcfcd83b0c7f5c47e9b", null ],
    [ "setupUi", "classDigikam_1_1TagsManager.html#a13d791e8ab9237a18eb048e133236482", null ],
    [ "showEvent", "classDigikam_1_1TagsManager.html#abbcb83d79096d3634267c93e0d6d8bc2", null ],
    [ "signalSelectionChanged", "classDigikam_1_1TagsManager.html#abc7aab62dd6c9f0404e4e4c983958ce7", null ]
];