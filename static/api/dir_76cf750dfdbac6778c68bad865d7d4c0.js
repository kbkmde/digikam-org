var dir_76cf750dfdbac6778c68bad865d7d4c0 =
[
    [ "vidslidealbumspage.cpp", "vidslidealbumspage_8cpp.html", null ],
    [ "vidslidealbumspage.h", "vidslidealbumspage_8h.html", [
      [ "VidSlideAlbumsPage", "classDigikamGenericVideoSlideShowPlugin_1_1VidSlideAlbumsPage.html", "classDigikamGenericVideoSlideShowPlugin_1_1VidSlideAlbumsPage" ]
    ] ],
    [ "vidslidefinalpage.cpp", "vidslidefinalpage_8cpp.html", null ],
    [ "vidslidefinalpage.h", "vidslidefinalpage_8h.html", [
      [ "VidSlideFinalPage", "classDigikamGenericVideoSlideShowPlugin_1_1VidSlideFinalPage.html", "classDigikamGenericVideoSlideShowPlugin_1_1VidSlideFinalPage" ]
    ] ],
    [ "vidslideimagespage.cpp", "vidslideimagespage_8cpp.html", null ],
    [ "vidslideimagespage.h", "vidslideimagespage_8h.html", [
      [ "VidSlideImagesPage", "classDigikamGenericVideoSlideShowPlugin_1_1VidSlideImagesPage.html", "classDigikamGenericVideoSlideShowPlugin_1_1VidSlideImagesPage" ]
    ] ],
    [ "vidslideintropage.cpp", "vidslideintropage_8cpp.html", null ],
    [ "vidslideintropage.h", "vidslideintropage_8h.html", [
      [ "VidSlideIntroPage", "classDigikamGenericVideoSlideShowPlugin_1_1VidSlideIntroPage.html", "classDigikamGenericVideoSlideShowPlugin_1_1VidSlideIntroPage" ]
    ] ],
    [ "vidslideoutputpage.cpp", "vidslideoutputpage_8cpp.html", null ],
    [ "vidslideoutputpage.h", "vidslideoutputpage_8h.html", [
      [ "VidSlideOutputPage", "classDigikamGenericVideoSlideShowPlugin_1_1VidSlideOutputPage.html", "classDigikamGenericVideoSlideShowPlugin_1_1VidSlideOutputPage" ]
    ] ],
    [ "vidslidevideopage.cpp", "vidslidevideopage_8cpp.html", null ],
    [ "vidslidevideopage.h", "vidslidevideopage_8h.html", [
      [ "VidSlideVideoPage", "classDigikamGenericVideoSlideShowPlugin_1_1VidSlideVideoPage.html", "classDigikamGenericVideoSlideShowPlugin_1_1VidSlideVideoPage" ]
    ] ],
    [ "vidslidewizard.cpp", "vidslidewizard_8cpp.html", null ],
    [ "vidslidewizard.h", "vidslidewizard_8h.html", [
      [ "VidSlideWizard", "classDigikamGenericVideoSlideShowPlugin_1_1VidSlideWizard.html", "classDigikamGenericVideoSlideShowPlugin_1_1VidSlideWizard" ]
    ] ]
];