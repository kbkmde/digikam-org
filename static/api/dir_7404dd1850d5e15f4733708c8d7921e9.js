var dir_7404dd1850d5e15f4733708c8d7921e9 =
[
    [ "common", "dir_cf173494b57fa7f40ad73e1fb27fbdc9.html", "dir_cf173494b57fa7f40ad73e1fb27fbdc9" ],
    [ "parser", "dir_42c39ba383c965be2a66733cf73dec95.html", "dir_42c39ba383c965be2a66733cf73dec95" ],
    [ "advancedrenamedialog.cpp", "advancedrenamedialog_8cpp.html", null ],
    [ "advancedrenamedialog.h", "advancedrenamedialog_8h.html", "advancedrenamedialog_8h" ],
    [ "advancedrenameinput.cpp", "advancedrenameinput_8cpp.html", null ],
    [ "advancedrenameinput.h", "advancedrenameinput_8h.html", [
      [ "AdvancedRenameInput", "classDigikam_1_1AdvancedRenameInput.html", "classDigikam_1_1AdvancedRenameInput" ],
      [ "AdvancedRenameLineEdit", "classDigikam_1_1AdvancedRenameLineEdit.html", "classDigikam_1_1AdvancedRenameLineEdit" ]
    ] ],
    [ "advancedrenamemanager.cpp", "advancedrenamemanager_8cpp.html", null ],
    [ "advancedrenamemanager.h", "advancedrenamemanager_8h.html", [
      [ "AdvancedRenameManager", "classDigikam_1_1AdvancedRenameManager.html", "classDigikam_1_1AdvancedRenameManager" ]
    ] ],
    [ "advancedrenameprocessdialog.cpp", "advancedrenameprocessdialog_8cpp.html", null ],
    [ "advancedrenameprocessdialog.h", "advancedrenameprocessdialog_8h.html", [
      [ "AdvancedRenameProcessDialog", "classDigikam_1_1AdvancedRenameProcessDialog.html", "classDigikam_1_1AdvancedRenameProcessDialog" ]
    ] ],
    [ "advancedrenamewidget.cpp", "advancedrenamewidget_8cpp.html", null ],
    [ "advancedrenamewidget.h", "advancedrenamewidget_8h.html", [
      [ "AdvancedRenameWidget", "classDigikam_1_1AdvancedRenameWidget.html", "classDigikam_1_1AdvancedRenameWidget" ]
    ] ]
];