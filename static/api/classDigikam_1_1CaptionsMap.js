var classDigikam_1_1CaptionsMap =
[
    [ "CaptionsMap", "classDigikam_1_1CaptionsMap.html#a4f866ba34ef6d999e48dcdf57a1343c8", null ],
    [ "~CaptionsMap", "classDigikam_1_1CaptionsMap.html#a1ebf55956b09611d4b720bf9c80cc3ee", null ],
    [ "authorsList", "classDigikam_1_1CaptionsMap.html#af1fb853823cb09257f6c60b3df488da4", null ],
    [ "datesList", "classDigikam_1_1CaptionsMap.html#ac7763ac804f979ac32b9a0cffe1f8c67", null ],
    [ "fromAltLangMap", "classDigikam_1_1CaptionsMap.html#ab3988c0812c0fd51bd07d43b769edaea", null ],
    [ "setAuthorsList", "classDigikam_1_1CaptionsMap.html#a1283d3d2419c9a6768b811cb1dd812ed", null ],
    [ "setData", "classDigikam_1_1CaptionsMap.html#a21f619ab3c497bd601f636eacf59653e", null ],
    [ "setDatesList", "classDigikam_1_1CaptionsMap.html#a3d6bd0d09a7ae558cb7741aeb0838370", null ],
    [ "toAltLangMap", "classDigikam_1_1CaptionsMap.html#a8bba2cdfed5e86450d72070e6a8a2654", null ]
];