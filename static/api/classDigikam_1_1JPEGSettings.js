var classDigikam_1_1JPEGSettings =
[
    [ "JPEGSettings", "classDigikam_1_1JPEGSettings.html#a28ec599887edabd77e1d1ff3973c6c86", null ],
    [ "~JPEGSettings", "classDigikam_1_1JPEGSettings.html#afe112d1a4783aaea82c798bf856b229e", null ],
    [ "getCompressionValue", "classDigikam_1_1JPEGSettings.html#a3055d0ed93ad7670014a781a94f2e5df", null ],
    [ "getSubSamplingValue", "classDigikam_1_1JPEGSettings.html#a5574a4b771fe04f2380b1277d2aededa", null ],
    [ "setCompressionValue", "classDigikam_1_1JPEGSettings.html#a8829203a9d5686ce2581c6b3a2d7122d", null ],
    [ "setSubSamplingValue", "classDigikam_1_1JPEGSettings.html#a6ac1207221df12c095a6686957b69c72", null ],
    [ "signalSettingsChanged", "classDigikam_1_1JPEGSettings.html#aea08f9be2bc4be618ea2f3624f365965", null ]
];