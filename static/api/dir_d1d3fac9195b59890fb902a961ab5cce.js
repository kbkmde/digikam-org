var dir_d1d3fac9195b59890fb902a961ab5cce =
[
    [ "dexpanderbox.cpp", "dexpanderbox_8cpp.html", null ],
    [ "dexpanderbox.h", "dexpanderbox_8h.html", [
      [ "DAdjustableLabel", "classDigikam_1_1DAdjustableLabel.html", "classDigikam_1_1DAdjustableLabel" ],
      [ "DArrowClickLabel", "classDigikam_1_1DArrowClickLabel.html", "classDigikam_1_1DArrowClickLabel" ],
      [ "DClickLabel", "classDigikam_1_1DClickLabel.html", "classDigikam_1_1DClickLabel" ],
      [ "DExpanderBox", "classDigikam_1_1DExpanderBox.html", "classDigikam_1_1DExpanderBox" ],
      [ "DExpanderBoxExclusive", "classDigikam_1_1DExpanderBoxExclusive.html", "classDigikam_1_1DExpanderBoxExclusive" ],
      [ "DLabelExpander", "classDigikam_1_1DLabelExpander.html", "classDigikam_1_1DLabelExpander" ],
      [ "DLineWidget", "classDigikam_1_1DLineWidget.html", "classDigikam_1_1DLineWidget" ],
      [ "DSqueezedClickLabel", "classDigikam_1_1DSqueezedClickLabel.html", "classDigikam_1_1DSqueezedClickLabel" ]
    ] ],
    [ "dlayoutbox.cpp", "dlayoutbox_8cpp.html", null ],
    [ "dlayoutbox.h", "dlayoutbox_8h.html", [
      [ "DHBox", "classDigikam_1_1DHBox.html", "classDigikam_1_1DHBox" ],
      [ "DVBox", "classDigikam_1_1DVBox.html", "classDigikam_1_1DVBox" ]
    ] ],
    [ "sidebar.cpp", "sidebar_8cpp.html", null ],
    [ "sidebar.h", "sidebar_8h.html", [
      [ "DMultiTabBar", "classDigikam_1_1DMultiTabBar.html", "classDigikam_1_1DMultiTabBar" ],
      [ "DMultiTabBarButton", "classDigikam_1_1DMultiTabBarButton.html", "classDigikam_1_1DMultiTabBarButton" ],
      [ "DMultiTabBarFrame", "classDigikam_1_1DMultiTabBarFrame.html", "classDigikam_1_1DMultiTabBarFrame" ],
      [ "DMultiTabBarTab", "classDigikam_1_1DMultiTabBarTab.html", "classDigikam_1_1DMultiTabBarTab" ],
      [ "Sidebar", "classDigikam_1_1Sidebar.html", "classDigikam_1_1Sidebar" ],
      [ "SidebarSplitter", "classDigikam_1_1SidebarSplitter.html", "classDigikam_1_1SidebarSplitter" ]
    ] ],
    [ "sidebar_dmultitabbar.cpp", "sidebar__dmultitabbar_8cpp.html", null ],
    [ "sidebar_dmultitabbarbutton.cpp", "sidebar__dmultitabbarbutton_8cpp.html", null ],
    [ "sidebar_dmultitabbarframe.cpp", "sidebar__dmultitabbarframe_8cpp.html", null ],
    [ "sidebar_dmultitabbartab.cpp", "sidebar__dmultitabbartab_8cpp.html", null ],
    [ "sidebar_p.cpp", "sidebar__p_8cpp.html", null ],
    [ "sidebar_p.h", "sidebar__p_8h.html", [
      [ "Private", "classDigikam_1_1DMultiTabBar_1_1Private.html", "classDigikam_1_1DMultiTabBar_1_1Private" ],
      [ "Private", "classDigikam_1_1DMultiTabBarFrame_1_1Private.html", "classDigikam_1_1DMultiTabBarFrame_1_1Private" ],
      [ "Private", "classDigikam_1_1DMultiTabBarTab_1_1Private.html", "classDigikam_1_1DMultiTabBarTab_1_1Private" ],
      [ "Private", "classDigikam_1_1Sidebar_1_1Private.html", "classDigikam_1_1Sidebar_1_1Private" ],
      [ "Private", "classDigikam_1_1SidebarSplitter_1_1Private.html", "classDigikam_1_1SidebarSplitter_1_1Private" ],
      [ "SidebarState", "classDigikam_1_1SidebarState.html", "classDigikam_1_1SidebarState" ]
    ] ],
    [ "sidebar_splitter.cpp", "sidebar__splitter_8cpp.html", null ],
    [ "statesavingobject.cpp", "statesavingobject_8cpp.html", null ],
    [ "statesavingobject.h", "statesavingobject_8h.html", [
      [ "StateSavingObject", "classDigikam_1_1StateSavingObject.html", "classDigikam_1_1StateSavingObject" ]
    ] ],
    [ "thumbbardock.cpp", "thumbbardock_8cpp.html", null ],
    [ "thumbbardock.h", "thumbbardock_8h.html", [
      [ "DragHandle", "classDigikam_1_1DragHandle.html", "classDigikam_1_1DragHandle" ],
      [ "ThumbBarDock", "classDigikam_1_1ThumbBarDock.html", "classDigikam_1_1ThumbBarDock" ]
    ] ]
];