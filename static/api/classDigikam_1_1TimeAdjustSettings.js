var classDigikam_1_1TimeAdjustSettings =
[
    [ "TimeAdjustSettings", "classDigikam_1_1TimeAdjustSettings.html#a440ebabe7262c8db8a330b5bcdbf7f69", null ],
    [ "~TimeAdjustSettings", "classDigikam_1_1TimeAdjustSettings.html#a270a6f7e888a16fc9d303828aafcd828", null ],
    [ "detAdjustmentByClockPhotoUrl", "classDigikam_1_1TimeAdjustSettings.html#a7fb005e1837e3cee6501a5e9a6005ba0", null ],
    [ "setCurrentItemUrl", "classDigikam_1_1TimeAdjustSettings.html#a4518bff70777d1911e9a0e638bad86ff", null ],
    [ "setSettings", "classDigikam_1_1TimeAdjustSettings.html#a3bd898dcd15007a1ed8deb2a48a375d5", null ],
    [ "settings", "classDigikam_1_1TimeAdjustSettings.html#ae45bac2cc9a2892b50e044cac51fc638", null ],
    [ "signalSettingsChanged", "classDigikam_1_1TimeAdjustSettings.html#ac0c3b420c483a8bd38ce7f4d9a6efadb", null ],
    [ "signalSettingsChangedTool", "classDigikam_1_1TimeAdjustSettings.html#a9d1f921a60c82c8e6c62bd9057ff08f6", null ]
];