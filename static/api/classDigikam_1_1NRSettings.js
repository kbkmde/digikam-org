var classDigikam_1_1NRSettings =
[
    [ "NRSettings", "classDigikam_1_1NRSettings.html#a722bdaf48c30bf7d0dafd248cfc21a79", null ],
    [ "~NRSettings", "classDigikam_1_1NRSettings.html#a57b9bb5fda5281809fd3d84e0d2a235f", null ],
    [ "defaultSettings", "classDigikam_1_1NRSettings.html#a5268f2ab2f09eb03b4114c5296d13c84", null ],
    [ "estimateNoise", "classDigikam_1_1NRSettings.html#a60e1ee92536bf2efd0d5831dc7f799bd", null ],
    [ "loadSettings", "classDigikam_1_1NRSettings.html#a7943abb9f2e68bc28aed42bb53dfe124", null ],
    [ "readSettings", "classDigikam_1_1NRSettings.html#a748d0a9c93cd63b4759d280c0b8e6dff", null ],
    [ "resetToDefault", "classDigikam_1_1NRSettings.html#a7dc7144571b4b92fafc683c214b15147", null ],
    [ "saveAsSettings", "classDigikam_1_1NRSettings.html#a7594a90894e9dce603d9d239c5f71bec", null ],
    [ "setEstimateNoise", "classDigikam_1_1NRSettings.html#ac4d081b65c53ac501fc99a22a33ba773", null ],
    [ "setSettings", "classDigikam_1_1NRSettings.html#a85bc78f266b0f2a810f429b6f3b0b75d", null ],
    [ "settings", "classDigikam_1_1NRSettings.html#aba141570651f24c306511b5b2fa096ad", null ],
    [ "signalEstimateNoise", "classDigikam_1_1NRSettings.html#a65991b176080b027984f971dc48822ec", null ],
    [ "signalSettingsChanged", "classDigikam_1_1NRSettings.html#ab152bfc7f2ed31de3a19f74f2f2b5844", null ],
    [ "writeSettings", "classDigikam_1_1NRSettings.html#aa2c587cc2ffd197f7b612b1bb82efcbb", null ]
];