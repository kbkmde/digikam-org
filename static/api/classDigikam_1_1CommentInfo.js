var classDigikam_1_1CommentInfo =
[
    [ "CommentInfo", "classDigikam_1_1CommentInfo.html#a7822dbbb6e65f17444b144506aa4cd60", null ],
    [ "isNull", "classDigikam_1_1CommentInfo.html#a6b7ede654907b449f2d7fbaa96fa43e8", null ],
    [ "author", "classDigikam_1_1CommentInfo.html#a2d675f4ef6d6eaa827f8efbd9fcfe186", null ],
    [ "comment", "classDigikam_1_1CommentInfo.html#a78351af72244b6a045b4a21e93e497ea", null ],
    [ "date", "classDigikam_1_1CommentInfo.html#a5fe7bf605bc945076beb9050b79ecdbb", null ],
    [ "id", "classDigikam_1_1CommentInfo.html#aee78d749a784a1f4fac7493c75fa1af1", null ],
    [ "imageId", "classDigikam_1_1CommentInfo.html#a767d1ff5da9af6b6c071a7bdd546c1b5", null ],
    [ "language", "classDigikam_1_1CommentInfo.html#a902667be3f7b0317923edbc41e606f4a", null ],
    [ "type", "classDigikam_1_1CommentInfo.html#aae91fcac6837df5a5661c3b6122c1c82", null ]
];