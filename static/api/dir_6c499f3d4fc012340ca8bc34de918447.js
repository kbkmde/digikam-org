var dir_6c499f3d4fc012340ca8bc34de918447 =
[
    [ "dimgloader.cpp", "dimgloader_8cpp.html", null ],
    [ "dimgloader.h", "dimgloader_8h.html", [
      [ "DImgLoader", "classDigikam_1_1DImgLoader.html", "classDigikam_1_1DImgLoader" ]
    ] ],
    [ "dimgloaderobserver.h", "dimgloaderobserver_8h.html", [
      [ "DImgLoaderObserver", "classDigikam_1_1DImgLoaderObserver.html", "classDigikam_1_1DImgLoaderObserver" ]
    ] ],
    [ "heifsettings.cpp", "heifsettings_8cpp.html", null ],
    [ "heifsettings.h", "heifsettings_8h.html", [
      [ "HEIFSettings", "classDigikam_1_1HEIFSettings.html", "classDigikam_1_1HEIFSettings" ]
    ] ],
    [ "jp2ksettings.cpp", "jp2ksettings_8cpp.html", null ],
    [ "jp2ksettings.h", "jp2ksettings_8h.html", [
      [ "JP2KSettings", "classDigikam_1_1JP2KSettings.html", "classDigikam_1_1JP2KSettings" ]
    ] ],
    [ "jpegsettings.cpp", "jpegsettings_8cpp.html", null ],
    [ "jpegsettings.h", "jpegsettings_8h.html", [
      [ "JPEGSettings", "classDigikam_1_1JPEGSettings.html", "classDigikam_1_1JPEGSettings" ]
    ] ],
    [ "pgfsettings.cpp", "pgfsettings_8cpp.html", null ],
    [ "pgfsettings.h", "pgfsettings_8h.html", [
      [ "PGFSettings", "classDigikam_1_1PGFSettings.html", "classDigikam_1_1PGFSettings" ]
    ] ],
    [ "pngsettings.cpp", "pngsettings_8cpp.html", null ],
    [ "pngsettings.h", "pngsettings_8h.html", [
      [ "PNGSettings", "classDigikam_1_1PNGSettings.html", "classDigikam_1_1PNGSettings" ]
    ] ],
    [ "tiffsettings.cpp", "tiffsettings_8cpp.html", null ],
    [ "tiffsettings.h", "tiffsettings_8h.html", [
      [ "TIFFSettings", "classDigikam_1_1TIFFSettings.html", "classDigikam_1_1TIFFSettings" ]
    ] ]
];