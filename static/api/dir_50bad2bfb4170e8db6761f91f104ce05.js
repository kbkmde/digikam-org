var dir_50bad2bfb4170e8db6761f91f104ce05 =
[
    [ "dngsettings.cpp", "dngsettings_8cpp.html", null ],
    [ "dngsettings.h", "dngsettings_8h.html", [
      [ "DNGSettings", "classDigikam_1_1DNGSettings.html", "classDigikam_1_1DNGSettings" ]
    ] ],
    [ "dngwriter.cpp", "dngwriter_8cpp.html", null ],
    [ "dngwriter.h", "dngwriter_8h.html", [
      [ "DNGWriter", "classDigikam_1_1DNGWriter.html", "classDigikam_1_1DNGWriter" ]
    ] ],
    [ "dngwriter_backupraw.cpp", "dngwriter__backupraw_8cpp.html", null ],
    [ "dngwriter_convert.cpp", "dngwriter__convert_8cpp.html", null ],
    [ "dngwriter_exif.cpp", "dngwriter__exif_8cpp.html", null ],
    [ "dngwriter_export.cpp", "dngwriter__export_8cpp.html", null ],
    [ "dngwriter_import.cpp", "dngwriter__import_8cpp.html", null ],
    [ "dngwriter_makernote.cpp", "dngwriter__makernote_8cpp.html", null ],
    [ "dngwriter_mosaic.cpp", "dngwriter__mosaic_8cpp.html", null ],
    [ "dngwriter_negative.cpp", "dngwriter__negative_8cpp.html", null ],
    [ "dngwriter_p.cpp", "dngwriter__p_8cpp.html", null ],
    [ "dngwriter_p.h", "dngwriter__p_8h.html", "dngwriter__p_8h" ],
    [ "dngwriter_postprocess.cpp", "dngwriter__postprocess_8cpp.html", null ],
    [ "dngwriter_xmp.cpp", "dngwriter__xmp_8cpp.html", null ],
    [ "dngwriterhost.cpp", "dngwriterhost_8cpp.html", null ],
    [ "dngwriterhost.h", "dngwriterhost_8h.html", [
      [ "DNGWriterHost", "classDigikam_1_1DNGWriterHost.html", "classDigikam_1_1DNGWriterHost" ]
    ] ]
];