var classDigikamGenericMjpegStreamPlugin_1_1MjpegServerMngr =
[
    [ "albumsShared", "classDigikamGenericMjpegStreamPlugin_1_1MjpegServerMngr.html#aab0830f22fdba0a72dd2565b7ac87b3c", null ],
    [ "cleanUp", "classDigikamGenericMjpegStreamPlugin_1_1MjpegServerMngr.html#aa1477d1ad194ad7e858e167861beafbc", null ],
    [ "collectionMap", "classDigikamGenericMjpegStreamPlugin_1_1MjpegServerMngr.html#ad79c5630758e8423ad8a8dfd3d37f348", null ],
    [ "configGroupName", "classDigikamGenericMjpegStreamPlugin_1_1MjpegServerMngr.html#a61729325889638bc422e81aa0ab4bd65", null ],
    [ "configStartServerOnStartupEntry", "classDigikamGenericMjpegStreamPlugin_1_1MjpegServerMngr.html#a9c0b67e379c25e262f00119cd9e0512b", null ],
    [ "isRunning", "classDigikamGenericMjpegStreamPlugin_1_1MjpegServerMngr.html#abe4796ea2b1f79b92bb741d9ed5defb5", null ],
    [ "itemsList", "classDigikamGenericMjpegStreamPlugin_1_1MjpegServerMngr.html#ac16cd15dd3d3145328da9eb4a0bbd430", null ],
    [ "itemsShared", "classDigikamGenericMjpegStreamPlugin_1_1MjpegServerMngr.html#a55356bd7377e4ef2a601419f594f4429", null ],
    [ "load", "classDigikamGenericMjpegStreamPlugin_1_1MjpegServerMngr.html#a7a936e055cbe56512b80fe54f39bd2f4", null ],
    [ "loadAtStartup", "classDigikamGenericMjpegStreamPlugin_1_1MjpegServerMngr.html#aab6bcfbd3083002cbc911553f7e0a34b", null ],
    [ "mjpegServerNotification", "classDigikamGenericMjpegStreamPlugin_1_1MjpegServerMngr.html#aa47411b797ca1da3067267d74e87eade", null ],
    [ "save", "classDigikamGenericMjpegStreamPlugin_1_1MjpegServerMngr.html#a1370e558e370db9e2c9ab641bf2d73e0", null ],
    [ "saveAtShutdown", "classDigikamGenericMjpegStreamPlugin_1_1MjpegServerMngr.html#aa25338911fdf02b07582313ef079864d", null ],
    [ "setCollectionMap", "classDigikamGenericMjpegStreamPlugin_1_1MjpegServerMngr.html#a4e65cbe102b0a69f34a0b1289496fc7e", null ],
    [ "setItemsList", "classDigikamGenericMjpegStreamPlugin_1_1MjpegServerMngr.html#a77dca11e2f8be10ce784510584fb21b4", null ],
    [ "setSettings", "classDigikamGenericMjpegStreamPlugin_1_1MjpegServerMngr.html#a781c79cc1728418859d2a6f836b5f6c7", null ],
    [ "settings", "classDigikamGenericMjpegStreamPlugin_1_1MjpegServerMngr.html#a061a9894524a957d872e683b5b419118", null ],
    [ "startMjpegServer", "classDigikamGenericMjpegStreamPlugin_1_1MjpegServerMngr.html#aebffda7ee1fcc308e582c4e0c1d22d9f", null ],
    [ "MjpegServerMngrCreator", "classDigikamGenericMjpegStreamPlugin_1_1MjpegServerMngr.html#a11373aac02b522af322ae15e725c29bf", null ]
];