var gswidget_8h =
[
    [ "GSWidget", "classDigikamGenericGoogleServicesPlugin_1_1GSWidget.html", "classDigikamGenericGoogleServicesPlugin_1_1GSWidget" ],
    [ "GPhotoTagsBehaviour", "gswidget_8h.html#a3267ee910bc207f9d99cb7670881b943", [
      [ "GPTagLeaf", "gswidget_8h.html#a3267ee910bc207f9d99cb7670881b943abe37d07ad01b70550a20be3d9a489ecd", null ],
      [ "GPTagSplit", "gswidget_8h.html#a3267ee910bc207f9d99cb7670881b943ae7895ba44b9ba18dada7c6970d21ac58", null ],
      [ "GPTagCombined", "gswidget_8h.html#a3267ee910bc207f9d99cb7670881b943af8e9912514fb3fc208a6e3e848129c81", null ]
    ] ]
];