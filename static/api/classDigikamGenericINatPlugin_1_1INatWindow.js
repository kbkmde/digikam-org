var classDigikamGenericINatPlugin_1_1INatWindow =
[
    [ "INatWindow", "classDigikamGenericINatPlugin_1_1INatWindow.html#adde55902e76c6a9e844a3125ef1319fc", null ],
    [ "~INatWindow", "classDigikamGenericINatPlugin_1_1INatWindow.html#a43f45c260b411d03c0fd43681fb88905", null ],
    [ "addButton", "classDigikamGenericINatPlugin_1_1INatWindow.html#a338ad1ca1d39489c83775e3286302c59", null ],
    [ "cancelClicked", "classDigikamGenericINatPlugin_1_1INatWindow.html#a6f9919249e2e8aa237fabb2ed72e75a2", null ],
    [ "reactivate", "classDigikamGenericINatPlugin_1_1INatWindow.html#a50da45044d0966dd27a2ebf1e57e935e", null ],
    [ "restoreDialogSize", "classDigikamGenericINatPlugin_1_1INatWindow.html#a5da66f5eb64468e9524230138e09c2a2", null ],
    [ "saveDialogSize", "classDigikamGenericINatPlugin_1_1INatWindow.html#a5496bc5fd80e57edd35e652dbb8361ae", null ],
    [ "setItemsList", "classDigikamGenericINatPlugin_1_1INatWindow.html#a457921a32bae22480aeb7ec08f4d8b98", null ],
    [ "setMainWidget", "classDigikamGenericINatPlugin_1_1INatWindow.html#ae956aceb6ae24079791e2f491957d446", null ],
    [ "setPlugin", "classDigikamGenericINatPlugin_1_1INatWindow.html#a8f9473a19990a8001d98cde6286d1059", null ],
    [ "setRejectButtonMode", "classDigikamGenericINatPlugin_1_1INatWindow.html#a32667efc4f6a02e644886729952fd354", null ],
    [ "startButton", "classDigikamGenericINatPlugin_1_1INatWindow.html#a34e7c6deb1458c37066862375a487600", null ],
    [ "m_buttons", "classDigikamGenericINatPlugin_1_1INatWindow.html#a06170b9f45856493f30cb0edd368a3c0", null ]
];