var pointtransformaffine_8h =
[
    [ "PointTransformAffine", "classDigikam_1_1PointTransformAffine.html", "classDigikam_1_1PointTransformAffine" ],
    [ "findAffineTransform", "pointtransformaffine_8h.html#a99aaefcd219ef1e59f1aecf9a16a9a02", null ],
    [ "findSimilarityTransform", "pointtransformaffine_8h.html#a6719f88ea6b1415b5a00069c982717b4", null ],
    [ "inv", "pointtransformaffine_8h.html#a3a490b30bd0adc09fa9dbc9b33929ee7", null ],
    [ "operator*", "pointtransformaffine_8h.html#a568ff063946343728348326e43ae7eb7", null ]
];