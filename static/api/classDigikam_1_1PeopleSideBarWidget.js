var classDigikam_1_1PeopleSideBarWidget =
[
    [ "StateSavingDepth", "classDigikam_1_1PeopleSideBarWidget.html#abdf8ad03070bc560242e56575909d6b6", [
      [ "INSTANCE", "classDigikam_1_1PeopleSideBarWidget.html#abdf8ad03070bc560242e56575909d6b6a535f22ead553f373020dce094f0ae735", null ],
      [ "DIRECT_CHILDREN", "classDigikam_1_1PeopleSideBarWidget.html#abdf8ad03070bc560242e56575909d6b6a402f3f077dcfc1835196020f1986fb55", null ],
      [ "RECURSIVE", "classDigikam_1_1PeopleSideBarWidget.html#abdf8ad03070bc560242e56575909d6b6a1d31a55830067a26a4c25eabb48f6d4f", null ]
    ] ],
    [ "PeopleSideBarWidget", "classDigikam_1_1PeopleSideBarWidget.html#a25135fe8ef51a2abc19ef2b8a4b67344", null ],
    [ "~PeopleSideBarWidget", "classDigikam_1_1PeopleSideBarWidget.html#a436cee9289d3dfdad27f3a74a932d0e6", null ],
    [ "applySettings", "classDigikam_1_1PeopleSideBarWidget.html#a9f4273f9f9e21741812341c0b87ee41e", null ],
    [ "changeAlbumFromHistory", "classDigikam_1_1PeopleSideBarWidget.html#adb86b49207e9641892b4d17ebc2ccaee", null ],
    [ "doLoadState", "classDigikam_1_1PeopleSideBarWidget.html#a89de629b63fcc9adc4b57c56fdeb912d", null ],
    [ "doSaveState", "classDigikam_1_1PeopleSideBarWidget.html#a2cb132390710cefac2fb54bf3c631159", null ],
    [ "entryName", "classDigikam_1_1PeopleSideBarWidget.html#a226ce9b9faa055120ba386b85509bcdf", null ],
    [ "getCaption", "classDigikam_1_1PeopleSideBarWidget.html#af2a3da3508e4e8769d8dab36f1eba2ac", null ],
    [ "getConfigGroup", "classDigikam_1_1PeopleSideBarWidget.html#a3dce4d65e29a07ea09d2e7b4746f8350", null ],
    [ "getIcon", "classDigikam_1_1PeopleSideBarWidget.html#a4ff7cc9e0ab908ce83c67586519b26d3", null ],
    [ "getStateSavingDepth", "classDigikam_1_1PeopleSideBarWidget.html#a340090725416a1b9030c67d7d6a77d6e", null ],
    [ "loadState", "classDigikam_1_1PeopleSideBarWidget.html#a884a7c5ec2c82c6b61519a6c035577c2", null ],
    [ "requestActiveTab", "classDigikam_1_1PeopleSideBarWidget.html#a247452550423658b7ca3f5ba97fb1f07", null ],
    [ "requestFaceMode", "classDigikam_1_1PeopleSideBarWidget.html#ac580f1ffed750edb5ce8156b5fc195d2", null ],
    [ "saveState", "classDigikam_1_1PeopleSideBarWidget.html#a0db6b9c2daff1b521bce63f78d8f1652", null ],
    [ "setActive", "classDigikam_1_1PeopleSideBarWidget.html#a1d7cc82b14148d7ece2bf337e114a696", null ],
    [ "setConfigGroup", "classDigikam_1_1PeopleSideBarWidget.html#aa37c59ac6e91ba60c3c0dd14cc6fa71e", null ],
    [ "setEntryPrefix", "classDigikam_1_1PeopleSideBarWidget.html#ac289161ea3144b78c348c283cbb7777d", null ],
    [ "setStateSavingDepth", "classDigikam_1_1PeopleSideBarWidget.html#a1023eaabd6d22bcfcd83b0c7f5c47e9b", null ],
    [ "signalFindDuplicates", "classDigikam_1_1PeopleSideBarWidget.html#abb3a6f5c4b349d8def43946e0ca1f66a", null ],
    [ "signalNotificationError", "classDigikam_1_1PeopleSideBarWidget.html#a12e1b990cb7a7f652ce6f10df1ccfc11", null ]
];