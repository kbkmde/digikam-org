var classDigikamGenericMetadataEditPlugin_1_1XMPProperties =
[
    [ "XMPProperties", "classDigikamGenericMetadataEditPlugin_1_1XMPProperties.html#a7e255be32f76cffc75cf8b3850f4d0bb", null ],
    [ "~XMPProperties", "classDigikamGenericMetadataEditPlugin_1_1XMPProperties.html#acfafbc33737da8d6dc3b114727cadb1c", null ],
    [ "applyMetadata", "classDigikamGenericMetadataEditPlugin_1_1XMPProperties.html#ae86755700d91c6a5f8fd0a40687aa680", null ],
    [ "readMetadata", "classDigikamGenericMetadataEditPlugin_1_1XMPProperties.html#afbfda741b75116f6238017ce62dbbebb", null ],
    [ "signalModified", "classDigikamGenericMetadataEditPlugin_1_1XMPProperties.html#acb8ac3dee2e38ade9b6f0a65ae850174", null ]
];