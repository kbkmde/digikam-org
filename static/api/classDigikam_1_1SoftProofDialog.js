var classDigikam_1_1SoftProofDialog =
[
    [ "SoftProofDialog", "classDigikam_1_1SoftProofDialog.html#a492dbcdf90b7d41972c71510c4116b3e", null ],
    [ "~SoftProofDialog", "classDigikam_1_1SoftProofDialog.html#ad42dff771f3341cf0177c0ee54982def", null ],
    [ "readSettings", "classDigikam_1_1SoftProofDialog.html#af05857526495eef99520e446ca0250a5", null ],
    [ "shallEnableSoftProofView", "classDigikam_1_1SoftProofDialog.html#a7f2fe8976492060e2c733ba371db1f07", null ],
    [ "updateGamutCheckState", "classDigikam_1_1SoftProofDialog.html#ad73185a152a4351e5ff28f519e2f4c29", null ],
    [ "updateOkButtonState", "classDigikam_1_1SoftProofDialog.html#a5b2abee37e2e1bc3d0ea7bcec7cc2cf4", null ],
    [ "writeSettings", "classDigikam_1_1SoftProofDialog.html#a4dc16da1a3036928351cb0cb42b356f8", null ]
];