var dir_1ff09a6d39e243d390d2cfdb52bac7eb =
[
    [ "slideend.cpp", "slideend_8cpp.html", null ],
    [ "slideend.h", "slideend_8h.html", [
      [ "SlideEnd", "classDigikamGenericSlideShowPlugin_1_1SlideEnd.html", "classDigikamGenericSlideShowPlugin_1_1SlideEnd" ]
    ] ],
    [ "slideerror.cpp", "slideerror_8cpp.html", null ],
    [ "slideerror.h", "slideerror_8h.html", [
      [ "SlideError", "classDigikamGenericSlideShowPlugin_1_1SlideError.html", "classDigikamGenericSlideShowPlugin_1_1SlideError" ]
    ] ],
    [ "slideimage.cpp", "slideimage_8cpp.html", null ],
    [ "slideimage.h", "slideimage_8h.html", [
      [ "SlideImage", "classDigikamGenericSlideShowPlugin_1_1SlideImage.html", "classDigikamGenericSlideShowPlugin_1_1SlideImage" ]
    ] ],
    [ "slideosd.cpp", "slideosd_8cpp.html", null ],
    [ "slideosd.h", "slideosd_8h.html", [
      [ "SlideOSD", "classDigikamGenericSlideShowPlugin_1_1SlideOSD.html", "classDigikamGenericSlideShowPlugin_1_1SlideOSD" ]
    ] ],
    [ "slideproperties.cpp", "slideproperties_8cpp.html", null ],
    [ "slideproperties.h", "slideproperties_8h.html", [
      [ "SlideProperties", "classDigikamGenericSlideShowPlugin_1_1SlideProperties.html", "classDigikamGenericSlideShowPlugin_1_1SlideProperties" ]
    ] ],
    [ "slidetoolbar.cpp", "slidetoolbar_8cpp.html", null ],
    [ "slidetoolbar.h", "slidetoolbar_8h.html", [
      [ "SlideToolBar", "classDigikamGenericSlideShowPlugin_1_1SlideToolBar.html", "classDigikamGenericSlideShowPlugin_1_1SlideToolBar" ]
    ] ]
];