var dir_0d7adc5bafc3d7c0e1ba8b593af7481f =
[
    [ "piwigoitem.h", "piwigoitem_8h.html", [
      [ "PiwigoAlbum", "classDigikamGenericPiwigoPlugin_1_1PiwigoAlbum.html", "classDigikamGenericPiwigoPlugin_1_1PiwigoAlbum" ]
    ] ],
    [ "piwigologindlg.cpp", "piwigologindlg_8cpp.html", null ],
    [ "piwigologindlg.h", "piwigologindlg_8h.html", [
      [ "PiwigoLoginDlg", "classDigikamGenericPiwigoPlugin_1_1PiwigoLoginDlg.html", "classDigikamGenericPiwigoPlugin_1_1PiwigoLoginDlg" ]
    ] ],
    [ "piwigoplugin.cpp", "piwigoplugin_8cpp.html", null ],
    [ "piwigoplugin.h", "piwigoplugin_8h.html", "piwigoplugin_8h" ],
    [ "piwigosession.cpp", "piwigosession_8cpp.html", null ],
    [ "piwigosession.h", "piwigosession_8h.html", [
      [ "PiwigoSession", "classDigikamGenericPiwigoPlugin_1_1PiwigoSession.html", "classDigikamGenericPiwigoPlugin_1_1PiwigoSession" ]
    ] ],
    [ "piwigotalker.cpp", "piwigotalker_8cpp.html", null ],
    [ "piwigotalker.h", "piwigotalker_8h.html", [
      [ "PiwigoTalker", "classDigikamGenericPiwigoPlugin_1_1PiwigoTalker.html", "classDigikamGenericPiwigoPlugin_1_1PiwigoTalker" ]
    ] ],
    [ "piwigowindow.cpp", "piwigowindow_8cpp.html", null ],
    [ "piwigowindow.h", "piwigowindow_8h.html", [
      [ "PiwigoWindow", "classDigikamGenericPiwigoPlugin_1_1PiwigoWindow.html", "classDigikamGenericPiwigoPlugin_1_1PiwigoWindow" ]
    ] ]
];