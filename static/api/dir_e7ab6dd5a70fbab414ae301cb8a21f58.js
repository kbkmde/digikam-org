var dir_e7ab6dd5a70fbab414ae301cb8a21f58 =
[
    [ "showfotostackviewfavoriteitem.cpp", "showfotostackviewfavoriteitem_8cpp.html", null ],
    [ "showfotostackviewfavoriteitem.h", "showfotostackviewfavoriteitem_8h.html", [
      [ "ShowfotoStackViewFavoriteItem", "classShowFoto_1_1ShowfotoStackViewFavoriteItem.html", "classShowFoto_1_1ShowfotoStackViewFavoriteItem" ]
    ] ],
    [ "showfotostackviewfavoriteitemdlg.cpp", "showfotostackviewfavoriteitemdlg_8cpp.html", null ],
    [ "showfotostackviewfavoriteitemdlg.h", "showfotostackviewfavoriteitemdlg_8h.html", [
      [ "ShowfotoStackViewFavoriteItemDlg", "classShowFoto_1_1ShowfotoStackViewFavoriteItemDlg.html", "classShowFoto_1_1ShowfotoStackViewFavoriteItemDlg" ]
    ] ],
    [ "showfotostackviewfavoritelist.cpp", "showfotostackviewfavoritelist_8cpp.html", null ],
    [ "showfotostackviewfavoritelist.h", "showfotostackviewfavoritelist_8h.html", [
      [ "ShowfotoStackViewFavoriteList", "classShowFoto_1_1ShowfotoStackViewFavoriteList.html", "classShowFoto_1_1ShowfotoStackViewFavoriteList" ]
    ] ],
    [ "showfotostackviewfavorites.cpp", "showfotostackviewfavorites_8cpp.html", null ],
    [ "showfotostackviewfavorites.h", "showfotostackviewfavorites_8h.html", [
      [ "ShowfotoStackViewFavorites", "classShowFoto_1_1ShowfotoStackViewFavorites.html", "classShowFoto_1_1ShowfotoStackViewFavorites" ]
    ] ],
    [ "showfotostackviewitem.cpp", "showfotostackviewitem_8cpp.html", null ],
    [ "showfotostackviewitem.h", "showfotostackviewitem_8h.html", [
      [ "ShowfotoStackViewItem", "classShowFoto_1_1ShowfotoStackViewItem.html", "classShowFoto_1_1ShowfotoStackViewItem" ]
    ] ],
    [ "showfotostackviewlist.cpp", "showfotostackviewlist_8cpp.html", null ],
    [ "showfotostackviewlist.h", "showfotostackviewlist_8h.html", [
      [ "ShowfotoStackViewList", "classShowFoto_1_1ShowfotoStackViewList.html", "classShowFoto_1_1ShowfotoStackViewList" ]
    ] ],
    [ "showfotostackviewsidebar.cpp", "showfotostackviewsidebar_8cpp.html", null ],
    [ "showfotostackviewsidebar.h", "showfotostackviewsidebar_8h.html", [
      [ "ShowfotoStackViewSideBar", "classShowFoto_1_1ShowfotoStackViewSideBar.html", "classShowFoto_1_1ShowfotoStackViewSideBar" ]
    ] ],
    [ "showfotostackviewtooltip.cpp", "showfotostackviewtooltip_8cpp.html", null ],
    [ "showfotostackviewtooltip.h", "showfotostackviewtooltip_8h.html", [
      [ "ShowfotoStackViewToolTip", "classShowFoto_1_1ShowfotoStackViewToolTip.html", "classShowFoto_1_1ShowfotoStackViewToolTip" ]
    ] ]
];