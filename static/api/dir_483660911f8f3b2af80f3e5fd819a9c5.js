var dir_483660911f8f3b2af80f3e5fd819a9c5 =
[
    [ "dlnaserver.cpp", "dlnaserver_8cpp.html", null ],
    [ "dlnaserver.h", "dlnaserver_8h.html", [
      [ "DLNAMediaServer", "classDigikamGenericMediaServerPlugin_1_1DLNAMediaServer.html", "classDigikamGenericMediaServerPlugin_1_1DLNAMediaServer" ]
    ] ],
    [ "dlnaserverdelegate.cpp", "dlnaserverdelegate_8cpp.html", null ],
    [ "dlnaserverdelegate.h", "dlnaserverdelegate_8h.html", [
      [ "DLNAMediaServerDelegate", "classDigikamGenericMediaServerPlugin_1_1DLNAMediaServerDelegate.html", "classDigikamGenericMediaServerPlugin_1_1DLNAMediaServerDelegate" ]
    ] ],
    [ "dmediaserver.cpp", "dmediaserver_8cpp.html", "dmediaserver_8cpp" ],
    [ "dmediaserver.h", "dmediaserver_8h.html", "dmediaserver_8h" ],
    [ "dmediaservermngr.cpp", "dmediaservermngr_8cpp.html", null ],
    [ "dmediaservermngr.h", "dmediaservermngr_8h.html", [
      [ "DMediaServerMngr", "classDigikamGenericMediaServerPlugin_1_1DMediaServerMngr.html", "classDigikamGenericMediaServerPlugin_1_1DMediaServerMngr" ]
    ] ]
];