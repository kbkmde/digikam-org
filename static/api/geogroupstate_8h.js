var geogroupstate_8h =
[
    [ "GeoGroupStateEnum", "geogroupstate_8h.html#a7e6675475bf1628170d244b1122eaf2e", [
      [ "SelectedMask", "geogroupstate_8h.html#a7e6675475bf1628170d244b1122eaf2ea25d5d092c3be795757d09c3c4fcf8c5f", null ],
      [ "SelectedNone", "geogroupstate_8h.html#a7e6675475bf1628170d244b1122eaf2ea057af187eacd67f5df77b6913ec97ffc", null ],
      [ "SelectedSome", "geogroupstate_8h.html#a7e6675475bf1628170d244b1122eaf2ea5585982178ab1825613820abea5e4648", null ],
      [ "SelectedAll", "geogroupstate_8h.html#a7e6675475bf1628170d244b1122eaf2ea3700fada2a4aa08f17f01e9753b58def", null ],
      [ "FilteredPositiveMask", "geogroupstate_8h.html#a7e6675475bf1628170d244b1122eaf2ea133f097819b3e3ca08c8512942442a96", null ],
      [ "FilteredPositiveNone", "geogroupstate_8h.html#a7e6675475bf1628170d244b1122eaf2eaeaebc883254186701bda48c3a8cf0431", null ],
      [ "FilteredPositiveSome", "geogroupstate_8h.html#a7e6675475bf1628170d244b1122eaf2ea9649305523c54b5c7b241ff10a639a21", null ],
      [ "FilteredPositiveAll", "geogroupstate_8h.html#a7e6675475bf1628170d244b1122eaf2ea1ce82eeaefdb249a6ddfde6e7aa3b41a", null ],
      [ "RegionSelectedMask", "geogroupstate_8h.html#a7e6675475bf1628170d244b1122eaf2ea71bae027e227c4c33e3baba8b47953ef", null ],
      [ "RegionSelectedNone", "geogroupstate_8h.html#a7e6675475bf1628170d244b1122eaf2ea6a3b8e0e060318685c29e6d29a7e1560", null ],
      [ "RegionSelectedSome", "geogroupstate_8h.html#a7e6675475bf1628170d244b1122eaf2ea2ecaa5282f421ba60f1cc0c20f8a23ea", null ],
      [ "RegionSelectedAll", "geogroupstate_8h.html#a7e6675475bf1628170d244b1122eaf2ea5698e4459b2ab397f014a9d3164b4416", null ]
    ] ]
];