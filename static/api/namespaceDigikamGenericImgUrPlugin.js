var namespaceDigikamGenericImgUrPlugin =
[
    [ "ImgurImageListViewItem", "classDigikamGenericImgUrPlugin_1_1ImgurImageListViewItem.html", "classDigikamGenericImgUrPlugin_1_1ImgurImageListViewItem" ],
    [ "ImgurImagesList", "classDigikamGenericImgUrPlugin_1_1ImgurImagesList.html", "classDigikamGenericImgUrPlugin_1_1ImgurImagesList" ],
    [ "ImgUrPlugin", "classDigikamGenericImgUrPlugin_1_1ImgUrPlugin.html", "classDigikamGenericImgUrPlugin_1_1ImgUrPlugin" ],
    [ "ImgurTalker", "classDigikamGenericImgUrPlugin_1_1ImgurTalker.html", "classDigikamGenericImgUrPlugin_1_1ImgurTalker" ],
    [ "ImgurTalkerAction", "structDigikamGenericImgUrPlugin_1_1ImgurTalkerAction.html", "structDigikamGenericImgUrPlugin_1_1ImgurTalkerAction" ],
    [ "ImgurTalkerResult", "structDigikamGenericImgUrPlugin_1_1ImgurTalkerResult.html", "structDigikamGenericImgUrPlugin_1_1ImgurTalkerResult" ],
    [ "ImgurWindow", "classDigikamGenericImgUrPlugin_1_1ImgurWindow.html", "classDigikamGenericImgUrPlugin_1_1ImgurWindow" ],
    [ "ImgurTalkerActionType", "namespaceDigikamGenericImgUrPlugin.html#adb4f09fb742804c207446a8217c933b1", [
      [ "ACCT_INFO", "namespaceDigikamGenericImgUrPlugin.html#adb4f09fb742804c207446a8217c933b1a75430febbb2095058bd164955fb19f3d", null ],
      [ "IMG_UPLOAD", "namespaceDigikamGenericImgUrPlugin.html#adb4f09fb742804c207446a8217c933b1a24f90f807f8e88369a150919f60e2097", null ],
      [ "ANON_IMG_UPLOAD", "namespaceDigikamGenericImgUrPlugin.html#adb4f09fb742804c207446a8217c933b1aa90a0fdb9c039b11fd5bf66c5c9557e1", null ]
    ] ]
];