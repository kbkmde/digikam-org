var classDigikam_1_1AdvancedSettings =
[
    [ "AdvancedSettings", "classDigikam_1_1AdvancedSettings.html#af8fbc91ea7e69c40dd07ab1bbb0dc9d7", null ],
    [ "~AdvancedSettings", "classDigikam_1_1AdvancedSettings.html#a36cae0dd6379f0bda6242e8945912663", null ],
    [ "readSettings", "classDigikam_1_1AdvancedSettings.html#a88854e925d6c552d40f701751288e245", null ],
    [ "saveSettings", "classDigikam_1_1AdvancedSettings.html#a6bde7b5f1170c61a6e00cc17ea657c65", null ],
    [ "settings", "classDigikam_1_1AdvancedSettings.html#aea73cf9835f85aa9256e7051250f62d2", null ],
    [ "signalDownloadNameChanged", "classDigikam_1_1AdvancedSettings.html#a214e206aeb98ed8371203aac89083d3d", null ]
];