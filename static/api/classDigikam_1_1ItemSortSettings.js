var classDigikam_1_1ItemSortSettings =
[
    [ "CategorizationMode", "classDigikam_1_1ItemSortSettings.html#aaa30aea5c469e84c50d83af736e9b9a5", [
      [ "NoCategories", "classDigikam_1_1ItemSortSettings.html#aaa30aea5c469e84c50d83af736e9b9a5ad87feb5cb09ee4fce72d39beb41e627f", null ],
      [ "OneCategory", "classDigikam_1_1ItemSortSettings.html#aaa30aea5c469e84c50d83af736e9b9a5ac8dd7da496618e3e7859888f45e70e72", null ],
      [ "CategoryByAlbum", "classDigikam_1_1ItemSortSettings.html#aaa30aea5c469e84c50d83af736e9b9a5afb49d5d55bba842dc34a4d9db0d1067f", null ],
      [ "CategoryByFormat", "classDigikam_1_1ItemSortSettings.html#aaa30aea5c469e84c50d83af736e9b9a5af93e042e24ae500874680e2b5df0da13", null ],
      [ "CategoryByMonth", "classDigikam_1_1ItemSortSettings.html#aaa30aea5c469e84c50d83af736e9b9a5a926fc021a18a131c1f4972ab0f47835d", null ],
      [ "CategoryByFaces", "classDigikam_1_1ItemSortSettings.html#aaa30aea5c469e84c50d83af736e9b9a5a097ac62a2f0f68cce607547d186f8807", null ]
    ] ],
    [ "SortOrder", "classDigikam_1_1ItemSortSettings.html#af25ab053c0c3550b771144d3255ece8c", [
      [ "AscendingOrder", "classDigikam_1_1ItemSortSettings.html#af25ab053c0c3550b771144d3255ece8ca9dc9e939430eb7d9e3312ee153ea2da5", null ],
      [ "DescendingOrder", "classDigikam_1_1ItemSortSettings.html#af25ab053c0c3550b771144d3255ece8cae6081c07e4a6492421643cc5a2fd8f64", null ],
      [ "DefaultOrder", "classDigikam_1_1ItemSortSettings.html#af25ab053c0c3550b771144d3255ece8ca9946e97383b7641064631a54f4641f8f", null ]
    ] ],
    [ "SortRole", "classDigikam_1_1ItemSortSettings.html#a4f28ee9fef676ec867f9a084a58f149b", [
      [ "SortByFileName", "classDigikam_1_1ItemSortSettings.html#a4f28ee9fef676ec867f9a084a58f149ba3851c5cd80c2965fd4f2a9df86a821c0", null ],
      [ "SortByFilePath", "classDigikam_1_1ItemSortSettings.html#a4f28ee9fef676ec867f9a084a58f149baac38bc66497056c7964e399e60675cdd", null ],
      [ "SortByCreationDate", "classDigikam_1_1ItemSortSettings.html#a4f28ee9fef676ec867f9a084a58f149ba9f4de4237197bb7005eaf130043a8f52", null ],
      [ "SortByModificationDate", "classDigikam_1_1ItemSortSettings.html#a4f28ee9fef676ec867f9a084a58f149ba2b8524a3a2a3df6d3ae27326ef912a00", null ],
      [ "SortByFileSize", "classDigikam_1_1ItemSortSettings.html#a4f28ee9fef676ec867f9a084a58f149baece7a3ddce293d3ec6095db8ab65ff7d", null ],
      [ "SortByRating", "classDigikam_1_1ItemSortSettings.html#a4f28ee9fef676ec867f9a084a58f149baf73a88149b468d37c08e2821f7989d82", null ],
      [ "SortByImageSize", "classDigikam_1_1ItemSortSettings.html#a4f28ee9fef676ec867f9a084a58f149ba3d4399a05126b9e44d89ae1e0acbd36b", null ],
      [ "SortByAspectRatio", "classDigikam_1_1ItemSortSettings.html#a4f28ee9fef676ec867f9a084a58f149ba359132817f8628c5bc2ca9734b0ffb4a", null ],
      [ "SortByFaces", "classDigikam_1_1ItemSortSettings.html#a4f28ee9fef676ec867f9a084a58f149ba7e22865eab2d676ef3a56f7ba7df7c23", null ],
      [ "SortBySimilarity", "classDigikam_1_1ItemSortSettings.html#a4f28ee9fef676ec867f9a084a58f149ba3369537b22ee572e4991a8d7f0614627", null ],
      [ "SortByManualOrderAndName", "classDigikam_1_1ItemSortSettings.html#a4f28ee9fef676ec867f9a084a58f149bac1340bb2029a7560f60022c38ac6c3cb", null ],
      [ "SortByManualOrderAndDate", "classDigikam_1_1ItemSortSettings.html#a4f28ee9fef676ec867f9a084a58f149ba001adac69267f7dc13339933f5b38e1f", null ]
    ] ],
    [ "ItemSortSettings", "classDigikam_1_1ItemSortSettings.html#ae2cba1b0b8326707551dbf99d7911c7f", null ],
    [ "compare", "classDigikam_1_1ItemSortSettings.html#af385a5ab697f1cfd164f6ed8f85cb6cc", null ],
    [ "compare", "classDigikam_1_1ItemSortSettings.html#abe0a7a513807e1d0884f46f9ff284e06", null ],
    [ "compareCategories", "classDigikam_1_1ItemSortSettings.html#a4d3bea0a6b097cf5b74de505fa5af0b1", null ],
    [ "isCategorized", "classDigikam_1_1ItemSortSettings.html#a80f5e5af423db6b364c8a2c43b16034d", null ],
    [ "lessThan", "classDigikam_1_1ItemSortSettings.html#aab6737d495a21c2fe727b630c09ae0ea", null ],
    [ "lessThan", "classDigikam_1_1ItemSortSettings.html#a0efd530000b3880fd0d9835f2b6bd637", null ],
    [ "operator==", "classDigikam_1_1ItemSortSettings.html#a9fc51241d074b672d68eaf6c8e85715a", null ],
    [ "setCategorizationMode", "classDigikam_1_1ItemSortSettings.html#ad2495b98dd5a90075a12113e8387a047", null ],
    [ "setCategorizationSortOrder", "classDigikam_1_1ItemSortSettings.html#af31c004a8256a7026dbc4b6b245e09ed", null ],
    [ "setSortOrder", "classDigikam_1_1ItemSortSettings.html#a28a10a866a630e896caaa2944c01be50", null ],
    [ "setSortRole", "classDigikam_1_1ItemSortSettings.html#ac5ebe4ca570d304012249b0419e3339c", null ],
    [ "setStringTypeNatural", "classDigikam_1_1ItemSortSettings.html#a0209428666f9c9c81c0e110acbca84d2", null ],
    [ "watchFlags", "classDigikam_1_1ItemSortSettings.html#af23b9b9ff115240ca79758830d1aeb21", null ],
    [ "categorizationCaseSensitivity", "classDigikam_1_1ItemSortSettings.html#a0bd4b43f46f19f14990e45cc691c2c18", null ],
    [ "categorizationMode", "classDigikam_1_1ItemSortSettings.html#ae35c4d1d2dc2b23487890ab27dd95418", null ],
    [ "categorizationSortOrder", "classDigikam_1_1ItemSortSettings.html#ab2a3acca1a23e90e265e6587bd823c5c", null ],
    [ "currentCategorizationSortOrder", "classDigikam_1_1ItemSortSettings.html#aa325c9a109538bb879f06a2a419e3148", null ],
    [ "currentSortOrder", "classDigikam_1_1ItemSortSettings.html#a2d0b31cc90f6d4011fd114a3892b9bc1", null ],
    [ "sortCaseSensitivity", "classDigikam_1_1ItemSortSettings.html#a68c334c76ea03be24997340d925aaf17", null ],
    [ "sortOrder", "classDigikam_1_1ItemSortSettings.html#a7dd225f903198fa7bb106fddff0125b3", null ],
    [ "sortRole", "classDigikam_1_1ItemSortSettings.html#afb79e98ac0968e004593966f0a80f397", null ],
    [ "strTypeNatural", "classDigikam_1_1ItemSortSettings.html#a0fc886f31fcae20a9f0392a418959891", null ]
];