var dir_bd94b93c261756a618b5955ed78c210e =
[
    [ "refocusfilter.cpp", "refocusfilter_8cpp.html", "refocusfilter_8cpp" ],
    [ "refocusfilter.h", "refocusfilter_8h.html", [
      [ "RefocusFilter", "classDigikam_1_1RefocusFilter.html", "classDigikam_1_1RefocusFilter" ]
    ] ],
    [ "refocusmatrix.cpp", "refocusmatrix_8cpp.html", "refocusmatrix_8cpp" ],
    [ "refocusmatrix.h", "refocusmatrix_8h.html", [
      [ "CMat", "structDigikam_1_1CMat.html", "structDigikam_1_1CMat" ],
      [ "Mat", "structDigikam_1_1Mat.html", "structDigikam_1_1Mat" ],
      [ "RefocusMatrix", "classDigikam_1_1RefocusMatrix.html", null ]
    ] ],
    [ "sharpenfilter.cpp", "sharpenfilter_8cpp.html", "sharpenfilter_8cpp" ],
    [ "sharpenfilter.h", "sharpenfilter_8h.html", [
      [ "SharpenFilter", "classDigikam_1_1SharpenFilter.html", "classDigikam_1_1SharpenFilter" ]
    ] ],
    [ "sharpsettings.cpp", "sharpsettings_8cpp.html", null ],
    [ "sharpsettings.h", "sharpsettings_8h.html", [
      [ "SharpContainer", "classDigikam_1_1SharpContainer.html", "classDigikam_1_1SharpContainer" ],
      [ "SharpSettings", "classDigikam_1_1SharpSettings.html", "classDigikam_1_1SharpSettings" ]
    ] ],
    [ "unsharpmaskfilter.cpp", "unsharpmaskfilter_8cpp.html", null ],
    [ "unsharpmaskfilter.h", "unsharpmaskfilter_8h.html", [
      [ "UnsharpMaskFilter", "classDigikam_1_1UnsharpMaskFilter.html", "classDigikam_1_1UnsharpMaskFilter" ]
    ] ]
];