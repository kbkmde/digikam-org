var classDigikam_1_1FacialRecognitionWrapper_1_1Private =
[
    [ "Private", "classDigikam_1_1FacialRecognitionWrapper_1_1Private.html#a11e3d7e6f3cc1d9c9347963c56e3870a", null ],
    [ "~Private", "classDigikam_1_1FacialRecognitionWrapper_1_1Private.html#aad0f2bd67f4658552b78dd949dfea2b2", null ],
    [ "applyParameters", "classDigikam_1_1FacialRecognitionWrapper_1_1Private.html#a4fdde58831e399e0fa40cbdc55414320", null ],
    [ "clear", "classDigikam_1_1FacialRecognitionWrapper_1_1Private.html#abb0050f23358640e6cf0ced2a5591b9c", null ],
    [ "findByAttribute", "classDigikam_1_1FacialRecognitionWrapper_1_1Private.html#acbcb812ab46603860fee422637ec7666", null ],
    [ "findByAttributes", "classDigikam_1_1FacialRecognitionWrapper_1_1Private.html#aa0c356e4448282b164e071d96d37d136", null ],
    [ "trainIdentityBatch", "classDigikam_1_1FacialRecognitionWrapper_1_1Private.html#ab7b3df886d6ad6e967eaf787ed9bb8a3", null ],
    [ "dbAvailable", "classDigikam_1_1FacialRecognitionWrapper_1_1Private.html#a95399e2291c752af341adae30d914362", null ],
    [ "identityCache", "classDigikam_1_1FacialRecognitionWrapper_1_1Private.html#a329e6451ddf5541fccebc72e0440a316", null ],
    [ "mutex", "classDigikam_1_1FacialRecognitionWrapper_1_1Private.html#a9ffccf48df6be8e05d7c38eb2248afd5", null ],
    [ "parameters", "classDigikam_1_1FacialRecognitionWrapper_1_1Private.html#a51ec984039f718e0eefec8c6dc3f3f7f", null ],
    [ "recognizer", "classDigikam_1_1FacialRecognitionWrapper_1_1Private.html#a87cdf8d0b70c2439bd5c59bd903bd78f", null ],
    [ "ref", "classDigikam_1_1FacialRecognitionWrapper_1_1Private.html#adb404c0a5a471b509a7c40f1d33edb78", null ]
];