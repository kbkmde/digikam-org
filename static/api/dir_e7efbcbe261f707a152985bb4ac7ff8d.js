var dir_e7efbcbe261f707a152985bb4ac7ff8d =
[
    [ "autooptimiserbinary.cpp", "autooptimiserbinary_8cpp.html", null ],
    [ "autooptimiserbinary.h", "autooptimiserbinary_8h.html", [
      [ "AutoOptimiserBinary", "classDigikamGenericPanoramaPlugin_1_1AutoOptimiserBinary.html", "classDigikamGenericPanoramaPlugin_1_1AutoOptimiserBinary" ]
    ] ],
    [ "cpcleanbinary.cpp", "cpcleanbinary_8cpp.html", null ],
    [ "cpcleanbinary.h", "cpcleanbinary_8h.html", [
      [ "CPCleanBinary", "classDigikamGenericPanoramaPlugin_1_1CPCleanBinary.html", "classDigikamGenericPanoramaPlugin_1_1CPCleanBinary" ]
    ] ],
    [ "cpfindbinary.cpp", "cpfindbinary_8cpp.html", null ],
    [ "cpfindbinary.h", "cpfindbinary_8h.html", [
      [ "CPFindBinary", "classDigikamGenericPanoramaPlugin_1_1CPFindBinary.html", "classDigikamGenericPanoramaPlugin_1_1CPFindBinary" ]
    ] ],
    [ "enblendbinary.cpp", "enblendbinary_8cpp.html", null ],
    [ "enblendbinary.h", "enblendbinary_8h.html", [
      [ "EnblendBinary", "classDigikamGenericPanoramaPlugin_1_1EnblendBinary.html", "classDigikamGenericPanoramaPlugin_1_1EnblendBinary" ]
    ] ],
    [ "huginexecutorbinary.cpp", "huginexecutorbinary_8cpp.html", null ],
    [ "huginexecutorbinary.h", "huginexecutorbinary_8h.html", [
      [ "HuginExecutorBinary", "classDigikamGenericPanoramaPlugin_1_1HuginExecutorBinary.html", "classDigikamGenericPanoramaPlugin_1_1HuginExecutorBinary" ]
    ] ],
    [ "makebinary.cpp", "makebinary_8cpp.html", null ],
    [ "makebinary.h", "makebinary_8h.html", [
      [ "MakeBinary", "classDigikamGenericPanoramaPlugin_1_1MakeBinary.html", "classDigikamGenericPanoramaPlugin_1_1MakeBinary" ]
    ] ],
    [ "nonabinary.cpp", "nonabinary_8cpp.html", null ],
    [ "nonabinary.h", "nonabinary_8h.html", [
      [ "NonaBinary", "classDigikamGenericPanoramaPlugin_1_1NonaBinary.html", "classDigikamGenericPanoramaPlugin_1_1NonaBinary" ]
    ] ],
    [ "panomodifybinary.cpp", "panomodifybinary_8cpp.html", null ],
    [ "panomodifybinary.h", "panomodifybinary_8h.html", [
      [ "PanoModifyBinary", "classDigikamGenericPanoramaPlugin_1_1PanoModifyBinary.html", "classDigikamGenericPanoramaPlugin_1_1PanoModifyBinary" ]
    ] ],
    [ "pto2mkbinary.cpp", "pto2mkbinary_8cpp.html", null ],
    [ "pto2mkbinary.h", "pto2mkbinary_8h.html", [
      [ "Pto2MkBinary", "classDigikamGenericPanoramaPlugin_1_1Pto2MkBinary.html", "classDigikamGenericPanoramaPlugin_1_1Pto2MkBinary" ]
    ] ]
];