var classDigikam_1_1CollectionScanner_1_1Private =
[
    [ "Private", "classDigikam_1_1CollectionScanner_1_1Private.html#a031335286b83cab3cc72f627790f60f2", null ],
    [ "checkDeferred", "classDigikam_1_1CollectionScanner_1_1Private.html#a7d449d2a5415e479311d77a4b20fd30d", null ],
    [ "checkObserver", "classDigikam_1_1CollectionScanner_1_1Private.html#a08ea304caecb3d8fadeedc4ab5bb0e84", null ],
    [ "finishScanner", "classDigikam_1_1CollectionScanner_1_1Private.html#a866b2ee677731c21e7fd63b4674bedb1", null ],
    [ "removedItems", "classDigikam_1_1CollectionScanner_1_1Private.html#a9cf8c295aa89e19d3870c97ee8940ccb", null ],
    [ "resetRemovedItemsTime", "classDigikam_1_1CollectionScanner_1_1Private.html#aa17ace9d3ad91942a3dde9f58f437cd4", null ],
    [ "albumDateCache", "classDigikam_1_1CollectionScanner_1_1Private.html#adb7b4d34c8afe43b442da30bc0fdaa6c", null ],
    [ "audioFilterSet", "classDigikam_1_1CollectionScanner_1_1Private.html#aab259b45c1214e8b753dc926a9990bf9", null ],
    [ "deferredAlbumPaths", "classDigikam_1_1CollectionScanner_1_1Private.html#a5f382008cffb6bc238783ae9bf1adf83", null ],
    [ "deferredFileScanning", "classDigikam_1_1CollectionScanner_1_1Private.html#a680bceadff93b11808364f4b75a4bfcd", null ],
    [ "establishedSourceAlbums", "classDigikam_1_1CollectionScanner_1_1Private.html#a28fe9cd8620bf8753337250f6098fb32", null ],
    [ "hints", "classDigikam_1_1CollectionScanner_1_1Private.html#af8adc65abb51b76d17ac7a4d8023ebdf", null ],
    [ "ignoreDirectory", "classDigikam_1_1CollectionScanner_1_1Private.html#a0736f44945c673441ebc88a7220ba349", null ],
    [ "imageFilterSet", "classDigikam_1_1CollectionScanner_1_1Private.html#a6129fccece9a5ce0d80bd57a8f0f0176", null ],
    [ "nameFilters", "classDigikam_1_1CollectionScanner_1_1Private.html#a6f730b69ca81ff4a34bef756b2b180f9", null ],
    [ "needResolveHistorySet", "classDigikam_1_1CollectionScanner_1_1Private.html#a5e7e9edd6bc677dcfc3f964d99ad664b", null ],
    [ "needTaggingHistorySet", "classDigikam_1_1CollectionScanner_1_1Private.html#ac2a8b302a61c538bd55302fd00efa711", null ],
    [ "needTotalFiles", "classDigikam_1_1CollectionScanner_1_1Private.html#ace14a7562a40918e011ff6eda0c72273", null ],
    [ "newIdsList", "classDigikam_1_1CollectionScanner_1_1Private.html#a3d079ef92c471ccb7bec215ee6b8423e", null ],
    [ "observer", "classDigikam_1_1CollectionScanner_1_1Private.html#a6642f24524005d399279b9ade95d258f", null ],
    [ "performFastScan", "classDigikam_1_1CollectionScanner_1_1Private.html#a80614973a7fb52b2c89a286264ed77d0", null ],
    [ "recordHistoryIds", "classDigikam_1_1CollectionScanner_1_1Private.html#a3db1b63befbc4bd9715c280e1ec32d6a", null ],
    [ "removedItemsTime", "classDigikam_1_1CollectionScanner_1_1Private.html#aadf4b2c68c7d56ca3460366f55bbda50", null ],
    [ "scannedAlbums", "classDigikam_1_1CollectionScanner_1_1Private.html#a3ea34741e065eb15888eb14f0729a00e", null ],
    [ "updatingHashHint", "classDigikam_1_1CollectionScanner_1_1Private.html#ad47489b20d336781469b4f8fa2e475ed", null ],
    [ "videoFilterSet", "classDigikam_1_1CollectionScanner_1_1Private.html#a1eb5808f5d74d0f10847cc231c007cbe", null ],
    [ "wantSignals", "classDigikam_1_1CollectionScanner_1_1Private.html#aeae218a1853e2b9fb5e4c9f0dbcfc1d0", null ]
];