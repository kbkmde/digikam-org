var dir_0ac36df7177f6a00d051fd7dcc1e45db =
[
    [ "advancedmetadatatab.cpp", "advancedmetadatatab_8cpp.html", null ],
    [ "advancedmetadatatab.h", "advancedmetadatatab_8h.html", [
      [ "AdvancedMetadataTab", "classDigikam_1_1AdvancedMetadataTab.html", "classDigikam_1_1AdvancedMetadataTab" ]
    ] ],
    [ "namespaceeditdlg.cpp", "namespaceeditdlg_8cpp.html", null ],
    [ "namespaceeditdlg.h", "namespaceeditdlg_8h.html", [
      [ "NamespaceEditDlg", "classDigikam_1_1NamespaceEditDlg.html", "classDigikam_1_1NamespaceEditDlg" ]
    ] ],
    [ "namespacelistview.cpp", "namespacelistview_8cpp.html", null ],
    [ "namespacelistview.h", "namespacelistview_8h.html", [
      [ "NamespaceListView", "classDigikam_1_1NamespaceListView.html", "classDigikam_1_1NamespaceListView" ]
    ] ],
    [ "setupmetadata.cpp", "setupmetadata_8cpp.html", null ],
    [ "setupmetadata.h", "setupmetadata_8h.html", [
      [ "SetupMetadata", "classDigikam_1_1SetupMetadata.html", "classDigikam_1_1SetupMetadata" ]
    ] ],
    [ "setupmetadata_baloo.cpp", "setupmetadata__baloo_8cpp.html", null ],
    [ "setupmetadata_behavior.cpp", "setupmetadata__behavior_8cpp.html", null ],
    [ "setupmetadata_display.cpp", "setupmetadata__display_8cpp.html", null ],
    [ "setupmetadata_p.cpp", "setupmetadata__p_8cpp.html", null ],
    [ "setupmetadata_p.h", "setupmetadata__p_8h.html", [
      [ "Private", "classDigikam_1_1SetupMetadata_1_1Private.html", "classDigikam_1_1SetupMetadata_1_1Private" ]
    ] ],
    [ "setupmetadata_rotation.cpp", "setupmetadata__rotation_8cpp.html", null ],
    [ "setupmetadata_sidecars.cpp", "setupmetadata__sidecars_8cpp.html", null ]
];