var classDigikamEditorInsertTextToolPlugin_1_1InsertTextTool =
[
    [ "InsertTextTool", "classDigikamEditorInsertTextToolPlugin_1_1InsertTextTool.html#aa4970f5becbb2406acfc1eafdc9a5140", null ],
    [ "~InsertTextTool", "classDigikamEditorInsertTextToolPlugin_1_1InsertTextTool.html#a8f4742f64687862577524190cd1b3c71", null ],
    [ "cancelClicked", "classDigikamEditorInsertTextToolPlugin_1_1InsertTextTool.html#a079c4e8e0189f2b08ac97c57d39d4e1d", null ],
    [ "exposureSettingsChanged", "classDigikamEditorInsertTextToolPlugin_1_1InsertTextTool.html#a77604957de501c1286831c44c32691f3", null ],
    [ "ICCSettingsChanged", "classDigikamEditorInsertTextToolPlugin_1_1InsertTextTool.html#a9220394e71620f7262b306d2ceda49fd", null ],
    [ "init", "classDigikamEditorInsertTextToolPlugin_1_1InsertTextTool.html#ade313efb59416fe168e9d494a8261714", null ],
    [ "okClicked", "classDigikamEditorInsertTextToolPlugin_1_1InsertTextTool.html#aaf16461888d12351963e1892317b8d02", null ],
    [ "plugin", "classDigikamEditorInsertTextToolPlugin_1_1InsertTextTool.html#ac9f258dfdcc8e161b2e3e04945640053", null ],
    [ "setBusy", "classDigikamEditorInsertTextToolPlugin_1_1InsertTextTool.html#adc9455ddc0cc3e74ffd177fa85a89c2c", null ],
    [ "setInitPreview", "classDigikamEditorInsertTextToolPlugin_1_1InsertTextTool.html#ac253001bb85c9329a687fc21298e680d", null ],
    [ "setPlugin", "classDigikamEditorInsertTextToolPlugin_1_1InsertTextTool.html#af1c52946b840a515d1e15c05a077e0e6", null ],
    [ "setPreviewModeMask", "classDigikamEditorInsertTextToolPlugin_1_1InsertTextTool.html#aaf540040a7c43ac693eed06537d8c55f", null ],
    [ "setToolCategory", "classDigikamEditorInsertTextToolPlugin_1_1InsertTextTool.html#a9a2b760ae7d24d126b5e8b8295a804bf", null ],
    [ "setToolHelp", "classDigikamEditorInsertTextToolPlugin_1_1InsertTextTool.html#a5a58fa2bcc13672c3fa3f372e006a416", null ],
    [ "setToolIcon", "classDigikamEditorInsertTextToolPlugin_1_1InsertTextTool.html#aab748c0d89f3791f1096867dcc03a0ca", null ],
    [ "setToolInfoMessage", "classDigikamEditorInsertTextToolPlugin_1_1InsertTextTool.html#aafb8d4048e28c142cdec61af4e9f791f", null ],
    [ "setToolName", "classDigikamEditorInsertTextToolPlugin_1_1InsertTextTool.html#ab4f48edf807c06660bba77d411672336", null ],
    [ "setToolSettings", "classDigikamEditorInsertTextToolPlugin_1_1InsertTextTool.html#a81a16e17809e0c1517c09cff1134041e", null ],
    [ "setToolVersion", "classDigikamEditorInsertTextToolPlugin_1_1InsertTextTool.html#abd5232a8268bb211859bba4acd386277", null ],
    [ "setToolView", "classDigikamEditorInsertTextToolPlugin_1_1InsertTextTool.html#a22889920743aafd0773c877a25f69f2e", null ],
    [ "signalUpdatePreview", "classDigikamEditorInsertTextToolPlugin_1_1InsertTextTool.html#abe760f23ce84ef4fd83cc0f1d9b3bc47", null ],
    [ "slotApplyTool", "classDigikamEditorInsertTextToolPlugin_1_1InsertTextTool.html#acb6a259870c46dcaf95ff3ea0d583e83", null ],
    [ "slotCancel", "classDigikamEditorInsertTextToolPlugin_1_1InsertTextTool.html#acf4b645a0f5a53214d05e93ce4443f57", null ],
    [ "slotChannelChanged", "classDigikamEditorInsertTextToolPlugin_1_1InsertTextTool.html#a475dbb46a84bc63ebf9cb6eeb9863b2a", null ],
    [ "slotCloseTool", "classDigikamEditorInsertTextToolPlugin_1_1InsertTextTool.html#a2baf799e8d8ae25fd947696fe99263bb", null ],
    [ "slotInit", "classDigikamEditorInsertTextToolPlugin_1_1InsertTextTool.html#a4584e75ce41f939f9fbb75bb79d10bb8", null ],
    [ "slotLoadSettings", "classDigikamEditorInsertTextToolPlugin_1_1InsertTextTool.html#af4ebc905a87855fcd79705bdfe835afa", null ],
    [ "slotOk", "classDigikamEditorInsertTextToolPlugin_1_1InsertTextTool.html#ad8e21ba2ef3a96b5cab346a9dcbc33f7", null ],
    [ "slotPreview", "classDigikamEditorInsertTextToolPlugin_1_1InsertTextTool.html#a30658bf5f3d824a27655484b10f2be54", null ],
    [ "slotPreviewModeChanged", "classDigikamEditorInsertTextToolPlugin_1_1InsertTextTool.html#a5d112b1773806c67813deb6ef764787e", null ],
    [ "slotSaveAsSettings", "classDigikamEditorInsertTextToolPlugin_1_1InsertTextTool.html#afc11c0ed3ea697f00b4fc166fb4ea8ce", null ],
    [ "slotScaleChanged", "classDigikamEditorInsertTextToolPlugin_1_1InsertTextTool.html#ac2385c79f4cf636bc1c615dc23ec657a", null ],
    [ "slotTimer", "classDigikamEditorInsertTextToolPlugin_1_1InsertTextTool.html#a5cef4546adc836a3fd9bfea6aad8e1d2", null ],
    [ "slotUpdateSpotInfo", "classDigikamEditorInsertTextToolPlugin_1_1InsertTextTool.html#abf7a46f52815a5ead35007caa77a3ff7", null ],
    [ "toolCategory", "classDigikamEditorInsertTextToolPlugin_1_1InsertTextTool.html#a96deea8a2ee1155e014bfef257b84d3f", null ],
    [ "toolHelp", "classDigikamEditorInsertTextToolPlugin_1_1InsertTextTool.html#aa3a4ca4085bc733beb0a577929a535c2", null ],
    [ "toolIcon", "classDigikamEditorInsertTextToolPlugin_1_1InsertTextTool.html#ac8e1df7974d5dbfd448a1d4d87b4273f", null ],
    [ "toolName", "classDigikamEditorInsertTextToolPlugin_1_1InsertTextTool.html#a18c70c25e0103b592fd74c5dcdf30a21", null ],
    [ "toolSettings", "classDigikamEditorInsertTextToolPlugin_1_1InsertTextTool.html#a20fa60db7f77d9955314bf52900b42a2", null ],
    [ "toolVersion", "classDigikamEditorInsertTextToolPlugin_1_1InsertTextTool.html#a3deae60f1ffeea29811b8fe75783f0a4", null ],
    [ "toolView", "classDigikamEditorInsertTextToolPlugin_1_1InsertTextTool.html#aeb9bb1a5bcab1ee44299230a1c935a58", null ]
];