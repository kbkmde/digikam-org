var ipfstalker_8h =
[
    [ "IpfsTalker", "classDigikamGenericIpfsPlugin_1_1IpfsTalker.html", "classDigikamGenericIpfsPlugin_1_1IpfsTalker" ],
    [ "IpfsTalkerAction", "structDigikamGenericIpfsPlugin_1_1IpfsTalkerAction.html", "structDigikamGenericIpfsPlugin_1_1IpfsTalkerAction" ],
    [ "IpfsTalkerResult", "structDigikamGenericIpfsPlugin_1_1IpfsTalkerResult.html", "structDigikamGenericIpfsPlugin_1_1IpfsTalkerResult" ],
    [ "IPFSImage", "structDigikamGenericIpfsPlugin_1_1IpfsTalkerResult_1_1IPFSImage.html", "structDigikamGenericIpfsPlugin_1_1IpfsTalkerResult_1_1IPFSImage" ],
    [ "IpfsTalkerActionType", "ipfstalker_8h.html#a47b24e81da9f533fd531416fc5c00c14", [
      [ "IMG_UPLOAD", "ipfstalker_8h.html#a47b24e81da9f533fd531416fc5c00c14a24f90f807f8e88369a150919f60e2097", null ]
    ] ]
];