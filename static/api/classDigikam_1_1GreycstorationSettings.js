var classDigikam_1_1GreycstorationSettings =
[
    [ "GreycstorationSettings", "classDigikam_1_1GreycstorationSettings.html#a8ffc983150f77778fc68dc2b25ca1033", null ],
    [ "~GreycstorationSettings", "classDigikam_1_1GreycstorationSettings.html#a413dc9bab92e1c17e3616e49c1a5d7c7", null ],
    [ "loadSettings", "classDigikam_1_1GreycstorationSettings.html#abad481bf39cab4abac932fc924aca76f", null ],
    [ "saveSettings", "classDigikam_1_1GreycstorationSettings.html#adbd66e8be6c87c11397e907979467e1d", null ],
    [ "setDefaultSettings", "classDigikam_1_1GreycstorationSettings.html#a5f1ff7a92c3c87805edbc3bcc722abca", null ],
    [ "setEnabled", "classDigikam_1_1GreycstorationSettings.html#a7d8cac78db444fa744fd76def66e20dc", null ],
    [ "setSettings", "classDigikam_1_1GreycstorationSettings.html#a8215b08c126c2a13b0d9f1723bb6afd8", null ],
    [ "settings", "classDigikam_1_1GreycstorationSettings.html#aaf9650b85cc885cf05032b3f485aa5a2", null ]
];