var dir_4d81a6322b5ca41ae1c7f409c2372820 =
[
    [ "exiftoolparser.cpp", "exiftoolparser_8cpp.html", null ],
    [ "exiftoolparser.h", "exiftoolparser_8h.html", [
      [ "ExifToolParser", "classDigikam_1_1ExifToolParser.html", "classDigikam_1_1ExifToolParser" ]
    ] ],
    [ "exiftoolparser_command.cpp", "exiftoolparser__command_8cpp.html", null ],
    [ "exiftoolparser_output.cpp", "exiftoolparser__output_8cpp.html", null ],
    [ "exiftoolparser_p.cpp", "exiftoolparser__p_8cpp.html", null ],
    [ "exiftoolparser_p.h", "exiftoolparser__p_8h.html", [
      [ "Private", "classDigikam_1_1ExifToolParser_1_1Private.html", "classDigikam_1_1ExifToolParser_1_1Private" ]
    ] ],
    [ "exiftoolprocess.cpp", "exiftoolprocess_8cpp.html", null ],
    [ "exiftoolprocess.h", "exiftoolprocess_8h.html", [
      [ "ExifToolProcess", "classDigikam_1_1ExifToolProcess.html", "classDigikam_1_1ExifToolProcess" ]
    ] ],
    [ "exiftoolprocess_p.cpp", "exiftoolprocess__p_8cpp.html", null ],
    [ "exiftoolprocess_p.h", "exiftoolprocess__p_8h.html", [
      [ "Private", "classDigikam_1_1ExifToolProcess_1_1Private.html", "classDigikam_1_1ExifToolProcess_1_1Private" ],
      [ "Command", "classDigikam_1_1ExifToolProcess_1_1Private_1_1Command.html", "classDigikam_1_1ExifToolProcess_1_1Private_1_1Command" ]
    ] ]
];