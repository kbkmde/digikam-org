var classDigikam_1_1TileGrouper =
[
    [ "TileGrouper", "classDigikam_1_1TileGrouper.html#a39d3faa7a7f0c717053e878b98bbdb28", null ],
    [ "~TileGrouper", "classDigikam_1_1TileGrouper.html#afc20f9a6db8a1fc61f86e0883780e11c", null ],
    [ "getClustersDirty", "classDigikam_1_1TileGrouper.html#a789b6bd37e18185cbda74d7567c84bb5", null ],
    [ "setClustersDirty", "classDigikam_1_1TileGrouper.html#a698eb50d443b10233d96a936ac232f2f", null ],
    [ "setCurrentBackend", "classDigikam_1_1TileGrouper.html#a361a1da08b3447aab6278aacbd6cdd4f", null ],
    [ "updateClusters", "classDigikam_1_1TileGrouper.html#a674de89b3ab907e079b85f2e1882258f", null ]
];