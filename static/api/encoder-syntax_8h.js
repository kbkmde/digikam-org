var encoder_syntax_8h =
[
    [ "SplitType", "encoder-syntax_8h.html#af6476e94e72f0533643ab99c2050c3a6", [
      [ "ForcedNonSplit", "encoder-syntax_8h.html#af6476e94e72f0533643ab99c2050c3a6a3b09d56da7bdc116a4438e8ee6a641d0", null ],
      [ "ForcedSplit", "encoder-syntax_8h.html#af6476e94e72f0533643ab99c2050c3a6ad760221f0709e9878a7ab8374d2e52ec", null ],
      [ "OptionalSplit", "encoder-syntax_8h.html#af6476e94e72f0533643ab99c2050c3a6afadc887a4c3954ad257fb3905c4a8a5d", null ]
    ] ],
    [ "encode_cbf_chroma", "encoder-syntax_8h.html#aaaecd13a7f38b71071c529de448b75d1", null ],
    [ "encode_cbf_luma", "encoder-syntax_8h.html#a513157476d47630e13ff40d564576e2a", null ],
    [ "encode_coding_unit", "encoder-syntax_8h.html#adb032d95d582f76b7d850f553b9910a2", null ],
    [ "encode_ctb", "encoder-syntax_8h.html#a387f86b52120fd8f6f248e079a623a4f", null ],
    [ "encode_cu_skip_flag", "encoder-syntax_8h.html#a5f3ebb3136717de9384def3311c21c4c", null ],
    [ "encode_merge_idx", "encoder-syntax_8h.html#a62950180ca54f6dbcd759a92ad610477", null ],
    [ "encode_quadtree", "encoder-syntax_8h.html#a51955628e2ab31532fa66d2441bc1248", null ],
    [ "encode_split_cu_flag", "encoder-syntax_8h.html#aec1b3bc65fd715419bab62797a7b5d7f", null ],
    [ "encode_split_transform_flag", "encoder-syntax_8h.html#a150206462de7d0a491c44244f963b649", null ],
    [ "encode_transform_tree", "encoder-syntax_8h.html#a3d2068cd416d42cc2ffaa99b0a6e488f", null ],
    [ "encode_transform_unit", "encoder-syntax_8h.html#a031be59de650ee611b6234b2f3ba6cf2", null ],
    [ "get_split_type", "encoder-syntax_8h.html#a1a36bcc362854b41db3662e626fa60e2", null ],
    [ "recursive_cbfChroma_rate", "encoder-syntax_8h.html#a8b8d2eece66b7df293bf19ac438574df", null ]
];