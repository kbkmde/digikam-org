var classDigikamGenericPiwigoPlugin_1_1PiwigoTalker =
[
    [ "State", "classDigikamGenericPiwigoPlugin_1_1PiwigoTalker.html#a2da57b37f0232f208992666478d4d0f7", [
      [ "GE_LOGOUT", "classDigikamGenericPiwigoPlugin_1_1PiwigoTalker.html#a2da57b37f0232f208992666478d4d0f7a278179fadc8c65898dfddc067af703a8", null ],
      [ "GE_LOGIN", "classDigikamGenericPiwigoPlugin_1_1PiwigoTalker.html#a2da57b37f0232f208992666478d4d0f7af86b1cdfa99b7eaca1c78e2eeb50ddd7", null ],
      [ "GE_GETVERSION", "classDigikamGenericPiwigoPlugin_1_1PiwigoTalker.html#a2da57b37f0232f208992666478d4d0f7a0872b85bebfa88f619cbb4cecfcb2353", null ],
      [ "GE_LISTALBUMS", "classDigikamGenericPiwigoPlugin_1_1PiwigoTalker.html#a2da57b37f0232f208992666478d4d0f7a4a9b3384d0cf8dee65fde541883cac3d", null ],
      [ "GE_CHECKPHOTOEXIST", "classDigikamGenericPiwigoPlugin_1_1PiwigoTalker.html#a2da57b37f0232f208992666478d4d0f7a91fef1a5d82787e9e98c526c24056f62", null ],
      [ "GE_GETINFO", "classDigikamGenericPiwigoPlugin_1_1PiwigoTalker.html#a2da57b37f0232f208992666478d4d0f7a959f7afe80b94ef04becccb37d5dc88a", null ],
      [ "GE_SETINFO", "classDigikamGenericPiwigoPlugin_1_1PiwigoTalker.html#a2da57b37f0232f208992666478d4d0f7a7ca0cad66e1a3de8f10d49a5de76dba3", null ],
      [ "GE_ADDPHOTOCHUNK", "classDigikamGenericPiwigoPlugin_1_1PiwigoTalker.html#a2da57b37f0232f208992666478d4d0f7ab251d99a0d52bd9fc830d6b8d242f5f3", null ],
      [ "GE_ADDPHOTOSUMMARY", "classDigikamGenericPiwigoPlugin_1_1PiwigoTalker.html#a2da57b37f0232f208992666478d4d0f7a59696444d90b4e366e631bfc39233b01", null ]
    ] ],
    [ "PiwigoTalker", "classDigikamGenericPiwigoPlugin_1_1PiwigoTalker.html#a38adae13edf2704cef71cb83dd627446", null ],
    [ "~PiwigoTalker", "classDigikamGenericPiwigoPlugin_1_1PiwigoTalker.html#a5fba7d34b9a1e47d6407f734ef544b8d", null ],
    [ "addPhoto", "classDigikamGenericPiwigoPlugin_1_1PiwigoTalker.html#a2a3f037747f82674f3f716471fd40105", null ],
    [ "cancel", "classDigikamGenericPiwigoPlugin_1_1PiwigoTalker.html#ac7da5402ba35a9f0af2c0fa9f48e7830", null ],
    [ "listAlbums", "classDigikamGenericPiwigoPlugin_1_1PiwigoTalker.html#ab30bfd5aa80aab80a305a21f28f343c4", null ],
    [ "listPhotos", "classDigikamGenericPiwigoPlugin_1_1PiwigoTalker.html#ae8c25c243ab8e2f8d7607d73c6061c58", null ],
    [ "loggedIn", "classDigikamGenericPiwigoPlugin_1_1PiwigoTalker.html#ad09f65cf97fb6a9a3908876ddc5c2a61", null ],
    [ "login", "classDigikamGenericPiwigoPlugin_1_1PiwigoTalker.html#a285adfbbdec551774a5adca2d68a0cbe", null ],
    [ "signalAddPhotoFailed", "classDigikamGenericPiwigoPlugin_1_1PiwigoTalker.html#ad9d677ce98963bde8c3c230fdfb96ff5", null ],
    [ "signalAddPhotoSucceeded", "classDigikamGenericPiwigoPlugin_1_1PiwigoTalker.html#abdfef3f98a96461b784e5a37b5521a63", null ],
    [ "signalAlbums", "classDigikamGenericPiwigoPlugin_1_1PiwigoTalker.html#a9650e77f82845e46432cece30b2bf695", null ],
    [ "signalBusy", "classDigikamGenericPiwigoPlugin_1_1PiwigoTalker.html#a7e58980dd3ec8fb9a53950e1be319eb0", null ],
    [ "signalError", "classDigikamGenericPiwigoPlugin_1_1PiwigoTalker.html#ab1ffdbe26677f92c8a1f3944a074d6de", null ],
    [ "signalLoginFailed", "classDigikamGenericPiwigoPlugin_1_1PiwigoTalker.html#ab01895bc2c2d42ec565088d942cbde11", null ],
    [ "signalProgressInfo", "classDigikamGenericPiwigoPlugin_1_1PiwigoTalker.html#a4aff4c882290a87be6c898a5d19ff1a1", null ]
];