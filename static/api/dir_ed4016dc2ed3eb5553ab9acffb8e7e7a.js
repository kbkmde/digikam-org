var dir_ed4016dc2ed3eb5553ab9acffb8e7e7a =
[
    [ "ipfsimageslist.cpp", "ipfsimageslist_8cpp.html", null ],
    [ "ipfsimageslist.h", "ipfsimageslist_8h.html", [
      [ "IpfsImagesList", "classDigikamGenericIpfsPlugin_1_1IpfsImagesList.html", "classDigikamGenericIpfsPlugin_1_1IpfsImagesList" ],
      [ "IpfsImagesListViewItem", "classDigikamGenericIpfsPlugin_1_1IpfsImagesListViewItem.html", "classDigikamGenericIpfsPlugin_1_1IpfsImagesListViewItem" ]
    ] ],
    [ "ipfsplugin.cpp", "ipfsplugin_8cpp.html", null ],
    [ "ipfsplugin.h", "ipfsplugin_8h.html", "ipfsplugin_8h" ],
    [ "ipfstalker.cpp", "ipfstalker_8cpp.html", null ],
    [ "ipfstalker.h", "ipfstalker_8h.html", "ipfstalker_8h" ],
    [ "ipfswindow.cpp", "ipfswindow_8cpp.html", null ],
    [ "ipfswindow.h", "ipfswindow_8h.html", [
      [ "IpfsWindow", "classDigikamGenericIpfsPlugin_1_1IpfsWindow.html", "classDigikamGenericIpfsPlugin_1_1IpfsWindow" ]
    ] ]
];