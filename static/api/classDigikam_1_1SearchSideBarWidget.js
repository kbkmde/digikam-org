var classDigikam_1_1SearchSideBarWidget =
[
    [ "StateSavingDepth", "classDigikam_1_1SearchSideBarWidget.html#abdf8ad03070bc560242e56575909d6b6", [
      [ "INSTANCE", "classDigikam_1_1SearchSideBarWidget.html#abdf8ad03070bc560242e56575909d6b6a535f22ead553f373020dce094f0ae735", null ],
      [ "DIRECT_CHILDREN", "classDigikam_1_1SearchSideBarWidget.html#abdf8ad03070bc560242e56575909d6b6a402f3f077dcfc1835196020f1986fb55", null ],
      [ "RECURSIVE", "classDigikam_1_1SearchSideBarWidget.html#abdf8ad03070bc560242e56575909d6b6a1d31a55830067a26a4c25eabb48f6d4f", null ]
    ] ],
    [ "SearchSideBarWidget", "classDigikam_1_1SearchSideBarWidget.html#a2d256e27d975476efb9483e16e3826bc", null ],
    [ "~SearchSideBarWidget", "classDigikam_1_1SearchSideBarWidget.html#a1609f94bbe1bb0d95c1a2fd5724c013d", null ],
    [ "applySettings", "classDigikam_1_1SearchSideBarWidget.html#a10db6930d1aad984eb0518f143e72258", null ],
    [ "changeAlbumFromHistory", "classDigikam_1_1SearchSideBarWidget.html#a118301719edb880cbfd252c3c8be36cb", null ],
    [ "doLoadState", "classDigikam_1_1SearchSideBarWidget.html#a068d8d68440844da19f5b275832ea379", null ],
    [ "doSaveState", "classDigikam_1_1SearchSideBarWidget.html#a3ce248d9323d59f0ce1b61a571ba8213", null ],
    [ "entryName", "classDigikam_1_1SearchSideBarWidget.html#a226ce9b9faa055120ba386b85509bcdf", null ],
    [ "getCaption", "classDigikam_1_1SearchSideBarWidget.html#ac32d6dbaaf739d131d2ae8be1ea8eb3a", null ],
    [ "getConfigGroup", "classDigikam_1_1SearchSideBarWidget.html#a3dce4d65e29a07ea09d2e7b4746f8350", null ],
    [ "getIcon", "classDigikam_1_1SearchSideBarWidget.html#afee0e5a2d77cae142c5a14d7f170457a", null ],
    [ "getStateSavingDepth", "classDigikam_1_1SearchSideBarWidget.html#a340090725416a1b9030c67d7d6a77d6e", null ],
    [ "loadState", "classDigikam_1_1SearchSideBarWidget.html#a884a7c5ec2c82c6b61519a6c035577c2", null ],
    [ "newAdvancedSearch", "classDigikam_1_1SearchSideBarWidget.html#a7730e42d7e6f7734a84a19d02a2fed20", null ],
    [ "newKeywordSearch", "classDigikam_1_1SearchSideBarWidget.html#a1d816664d748d7ce81e1e28b2318cbf5", null ],
    [ "requestActiveTab", "classDigikam_1_1SearchSideBarWidget.html#a247452550423658b7ca3f5ba97fb1f07", null ],
    [ "saveState", "classDigikam_1_1SearchSideBarWidget.html#a0db6b9c2daff1b521bce63f78d8f1652", null ],
    [ "setActive", "classDigikam_1_1SearchSideBarWidget.html#a83e338b4044b03f62ed7b9da935ab06e", null ],
    [ "setConfigGroup", "classDigikam_1_1SearchSideBarWidget.html#aa37c59ac6e91ba60c3c0dd14cc6fa71e", null ],
    [ "setEntryPrefix", "classDigikam_1_1SearchSideBarWidget.html#ac289161ea3144b78c348c283cbb7777d", null ],
    [ "setStateSavingDepth", "classDigikam_1_1SearchSideBarWidget.html#a1023eaabd6d22bcfcd83b0c7f5c47e9b", null ],
    [ "signalNotificationError", "classDigikam_1_1SearchSideBarWidget.html#a12e1b990cb7a7f652ce6f10df1ccfc11", null ]
];